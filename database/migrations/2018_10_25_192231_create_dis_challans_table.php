<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisChallansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dis_challans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qty');
            $table->string('grand_total');
            $table->integer('poitemid')->unsigned();
            $table->foreign('poitemid')->references('id')->on('po_items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dis_challans');
    }
}
