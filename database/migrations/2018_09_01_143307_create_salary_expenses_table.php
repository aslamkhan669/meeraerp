<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('doi')->nullable();
            $table->string('tmp_code')->nullable();
            $table->string('name')->nullable();
            $table->string('role')->nullable();
            $table->string('salary')->nullable();
            $table->string('mop')->nullable();
            $table->string('dop')->nullable();
            $table->string('paid_amt')->nullable();
            $table->string('due_amt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_expenses');
    }
}
