<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispatchInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispatch_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('d_order_id')->unsigned();
            $table->foreign('d_order_id')->references('id')->on('dispatched_orders');
            $table->string('material_recieved')->nullable();
            $table->integer('qty')->nullable();
            $table->integer('reject_qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispatch_infos');
    }
}
