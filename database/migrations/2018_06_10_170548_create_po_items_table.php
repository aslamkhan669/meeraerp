<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qty');
            $table->string('grand_total');
            $table->integer('qitemid')->unsigned();
            $table->foreign('qitemid')->references('id')->on('query_items');
            $table->integer('po_id')->unsigned();
            $table->foreign('po_id')->references('id')->on('po_formattings');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_items');
    }
}
