<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryDispatchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_dispatchs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('disregisterid')->unsigned();
            $table->foreign('disregisterid')->references('id')->on('dispatch_registers');
            $table->string('reason')->nullable();
            $table->string('extra_qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_dispatchs');
    }
}
