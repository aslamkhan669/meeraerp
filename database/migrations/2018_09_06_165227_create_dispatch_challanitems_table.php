<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispatchChallanitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispatch_challanitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dt_challan_id')->unsigned();
            $table->foreign('dt_challan_id')->references('id')->on('dispatch_challans');
            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('item_lists');
            $table->integer('qty');
            $table->string('gst');
            $table->string('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispatch_challanitems');
    }
}
