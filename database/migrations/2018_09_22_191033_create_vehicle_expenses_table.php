<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('person')->nullable();
            $table->string('after_reading')->nullable();
            $table->string('before_reading')->nullable();
            $table->string('difference')->nullable();
            $table->string('unit_rate')->nullable();
            $table->string('amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_expenses');
    }
}
