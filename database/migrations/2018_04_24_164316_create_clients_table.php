<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_name');
            $table->string('client_code');
            $table->string('client_type');
            $table->string('rating');
            $table->string('purchase_category');
            $table->string('sales_center');
            $table->string('email');
            $table->string('contact_person1')->nullable();
            $table->string('contact_number1')->nullable();
            $table->string('contact_person2')->nullable();
            $table->string('contact_number2')->nullable();
            $table->string('contact_person3')->nullable();
            $table->string('contact_number3')->nullable();
            $table->string('contact_person4')->nullable();
            $table->string('contact_number4')->nullable();
            $table->string('street')->nullable();
            $table->string('landmark')->nullable();
            $table->string('sector')->nullable();
            $table->string('city')->nullable();
            $table->string('pincode')->nullable();
            $table->string('state')->nullable();
            $table->string('zone')->nullable();
            $table->string('pancard')->nullable();
            $table->string('adharcard')->nullable();
            $table->string('gst_no')->nullable();
            $table->string('payment_term')->nullable();
            $table->string('payment_mode')->nullable();
            $table->string('excisable')->nullable();
            $table->string('demand_frequency')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
