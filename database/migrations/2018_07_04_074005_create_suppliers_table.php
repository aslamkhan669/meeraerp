<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('supplier_name');
            $table->string('supplier_code');
            $table->string('pricing_center');
            $table->string('supplier_type');
            $table->string('mobile');
            $table->string('landline');
            $table->string('email');
            $table->string('rating');
            $table->string('contact_person1')->nullable();
            $table->string('contact_number1')->nullable();
            $table->string('contact_person2')->nullable();
            $table->string('contact_number2')->nullable();
            $table->string('contact_person3')->nullable();
            $table->string('contact_number3')->nullable();
            $table->string('contact_person4')->nullable();
            $table->string('contact_number4')->nullable();
            $table->string('supplier_catalog')->nullable();
            $table->string('street')->nullable();
            $table->string('landmark')->nullable();
            $table->string('sector')->nullable();
            $table->string('city')->nullable();
            $table->string('pincode')->nullable();
            $table->string('state')->nullable();
            $table->string('website')->nullable();
            $table->string('zone')->nullable();
            $table->string('pancard')->nullable();
            $table->string('adharcard')->nullable();
            $table->string('gst_no')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->string('account_no')->nullable();
            $table->string('cancelled_cheque')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
