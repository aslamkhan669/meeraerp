<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOncallPricemapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oncall_pricemaps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('listprice')->nullable();
            $table->string('insurance_shipping')->nullable();
            $table->string('custom_clearance')->nullable();
            $table->string('discount')->nullable();
            $table->string('discounted_price')->nullable();
            $table->string('gst')->nullable();
            $table->string('total')->nullable();
            $table->string('quantity')->nullable();
            $table->string('grand_total')->nullable();            
            $table->string('delivery_term')->nullable();
            $table->string('warranty')->nullable();
            $table->string('standard_package')->nullable();
            $table->integer('is_selected')->nullable();
            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('query_items');
            $table->integer('supplier_id')->unsigned();
            $table->foreign('supplier_id')->references('id')->on('suppliers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oncall_pricemaps');
    }
}
