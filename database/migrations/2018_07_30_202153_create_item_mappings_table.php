<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('listprice');
            $table->string('insurance_shipping')->nullable();
            $table->string('custom_clearance')->nullable();
            $table->string('discount')->nullable();
            $table->string('discounted_price')->nullable();
            $table->string('gst');
            $table->string('total');
            $table->string('quantity');
            $table->string('delivery_term');
            $table->string('warranty');
            $table->string('standard_package')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_mappings');
    }
}
