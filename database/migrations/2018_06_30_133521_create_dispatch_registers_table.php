<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispatchRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispatch_registers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poitemid')->unsigned();
            $table->foreign('poitemid')->references('id')->on('po_items');
            $table->integer('dt_user_id')->unsigned();
            $table->foreign('dt_user_id')->references('id')->on('users');
            $table->integer('qty');
            $table->date('purchase_date');
            $table->string('day_shift');
            $table->string('priority');
            $table->string('transportation_mode');
            $table->string('payment_amount');            
            $table->string('bank');
            $table->string('payment_mode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispatch_registers');
    }
}
