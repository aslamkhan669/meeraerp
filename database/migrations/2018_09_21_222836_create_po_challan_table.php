<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoChallanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_challans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('qty');
            $table->string('grand_total');
            $table->integer('poitemid')->unsigned();
            $table->foreign('poitemid')->references('id')->on('po_items');
            $table->integer('qitemid')->unsigned();
            $table->foreign('qitemid')->references('id')->on('query_items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_challans');
    }
}
