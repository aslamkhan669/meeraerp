<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('company_code');
            $table->string('contact_person');
            $table->string('phone_no');
            $table->string('email');
            $table->string('pan_no');
            $table->string('mobile');
            $table->text('delivery_address');
            $table->text('address');
            $table->string('aadhar_no');
            $table->string('gst_no');
            $table->string('state_code');
            $table->string('reg_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_details');
    }
}
