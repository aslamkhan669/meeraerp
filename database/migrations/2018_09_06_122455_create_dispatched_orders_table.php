<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispatchedOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispatched_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('disregisterid')->unsigned();
            $table->foreign('disregisterid')->references('id')->on('dispatch_registers');
            $table->integer('ewaybill');
            $table->integer('ml_purchased');
            $table->integer('ml_recieved');
            $table->string('payment_mode');
            $table->integer('bill_challan_no');
            $table->string('bill_challan_amt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispatched_orders');
    }
}
