<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOncallQuotationitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oncall_quotationitems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('oncallquotation_id')->unsigned();
            $table->foreign('oncallquotation_id')->references('id')->on('oncall_quotations');
            $table->integer('manualitem_id')->unsigned();
            $table->foreign('manualitem_id')->references('id')->on('manual_items');
            $table->string('profit');
            $table->string('profit_amount');
            $table->string('gstamount');
            $table->string('mtotal');
            $table->string('pup');
            $table->string('mqty');
            $table->string('mgrand_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oncall_quotationitems');
    }
}
