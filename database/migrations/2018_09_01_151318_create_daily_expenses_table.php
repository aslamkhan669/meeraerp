<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tea_id')->unsigned();
            $table->foreign('tea_id')->references('id')->on('teas');
            $table->integer('water_id')->unsigned();
            $table->foreign('water_id')->references('id')->on('waters');
            $table->integer('stationery_id')->unsigned();
            $table->foreign('stationery_id')->references('id')->on('stationeries');
            $table->integer('it_phone_id')->unsigned();
            $table->foreign('it_phone_id')->references('id')->on('it_phones');
            $table->integer('salary_expenses_id')->unsigned();
            $table->foreign('salary_expenses_id')->references('id')->on('salary_expenses');
            $table->integer('other_expenses_id')->unsigned();
            $table->foreign('other_expenses_id')->references('id')->on('other_expenses');
            $table->integer('client_visit_id')->unsigned();
            $table->foreign('client_visit_id')->references('id')->on('client_visits');
            $table->integer('supplier_visit_id')->unsigned();
            $table->foreign('supplier_visit_id')->references('id')->on('supplier_visits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_expenses');
    }
}
