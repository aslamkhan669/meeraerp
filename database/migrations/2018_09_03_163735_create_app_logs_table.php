<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppLogsTable extends Migration
{


    public function up()
    {
        Schema::create('app_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('query_id')->unsigned();  
            $table->foreign('query_id')->references('id')->on('queries');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('app_logs');
    }
}
