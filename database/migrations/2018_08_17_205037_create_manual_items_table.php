<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manual_id')->unsigned();
            $table->foreign('manual_id')->references('id')->on('manual_purchases');
            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('item_lists');
            $table->string('description')->nullable();
            $table->string('specification')->nullable();
            $table->integer('color')->unsigned();
            $table->foreign('color')->references('id')->on('colors');
            $table->string('model_no');
            $table->integer('brand_id')->unsigned();
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->integer('unit')->unsigned();
            $table->foreign('unit')->references('id')->on('uoms');
            $table->string('remarks');
            $table->string('image');
            $table->string('hsn_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual_items');
    }
}
