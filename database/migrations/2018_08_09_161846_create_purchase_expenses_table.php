<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poformatid')->unsigned();
            $table->foreign('poformatid')->references('id')->on('po_formattings');
            $table->string('amt_recieved');
            $table->string('amt_spent');
            $table->string('amt_returned');
            $table->string('mode_payment');  
            $table->string('remarks');
            $table->string('total')->nullable();         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_expenses');
    }
}
