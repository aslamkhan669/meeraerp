<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallanItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challan_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('challan_id')->unsigned();
            $table->foreign('challan_id')->references('id')->on('challans');
            $table->integer('item_id');
            $table->string('description');
            $table->string('hsn_code');
            $table->integer('qty');
            $table->string('total');
            $table->string('gst');
            $table->string('grand_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challan_items');
    }
}
