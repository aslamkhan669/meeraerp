<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');
            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('categories');
            $table->integer('unit')->unsigned();
            $table->foreign('unit')->references('id')->on('uoms');
            $table->integer('color')->unsigned();
            $table->foreign('color')->references('id')->on('colors');
            $table->string('item_code');
            $table->string('hsn_code');
            $table->string('model_no');
            $table->string('package_qty');
            $table->string('purchase_source');
            $table->string('description');
            $table->string('remarks');
            $table->string('specification');
            $table->string('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_lists');
    }
}
