<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_visits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_code')->nullable();
            $table->string('supplier')->nullable();
            $table->integer('amt_paid')->nullable();
            $table->integer('amt_spend')->nullable();
            $table->integer('amt_return')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_visits');
    }
}
