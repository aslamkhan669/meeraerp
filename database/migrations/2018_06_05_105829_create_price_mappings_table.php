<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('listprice');
            $table->string('insurance_shipping');
            $table->string('custom_clearance');
            $table->string('discount');
            $table->string('discounted_price');
            $table->string('gst');
            $table->string('total');
            $table->string('quantity');
            $table->string('grand_total');
            $table->integer('qitem_id')->unsigned();
            $table->foreign('qitem_id')->references('id')->on('query_items');
            $table->integer('supplier_id')->unsigned();
            $table->foreign('supplier_id')->references('id')->on('suppliers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_mappings');
    }
}
