<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasedOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchased_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poregisterid')->unsigned();
            $table->foreign('poregisterid')->references('id')->on('po_registers');
            $table->integer('ewaybill');
            $table->integer('ml_purchased');
            $table->integer('ml_recieved');
            $table->string('payment_mode');
            $table->integer('bill_challan_no');
            $table->string('bill_challan_amt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchased_orders');
    }
}
