<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispatchExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispatch_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dispatch_person')->nullable();
            $table->string('amt_recieved')->nullable();
            $table->string('amt_spent')->nullable();
            $table->string('amt_returned')->nullable();
            $table->string('mode_payment')->nullable();
            $table->string('for_what')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispatch_expenses');
    }
}
