<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOncallQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oncall_quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('manual_id')->unsigned();
            $table->foreign('manual_id')->references('id')->on('manual_purchases');
            $table->integer('is_confirmed');
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oncall_quotations');
    }
}
