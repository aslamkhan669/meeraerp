<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoTermconditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_termconditions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poform_id')->unsigned();
            $table->foreign('poform_id')->references('id')->on('po_formattings');
            $table->text('payment_term')->nullable();
            $table->text('freight_charges')->nullable();
            $table->text('material_insurance')->nullable();
            $table->text('po_validity')->nullable();
            $table->text('comment1')->nullable();
            $table->text('comment2')->nullable();
            $table->text('comment3')->nullable();
            $table->text('comment4')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_termconditions');
    }
}
