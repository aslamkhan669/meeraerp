<?php

Route::group([], function () {
    Route::get('chartjs', 'HomeController@chartjs');
Route::group(['namespace' => 'ProductManagement'],function(){
        Route::resource('category', 'CategoryController');
		Route::get('subcategory', 'CategoryController@subindex')->name('subcategory.index');
		Route::get('subcategory/create', 'CategoryController@subcreate')->name('subcategory.create');
		Route::post('subcategory', 'CategoryController@substore')->name('subcategory.store');
		Route::get('subcategory/{id}/edit', 'CategoryController@subedit')->name('subcategory.edit');
        Route::resource('product', 'ProductController');
        Route::resource('brand', 'BrandController');
        Route::resource('itemlist', 'ItemListController');
        Route::resource('uom', 'UomController');
        Route::resource('color', 'ColorController');
        Route::Post('/query/data','ItemListController@storebyitem');
        Route::Post('/fileuploaditem','ItemListController@fileuploaditem');
        ROUTE::get('products/ajax/{key}','ProductController@combodata');
        ROUTE::get('categories/ajax/{key}','CategoryController@combodata');
        Route::get('productcode/ajax/{pid}','ItemListController@productcodes');
        Route::resource('itemmapping', 'ItemMappingController');
        Route::get('/itemmapping/show/{id}','ItemMappingController@show');
        Route::get('/itemmapping/{id}/edit','ItemMappingController@edit');
        Route::get('parentscategories/ajax/{cid}','ItemListController@parentcategories');
        Route::get('parentsbrands/ajax/{cid}','ItemListController@parentbrands');
        Route::get('subscategories/ajax/{cid}','ItemListController@subcategories');
        Route::Post('searchproduct','ProductController@search');
        Route::Post('searchsub','CategoryController@searchsub');
        Route::Post('searchitem','ItemListController@searchitem');
        Route::Post('searchcategory','CategoryController@searchcategory');
        Route::Post('searchitems','ItemMappingController@search');
        Route::get('/infoitems/{id}','ItemMappingController@infoitems');
        ROUTE::get('brands/ajax/{key}','BrandController@combodata');
		ROUTE::post('/itemmapping/{id}/edit', 'ItemMappingController@update');
        ROUTE::get('parents/ajax/{key}','CategoryController@comboinfo');
        Route::Post('/itempriceinfo', 'ItemMappingController@itempriceinfo');

});

Route::group(['namespace' => 'Master'],function(){
    Route::resource('client', 'ClientController'); 
    Route::resource('supplier', 'SupplierController');   
    Route::resource('paymentterm', 'TermController');    
    Route::resource('clienttype', 'ClientTypeController');    
    Route::resource('paymentmode', 'PaymentModeController');    
    Route::get('/categorysupplier/delete/{id}', 'SupplierController@deleteItem');
    Route::get('supplier/excelcreate', 'SupplierController@excelcreate');
    Route::get('parentcategories/{cid}','SupplierController@parentcategories');
    Route::Post('searchsupplier','SupplierController@searchsupplier');
    Route::Post('searchclient','ClientController@searchclient');

});


Route::group(['namespace' => 'QueryManagement'],function(){
    Route::resource('query', 'QueryController');
    Route::get('/queries/ajax/{cid}','QueryController@queries');
    Route::resource('pricing', 'PricingController');
    Route::get('/pricing/show/{id}','PricingController@show');
    Route::get('/suppliers/{id}', 'PricingController@suppliers');
    Route::get('/changecolor/{id}', 'PricingController@changecolor');
    Route::Post('/pricemapping','PricingController@pricemapping');
    Route::Post('/pricemappings/update','PricingController@mappingupdate');
    Route::get('/mappings/{id}', 'PricingController@mappings');
    Route::get('/query/delete/{id}', 'QueryController@deleteItem');
    Route::get('/queryiddata','PricingController@queryid');
    Route::get('/queryitems/{id}','PricingController@queryitems');
    Route::Post('/fileupload','QueryController@fileupload');
    Route::Post('/iteminfo', 'PricingController@iteminfo');
    Route::get('/getbyclient/{id}','QueryController@getByClient');
    Route::get('/getbycenter/{id}','QueryController@getByCompany');
    Route::get('/getbyitemdesc/{id}','QueryController@getbyitemdesc');
    Route::get('parentcategories/ajax/{cid}','QueryController@parentcategories');
    Route::get('parentbrands/ajax/{cid}','QueryController@parentbrands');
    Route::get('subcategories/ajax/{cid}','QueryController@subcategories');
    ROUTE::get('/api/itemlist/getbycategory/{id}','QueryController@getByCategory');
    Route::get('/historyinfo/{id}', 'PricingController@historyinfo');
    Route::get('/infoitems/{id}','PricingController@infoitems');
    Route::Post('searchpricing','PricingController@search');
    Route::post('termsdata','PricingController@termsdata');

});


Route::group(['namespace' => 'QuotationManagement'],function(){
    Route::resource('quotation', 'QuotationController');
    Route::resource('quotationpdf', 'QuotationpdfController');
    Route::get('/quotationpdf/pdf/{id}','QuotationpdfController@streamPDF');
    Route::get('/quotation/show/{id}','QuotationController@show');
    Route::get('/quotation/showprofit/{id}','QuotationController@showprofit');
    Route::get('/quotation/pdf/{id}','QuotationController@streamPDF');
    Route::get('/quotation/reject/{id}','QuotationController@reject');
    Route::get('/quotation/hold/{id}','QuotationController@hold');
    Route::get('/quotation/cancel/{id}','QuotationController@cancel');
    Route::get('/quotationsdata/{id}','QuotationController@queryitems');
    Route::get('/quotationsdatapdf/{id}','QuotationpdfController@queryitems');
    Route::Post('searchquotation','QuotationController@searchinfo');
    Route::post('quotationdata','QuotationController@quotationdata');

});

Route::group(['namespace' => 'PurchaseOrder'],function(){
    Route::resource('poformatting', 'PoFormattingController');
    Route::resource('oncallpoformatting', 'OncallPoFormattingController');
    Route::get('/poformatting/queryitems/{id}','PoFormattingController@queryitems');
    Route::get('/poformatting/manualitems/{id}','PoFormattingController@manualitems');
    Route::Post('/poformatting/store','PoFormattingController@store');
    Route::Post('/oncallpoformatting/store','OncallPoFormattingController@store');
    Route::Post('/oncallpoitems/store','OncallPoFormattingController@poitems');
    Route::Post('/poitems/store','PoFormattingController@poitems');
    Route::Post('searchpoform','PoFormattingController@search');
    Route::get('/queitemis/{id}','PoFormattingController@infoitems');
    Route::get('/quesitems/{id}','PoFormattingController@infositems');


});

Route::group(['namespace' => 'Setting'],function(){
    Route::resource('companydetail', 'CompanyDetailController');
    Route::resource('freight', 'FreightController');
    Route::resource('transportinsurance', 'TransportInsuranceController');
});

Route::group(['namespace' => 'ExcelUpload'],function(){
    Route::get('productexcel/create', 'ProductExcelController@create')->name('productexcel.create');
    Route::post('productexcel/create', 'ProductExcelController@store')->name('productexcel.create');
    Route::get('categoryexcel/create', 'CategoryExcelController@create')->name('categoryexcel.create');
    Route::post('categoryexcel/create', 'CategoryExcelController@store')->name('categoryexcel.create');
    Route::get('productimage/create', 'ProductImageController@create')->name('productimage.create');
    Route::post('productimage/create', 'ProductImageController@store')->name('productimage.create');
    Route::get('excel/create', 'ExcelController@create')->name('excel.create');
    Route::post('excel/create', 'ExcelController@store')->name('excel.create');
});

Route::group(['namespace' => 'PoPlanner'],function(){
    Route::Resource('planner', 'PoPlannerController');
    Route::post('store','PoPlannerController@store');
    Route::Resource('poregister', 'PoRegisterController');
    Route::post('poaccounts','PoRegisterController@poaccounts');
    Route::post('/purchaseexpenses','PoRegisterController@purchaseexpenses');
    Route::Post('/purchasechallan/info','PurchaseChallanController@storebychallan');
    Route::get('/purchasechallan/pdf/{id}','PurchaseChallanController@streamPDF');
    Route::get('poregister/edit/{id}/{data}','PoRegisterController@edit');
    Route::get('/infoitems/{id}','PurchaseChallanController@infoitems');
    Route::Resource('purchasechallan', 'PurchaseChallanController');
    Route::get('purchasechallan/edit/{id}','PurchaseChallanController@edit');
    Route::get('/infoitems/{id}','PoRegisterController@infoitems');
    Route::Resource('pogenerate', 'PoGenerateController');
    Route::get('/pogenerate/pdf/{id}','PoGenerateController@streamPDF');
    Route::get('/pogenerate/poitems/{id}','PoGenerateController@poitems');
    Route::Post('/poitems/data','PoGenerateController@poitem');
    Route::get('/purchasechallan/poitems/{id}','PurchaseChallanController@poitems');
    Route::Post('/purchasechallan/data','PurchaseChallanController@poitem');
    Route::get('/poregister/show/{id}/{info}','PoRegisterController@show');
    Route::get('/queitems/{id}','PoPlannerController@infoitems');
    Route::Post('searchpoplan','PoPlannerController@search');
    Route::Post('searchchallan','PurchaseChallanController@search');
    Route::Post('searchpo','PoGenerateController@search');
    ROUTE::get('pogenerate/ajax/{key}','PoRegisterController@combodata');
    Route::Post('searchporegister','PoRegisterController@search');
    Route::get('/pogenerate/show/{id}','PoGenerateController@show');
    Route::post('pogeneratedata','PoGenerateController@pogeneratedata');
    Route::get('/podata/{id}','PoGenerateController@queryitems');
    ROUTE::get('itemsearch/ajax/{key}','PoRegisterController@comboinfo');
    ROUTE::get('itemnosearch/ajax/{key}','PoRegisterController@comboiteminfo');
    ROUTE::get('purchasechallan/ajax/{key}','PurchaseChallanController@combodata');
    Route::get('/purchallan/{id}','PurchaseChallanController@queryitems');
    ROUTE::Post('/inventorydata','PoRegisterController@storeinv');
    ROUTE::get('/poregister/create','PoRegisterController@indexinfo');

  
});

Route::group(['namespace' => 'PurchaseTeam'],function(){
    Route::Resource('purchase', 'PurchaseTeamController');
    Route::get('/purchase/edit/{id}','PurchaseTeamController@edit');
    Route::post('purchaseorder','PurchaseTeamController@purchaseorder');
    Route::get('/purchaseritems/{id}','PurchaseTeamController@purchaseritems');

});
Route::group(['namespace' => 'DispatchTeam'],function(){
    Route::Resource('dispatch', 'DispatchTeamController');
    Route::get('/dispatch/edit/{id}/{data}','DispatchTeamController@edit');
    Route::post('dispatchorder','DispatchTeamController@dispatchorder');
    Route::get('/dispatchitem/{id}','DispatchTeamController@queryitems');


});

Route::group(['namespace' => 'DispatchRegister'],function(){
    Route::Resource('dispatchregister','DispatchRegisterController');
    Route::get('/dispatch/show/{id}','DispatchRegisterController@show');
    Route::post('dispatch/store','DispatchRegisterController@store');
    Route::Resource('dispatchaccount','DispatchAccountController');
    Route::get('/dispatchaccount/edit/{id}','DispatchAccountController@edit');
    Route::post('dispatchaccounts','DispatchAccountController@dispatchaccounts');
    Route::post('/dispatchexpenses','DispatchAccountController@dispatchexpenses');
    Route::get('/infoitems/{id}','DispatchChallanController@infoitems');
    Route::Resource('dispatchchallan', 'DispatchChallanController');
    Route::get('dispatchchallan/edit/{id}','DispatchChallanController@edit');
    Route::Post('/dispatchchal/data','DispatchChallanController@storebychallan');
    Route::get('/dispatchchallan/pdf/{id}','DispatchChallanController@streamPDF');
    Route::get('/dispatchitems/{id}','DispatchRegisterController@dispatchitems');
    Route::get('/dispitem/{id}','DispatchAccountController@dispitem');
    Route::get('/dispatchaccount/edit/{id}/{data}','DispatchAccountController@edit');
    Route::get('/dispatchaccount/show/{id}/{data}','DispatchAccountController@show');
    Route::Post('searchdisplan','DispatchRegisterController@search');
    ROUTE::get('itemsearch/ajax/{key}','DispatchRegisterController@comboinfo');
    ROUTE::get('itemnosearch/ajax/{key}','DispatchRegisterController@comboiteminfo');
    Route::get('/dispatchchallan/poitems/{id}','DispatchChallanController@poitems');
    Route::Post('/dispatchchallan/data','DispatchChallanController@poitem');
    Route::Post('searchdischallan','DispatchChallanController@search');
    ROUTE::get('dispatchchallan/ajax/{key}','DispatchChallanController@combodata');
    Route::get('/dischallan/{id}','DispatchChallanController@queryitems');
    ROUTE::Post('/dispatchinventory','DispatchAccountController@invstore');
    ROUTE::get('/dispatchaccount/create','DispatchAccountController@indexinfo');

});

Route::group(['namespace'=>'CatalogueManagement'],function(){
   Route::get('/subcatalogue','CatalogueController@index');
   ROUTE::Post('/subcatalogue','CatalogueController@getpdf')->name('subcatalogue');
   ROUTE::get('catproducts/ajax/{key}','CatalogueController@products');
});

Route::group(['namespace'=>'InventoryManagement'],function(){
    ROUTE::get('/invdata/show/{id}','InventoryController@show');
    Route::Resource('inventorydetails','InventoryController');
    Route::Resource('inventorydispatch','InventoryDispatchController');
    ROUTE::get('/disinvdata/show/{id}','InventoryDispatchController@show');
    Route::get('/purchaseinv/{id}','InventoryController@queryitems');
    Route::get('/dispatchinvento/{id}','InventoryDispatchController@queryitems');
    Route::Post('searchinvetorypurchase','InventoryController@search');
    Route::Post('searchinvetorydispatch','InventoryDispatchController@search');


 });
 Route::group(['namespace'=>'ExpensesRegister'],function(){
    Route::Resource('dailyexpenses', 'DailyExpensesController');
    Route::Post('/dailyexpenses/data','DailyExpensesController@store');
    Route::Post('/dailyexpenses/ajax','DailyExpensesController@update');
    Route::Post('searchdaily','DailyExpensesController@search');
    Route::get('/dailyexpenses/delete/{id}', 'DailyExpensesController@deleteStationery');
    Route::get('/dailyexpensesit/delete/{id}', 'DailyExpensesController@deleteItPhone');
    Route::get('/dailyexpensessalary/delete/{id}', 'DailyExpensesController@deleteSalary');
    Route::get('/dailyexpensesclient/delete/{id}', 'DailyExpensesController@deleteClient');
    Route::get('/dailyexpensessupplier/delete/{id}', 'DailyExpensesController@deleteSupplier');
    Route::get('/dailyexpensespurchase/delete/{id}', 'DailyExpensesController@deletePurchase');
    Route::get('/dailyexpensesdispatch/delete/{id}', 'DailyExpensesController@deleteDispatch');
    Route::get('/dailyexpensesvehicle/delete/{id}', 'DailyExpensesController@deleteVehicle');
    Route::get('/dailyexpensesmisc/delete/{id}', 'DailyExpensesController@deleteMisc');



});
Route::group(['namespace' => 'ManualPurchasing'],function(){
    Route::resource('manualpurchase', 'ManualPurchaseController');
    Route::resource('manualpricing', 'ManualPricingController');
    Route::resource('manualquotation', 'ManualQuotationController');
    Route::get('/getbycenters/{id}','ManualPurchaseController@getByCompany');
    Route::get('/getbyclient/{id}','ManualPurchaseController@getByClient');
    Route::get('parentcategories/ajax/{cid}','ManualPurchaseController@parentcategories');
    Route::get('subcategories/ajax/{cid}','ManualPurchaseController@subcategories');
    ROUTE::get('/api/itemlist/getbycategory/{id}','ManualPurchaseController@getByCategory');
    Route::get('parentbrands/ajax/{cid}','ManualPurchaseController@parentbrands');
    // Route::get('/getbyitemdesc/{id}','ManualPurchaseController@getbyitemdesc');
    Route::get('/manualpricing/show/{id}','ManualPricingController@show');
    Route::get('/onsuppliers/{id}', 'ManualPricingController@suppliers');
    Route::Post('/iteminfo', 'ManualPricingController@iteminfo');
    Route::Post('/oncallpricemapping','ManualPricingController@pricemapping');
    Route::Post('/onpricemapping/update','ManualPricingController@mappingupdate');
    Route::get('/onhistoryinfo/{id}', 'ManualPricingController@historyinfo');
    Route::get('/oninfoitems/{id}','ManualPricingController@infoitems');
    Route::get('/onmappings/{id}', 'ManualPricingController@mappings');
    Route::get('/queryId','ManualPricingController@queryId');
    Route::get('/queryitems/{id}','ManualPricingController@queryitems');
    Route::Post('/manualpurchase/data','ManualPurchaseController@storebyitem');
    Route::get('/manualquotation/show/{id}','ManualQuotationController@show');
    Route::get('/manualquotation/pdf/{id}','ManualQuotationController@streamPDF');
    Route::post('oncallquotationdata','ManualQuotationController@quotationdata');
    Route::Post('searchmanualquotation','ManualQuotationController@searchinfo');
    Route::get('/manualquotation/hold/{id}','ManualQuotationController@hold');
    Route::get('/manualquotation/reject/{id}','ManualQuotationController@reject');
    Route::get('/manualquotation/cancel/{id}','ManualQuotationController@cancel');
    Route::resource('manualquotationpdf', 'ManualQuotationpdfController');
    Route::get('/manualquotationpdf/pdf/{id}','ManualQuotationpdfController@streamPDF');
    Route::get('/manualquotation/showprofit/{id}','ManualQuotationController@showprofit');
    Route::post('termsdatas','ManualPricingController@termsdata');
    Route::get('/manualquotationsdata/{id}','ManualQuotationController@queryitems');
    Route::get('/manualquotationsdatapdf/{id}','ManualQuotationpdfController@queryitems');

});
});





