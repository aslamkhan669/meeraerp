<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['namespace' => 'Backend', 'as' => 'backend.'], function () {
//     Route::group(['namespace' => 'ProductManagement'],function(){
//         Route::post('/itemlist/new', 'ItemListController@newItem');
    
//     });
    
// });
Route::group(['namespace' => 'Backend', 'as' => 'backend.'], function () {
    Route::group(['namespace' => 'QueryManagement'],function(){
        Route::post('/itemlist/new', 'QueryController@newItem');
        Route::post('/query/save', 'QueryController@saveQuery');
    
    });
    // Route::group(['namespace' => 'ProductManagement'],function(){
    //     Route::get('/itemlist/getbycategory/{id}', 'ItemListController@getByCategory');
        
    
    // });
});