$(document).ready(function () {
    var gqitem_id;
    var data_item_id;
    

    //two call for edit and create price mapping
        $(".onpmap").on('click', function () {
            debugger;
             $( this ).closest( "tr" ).css( "background-color" , "#65dd65" );
        gqitem_id = $(this).closest('tr').find('.sub').val();
        item_id = $(this).attr('data-id');
        data_item_id = $(this).attr('data-item-id');
        $.ajax({
            url: '/admin/onsuppliers/' + encodeURI(item_id),
            type: "GET",
            dataType: "json",
            success: function (data) {
                var html = '';
                for (var i = 0; i < data.length; i++) {
                    var str = "V000"; 
                    var ch = "V";
                    var stri=  str + data[i].supplier_id  ;
                    var ree = stri.substr(-3); 
                    var icode =  ch + ree;
                    var a=i+1
                    html += "<tr><th scope='row'>"  + a + "</th><td>" + data[i].supplier.supplier_name + "</td><td>" + icode + "</td><td>" + data[i].supplier.mobile + "</td><td>" + data[i].supplier.email + "</td><td><button class='btn btn-success btn-sm pull-right'>Send mail</button></td></tr>";
                }
                $('#sec').append(html);
               
            }
        });
  
       // show price in text
        if (gqitem_id) {
            $.ajax({
                url: '/admin/onmappings/' + encodeURI(gqitem_id),
                type: "GET",
                dataType: "json",
                success: function (data) {
                    debugger;

                  if(data.length>0 && data!=null && data!=undefined){
                    var html = '';
                    for (var i = 0; i < data.length; i++) {
                       if(data[i].supplier.supplier_type =='Overseas'){
                        html += "<tr class='overseas'><td>" + data[i].supplier.supplier_name + "/" + data[i].supplier_id + "</td>";
                        html+="<td>" + data[i].supplier.mobile + "/"+data[i].supplier.street+"/"+data[i].supplier.landmark+"/"+data[i].supplier.sector+"/" + data[i].supplier.city + "</td>";
                        html+="<td><button type='button' class='form-control suppinfo' data-toggle='modal' supplierId='" + data[i].supplier.id + "' data-target='#mysupplier' >History</button></td>";
                        html+="<td><input type='text' size='5' class='listprice' name='listprice' value='" + data[i].listprice + "'></td>";
                        html+="<td><input type='text' size='5' class='insurance' name='insurance_shipping' value='" + data[i].insurance_shipping + "'></td>";
                        html+="<td><input type='text' size='5' class='clearance' name='custom_clearance' value='" + data[i].custom_clearance + "'></td>";
                        html+="<td><input type='text' size='5' class='discount' name='discount'  style='background-color: #e4e4e4;' readonly></td>";
                        html+="<td><input type='text'  size='5' class='discounted-price' value='" + data[i].discounted_price + "' name='discounted_price'></td>";
                        html+="<td><input type='text' class='gst' size='5' value='" + data[i].gst + "' name='gst'></td>";
                        html+="<td><input type='text' class='total'  size='5' name='total' value='" + data[i].total + "' readonly></td>";
                        html+="<td><input type='text' class='qty'  size='5' name='quantity' value='" + data[i].quantity + "'></td>";
                        html+="<td><input type='text' class='grandamount' size='5' name='grand_amount' value='" + data[i].grand_total + "'></td>";
                        html+="<td><input type='text' class='delivery_term' size='5' name='delivery_term' value='" + data[i].delivery_term + "'></td>";
                        html+="<td><input type='text' class='warranty' size='5' name='warranty' value='" + data[i].warranty + "'></td>";
                        html+="<td><input type='text' class='standard_package' size='5' name='standard_package' value='" + data[i].standard_package + "'></td>";
                        html+="<td> <input type='radio' name='is_selected'></td>";
                        html+="<td><input type='button' class='form-control' data-id='' value='info' ></td>";
                        html+="<td><input type='button' class='form-control save' data-id='" + data[i].id + "' value='Update'></td>";
                        html += "<input type='hidden' class='qitemid' value='" + data[i].id + "'></tr>";
                    }else{
                        
                        html += "<tr><td>" + data[i].supplier.supplier_name + "/" + data[i].supplier_id+ "</td>";
                        html+="<td>" + data[i].supplier.mobile + "/"+data[i].supplier.street+"/"+data[i].supplier.landmark+"/"+data[i].supplier.sector+"/" + data[i].supplier.city + "</td>";
                        html+="<td><button type='button' class='form-control suppinfo' data-toggle='modal' supplierId='" + data[i].supplier.id + "' data-target='#mysupplier' >History</button></td>";
                        html+="<td><input type='text' size='5' class='listprice' name='listprice' value='" + data[i].listprice + "'></td>";
                        html+="<td><input  type='text' size='5' class='insurance' name='insurance_shipping' style='background-color: #e4e4e4;' readonly></td>";
                        html+="<td><input type='text' size='5' class='clearance' name='custom_clearance' style='background-color: #e4e4e4;' readonly></td>";
                        html+="<td><input type='text' size='5' class='discount' value='"+ data[i].discount +"' name='discount'></td>";
                        html+="<td><input type='text'  size='5' class='discounted-price' value='" + data[i].discounted_price + "' name='discounted_price' ></td>";
                        html+="<td><input type='text' class='gst' size='5' value='" + data[i].gst + "' name='gst'></td>";
                        html+="<td><input type='text' class='total'  size='5' name='total' value='" + data[i].total + "' readonly></td>";
                        html+="<td><input type='text' class='qty'  size='5' name='quantity' value='" + data[i].quantity + "'></td>";
                        html+="<td><input type='text' class='grandamount' size='5' name='grand_amount' value='" + data[i].grand_total + "'></td>";
                        html+="<td><input type='text' class='delivery_term' size='5' name='delivery_term' value='" + data[i].delivery_term + "'></td>";
                        html+="<td><input type='text' class='warranty' size='5' name='warranty' value='" + data[i].warranty + "'></td>";
                        html+="<td><input type='text' class='standard_package' size='5' name='standard_package' value='" + data[i].standard_package + "'></td>";
                        html+="<td><input type='radio' name='is_selected'></td>";
                        html+="<td><input type='button' class='form-control' data-id='' value='info' ></td>";
                        html+="<td><input type='button' class='form-control save' data-id='" + data[i].id + "' value='Update'></td>";
                        html += "<input type='hidden' class='qitemid' value='" + data[i].id + "'></tr>";
                    }
                    }
                    $('#onpmap').html(html);

                          //history supplier
       $(".suppinfo").on('click', function () {
        debugger;

     var   supplier_id = $(this).attr('supplierId');

       $.ajax({
        url: '/admin/onhistoryinfo/' + encodeURI(supplier_id),
        type: "GET",
        dataType: "json",
        success: function (data) {
            var html = '';
                for (var i = 0; i < data.length; i++) {
                    html += "<tr>";
                     html+=  " <td>" + data[i].supplier.supplier_type + "</td>";
                     html+="<td><button type='button' class='btn btn-info tdClick' data-id='"+ data[i].item_id +"' data-toggle='modal' data-target='#ModalItem'>View</button></td>";
                 html+= " <td>" + data[i].listprice + "</td><td>" + data[i].insurance_shipping + "</td><td>" + data[i].custom_clearance + "</td><td>" + data[i].discount + "</td><td>" + data[i].discounted_price + "</td><td>" + data[i].gst + "</td>";
                html+= "</tr>";
                }
                $('.history').html(html);
                $('.tdClick').on('click',function(){
                    debugger;
                    var qitem_id= $(this).attr('data-id');
                    $.ajax({
                            url:'/admin/oninfoitems/' + encodeURI(qitem_id),
                            type:'get',
                            dataType:'json',
                            success:function(data){ 
                                var str = "I0000"; 
                                var ch = "I";
                                var stri=  str + data[0].id  ;
                                var ree = stri.substr(-4); 
                                var icode=  ch + ree; 
                                var strp = "P00" ;
                                var chp ="P";
                                var strq = strp +data[0].product_id;
                                var re = strq.substr(-2);
                                var pcode = chp +re;         
                                html = "<tr><td>"+pcode+icode + "</td><td>" + data[0].description + "</td><td>" + data[0].specification + "</td><td>" + data[0].color.color + "</td><td>" + data[0].model_no + "</td><td>" + data[0].brand.brand_name + "</td><td>" + data[0].hsn_code + "</td><td>" + data[0].uom.unit + "</td><td>" + data[0].remarks + "</td></tr>";
                                $('.sec').html(html);
                            }
                    })        
                })
        }
    });
});
                    (($('input[name=is_selected]').val())!=0) ? $('input[name=is_selected]').attr("checked","checked"): "";
                    $(".discount").on("input", function () {
                        $discount = $(this).val();
                        $price = $(this).closest("tr").find(".listprice").val();
                        var calcPrice = ($price - ($price * $discount / 100)).toFixed(2);
                        $(this).closest("tr").find(".discounted-price").val(calcPrice);
                    })
                    $(".gst").on("input", function () {
                        $price = $(this).closest("tr").find(".discounted-price").val();
                        $gst = $(this).val();

                        var calcPrice = (parseFloat($price) + parseFloat($price * $gst / 100)).toFixed(2);
                        $(this).closest("tr").find(".total").val(calcPrice);

                    })
                    $(".save").on('click', function () {
                        
                        var p_id = $(this).attr("data-id");
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            url: '/admin/onpricemapping/update',
                            data: {
                                listprice: $(this).closest('tr').find('input[name=listprice]').val(),
                                discount: $(this).closest('tr').find('input[name=discount]').val(),
                                discounted_price: $(this).closest('tr').find('input[name=discounted_price]').val(),
                                gst: $(this).closest('tr').find('input[name=gst]').val(),
                                total: $(this).closest('tr').find('input[name=total]').val(),
                                item_id: gqitem_id,
                                id: p_id,
                                is_selected: $(this).closest('tr').find('input[type=radio]:checked').length,
                                insurance: $(this).closest('tr').find('.insurance').val(),
                                clearance: $(this).closest('tr').find('.clearance').val(),
                                grandamount: $(this).closest('tr').find('.grandamount').val(),
                                Quantity: $(this).closest('tr').find('.qty').val(),
                                DeliveryTerm: $(this).closest('tr').find('.delivery_term').val(),
                                Warranty: $(this).closest('tr').find('.warranty').val(),
                                Std_package: $(this).closest('tr').find('.standard_package').val(),
                            },
                            success: function (response) {
                                debugger;
                                if (response == 'true') {
                                    alert('Data updated successfully.');
                                    
                                    
                                    $("#q").removeAttr('disabled');

                                } else {
                                    alert('Data Already Exists.');
                                }
                            },
                        });
                    });
                }
                else{
                   //after if condition fulfill it will work
                    if (item_id) {
                      debugger;
                        $.ajax({
                            url: '/admin/onsuppliers/' + encodeURI(item_id),
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                              
                                $("#q").removeAttr('disabled');
                                var html = '';
                                for (var i = 0; i < data.length; i++) {
                                    var str = "V000"; 
                                    var ch = "V";
                                    var stri=  str + data[i].supplier_id  ;
                                    var ree = stri.substr(-3); 
                                    var icode =  ch + ree; 
                                    if((data[i].supplier.supplier_type).toLowerCase()=='overseas'){
                                        html += "<tr class='overseas' data-id='"+data[i].supplier.supplier_type+"'><td>" + data[i].supplier.supplier_name + "/" + icode + "</td><td>" + data[i].supplier.mobile + "/"+data[i].supplier.street+"/"+data[i].supplier.landmark+"/"+data[i].supplier.sector+"/"+ data[i].supplier.city + "</td><td><button type='button' class='form-control suppinfo' data-toggle='modal' supplierId='" + data[i].supplier.id + "' data-target='#mysupplier' >History</button></td><td><input type='text' size='5' class='listprice' id='listprice' name='listprice' value=''></td><td><input type='text' size='5' class='insurance' id='insurance' name='insurance_shipping' value=''></td><td><input type='text' size='5' class='clearance' name='custom_clearance' value=''></td><td><input type='text' size='5' class='discount' value='' name='discount'  style='background-color: #e4e4e4;' readonly></td><td><input type='text'  size='5' class='discounted-price' value='' name='discounted_price'></td><td><input type='text' class='gst' size='5' value='' name='gst'></td><td><input type='text' class='total'  size='5' name='total' value='' readonly></td><td><input type='text' class='qty'  size='5' name='quantity' value=''></td><td><input type='text' class='grandamount' size='5' name='grand_amount' value=''></td><td><input type='text' class='delivery_term' size='5' name='delivery_term' value=''></td><td><input type='text' class='warranty' size='5' name='warranty' value=''></td><td><input type='text' class='standard_package' size='5' name='standard_package' value=''></td><td><input type='radio' name='is_selected'></td><td><button type='button' class='form-control info' data-item-id='"+data_item_id+"'  value='" + data[i].supplier.id + "'>info</button></td><td><input type='button' class='form-control save' data-id='" + data[i].supplier.id + "' value='Save'></td>";
                                        html += "<input type='hidden' class='qitemid' value='" + data[i].id + "'></tr>";
                                    }
                                    else{
                                        html += "<tr data-id='"+data[i].supplier.supplier_type+"'><td>" + data[i].supplier.supplier_name + "/" + icode + "</td><td>" + data[i].supplier.mobile + "/"+data[i].supplier.street+"/"+data[i].supplier.landmark+"/"+data[i].supplier.sector+"/" + data[i].supplier.city + "</td><td><button type='button' class='form-control suppinfo' data-toggle='modal' supplierId='" + data[i].supplier.id + "' data-target='#mysupplier' >History</button></td><td><input type='text' size='5' class='listprice' id='listprice' name='listprice' value=''></td><td><input type='text' size='5' class='insurance' id='insurance' name='insurance_shipping' style='background-color: #e4e4e4;' readonly></td><td><input type='text' size='5' class='clearance' name='custom_clearance' style='background-color: #e4e4e4;' readonly></td><td><input type='text' size='5' class='discount' value='' name='discount'></td><td><input type='text'  size='5' class='discounted-price' value='' name='discounted_price'></td><td><input type='text' class='gst' size='5' value='' name='gst'></td><td><input type='text' class='total'  size='5' name='total' value='' readonly></td><td><input type='text' class='qty'  size='5' name='quantity' value=''></td><td><input type='text' class='grandamount' size='5' name='grand_amount' value=''></td><td><input type='text' class='delivery_term' size='5' name='delivery_term' value=''></td><td><input type='text' class='warranty' size='5' name='warranty' value=''></td><td><input type='text' class='standard_package' size='5' name='standard_package' value=''></td><td><input type='radio' name='is_selected'></td><td><button type='button' class='form-control info' data-item-id='"+data_item_id+"'  value='" + data[i].supplier.id + "'>info</button></td><td><input type='button' class='form-control save' data-id='" + data[i].supplier.id + "' value='Save'></td>";
                                        html += "<input type='hidden' class='qitemid' value='" + data[i].id + "'></tr>";
                                    }
                                }
                                $('#onpmap').html(html);
               
            
                                $(".discount").on("input", function () {
                                    $discount = $(this).val();
                                    $price = $(this).closest("tr").find(".listprice").val();
                                    if(parseFloat($(this).closest("tr").find(".insurance").val()) && parseFloat($(this).closest("tr").find(".clearance").val())){
                                    $total=(parseFloat($price)+parseFloat($(this).closest("tr").find(".insurance").val())+parseFloat($(this).closest("tr").find(".clearance").val()));
                                    }else{
                                    $total=(parseFloat($price));
                                    }
                                    var calcPrice = ($total - ($total * $discount / 100)).toFixed(2);
                                    $(this).closest("tr").find(".discounted-price").val(calcPrice);
                                })

                                $(".clearance").on("input", function () {
                                    
                                    var price = $(this).closest("tr").find(".listprice").val();
                                    var total=(parseFloat(price)+parseFloat($(this).closest("tr").find(".insurance").val())+parseFloat($(this).closest("tr").find(".clearance").val()));
                                    $(this).closest("tr").find(".discounted-price").val(total);
                                })
                                $(".gst").on("input", function () {
                                    $price = $(this).closest("tr").find(".discounted-price").val();
                                    $gst = $(this).val();
                                    var calcPrice = (parseFloat($price) + parseFloat($price * $gst / 100)).toFixed(2);
                                    var ftotal=$(this).closest("tr").find(".total").val(calcPrice);
                                })
                                $(".qty").on("input",function(){
                                    var fftotal=$(this).closest("tr").find(".total").val();
                                    var gtotal=(parseFloat($(this).closest("tr").find(".qty").val())*parseFloat(fftotal)).toFixed(2);
                                    $(this).closest("tr").find(".grandamount").val(gtotal);
                                })
                                
                    $(".info").on('click', function (e) {
                        debugger;
                        e.stopImmediatePropagation();
                        var supplierid = $(this).val();
                        var index=$(".info").index(this);

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "POST",
                      url: '/admin/iteminfo',
                        dataType: "json",
                        data: {itemid:data_item_id,suppid:supplierid },
                        success: function (data) {
                            $('.listprice').eq(index).closest('tr').find('.listprice').val(data[0].listprice);
                            $('.insurance').eq(index).closest('tr').find('.insurance').val(data[0].insurance_shipping);
                            $('.clearance').eq(index).closest('tr').find('.clearance').val(data[0].custom_clearance);
                            $('.discount').eq(index).closest('tr').find('.discount').val(data[0].discount);
                            $('.discounted-price').eq(index).closest('tr').find('.discounted-price').val(data[0].discounted_price);
                            $('.gst').eq(index).closest('tr').find('.gst').val(data[0].gst);
                            $('.total').eq(index).closest('tr').find('.total').val(data[0].total);
                            $('.qty').eq(index).closest('tr').find('.qty').val(data[0].quantity);
                            $('.grandamount').eq(index).closest('tr').find('.grandamount').val(data[0].grand_amount);
                            $('.delivery_term').eq(index).closest('tr').find('.delivery_term').val(data[0].delivery_term);
                            $('.warranty').eq(index).closest('tr').find('.warranty').val(data[0].warranty);
                            $('.standard_package').eq(index).closest('tr').find('.standard_package').val(data[0].standard_package);


                        }
                    });
                });
       //history supplier
       $(".suppinfo").on('click', function () {
        debugger;

     var   supplier_id = $(this).attr('supplierId');

       $.ajax({
        url: '/admin/onhistoryinfo/' + encodeURI(supplier_id),
        type: "GET",
        dataType: "json",
        success: function (data) {
            var html = '';
                for (var i = 0; i < data.length; i++) {
            
                    html += "<tr>";
                    html+=  " <td>" + data[i].supplier.supplier_type + "</td>";
                    html+="<td><button type='button' class='btn btn-info tdClick' data-id='"+ data[i].item_id +"' data-toggle='modal' data-target='#ModalItem'>View</button></td>";
                html+= " <td>" + data[i].listprice + "</td><td>" + data[i].insurance_shipping + "</td><td>" + data[i].custom_clearance + "</td><td>" + data[i].discount + "</td><td>" + data[i].discounted_price + "</td><td>" + data[i].gst + "</td>";
               html+= "</tr>";
                }
                $('.history').html(html);
                $('.tdClick').on('click',function(){
                    debugger;
                    var qitem_id= $(this).attr('data-id');
                    $.ajax({
                            url:'/admin/oninfoitems/' + encodeURI(qitem_id),
                            type:'get',
                            dataType:'json',
                            success:function(data){ 
                                var str = "I0000"; 
                                var ch = "I";
                                var stri=  str + data[0].id  ;
                                var ree = stri.substr(-4); 
                                var icode=  ch + ree; 
                                var strp = "P00" ;
                                var chp ="P";
                                var strq = strp +data[0].product_id;
                                var re = strq.substr(-2);
                                var pcode = chp +re;         
                                html = "<tr><td>"+pcode+icode + "</td><td>" + data[0].description + "</td><td>" + data[0].specification + "</td><td>" + data[0].color.color + "</td><td>" + data[0].model_no + "</td><td>" + data[0].brand.brand_name + "</td><td>" + data[0].hsn_code + "</td><td>" + data[0].uom.unit + "</td><td>" + data[0].remarks + "</td></tr>";
                                $('.sec').html(html);
                            }
                    })        
                })
        }
    });
});
                                $(".save").on('click', function () {
                                    var sup_id = $(this).attr("data-id");  
                                    $.ajax({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        type: 'POST',
                                        url: '/admin/oncallpricemapping',
                                        data: {
                                            listprice: $(this).closest('tr').find('input[name=listprice]').val(),
                                            discount: $(this).closest('tr').find('input[name=discount]').val(),
                                            discounted_price: $(this).closest('tr').find('input[name=discounted_price]').val(),
                                            gst: $(this).closest('tr').find('input[name=gst]').val(),
                                            total: $(this).closest('tr').find('input[name=total]').val(),
                                            item_id: gqitem_id,
                                            supplier_id: sup_id,
                                            is_selected: $(this).closest('tr').find('input[type=radio]:checked').length,
                                            insurance: $(this).closest('tr').find('.insurance').val(),
                                            clearance: $(this).closest('tr').find('.clearance').val(),
                                            grandamount: $(this).closest('tr').find('.grandamount').val(),
                                            Quantity: $(this).closest('tr').find('.qty').val(),
                                            DeliveryTerm: $(this).closest('tr').find('.delivery_term').val(),
                                            Warranty: $(this).closest('tr').find('.warranty').val(),
                                            Std_package: $(this).closest('tr').find('.standard_package').val(),
                                        },
                                        success: function (response) {
                                            if (response == 'true') {
                                                alert('Data inserted successfully.');
                                            } else {
                                                alert('Data Already exist.');
                                            }
                                        },
                                    });
                                });
                            }
                        });
                    }
                }
            }
        });
    }
    });
    

    $("#close-button").on('click', function (e) {
        var html = '';
        $('#sec').html(html);
    })

    $('#q').on('click',function(){
        debugger;
        var freight_id=$('.freight_id :selected').val();
        var insurance_id=$('.insurance_id :selected').val();
        var payment_days=$('input[name=payment_days]').val();
        var validity_days=$('input[name=validity_days]').val();
        var comment_one=$('input[name=comment_one]').val();
        var comment_two=$('input[name=comment_two]').val();
        var comment_three=$('input[name=comment_three]').val();
        var comment_four=$('input[name=comment_four]').val();
        var qid=$('#qid').val();
        var userName=$('#userName').val();
        var userId=$('#userId').val();
        var mod=$('#module').val();
        var act=$('#action').val();
        var activ=$('#activity').val();
        $.ajax({
            type:'get',
            url:'/admin/queryId',
            dataType:'json',
            data:{ queryid:qid,userName:userName,userId:userId,module:mod,action:act,activity:activ},
            success:function(data){
                if (data==true) {
                    alert('Manual purchase Converted successfully.');
                }else{
                    alert('Manual purchase Already Converted.');
                }
            }
        })

     // Second post request for quotationtermscondition
     $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:'/admin/termsdatas',
        type:'post',
        data:{Query_id:qid,
            Freight_id:freight_id,
            Insurance_id:insurance_id,
            Payment_days:payment_days,
            Validity_days:validity_days,
            Comment_one:comment_one,
            Comment_two:comment_two,
            Comment_three:comment_three,
            comment_four:comment_four,
        },
        success:function(response){  
       }
        });
    })
});

