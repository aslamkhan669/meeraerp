$(document).ready(function () {
    $(".clearance").on("input", function () {
                                    
        var price = $(this).closest("tr").find(".listprice").val();
        var total=(parseFloat(price)+parseFloat($(this).closest("tr").find(".insurance").val())+parseFloat($(this).closest("tr").find(".clearance").val()));
        $(this).closest("tr").find(".discounted_price").val(total);
    })
    $(".gst").on("input", function () {
        $price = $(this).closest("tr").find(".discounted_price").val();
        $gst = $(this).val();
        var calcPrice = (parseFloat($price) + parseFloat($price * $gst / 100)).toFixed(2);
        var ftotal=$(this).closest("tr").find(".total").val(calcPrice);
    })
    $(".qty").on("input",function(){
        var fftotal=$(this).closest("tr").find(".total").val();
        var gtotal=(parseFloat($(this).closest("tr").find(".qty").val())*parseFloat(fftotal)).toFixed(2);
        $(this).closest("tr").find(".grand_amount").val(gtotal);
    })
    $(".discount").on("input", function () {
        debugger;
        $discount = $(this).val();
        $price = $(this).closest("tr").find(".listprice").val();
        var calcPrice = ($price - ($price * $discount / 100)).toFixed(2);
        $(this).closest("tr").find(".discounted_price").val(calcPrice);
    })
    $(".gst").on("input", function () {
        $price = $(this).closest("tr").find(".discounted_price").val();
        $gst = $(this).val();

        var calcPrice = (parseFloat($price) + parseFloat($price * $gst / 100)).toFixed(2);
        $(this).closest("tr").find(".total").val(calcPrice);

    })

    //info pricing

    $(".info").on('click', function (e) {
        debugger;
        e.stopImmediatePropagation();
        var supplierid = $(this).val();
        var index=$(".info").index(this);
        var data_item_id= $(this).attr('data-id');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
      url: '/admin/itempriceinfo',
        dataType: "json",
        data: {itemid:data_item_id,suppid:supplierid },
        success: function (data) {
            debugger;
            $('.listprice').eq(index).closest('tr').find('.listprice').val(data[0].listprice);
            $('.insurance').eq(index).closest('tr').find('.insurance').val(data[0].insurance_shipping);
            $('.clearance').eq(index).closest('tr').find('.clearance').val(data[0].custom_clearance);
            $('.discount').eq(index).closest('tr').find('.discount').val(data[0].discount);
            $('.discounted_price').eq(index).closest('tr').find('.discounted_price').val(data[0].discounted_price);
            $('.gst').eq(index).closest('tr').find('.gst').val(data[0].gst);
            $('.total').eq(index).closest('tr').find('.total').val(data[0].total);
            $('.qty').eq(index).closest('tr').find('.qty').val(data[0].quantity);
            $('.grandamount').eq(index).closest('tr').find('.grandamount').val(data[0].grand_amount);
            $('.delivery_term').eq(index).closest('tr').find('.delivery_term').val(data[0].delivery_term);
            $('.warranty').eq(index).closest('tr').find('.warranty').val(data[0].warranty);
            $('.standard_package').eq(index).closest('tr').find('.standard_package').val(data[0].standard_package);


        }
    });
});

           //history supplier
           $(".suppinfo").on('click', function () {
            debugger;
    
         var   supplier_id = $(this).attr('supplierId');
    
           $.ajax({
            url: '/admin/historyinfo/' + encodeURI(supplier_id),
            type: "GET",
            dataType: "json",
            success: function (data) {
                debugger;
                var html = '';
                    for (var i = 0; i < data.length; i++) {
                
                        html += "<tr>";
                     html+=  " <td>" + data[i].supplier.supplier_type + "</td>";
                     html+="<td><button type='button' class='btn btn-info tdClick' data-id='"+ data[i].item_id +"' data-toggle='modal' data-target='#ModalItem'>View</button></td>";
                 html+= " <td>" + data[i].listprice + "</td><td>" + data[i].insurance_shipping + "</td><td>" + data[i].custom_clearance + "</td><td>" + data[i].discount + "</td><td>" + data[i].discounted_price + "</td><td>" + data[i].gst + "</td>";
                html+= "</tr>";
                    }
                    $('.history').html(html);
                    $('.tdClick').on('click',function(){
                        debugger;
                        var qitem_id= $(this).attr('data-id');
                        $.ajax({
                                url:'/admin/infoitems/' + encodeURI(qitem_id),
                                type:'get',
                                dataType:'json',
                                success:function(data){ 
                                    var str = "I0000"; 
                                    var ch = "I";
                                    var stri=  str + data[0].id  ;
                                    var ree = stri.substr(-4); 
                                    var icode=  ch + ree; 
                                    var strp = "P00" ;
                                    var chp ="P";
                                    var strq = strp +data[0].product_id;
                                    var re = strq.substr(-2);
                                    var pcode = chp +re; 
                                    html = "<tr><td>"+pcode+icode + "</td><td>" + data[0].description + "</td><td>" + data[0].specification + "</td><td>" + data[0].color.color + "</td><td>" + data[0].model_no + "</td><td>" + data[0].brand.brand_name + "</td><td>" + data[0].hsn_code + "</td><td>" + data[0].uom.unit + "</td><td>" + data[0].remarks + "</td></tr>";
                                    $('#sec').html(html);
                                }
                        })        
                    })
            }
        });
    });

});