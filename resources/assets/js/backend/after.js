// Loaded after CoreUI app.js
switch(window.location.pathname){
    
    case '/admin/query/create':  
            require('./query.js');
            break;
    case '/admin/itemlist': 
           
            break;  
    case '/admin/supplier/create':  
            require('./supplier.js');
            break; 
    case '/admin/oncallpoformatting/create':  
    require('./oncallpoformatting.js');
    break; 
    case '/admin/poformatting/create':  
    require('./poformatting.js');
    break; 
    case '/admin/category/create':  
    require('./combo.js');
    break; 
    case '/admin/itemlist/create':  
    require('./combo.js');
    break; 
    case '/admin/dailyexpenses/create':  
    require('./dailyexpenses.js');
    break;
    case '/admin/subcatalogue':  
    require('./combo.js');
    break;
    case '/admin/purchase':  
    require('./purchase.js');
    break;
    case '/admin/manualpurchase/create':  
    require('./manualpurchase.js');
    break;
    case '/admin/dashboard':  
    require('./dashboard.js');
    break; 
    case '/admin/pogenerate/create':  
    require('./pogenerate.js');
    break;  
    case '/admin/purchasechallan/create':  
    require('./purchasechallan.js');
    break; 
    case '/admin/dispatchchallan/create':  
    require('./dispatchchallan.js');
    break; 
    case '/admin/dispatch':  
    require('./dispatch.js');
    break; 
    case '/admin/inventorydetails':  
    require('./purchaseinventory.js');
    break;  
    case '/admin/inventorydispatch':  
    require('./dispatchinventory.js');
    break;         
}

var route = window.location.pathname;
if(/admin\/pricing\/show\/[0-9]*/.test(route)){
    require('./pricing.js')
}
if(/admin\/itemmapping\/show\/[0-9]*/.test(route)){
    require('./itempricing.js')
}
if(/admin\/itemmapping\/[0-9]*\/edit/.test(route)){
    require('./itempricing.js')
}
if(/admin\/query\/[0-9]*\/edit/.test(route)){
    require('./query.js')
}
if(/admin\/queryitem\/[0-9]*\/edit/.test(route)){
    require('./query.js')
}
if(/admin\/supplier\/[0-9]*\/edit/.test(route)){
    require('./supplier.js')
}

if(/admin\/quotation\/show\/[0-9]*/.test(route)){
    require('./quotation.js')
}
if(/admin\/quotation\/showprofit\/[0-9]*/.test(route)){
    require('./quotationpdf.js')
}
if(/admin\/category\/[0-9]*\/edit/.test(route)){
    require('./combo.js')
}

if(/admin\/poformatting\/[0-9]*\/edit/.test(route)){
    require('./poformatting.js')
}
if(/admin\/planner\/[0-9]*\/edit/.test(route)){
    require('./poplanner.js')
}
if(/admin\/purchase\/edit\/[0-9]*/.test(route)){
    require('./purchase.js')
}

if(/admin\/poregister\/[0-9]*\/edit/.test(route)){
    require('./accounts.js')
}

if(/admin\/dispatch\/show\/[0-9]*/.test(route)){
    require('./dispatch.js')
}
if(/admin\/poregister\/edit\/[0-9]*/.test(route)){
    require('./accounts.js')
}
if(/admin\/purchasechallan\/edit\/[0-9]*/.test(route)){
    require('./accounts.js')
}
if(/admin\/dispatchaccount\/edit\/[0-9]*/.test(route)){
    require('./dispatchaccounts.js')
}
if(/admin\/dispatchchallan\/edit\/[0-9]*/.test(route)){
    require('./dispatchaccounts.js')
}
if(/admin\/manualpricing\/show\/[0-9]*/.test(route)){
    require('./manualpricing.js')
}
if(/admin\/dailyexpenses\/[0-9]*\/edit/.test(route)){
    require('./dailyexpenses.js')
}
if(/admin\/dispatch\/edit\/[0-9]*/.test(route)){
    require('./dispatch.js')
}
if(/admin\/purchase\/edit\/[0-9]*/.test(route)){
    require('./purchase.js')
}
if(/admin\/pogenerate\/show\/[0-9]*/.test(route)){
    require('./po.js')
}
if(/admin\/manualpurchase\/[0-9]*\/edit/.test(route)){
    require('./manualpurchase.js')
}
if(/admin\/manualquotation\/show\/[0-9]*/.test(route)){
    require('./manualquotation.js')
}
if(/admin\/manualquotation\/showprofit\/[0-9]*/.test(route)){
    require('./manualquotationpdf.js')
}