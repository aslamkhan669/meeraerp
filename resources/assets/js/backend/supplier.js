$(document).ready(function () {  
    $('.product_list_cart').click(function(){
        var tr = $(this).closest('tr'),
            del_id = $(this).attr('data-id');
        $.ajax({
            url: "/admin/categorysupplier/delete/"+ del_id,
            cache: false,
            success:function(result){
                tr.fadeOut(1000, function(){
                    $(this).remove();
                });
            }
        });
    });

$('.product_id').on('change', function () {
    var product_id = $(this).val();
    $('.cat_id').val(product_id);//what is this ?
    if (product_id) {
        $.ajax({
            url: '/admin/parentcategories/' + encodeURI(product_id),
            type: "GET",
            dataType: "json",
            success: function (data) {                
                if(data){
                var html = "<option value='false'>Select</option>";
                for (var i = 0; i < data.length; i++) {
                    html += "<option value=" + data[i].id + ">" + data[i].category_name + "</option>";
                }
                $(".cat").html(html);
                $('.unshowcat').show();
                categories=data;
               }
            }
        });
    }
})

$('.addcategory').on('click', function (e) {
    e.preventDefault();
    debugger;
    var inputs = $('#myForm :input');
    var data = {
        product_name:$('.product_id').find(":selected").text(),
        cat_name:$('.cat').find(":selected").text(),
        cat_id: $(".cat").val(), 
    };
    var html = "<tr><td>";
    // html+="<td><input type='hidden' class='i_product_name' name='product_name[]' value='"+data.product_name+"' ><input type='hidden' class='i_cat_id' name='catId[]' value='"+data.cat_id+"' >";
    html+= data.product_name +"</td>";
    html+="<td data-id='"+data.cat_id+"'>"+ data.cat_name+"</td>";
    html+="<td><span class='delete' data-id='"+data.cat_id+"'><a href=''>Delete</a></span></td>";   
    html+="</tr>";
    $(".supplierItems").append(html);
    $(".product-mapping").append("<input class='catIdh' type='hidden' name='catId[]' value='"+data.cat_id+"'>");
    $(".delete").on("click",function(e){
        debugger;
            e.preventDefault();

        e.stopImmediatePropagation();
        $(this).closest("tr").remove();
        var catid =  $(this).attr('data-id');
        $(".catIdh[value='"+catid+"']").remove()
    })
});
});