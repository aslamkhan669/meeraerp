
$(document).ready(function () {      
$(':radio').change(function() {
    console.log('New star rating: ' + this.value);
  })
    $('#addNewItems').on('click',function(e){
        e.preventDefault();
                    var tdata = {
                        hsn_code: $('#hsnval').val(),
                        product_id: $("#productid").val(),
                        brand_id: $("#brand").val(),
                        category: $("#cat_new").val(),
                        cat_id: $('#subcat_new').val(),
                        description: $("#descriptions").val(),
                        specification: $("#specifications").val(),
                        color: $("#colors").val(),
                        model_no: $("#modelno").val(),
                        unit: $("#units").val(),
                        remarks: $("#remark").val(),
                        image: $("#saveimages").val(),
                        is_active: $("#is_active").val(),
                        package_qty: $("#package_qty").val(),
                    }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type: 'POST',
            url: '/admin/query/data',
            data: tdata,
            success: function(response){
                if(response=='true'){
                    $('#myModal').modal('hide');
                    alert('New item saved successfully.')
                }else{
                    alert('Try again');
                }
            },
        });
                });
    
      $('select[name="product_id"]').on('change', function() {
            var product_id = $(this).val();      
            $('#cat_id').val(product_id);
                if(product_id) {
                    $.ajax({
                        url: '/admin/parentcategories/ajax/'+encodeURI(product_id),
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            var html = '<option>Select</option>';
                            for(var i=0;i<data.length;i++){ 
                                html+="<option value="+data[i].id+">"+data[i].category_name+"</option>";
                            }
                            $("#cat_new").html(html);
                        }
                    });
                }
        });
    //brand show
$('select[name="product_id"]').on('change', function() {
    var product_id = $(this).val();      
    debugger;
        if(product_id) {
            $.ajax({
                url: '/admin/parentbrands/ajax/'+encodeURI(product_id),
                type: "GET",
                dataType: "json",
                success:function(data) {
                    var html = "<option>Select</option>";
                    for(var i=0;i<data.length;i++){ 
                        html+="<option value="+data[i].id+">"+data[i].brand_name+"</option>";
                    }
                    $("#brand").html(html);
                }
            });
        }
});

$('.product_id').on('change', function() {
    var product_id = $(this).val();      
    debugger;
        if(product_id) {
            $.ajax({
                url: '/admin/parentbrands/ajax/'+encodeURI(product_id),
                type: "GET",
                dataType: "json",
                success:function(data) {
                    var html = "<option>Select</option>";
                    for(var i=0;i<data.length;i++){ 
                        html+="<option value="+data[i].id+">"+data[i].brand_name+"</option>";
                    }
                    $(".brand_id").html(html);
                }
            });
        }
});
    //On category change
        $('select[name="category_id"]').on('change', function() {
            var category_id = $(this).val();
            $('#cat_id').val(category_id);
                if(category_id) {
                    $.ajax({
                        url: '/admin/subcategories/ajax/'+encodeURI(category_id),
                        type: "GET",
                        dataType: "json",
                        success:function(data) {
                            if ($.trim(data)){ 
                            var html = "<option>Select</option>";
                            for(var i=0;i<data.length;i++){ 
                                html+="<option value="+data[i].id+">"+data[i].category_name+"</option>";
                            }
                            $("#subcat_new").html(html);
                            $('.unshow').show();
                            }else{   
                            alert("Subcategory not Exist");
                            $('.unshow').hide();
    
                            }
                        }
                    });     
                }
        });
    
        $('select[name="subcategory_id"]').on('change',function(){
            var cat_id=$(this).val();
            $('#cat_id').val(cat_id);
        })
    //binders
    var rowEditIndex = 0;
     var currentEditingRow = 0;
    $('#product_id').on('change', function () {
        var product_id = $(this).val();
        $('#cat_id').val(product_id);//what is this ?
        if (product_id) {
            $.ajax({
                url: '/admin/parentcategories/ajax/' + encodeURI(product_id),
                type: "GET",
                dataType: "json",
                success: function (data) {          
                    if(data){
                    var html = "<option value='false'>Select</option>";
                    for (var i = 0; i < data.length; i++) {
                        html += "<option value=" + data[i].id + ">" + data[i].category_name + "</option>";
                    }
                    $("#cat").html(html);
                    $('.unshowcat').show();
                    categories=data;
                   }
                }
            });
        }
    })

    //why there is #cat and #cat_id ?
    //On category change
    $('#cat').on('change', function () {
        var category_id = $(this).val();
        selectedCategoryIndex = categories.findIndex(x => x.id==category_id);
        selectedCategoryText=categories.filter(obj=>obj.id===category_id).category_name;
        $('#cat_id').val(category_id);  //Why ?
        if (category_id) {
            $.ajax({
                url: '/admin/subcategories/ajax/' + encodeURI(category_id),
                type: "GET",
                dataType: "json",
                success: function (data) {
                    if ($.trim(data)) {
                        var html = "<option>Select</option>";
                        for (var i = 0; i < data.length; i++) {
                            html += "<option value=" + data[i].id + ">" + data[i].category_name + "</option>";
                        }
                     $("#subcat").html(html);
                    } else {
                        alert("Subcategory not Exist");
                    }
                }
            });
        }
    });
    $("#subcat").on('change',function(){
        var subcategory_id = $(this).val();
        if (subcategory_id) {
        $.ajax({
            url: '/admin/api/itemlist/getbycategory/' + encodeURI(subcategory_id),
            type: "GET",
            dataType: "json",
            success: function (data) {
                if ($.trim(data)) {
                    var html = "<option>Select</option>";
                    debugger;
                    for (var i = 0; i < data.length; i++) {
                        html += "<option value=" + data[i].id + ">" + data[i].description + "    " + data[i].specification + "   " + data[i].color.color + "     " + data[i].model_no + "   " + data[i].hsn_code + " </option>";
                    }
                 $("#item").html(html);
                } else {
                    alert("Item not Exist");
                }
            }
        });
    }
    })
// change code here

    $("#item").on('change',function(){
        debugger;
        var item_id = $(this).val();
        if (item_id) {
        $.ajax({
            url: '/admin/getbyitemdesc/' + encodeURI(item_id),
            type: "GET",
            dataType: "json",
            success: function (data) {
                if ($.trim(data)) {

                    debugger;
                    $('#subcat').val(data[0].cat_id).css({'background-color' : 'lightcyan'});
                    $('#cat').val(data[0].category.parent_id).css({'background-color' : 'lightcyan'});
                 $('#product_id').val(data[0].product_id).css({'background-color' : 'lightcyan'});
                 $('#description').val(data[0].description).css({'background-color' : 'lightcyan'});
                 $('#specification').val(data[0].specification).css({'background-color' : 'lightcyan'});
                 $('#model_no').val(data[0].model_no).css({'background-color' : 'lightcyan'});
                 $('#remarks').val(data[0].remarks).css({'background-color' : 'lightcyan'});
                 $('#color').val(data[0].color).css({'background-color' : 'lightcyan'});
                 $('#brand_id').val(data[0].brand_id).css({'background-color' : 'lightcyan'});
                 $('#unit').val(data[0].unit).css({'background-color' : 'lightcyan'});
                 $('#hsn_code').val(data[0].hsn_code).css({'background-color' : 'lightcyan'});
                 var iname=(data[0].image).split('/');
                 $('#saveimage').val(iname[1]);
                 html='<td class="image-hover"><img src="../../../'+data[0].image+'" width="50px" height="50px"/></td>';
                 $('.image').html(html);
                } 
            }
        });
    }
    })






    
    $("#centercode").on('change',function(){
        var center_id = $(this).val();  
        if (center_id) {
        $.ajax({
            url: ' /admin/getbycenter/' + encodeURI(center_id),
            type: "GET",
            dataType: "json",
            success: function (data) {  
                 $("#meera_center_code").val(data[0].company_name);
            }
        });
    }
    })
    $(".clientcode").on('change',function(){
        var client_id = $(this).val();
        
        if (client_id) {
        $.ajax({
            url: ' /admin/getbyclient/' + encodeURI(client_id),
            type: "GET",
            dataType: "json",
            success: function (data) { 
                debugger; 
                 $(".client_detail").val(data[0].client_name);
                 var html = "<option>Select</option>";
                 for (var i = 0; i < data.length; i++) {
                     html += "<option value=" + data[i].contact_person1 + " data-itemid="+ data[i].id +" data-id="+ data[i].contact_number1 +" >" + data[i].contact_person1 + "  </option>";
                     html += "<option value=" + data[i].contact_person2 + " data-itemid="+ data[i].id +" data-id="+ data[i].contact_number2 +" >" + data[i].contact_person2 + "  </option>";
                     html += "<option value=" + data[i].contact_person3 + " data-itemid="+ data[i].id +" data-id="+ data[i].contact_number3 +" >" + data[i].contact_person3 + "  </option>";
                     html += "<option value=" + data[i].contact_person4 + " data-itemid="+ data[i].id +" data-id="+ data[i].contact_number4 +" >" + data[i].contact_person4 + "  </option>";
                     html += "<option value=" + data[i].contact_person5 + " data-itemid="+ data[i].id +" data-id="+ data[i].contact_number5 +" >" + data[i].contact_person5 + "  </option>";
                     html += "<option value=" + data[i].contact_person6 + " data-itemid="+ data[i].id +" data-id="+ data[i].contact_number6 +" >" + data[i].contact_person6 + "  </option>";
                     html += "<option value=" + data[i].contact_person7 + " data-itemid="+ data[i].id +" data-id="+ data[i].contact_number7 +" >" + data[i].contact_person7 + "  </option>";
                     html += "<option value=" + data[i].contact_person8 + " data-itemid="+ data[i].id +" data-id="+ data[i].contact_number8 +" >" + data[i].contact_person8 + "  </option>";

                 }
                 $(".query_person").html(html);
             
                 $(".sales_center").val(data[0].sales_center);

            }
        });
    }
    })
// contact number

$(".query_person").on('change',function(){
    var qperson=$(this).find(':selected').attr('data-id');
    var qitem_id= $(this).find(':selected').attr('data-itemid');

    debugger;
    if (qitem_id) {
    $.ajax({
        url: ' /admin/getbyclient/' + encodeURI(qitem_id),
        type: "GET",
        dataType: "json",
        success: function (data) { 
            debugger; 
             $(".contact_no").val(qperson);         
         
                  

        }
    });
}
})


    function n(n){
        return n>9?""+n:"0"+n;
    }
    //start upload file
    $("#fileinput").on('change',function(){
        var image = $(this).val();
        var file = $('#fileinput').prop('files');  
        var form = new FormData();
        form.append("image", $("#fileinput")[0].files[0]);
        if (file.length >0 ) {

        $.ajax({
            headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            enctype: 'multipart/form-data',
            url: '/admin/fileupload',
            type: "POST",
            data:form,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {  
                $("#saveimage").val(data);

            }
        });
    }
    })


    $("#fileinputitem").on('change',function(){
        var file = $('#fileinputitem').prop('files');  
        var form = new FormData();
        form.append("image", $("#fileinputitem")[0].files[0]);
        if (file.length >0 ) {
        $.ajax({
            headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            enctype: 'multipart/form-data',
            url: '/admin/fileuploaditem',
            type: "POST",
            data:form,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {  
                $("#saveimages").val(data);

            }
        });
    }
    })

    //end upload file
    
    $("#addItemsModal").on('click',function(e){
        $("#exampleModal").modal("show");
        resetModalWindow();
        $("#addItems").show();
        $("#editItem").hide();
    })
    
    $('#addItems').on('click', function (e) {
        e.preventDefault();
        var image=$('.checkimage').val();
        
        var item_id= $("#item").val();
        var description= $("#description").val();
        var specification= $("#specification").val();
        var color= $("#color").val();
        var model_no=$("#model_no").val();
        var brand_id= $("#brand_id").val();
        var hsn_code= $("#hsn_code").val();
        var unit= $("#unit").val();
        var qty= $("#qty").val();

        var remarks= $("#remarks").val();
        if(image=='')
        {
            alert('Select item image')
        }
        else if(item_id==null)
        {
            alert('Select item description')
        }
        else if(description=='')
        {
            alert('Enter Description')
        }
        else if(specification=='')
        {
            alert('Enter Specification')
        }
        else if(color=='')
        {
            alert('Enter Color')
        }
        else if(model_no=='')
        {
            alert('Enter Model No.')
        }
        else if(brand_id=='')
        {
            alert('Select Brand Name')
        }
        else if(hsn_code=='')
        {
            alert('Enter HSN Code')
        }
        else if(unit=='')
        {
            alert('Select Uom')
        }
        else if(qty=='')
        {
            alert('Select QTY')
        }
        else if(remarks=='')
        {
            alert('Enter Remarks')
        }
        else
        {
        var inputs = $('#myForm :input');
        var data = {
            product_name: $("#product_id").val(),
            category: $("#cat").val(),
            cat_id: $('#subcat').val(),
            item_id: $("#item").val(),
            description: $("#description").val(),
            specification: $("#specification").val(),
            color: $("#color").val(),
            model_no: $("#model_no").val(),
            brand_id: $("#brand_id").val(),
            hsn_code: $("#hsn_code").val(),
            unit: $("#unit").val(),
            qty: $("#qty").val(),
            remarks: $("#remarks").val(),
            image: $("#saveimage").val(),
        };
        var html = "<tr>";
        html+="<td><input type='hidden' class='i_cat_id' name='cat_id[]' value='"+data.cat_id+"' ><input type='hidden' class='i_category' name='category[]' value='"+data.category+"' ><input type='hidden' class='i_product_name' name='product_name[]' value='"+data.product_name+"' ><input type='hidden' class='i_items_id' name='item_id[]' value='"+data.item_id+"' ><input type='hidden' class='i_color' name='color[]' value='"+data.color+"'>";
        html+="<input type='hidden'  class='i_model_no' name='model_no[]' value='"+data.model_no+"'><input type='hidden' class='i_brand_id' name='brand_id[]' value='"+data.brand_id+"'><input type='hidden' class='i_hsn_code' name='hsn_code[]' value='"+data.hsn_code+"' >";
        html+="<input type='hidden' class='i_unit' name='unit[]' value='"+data.unit+"'><input type='hidden' class='i_qty' name='qty[]' value='"+data.qty+"'><input type='hidden' class='i_remarks' name='remarks[]' value='"+data.remarks+"'>";
        html+="<input type='hidden' class='i_specification' name='specification[]' value='"+data.specification+"'><input type='hidden' class='i_description' name='description[]' value='"+data.description+"'><input type='hidden' class='i_image' name='image[]' value='"+data.image+"'>";
        var str = "I0000"; 
        var ch = "I";
        var stri=  str + data.item_id  ;
        var ree = stri.substr(-4); 
        var icode=  ch + ree; 
        var strp = "P00" ;
        var chp ="P";
        var strq = strp +data.product_name;
        var re = strq.substr(-2);
        var pcode = chp +re;  
        html+= pcode+icode +"</td>";
        html+="<td>"+ data.description +"</td>";
        html+="<td>"+ data.specification+"</td>";
        html+="<td>"+ data.model_no+"</td>";
        html+="<td>"+data.hsn_code+"</td>";
        html+="<td>"+data.qty+"</td>";
        html+="<td><button class='btn btn-primary edit'>Edit</button>";
        html+="<span class='delete'><a href='#'>Delete</a></span></td>";
        html+="</tr>";
        $("#queryItems").append(html);
        $(".delete").on("click",function(e){
            e.stopImmediatePropagation();
            $(this).closest("tr").remove();
        })
        editItemRow();
        
        $("#exampleModal").modal("hide");
    }
    });
    $("#editItem").on('click', function (e) {
        e.preventDefault();
        debugger;
        var inputs = $('#myForm :input');
        var data = {
            product_name: $("#product_id").val(),
            category: $("#cat").val(),
            cat_id: $('#subcat').val(),
            item_id: $("#item").val(),
            description: $("#description").val(),
            specification: $("#specification").val(),
            color: $("#color").val(),
            model_no: $("#model_no").val(),
            brand_id: $("#brand_id").val(),
            hsn_code: $("#hsn_code").val(),
            unit: $("#unit").val(),
            qty: $("#qty").val(),
            remarks: $("#remarks").val(),
            image: $("#saveimage").val(),
        };
        var html = "";
        html+="<td><input type='hidden' class='i_cat_id' name='cat_id[]' value='"+data.cat_id+"' ><input type='hidden' class='i_category' name='category[]' value='"+data.category+"' ><input type='hidden' class='i_product_name' name='product_name[]' value='"+data.product_name+"' ><input type='hidden' class='i_items_id' name='item_id[]' value='"+data.item_id+"' ><input type='hidden' class='i_color' name='color[]' value='"+data.color+"'>";
        html+="<input type='hidden'  class='i_model_no' name='model_no[]' value='"+data.model_no+"'><input type='hidden' class='i_brand_id' name='brand_id[]' value='"+data.brand_id+"'><input type='hidden' class='i_hsn_code' name='hsn_code[]' value='"+data.hsn_code+"' >";
        html+="<input type='hidden' class='i_unit' name='unit[]' value='"+data.unit+"'><input type='hidden' class='i_qty' name='qty[]' value='"+data.qty+"'><input type='hidden' class='i_remarks' name='remarks[]' value='"+data.remarks+"'>";
        html+="<input type='hidden' class='i_specification' name='specification[]' value='"+data.specification+"'><input type='hidden' class='i_description' name='description[]' value='"+data.description+"'><input type='hidden' class='i_image' name='image[]' value='"+data.image+"'>";
        html+= data.description +"</td>";
        if(currentEditingRow==0){
            currentEditingRow =  $("#queryItems").find("tr").eq(currentEditingRow);
        }


        currentEditingRow.find("td").eq(0).html(html);
        currentEditingRow.find("td").eq(2).html(data.specification);
        currentEditingRow.find("td").eq(3).html(data.model_no);
        currentEditingRow.find("td").eq(4).html(data.hsn_code);

                //currentEditingRow.html(html);
        $("#exampleModal").modal("hide");
    })


});
function editItemRow(){
    debugger;
    $(".edit").on("click",function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        $("#addItems").hide();
        $("#editItem").show();
        
        var row = $(this).closest("tr");
        rowEditIndex = row.index();
        currentEditingRow = row;
        $("#description").val(row.find(".i_description").val());
        $("#specification").val(row.find(".i_specification").val());
        $("#color").val(row.find(".i_color").val());
        $("#model_no").val(row.find(".i_model_no").val());
        $("#brand_id").val(row.find(".i_brand_id").val());
        $("#hsn_code").val(row.find(".i_hsn_code").val());
        $("#unit").val(row.find(".i_unit").val());
        $("#qty").val(row.find(".i_qty").val());
        $("#remarks").val(row.find(".i_remarks").val());
        $("#image").val();
        $("#exampleModal").modal("show");
            
    })
}
$('#product_list_cart').click(function(){
    var tr = $(this).closest('tr'),
        del_id = $(this).attr('data-id');

    $.ajax({
        url: "/admin/query/delete/"+ del_id,
        cache: false,
        success:function(result){
            tr.fadeOut(1000, function(){
                $(this).remove();
            });
        }
    });
});

function resetModalWindow(){
    $("#product_id").val("");
    $("#cat").val("");
    $("#subcat").val("");
    $("#item").val("");
            $("#description").val("");
            $("#specification").val("");
            $("#color").val("");
            $("#model_no").val("");
            $("#brand_id").val("");
            $("#hsn_code").val("");
            $("#unit").val("");
            $("#qty").val("");
            $("#remarks").val("");
            $("#image").val("");
            $('.checkimage').val('');
            $("#fileinput").val("")
            $(".image-hover").hide();
            
}
$('.close').on('click',function(e)
{
    $(".modal-backdrop").hide();
})
$('#addItems').on('click',function(e)
{
    
    $(".modal-backdrop").hide();
})

$('.delete').on('click',function(e){
    e.stopImmediatePropagation();
            $(this).closest("tr").remove();
})
editItemRow();




