var sendAjax = function(url,type,data,header={}){
    return new Promise(function(resolve, reject) {
         jQuery.ajax({
                url: url,
                type: type,
                dataType: "json",
                data:data,
                headers: header,
                contentType: "application/x-www-form-urlencoded; charset=utf-8",
                success: function (data) {
                    resolve(data);
                },
                error: {},
         });
    });
}