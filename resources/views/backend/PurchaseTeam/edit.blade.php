@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.colors.purchase') . ' | ' . __('labels.backend.access.colors.createpurchase'))

@section('breadcrumb-links')
@endsection

@section('content')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Make Order
                            <small class="text-muted">Fill information </small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

          <div class="row mt-4">
            <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                    <th>M/L Purchased</th>
                    <th>M/L Recieved</th>
                    <th>Purchase Receipt </th>
                    <th>Qty</th>
                    <th>Pending Qty</th>
                        <th>Payment Mode</th>
                        <th >Remark</th>
                    </tr>
                    </thead>
                    <tr>
                    <td><input type="checkbox" name="ml_purchased"  <?php if($data->ml_purchased==1){ echo " checked=\"checked\""; } ?> value=""></td>
                    @if($data->purchasedOrders[0]->ml_recieved =='0')
                                <td><input type="button" value="Not Recieved" class="btn btn-info btn-sm"></td>
                                @elseif($data->purchasedOrders[0]->ml_recieved  =='1')
                                <td><input type="button" value="Recieved" class="btn btn-warning btn-sm"></td>       
                                @else
                                <td><Button class="btn btn-danger">Not Recieved</Button></td>    
                                @endif
                                @if($data->purchasedOrders[0]->recieved_doc =='0')
<td><input type="button" value="Not Recieved" class="btn btn-info btn-sm"></td>
@elseif($data->purchasedOrders[0]->recieved_doc =='1')
<td><input type="button" value="Recieved" class="btn btn-success btn-sm"></td> 
@else
<td><input type="button" value="Not Recieved" class="btn btn-info btn-sm"></td>
@endif
                    <td><input type="text" class="form-control qty" data-val="{{$data->qty}}" value='{{$data->qty}}' style="width: 50%!important;"></td>
                    <td><input type="text" class="form-control pending_qty" value='{{$data->purchasedOrders[0]->pending_qty}}' style="width: 50%!important;"></td>
                    <td>
                    <input type="radio" name="payment_mode" value="Cash" <?php echo ($data->payment_mode=='Cash')?'checked':'' ?>> Cash<br>
                    <input type="radio" name="payment_mode" value="Bill" <?php echo ($data->payment_mode=='Bill')?'checked':'' ?>> Bill<br>
                    <input type="radio" name="payment_mode" value="Challan" <?php echo ($data->payment_mode=='Challan')?'checked':'' ?>> Challan  
                    </td>
                    <td><input type="text" class="form-control remarks" value='' style="width: 300px"></td>
                    <input type="hidden" value="<?=$id?>" name="poregisterid">
                    </tr>
                    </table>
                    </div>
</div>
 </div> 
                     
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.purchase.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        <input class="btn btn-success btn-sm" id="sub" type="button"  value="Submit">
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection