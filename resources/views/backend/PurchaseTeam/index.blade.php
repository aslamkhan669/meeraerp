@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.quotation.management'))



@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                   Your Purchase Orders <small class="text-muted">Active Orders</small>
                </h4>
            </div><!--col-->

     
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>{{ __('labels.general.actions') }}</th>
                        <th>Status</th>
                        <th>Pending/Non Pending</th>
                            <th>Description</th>
                            <th>MIN PO NO</th>
                            <th>Supplier Name</th>
                            <th>Supplier Address/Contacts</th>
                            <th>Image</th>
                            <th>Day Shift</th>
                            <th>Priority</th>
                            <th>Qty</th>
                            <th>Transportation</th>
                            <th>Payment Amount</th>
                            <th>Payment Mode</th>
                            <th>Bank</th>
                        </tr>
                        </thead>
                        <tbody>
                       @foreach($purchasedata as $data)
                       <tr>
                       <td> <span><a class="btn btn-sm btn-success" href="{{ url('admin/purchase/edit/') }}/<?=$data->id?>">INPUT</a></span></td>
                       @if(isset($data))
                       @if ($data->purchasedOrders[0]->ml_purchased == '1')
                       <td><Button class="btn btn-danger">Purchased</Button></td>
                     @elseif ($data->purchasedOrders[0]->ml_purchased == '0')
                <td><Button class="btn btn-success">Purchased</Button></td> 
                 @else
               <td><Button class="btn btn-success">Non Purchased</Button></td> 
                     @endif      
                 @else
                       <td><Button class="btn btn-success">Non Purchased</Button></td>    
                   @endif
                       @if(isset($data))
                       @if ($data->purchasedOrders[0]->pending_qty == '1')
                       <td><Button class="btn btn-danger">Pending</Button></td>
                     @elseif ($data->purchasedOrders[0]->pending_qty > '1')
                <td><Button class="btn btn-danger">Pending</Button></td> 
                 @else
               <td><Button class="btn btn-success">Non Pending</Button></td> 
                     @endif      
                 @else
                       <td><Button class="btn btn-success">Non Pending</Button></td>    
                   @endif
                        <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{$data->poItem->queryItem->item_id}}" data-toggle="modal" data-target="#myModal">View</button></td>
                        <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$data->poItem->poFormatting->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$data->poItem->poFormatting->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$data->poItem->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$data->poItem->po_id),-2)}}</td>
 
                       <td>{{$data->poItem->queryitem->priceMappings[0]->supplier->supplier_name}}</td>
                       <td>{{$data->poItem->queryitem->priceMappings[0]->supplier->landmark}}, {{$data->poItem->queryitem->priceMappings[0]->supplier->street}}, {{$data->poItem->queryitem->priceMappings[0]->supplier->city }}, {{$data->poItem->queryitem->priceMappings[0]->supplier->state }}/{{$data->poItem->queryitem->priceMappings[0]->supplier->mobile}}, {{$data->poItem->queryitem->priceMappings[0]->supplier->landline}}</td>
                       <td class="image-hover"><img src="{{url($data->poItem->queryItem->itemList->image)}}" width="50px" height="50px"/></td>                                                                                                                        
                       @if(isset($data->day_shift))
                                @if($data->day_shift =='0')
                                <td><Button class="btn btn-info">First Half</Button></td>
                                @elseif($data->day_shift =='1')
                                <td><Button class="btn btn-success">Second Half</Button></td>         
                                @endif
                                @else
                                <td><Button class="btn btn-danger">Not Priced</Button></td>    
                            @endif
                       
                       @if(isset($data->priority))
                                @if($data->priority =='0')
                                <td><Button class="btn btn-info">Low</Button></td>
                                @elseif($data->priority =='1')
                                <td><Button class="btn btn-warning">Medium</Button></td> 
                                @elseif($data->priority =='2')
                                <td><Button class="btn btn-danger">High</Button></td>        
                                @endif
                                @else
                                <td><Button class="btn btn-danger">High</Button></td>    
                            @endif
                        <td>{{ $data->qty }}</td>
                        <td>{{ $data->transportation_mode }}</td>
                        <td>{{ $data->payment_amount }}</td>
                        <td>{{ $data->payment_mode }}</td>
                        <td>{{ $data->bank }}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
