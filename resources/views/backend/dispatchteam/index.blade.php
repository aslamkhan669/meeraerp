@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.quotation.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                   Your Dispatch Orders <small class="text-muted">Active Orders</small>
                </h4>
            </div><!--col-->

     
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>{{ __('labels.general.actions') }}</th>
                        <th>Status</th>
                        <th>Reject/Non Reject</th>
                        <th>Company Name</th>
                        <th>PO No</th>
                        <th>Client PO No</th>
                        <th>Contact&Address</th>
                        <th>Dispatch date</th>
                        <th>On Bill/Challan</th>
                        <th>Bill/Challan Number</th>
                        <th>PO Items Purchaser</th>
                        <th>PO Items Reciever</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $dis)

                       <tr>
                       <td><a class="btn btn-sm btn-info" href="{{ url('admin/dispatch/edit/') }}/<?=$dis->dt_user_id?>/<?=$dis->purchase_date?>">Input</a></td>
                       @if(isset($dis->dispatchedOrder[0]))
                       @if ($dis->dispatchedOrder[0]->ml_purchased == 1)
                       <td><Button class="btn btn-success">Dispatch</Button></td>
@elseif ($dis->dispatchedOrder[0]->ml_purchased == 0)
<td><Button class="btn btn-danger">Not Dispatch</Button></td> 
@endif
@else
<td><Button class="btn btn-danger">Not Dispatch</Button></td>    
@endif
                    @if(isset($dis->dispatchedOrder[0]))
                       @if ($dis->dispatchedOrder[0]->pending_qty == '1')
                       <td><Button class="btn btn-danger">Reject</Button></td>
                     @elseif ($dis->dispatchedOrder[0]->pending_qty > '1')
                <td><Button class="btn btn-danger">Reject</Button></td> 
                 @else
               <td><Button class="btn btn-success">Non Reject</Button></td> 
                     @endif      
                 @else
               <td><Button class="btn btn-success">Non Reject</Button></td> 
                   @endif
                       <td>{{ $dis->poItem->poFormatting->quotation->queries->client->client_name}}</td>
                       <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$dis->poItem->poFormatting->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$dis->poItem->poFormatting->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$dis->poItem->poFormatting->quotation->queries->client->created_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$dis->poItem->poFormatting->id),-2)}}</td>
                       <td>{{ $dis->poItem->poFormatting->c_p_owner_no }}</td>
                       <td>{{ $dis->poItem->poFormatting->quotation->queries->client->contact_number1}}/{{ $dis->poItem->poFormatting->quotation->queries->client->street }},{{ $dis->poItem->poFormatting->quotation->queries->client->sector }},{{ $dis->poItem->poFormatting->quotation->queries->client->city }},{{ $dis->poItem->poFormatting->quotation->queries->client->state}}</td>
                       <td>{{ $dis->purchase_date}}</td>   
                       <td>{{ isset($dis->dispatchedOrder[0]->bill_challan) ? $dis->dispatchedOrder[0]->bill_challan : '' }}</td>
                       <td>{{ isset($dis->dispatchedOrder[0]->bill_challan_no) ? $dis->dispatchedOrder[0]->bill_challan_no : '' }}</td>  
                        <td>{{ $dis->poItem->poFormatting->c_purchaser_name }}</td>
                        <td>{{ isset($dis->dispatchedOrder[0]->poitem_reciever) ? $dis->dispatchedOrder[0]->poitem_reciever : '' }}</td>  
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
