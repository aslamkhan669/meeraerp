@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.colors.purchase') . ' | ' . __('labels.backend.access.colors.createpurchase'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width: 900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Make Order
                            <small class="text-muted">Fill information </small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

          <div class="row mt-4">
            <div class="col-md-12">
                       <div class="table-responsive" >
                       <table class="table">
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>image</th>
                        <th>Day Shift</th>
                        <th>Mode of Travel</th>
                        <th>Priority</th>
                        <th>Receipt Slip</th>
                        <th>Material Recieved</th>
                        <th>Qty</th>
                        <th>Reject Qty</th>
                        <th>Remarks</th>

                    </tr>
                    </thead>
                    <tbody  class="data">
                    @foreach ($data as $po)
                  
                    <tr>
       
                 <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{ $po->poItem->queryItem->item_id }}" data-toggle="modal" data-target="#myModal">View</button></td>
                 <td class="image-hover"><img src="{{url( $po->poItem->queryItem->itemList->image)}}" width="50px" height="50px"/></td>   

                 @if(isset($po->day_shift))
                                @if($po->day_shift =='0')
                                <td><Button class="btn btn-info">First Half</Button></td>
                                @elseif($po->day_shift =='1')
                                <td><Button class="btn btn-success">Second Half</Button></td>         
                                @endif
                                @else
                                <td><Button class="btn btn-danger">Not Priced</Button></td>    
                            @endif
                 <td><input type="text" class="form-control transportation"  value="{{$po->transportation_mode}}" style="width: 100px;" readonly></td>

                                @if($po->priority =='0')
                                <td><input type="button" value="Low" class="btn btn-info btn-sm"></td>
                                @elseif($po->priority =='1')
                                <td><input type="button" value="Medium" class="btn btn-warning btn-sm"></td> 
                                @elseif($po->priority =='2')
                                <td><input type="button" value="High" class="btn btn-danger btn-sm"></td>        
                                @else
                                <td><Button class="btn btn-danger">High</Button></td>    
                                @endif

                       </td>

@if($po->dispatchedOrder[0]->recieved_doc =='0')
<td><input type="button" value="Not Recieved" class="btn btn-info btn-sm"></td>
@elseif($po->dispatchedOrder[0]->recieved_doc =='1')
<td><input type="button" value="Recieved" class="btn btn-success btn-sm"></td> 
@else
<td><input type="button" value="Not Recieved" class="btn btn-info btn-sm"></td>
@endif

</td>
<td><input type="checkbox" class="form-control material_recieved" name="material_recieved"  value=""></td>

<td><input type="text" class="form-control qty"  value="{{$po->dt_qty}}" style="width: 50px;" ></td>
<td><input type="text" class="form-control rejectqty" value="{{$po->dispatchedOrder[0]->pending_qty}}" style="width: 50px;" ></td>
<td><input type="text" class="form-control remarks"   style="width: 250px;" ></td>

<input type="hidden" class="form-control id" value="{{$po->dispatchedOrder[0]->id}}"  >


                    </tr>
                    @endforeach
                    </tbody>

                    </table>

                    </div>
</div>
 </div> 
       
                    </div><!--col-->

 <div class="form-group row">
                <label class="col-md-2 form-control-label" for="lab"></label>
                <h4>Total Amount</h4>

                            <div class="col-md-2">

                                <input type="text" class="form-control"  value="{{$totalamount[0]->total}}" readonly>
                            </div><!--col-->
                            <h4>E-wayBill</h4>
                            <div class="col-md-2">
                            <input type="text" class="form-control"  value=" {{$totalamount[0]->total >=50000?'Required':'Not Required'}}" readonly>
                            </div> 
                            <div class="col-md-8">
                            </div> 

                        </div><!--form-group-->

                </div><!--row-->
              
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.dispatch.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    <input class="btn btn-success btn-sm" id="sub" data-id="" type="button"  value="Submit">

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection