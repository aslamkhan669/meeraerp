@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.dailyexpenses.management') )

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($dailyexpense, 'PATCH', route('admin.dailyexpenses.update', $dailyexpense->id))->class('form-horizontal')->open() }}
<div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="module" type="hidden" value="PurchaseManagement">
                    <input name="action" id="action" type="hidden" value="DailyExpenses">
                    <input name="activity" id="activity" type="hidden" value="update">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                         Daily Expenses Register
                            <small class="text-muted">Update Daily Expenses</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                    <div class="col">
                </div><!--col-->
                        </div><!--form-group-->
                        <section>
                        <div class="form-group table-responsive">
                        <table class="table table-bordered table-striped ">
  <thead>
    <tr>
      <th scope="col">Tea</th>
      <th scope="col">Qty</th>
      <th scope="col">Special</th>
      <th scope="col">Normal</th>
      <th scope="col">SP.Rate</th>
      <th scope="col">NL.Rate</th>
      <th scope="col">Total</th>
    </tr>
  </thead>
  <tbody class="data">
    <tr>
    <input name="id" class="tid" type="hidden" value="{{$teaexp[0]->id}}">
    <input name="id" class="tid" type="hidden" value="{{$teaexp[1]->id}}">
    <input name="id" class="tid" type="hidden" value="{{$teaexp[2]->id}}">

      <th scope="row" class="timing" data-type="morningtea">Morning Tea</th>
      <td><input type="input" name="qty[]" value="{{$teaexp[0]->qty}}"  class="form-control qty" style="width: 65px;"></td>
      <td><input type="checkbox" class="form-control special" name="special[]" <?php if($teaexp[0]->special==1){ echo " checked=\"checked\""; } ?> ></td> 
      <td><input type="checkbox" class="form-control normal" name="normal[]" <?php if($teaexp[0]->normal==1){ echo " checked=\"checked\""; } ?> ></td>           
      <td><input type="input" name="sp_rate[]" value="{{$teaexp[0]->sp_rate}}" class="form-control sprate rate" style="width: 65px;"></td>
      <td><input type="input" name="ml_rate[]" value="{{$teaexp[0]->ml_rate}}" class="form-control mlrate rate" style="width: 65px;"></td>
      <td><input type="input" name="total[]" value="{{$teaexp[0]->total}}" class="form-control total ftotal" style="width: 65px;"></td>
    </tr>
    <tr>
      <th scope="row" class="timing" data-type="eveningtea">Evening Tea</th>
      <td><input type="input" name="qty[]" value="{{$teaexp[1]->qty}}" class="form-control qty" style="width: 65px;"></td>
      <td><input type="checkbox" class="form-control special" name="special[]" <?php if($teaexp[1]->special==1){ echo " checked=\"checked\""; } ?> ></td> 
      <td><input type="checkbox" class="form-control normal" name="normal[]"<?php if($teaexp[1]->normal==1){ echo " checked=\"checked\""; } ?> ></td> 
      <td><input type="input" name="sp_rate[]" value="{{$teaexp[1]->sp_rate}}" class="form-control sprate rate" style="width: 65px;"></td>
      <td><input type="input" name="ml_rate[]" value="{{$teaexp[1]->ml_rate}}" class="form-control mlrate rate" style="width: 65px;"></td>
      <td><input type="input" name="total[]" value="{{$teaexp[1]->total}}" class="form-control total ftotal" style="width: 65px;"></td>
    </tr>
    <tr>
      <th scope="row" class="timing" data-type="guesttea">Guest Tea</th>
      <td><input type="input" name="qty[]" value="{{$teaexp[2]->qty}}"  class="form-control qty" style="width: 65px;"></td>
      <td><input type="checkbox" class="form-control special" name="special[]" <?php if($teaexp[2]->special==1){ echo " checked=\"checked\""; } ?> ></td> 
      <td><input type="checkbox" class="form-control normal" name="normal[]" <?php if($teaexp[2]->normal==1){ echo " checked=\"checked\""; } ?> ></td> 
      <td><input type="input" name="sp_rate[]" value="{{$teaexp[2]->sp_rate}}"  class="form-control sprate rate" style="width: 65px;"></td>
      <td><input type="input" name="ml_rate[]" value="{{$teaexp[2]->ml_rate}}"  class="form-control mlrate rate" style="width: 65px;"></td>
      <td><input type="input" name="total[]" value="{{$teaexp[2]->total}}"  class="form-control total ftotal" style="width: 65px;"></td>
    </tr>
  </tbody>
</table>

                    </div><!--col-->
                  </section>
                  <section>
                    <div class="form-group table-responsive">
                    <table class="table table-bordered table-striped  ">
  <thead>
    <tr>
      <th scope="col">Water</th>
      <th scope="col">Qty</th>
      <th scope="col">Rate</th>
      <th scope="col">Total</th>
    </tr>
  </thead>
  <tbody class="water">
    <tr>
    <input name="id" class="wid" type="hidden" value="{{$waterexp[0]->id}}">

      <th scope="row" class="type" data-type="waterbottle">Water Bottle</th>
      <td><input type="input" name="qty[]" value="{{$waterexp[0]->qty}}"  class="form-control waterqty " style="width: 65px;"></td>
      <td><input type="input" name="rate[]" value="{{$waterexp[0]->rate}}" class="form-control waterrate" style="width: 65px;"></td>
      <td><input type="input" name="total[]" value="{{$waterexp[0]->total}}" class="form-control watertotal" style="width: 65px;"></td>
    </tr>
  </tbody>
</table>
                    </div><!--col-->
                  </section>
                    <div class="row">


<script src="https://code.jquery.com/jquery-latest.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<section class="container">
<div class=" table-responsive">
  <h5>Stationery</h5>
<table class="table table-striped table-bordered">
<thead>
	<tr>
    <th>Item Name</th>
        <th>Qty</th>
    	<th>Price</th>
        <th>Total</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer" class="stationery">
@foreach ($stationexp as $data)
<tr>
<input name="id" class="statid" type="hidden" value="{{$data->id}}">
<td><input name = "item_name[]" type="text" value = "{{$data->item_name}}" class="form-control staitemname" /></td><td><input name = "qty[]" type="text" value = "{{$data->qty}}" class="form-control staqty" /></td><td><input name = "price[]" type="text" value = "{{$data->price}}" class="form-control staprice" /></td><td> <input name="total[]" type="text" value="{{$data->total}}" class="form-control statotal"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove stationerydelete"><i class="fa fa-trash"></i></button></td>
</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="5">
    <button id="editbtnAdd" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-plus"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>


<section class="container">
<div class=" table-responsive">
    <h5>IT&Phone</h5>
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    	<th>Mobile No</th>
    	<th>User Code</th>
    	<th>Recharge Date</th>
    	<th>Plan Duration</th>
        <th>Due Date</th>
        <th>Plan Value</th>
    	<th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer1" class="phone">
@foreach ($phoneexp as $data)
<tr>
<input name="id" class="phoneid" type="hidden" value="{{$data->id}}">
<td><input name = "mobile[]" type="text" value = "{{$data->mobile}}" class="form-control Pmobile" /></td><td><input name = "user_code[]" type="text" value = "{{$data->user_code}}" class="form-control pusercode" /></td><td><input name = "recharge_date[]" type="date" value = "{{$data->recharge_date}}" class="form-control precharge" /></td><td><input name = "plan_duration[]" type="text" value = "{{$data->plan_duration}}" class="form-control pplan" /></td><td><input name = "due_date[]" type="date" value = "{{$data->due_date}}" class="form-control pdue" /></td><td> <input name="plan_value[]" type="text" value="{{$data->plan_value}}" class="form-control pplanvalue"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove itphonedelete"><i class="fa fa-trash"></i></button></td>
</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="7">
    <button id="editbtnAdd1" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-plus"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>

<section class="container">
<div class="table-responsive">
  <h5>Salary Expenses</h5>
<table class="table table-striped table-bordered">
<thead>
	<tr>
    	<th>DOI</th>
    	<th>TmpCode</th>
    	<th>Name</th>
    	<th>Role</th>
      <th>Salary</th>
    	<th>MOP</th>
    	<th>DOP</th>
    	<th>PaidAmt</th>
    	<th>DueAmt</th>
    	<th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer2" class="salary">
@foreach ($salaryexp as $data)
<tr>
<input name="id" class="salaryid" type="hidden" value="{{$data->id}}">
<td><input name = "doi[]" type="date" value = "{{$data->doi}}" class="form-control salarydoi" /></td><td><input name = "tmp_code[]" type="text" style="width: 120px;" value = "{{$data->tmp_code}}" class="form-control salarytmp" /></td><td><input name = "name[]" type="text" style="width: 120px;" value = "{{$data->name}}" class="form-control salaryname" /></td><td><input name = "role[]" type="text" value = "{{$data->role}}" style="width: 120px;" class="form-control salaryrole" /></td><td><input name = "salary[]" type="text" value = "{{$data->salary}}" style="width: 120px;" class="form-control sal" /></td><td><input name = "mop[]" type="text" value = "{{$data->mop}}" style="width: 120px;" class="form-control salarymop" /></td><td><input name = "dop[]" type="date" value = "{{$data->dop}}"  class="form-control salarydop" /></td><td><input name = "paid_amt[]" type="text" value = "{{$data->paid_amt}}" style="width: 120px;" class="form-control salarypaid" /></td><td> <input name="due_amt[]" type="text" value="{{$data->due_amt}}" style="width: 120px;" class="form-control salarydue"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove salarydelete"><i class="fa fa-trash"></i></button></td>
</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="10">
    <button id="editbtnAdd2" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-sign"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
<section>
<div class="table-responsive">
<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col">Other Expenses</th>
      <th scope="col">DOP</th>
      <th scope="col">Duration</th>
      <th scope="col">Amount</th>
      <th scope="col">Paid Amt</th>
      <th scope="col">Due Amt</th>
      <th scope="col">Remark</th>
    </tr>
  </thead>
  <tbody class="other">
    <tr>
    <input name="id" class="oid" type="hidden" value="{{$otherexp[0]->id}}">
    <input name="id" class="oid" type="hidden" value="{{$otherexp[1]->id}}">
    <input name="id" class="oid" type="hidden" value="{{$otherexp[2]->id}}">
    <input name="id" class="oid" type="hidden" value="{{$otherexp[3]->id}}">
      <th scope="row" class="types" data-type="electricity">Electricity</th>
      <td><input type="date" name="dop[]" value="{{$otherexp[0]->dop}}" class="form-control otherdop" style="width: 200px;"></td>
      <td><input type="input" name="duration[]" value="{{$otherexp[0]->duration}}" class="form-control otherduration" style="width: 100px;"></td>
      <td><input type="input" name="amount[]" value="{{$otherexp[0]->amount}}" class="form-control otheramount" style="width: 100px;"></td>
      <td><input type="input" name="paid_amt[]" value="{{$otherexp[0]->paid_amount}}" class="form-control otherpaid" style="width: 100px;"></td>
      <td><input type="input" name="dua_amt[]" value="{{$otherexp[0]->due_amt}}"  class="form-control otherdue" style="width: 100px;"></td>
      <td><input type="input" name="remark[]" value="{{$otherexp[0]->remark}}"  class="form-control otherremark" style="width: 100px;"></td>
    </tr>
    <tr>
      <th scope="row" class="types" data-type="cleaning">Cleaning</th>
      <td><input type="date" name="dop[]" value="{{$otherexp[1]->dop}}"  class="form-control otherdop" style="width: 200px;"></td>
      <td><input type="input" name="duration[]" value="{{$otherexp[1]->duration}}" class="form-control otherduration" style="width: 100px;"></td>
      <td><input type="input" name="amount[]" value="{{$otherexp[1]->amount}}"  class="form-control otheramount" style="width: 100px;"></td>
      <td><input type="input" name="paid_amt[]" value="{{$otherexp[1]->paid_amount}}" class="form-control otherpaid" style="width: 100px;"></td>
      <td><input type="input" name="dua_amt[]" value="{{$otherexp[1]->due_amt}}" class="form-control otherdue" style="width: 100px;"></td>
      <td><input type="input" name="remark[]" value="{{$otherexp[1]->remark}}"  class="form-control otherremark" style="width: 100px;"></td>
    </tr>
    <tr>
      <th scope="row" class="types" data-type="rent&lease">Rent&Lease</th>
      <td><input type="date" name="dop[]" value="{{$otherexp[2]->dop}}"  class="form-control otherdop" style="width: 200px;"></td>
      <td><input type="input" name="duration[]" value="{{$otherexp[2]->duration}}" class="form-control otherduration" style="width: 100px;"></td>
      <td><input type="input" name="amount[]" value="{{$otherexp[2]->amount}}" class="form-control otheramount" style="width: 100px;"></td>
      <td><input type="input" name="paid_amt[]" value="{{$otherexp[2]->paid_amount}}"  class="form-control otherpaid" style="width: 100px;"></td>
      <td><input type="input" name="dua_amt[]" value="{{$otherexp[2]->due_amt}}" class="form-control otherdue" style="width: 100px;"></td>
      <td><input type="input" name="remark[]" value="{{$otherexp[2]->remark}}" class="form-control otherremark" style="width: 100px;"></td>
    </tr>
    <tr>
      <th scope="row" class="types" data-type="legalexpenses">Legal Expenses</th>
      <td><input type="date" name="dop[]" value="{{$otherexp[3]->dop}}" class="form-control otherdop" style="width: 200px;"></td>
      <td><input type="input" name="duration[]" value="{{$otherexp[3]->duration}}"  class="form-control otherduration" style="width: 100px;"></td>
      <td><input type="input" name="amount[]" value="{{$otherexp[3]->amount}}" class="form-control otheramount" style="width: 100px;"></td>
      <td><input type="input" name="paid_amt[]" value="{{$otherexp[3]->paid_amount}}" class="form-control otherpaid" style="width: 100px;"></td>
      <td><input type="input" name="dua_amt[]" value="{{$otherexp[3]->due_amt}}" class="form-control otherdue" style="width: 100px;"></td>
      <td><input type="input" name="remark[]" value="{{$otherexp[3]->remark}}" class="form-control otherremark" style="width: 100px;"></td>
    </tr>
  </tbody>
</table>
</div>
</section>


<section>
<div class="table-responsive">
  <h5>Client Visit</h5>
<table class="table table-striped table-bordered">
<thead>
	<tr>
    <th>Emp Code</th>
        <th>Client</th>
    	<th>Amount Paid</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer3" class="client">
@foreach ($clientexp as $data)
<tr>
<input name="id" class="cid" type="hidden" value="{{$data->id}}">
<td><input name = "emp_code[]" type="text" value = "{{$data->emp_code}}" class="form-control clientemp" /></td><td><input name = "client[]" type="text" value = "{{$data->client}}" class="form-control cli" /></td><td><input name = "amt_paid[]" type="text" value = "{{$data->amt_paid}}" class="form-control clientpaid" /></td><td><input name = "amt_spend[]" type="text" value = "{{$data->amt_spend}}" class="form-control clientspend" /></td><td> <input name="amt_return[]" type="text" value="{{$data->amt_return}}" class="form-control clientreturn"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove clientdelete"><i class="fa fa-trash"></i></button></td>
</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="6">
    <button id="editbtnAdd3" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-plus"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>

<section>
<div class="table-responsive">
  <h5>Supplier Visit</h5>
<table class="table table-striped table-bordered">
<thead>
	<tr>
    <th>Emp Code</th>
        <th>Supplier</th>
    	<th>Amount Paid</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer4" class="supplier">
@foreach ($supplierexp as $data)
<tr>
<input name="id" class="suppid" type="hidden" value="{{$data->id}}">
<td><input name = "emp_code[]" type="text" value = "{{$data->emp_code}}" class="form-control supplieremp" /></td><td><input name = "supplier[]" type="text" value = "{{$data->supplier}}" class="form-control supp" /></td><td><input name = "amt_paid[]" type="text" value = "{{$data->amt_paid}}" class="form-control supplierpaid" /></td><td><input name = "amt_spend[]" type="text" value = "{{$data->amt_spend}}" class="form-control supplierspend" /></td><td> <input name="amt_return[]" type="text" value="{{$data->amt_return}}" class="form-control supplierreturn"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove supplierdelete"><i class="fa fa-trash"></i></button></td>
</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="6">
    <button id="editbtnAdd4" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-plus"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>

<section>
<div class="table-responsive">
  <h5>Purchase Expenses</h5>
<table class="table table-striped table-bordered">
<thead>
	<tr>
    <th>Purchase Person</th>
        <th>Amt Recieved</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Mode Of Payment</th>
        <th>for what</th>
        <th>Remarks</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer5" class="purchase">
@foreach ($purchaseexp as $data)
<tr>
<input name="id" class="purid" type="hidden" value="{{$data->id}}">
<td><input name = "purchase_person[]" type="text" value = "{{$data->purchase_person}}" style="width: 150px;" class="form-control purchaseperson" /></td><td><input name = "amt_recieved[]" type="text" value = "{{$data->amt_recieved}}" style="width: 100px;" class="form-control purchaserecieved" /></td><td><input name = "amt_spend[]" type="text" value = "{{$data->amt_spent}}" style="width: 100px;" class="form-control purchasespend" /></td><td> <input name="amt_return[]" type="text" value="{{$data->amt_returned}}" style="width: 100px;" class="form-control purchasereturn"> </td><td><input name="payment_mode[]" type="text" value="{{$data->mode_payment}}" style="width: 100px;" class="form-control purchasemode"> </td><td> <input name="for_what[]" type="text" value="{{$data->for_what}}" style="width: 100px;" class="form-control purchasewhat"> </td><td> <input name="remark[]" type="text" value="{{$data->remarks}}" style="width: 200px;" class="form-control purchaseremark"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove purchasedelete"><i class="fa fa-trash"></i></button></td>
</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="8">
    <button id="editbtnAdd5" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-plus"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>

<section>
  <div class="table-responsive">
  <h5>Dispatch Expenses</h5>
<table class="table  table-striped table-bordered">
<thead>
	<tr>
    <th>Dispatch Person</th>
        <th>Amt Recieved</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Mode Of Payment</th>
        <th>for what</th>
        <th>Remarks</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer6" class="dispatch">
@foreach ($dispatchexp as $data)
<tr>
<input name="id" class="disid" type="hidden" value="{{$data->id}}">
<td><input name = "dispatch_person[]" type="text" value = "{{$data->dispatch_person}}" style="width: 150px;" class="form-control dispatchperson" /></td><td><input name = "amt_recieved[]" type="text" value = "{{$data->amt_recieved}}" style="width: 100px;" class="form-control dispatchrecieved" /></td><td><input name = "amt_spend[]" type="text" value = "{{$data->amt_spent}}" style="width: 100px;" class="form-control dispatchspend" /></td><td> <input name="amt_return[]" type="text" value="{{$data->amt_returned}}" style="width: 100px;" class="form-control dispatchreturn"> </td><td> <input name="payment_mode[]" type="text" value="{{$data->mode_payment}}" style="width: 100px;" class="form-control dispatchmode"> </td><td> <input name="for_what[]" type="text" value="{{$data->for_what}}" style="width: 100px;" class="form-control dispatchwhat"> </td><td> <input name="remark[]" type="text" value="{{$data->remarks}}" style="width: 200px;" class="form-control dispatchremark"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove dispatchdelete"><i class="fa fa-trash"></i></button></td>

</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="8">
    <button id="editbtnAdd6" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-plus"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>

<section>
<div class="table-responsive">
  <h5>Vehicle Expenses</h5>
<table class="table table-striped table-bordered">
<thead>
	<tr>
    <th> Person</th>
        <th>After Reading</th>
        <th>Before Reading</th>
        <th>Defference</th>
        <th>Unit Rate</th>
        <th>Amount</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer7" class="vehicle">
@foreach ($vehicleexp as $data)
<tr>
<input name="id" class="vid" type="hidden" value="{{$data->id}}">
<td><input name = "person[]" type="text" value = "{{$data->person}}" class="form-control vehicleperson" /></td><td><input name = "after_reading[]" type="text" value = "{{$data->after_reading}}" class="form-control vehicleafter" /></td><td><input name = "before_reading[]" type="text" value = "{{$data->before_reading}}" class="form-control vehiclebefore" /></td><td> <input name="difference[]" type="text" value="{{$data->difference}}" class="form-control vehicledifference"> </td><td> <input name="unit_rate" type="text" value="{{$data->unit_rate}}" class="form-control vehicleunit"> </td><td> <input name="amount[]" type="text" value="{{$data->amount}}" class="form-control vehicleamount"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove vehicledelete"><i class="fa fa-trash"></i></button></td>
</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="7">
    <button id="editbtnAdd7" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-plus"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>

<section>
<div class="table-responsive">
  <h5>Miscellanceses Expenses</h5>
<table class="table table-striped table-bordered">
<thead>
	<tr>
    <th>Person</th>
        <th>Amt Recieved</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Mode Of Payment</th>
        <th>for what</th>
        <th>Remarks</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="editTextBoxContainer8" class="misc">
@foreach ($miscexp as $data)
<tr>
<input name="id" class="miscid" type="hidden" value="{{$data->id}}">
<td><input name = "person[]" type="text" value = "{{$data->person}}" style="width: 150px;" class="form-control miscperson" /></td><td><input name = "amt_recieved[]" type="text" value = "{{$data->amt_recieved}}" style="width: 100px;" class="form-control miscrecieved" /></td><td><input name = "amt_spend[]" type="text" value = "{{$data->amt_spent}}" style="width: 100px;" class="form-control miscspend" /></td><td> <input name="amt_return[]" type="text" value="{{$data->amt_returned}}" style="width: 100px;" class="form-control miscreturn"> </td><td> <input name="payment_mode[]" type="text" value="{{$data->mode_payment}}" style="width: 100px;" class="form-control miscmode"> </td><td> <input name="for_what[]" type="text" value="{{$data->for_what}}" class="form-control miscwhat"> </td><td> <input name="remark[]" type="text" value="{{$data->remarks}}" style="width: 200px;" class="form-control miscremark"> </td><td><button type="button" data-id="{{$data->id}}" class="btn btn-danger remove miscdelete"><i class="fa fa-trash"></i></button></td>
</tr>
@endforeach
</tbody>
<tfoot>
  <tr>
    <th colspan="8">
    <button id="editbtnAdd8" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="fa fa-plus"></i>&nbsp; Add</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.dailyexpenses.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    <button type="button" data-id="{{$dailyexpense->id}}" id="sendedit" class="btn btn-success">Send</button>
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->

    {{ html()->form()->close() }}
@endsection