@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.dailyexpenses.management') )



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.dailyexpenses.management') }} <small class="text-muted"></small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.dailyexpenses.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->
        <form action="searchdaily" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Date&Time" style="width: 20%; display: inline;">
    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                <div class="form-group row">
                <label class="col-md-2 form-control-label" for="lab"></label>

                            <div class="col-md-2">

                            </div><!--col-->
                            <div class="col-md-6"></div>                           
                        </div><!--form-group-->
                    <table class="table">
                        <thead>
                        <tr>
                            
                            <th>Date&Time</th>
                            <th>Days</th>
                            <th>office Expenses</th>
                            <th>{{ __('labels.general.actions') }}</th>
                            <th>EXCEL</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($dailys as $data)
                            <tr>
                            <td>{{$sa=substr(strtoupper($data->updated_at),0,-3)}}</td> 
                         <td>{{ Carbon\Carbon::parse($data->updated_at)->format('D') }}</td>
                         <td>{{$data->teas->sum('total')+$data->waters->sum('total')+$data->stationeries->sum('total')+$data->itPhones->sum('plan_value')+$data->salaryExpenses->sum('paid_amt')+$data->clientVisits->sum('amt_spend')+$data->supplierVisits->sum('amt_spend')+$data->purchaseExpenses->sum('amt_spend')+$data->dispatchExpenses->sum('amt_spend')+$data->vehicleExpenses->sum('amount')+$data->miscExpenses->sum('amt_spend')+$data->otherExpenses->sum('amt_spend')}}</td>
                            <td>{!! $data->action_buttons !!}</td>
                            <td><a href="{{ url('admin/dailyexpenses/') }}/<?=$data->id?>" class="btn btn-success">Export to Excel</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
