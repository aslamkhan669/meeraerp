@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.dailyexpenses.management') )

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.dailyexpenses.store'))->class('form-horizontal')->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="module" type="hidden" value="PurchaseManagement">
                    <input name="action" id="action" type="hidden" value="DailyExpenses">
                    <input name="activity" id="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                         Daily Expenses Register
                            <small class="text-muted">Create Daily Expenses</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                    <div class="col">
                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        <table class="table">
  <thead>
    <tr>
      <th scope="col">Tea</th>
      <th scope="col">Qty</th>
      <th scope="col">Special</th>
      <th scope="col">Normal</th>
      <th scope="col">SP.Rate</th>
      <th scope="col">NL.Rate</th>
      <th scope="col">Total</th>
    </tr>
  </thead>
  <tbody class="data">
    <tr>
      <th scope="row" class="timing" data-type="morningtea">Morning Tea</th>
      <td><input type="input" name="qty[]" value=""  class="form-control qty" style="width: 100px;"></td>
      <td><input type="checkbox" class="form-control special" name="special[]" ></td> 
      <td><input type="checkbox" class="form-control normal" name="normal[]"  ></td>           
      <td><input type="input" name="sp_rate[]"  class="form-control sprate rate" style="width: 100px;"></td>
      <td><input type="input" name="ml_rate[]"  class="form-control mlrate rate" style="width: 100px;"></td>
      <td><input type="input" name="total[]"  class="form-control total ftotal" style="width: 100px;"></td>
    </tr>
    <tr>
      <th scope="row" class="timing" data-type="eveningtea">Evening Tea</th>
      <td><input type="input" name="qty[]"  class="form-control qty" style="width: 100px;"></td>
      <td><input type="checkbox" class="form-control special" name="special[]"  ></td> 
      <td><input type="checkbox" class="form-control normal" name="normal[]" ></td> 
      <td><input type="input" name="sp_rate[]"  class="form-control sprate rate" style="width: 100px;"></td>
      <td><input type="input" name="ml_rate[]"  class="form-control mlrate rate" style="width: 100px;"></td>
      <td><input type="input" name="total[]"  class="form-control total ftotal" style="width: 100px;"></td>
    </tr>
    <tr>
      <th scope="row" class="timing" data-type="guesttea">Guest Tea</th>
      <td><input type="input" name="qty[]"  class="form-control qty" style="width: 100px;"></td>
      <td><input type="checkbox" class="form-control special" name="special[]"  ></td> 
      <td><input type="checkbox" class="form-control normal" name="normal[]"  ></td> 
      <td><input type="input" name="sp_rate[]"  class="form-control sprate rate" style="width: 100px;"></td>
      <td><input type="input" name="ml_rate[]"  class="form-control mlrate rate" style="width: 100px;"></td>
      <td><input type="input" name="total[]"  class="form-control total ftotal" style="width: 100px;"></td>
    </tr>
  </tbody>
</table>

                    </div><!--col-->
                    <div class="form-group row">
                    <table class="table">
  <thead>
    <tr>
      <th scope="col">Water</th>
      <th scope="col">Qty</th>
      <th scope="col">Rate</th>
      <th scope="col">Total</th>
    </tr>
  </thead>
  <tbody class="water">
    <tr>
      <th scope="row" class="type" data-type="waterbottle">Water Bottle</th>
      <td><input type="input" name="qty[]"  class="form-control waterqty " style="width: 100px;"></td>
      <td><input type="input" name="rate[]"  class="form-control waterrate" style="width: 100px;"></td>
      <td><input type="input" name="total[]"  class="form-control watertotal" style="width: 100px;"></td>
    </tr>
  </tbody>
</table>
                    </div><!--col-->
                    <div class="row">


<script src="https://code.jquery.com/jquery-latest.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<p>&nbsp;</p>
<h5 class="text-center">Stationery</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    <th>Item Name</th>
        <th>Qty</th>
    	<th>Price</th>
        <th>Total</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer" class="stationery">
</tbody>
<tfoot>
  <tr>
    <th colspan="5">
    <button id="btnAdd" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>

<h5 class="text-center">IT&Phone</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    	<th>Mobile No</th>
    	<th>User Code</th>
    	<th>Recharge Date</th>
    	<th>Plan Duration</th>
        <th>Due Date</th>
        <th>Plan Value</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer1" class="phone">
</tbody>
<tfoot>
  <tr>
    <th colspan="7">
    <button id="btnAdd1" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
<h5 class="text-center">Salary Expenses</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    	<th>DOI</th>
    	<th>Tmp Code</th>
    	<th>Name</th>
    	<th>Role</th>
        <th>Salary</th>
    	<th>MOP</th>
    	<th>DOP</th>
    	<th>Paid Amt</th>
    	<th>Due Amt</th>
      <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer2" class="salary">
</tbody>
<tfoot>
  <tr>
    <th colspan="10">
    <button id="btnAdd2" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
<section class="container">
<div class="table table-responsive">
<table class="other">
  <thead>
    <tr>
      <th scope="col">Other Expenses</th>
      <th scope="col">DOP</th>
      <th scope="col">Duration</th>
      <th scope="col">Amount</th>
      <th scope="col">Paid Amt</th>
      <th scope="col">Due Amt</th>
      <th scope="col">Remark</th>
    </tr>
  </thead>
  <tbody class="table">
    <tr>
      <th scope="row" class="types" data-type="electricity">Electricity</th>
      <td><input type="date" name="dop[]"  class="form-control otherdop" style="width: 200px;"></td>
      <td><input type="input" name="duration[]"  class="form-control otherduration" style="width: 100px;"></td>
      <td><input type="input" name="amount[]"  class="form-control otheramount" style="width: 100px;"></td>
      <td><input type="input" name="paid_amt[]"  class="form-control otherpaid" style="width: 100px;"></td>
      <td><input type="input" name="dua_amt[]"  class="form-control otherdue" style="width: 100px;"></td>
      <td><input type="input" name="remark[]"  class="form-control otherremark" style="width: 200px;"></td>
    </tr>
    <tr>
      <th scope="row" class="types" data-type="cleaning">Cleaning</th>
      <td><input type="date" name="dop[]"  class="form-control otherdop" style="width: 200px;"></td>
      <td><input type="input" name="duration[]"  class="form-control otherduration" style="width: 100px;"></td>
      <td><input type="input" name="amount[]"  class="form-control otheramount" style="width: 100px;"></td>
      <td><input type="input" name="paid_amt[]"  class="form-control otherpaid" style="width: 100px;"></td>
      <td><input type="input" name="dua_amt[]"  class="form-control otherdue" style="width: 100px;"></td>
      <td><input type="input" name="remark[]"  class="form-control otherremark" style="width: 200px;"></td>
    </tr>
    <tr>
      <th scope="row" class="types" data-type="rent&lease">Rent&Lease</th>
      <td><input type="date" name="dop[]"  class="form-control otherdop" style="width: 200px;"></td>
      <td><input type="input" name="duration[]"  class="form-control otherduration" style="width: 100px;"></td>
      <td><input type="input" name="amount[]"  class="form-control otheramount" style="width: 100px;"></td>
      <td><input type="input" name="paid_amt[]"  class="form-control otherpaid" style="width: 100px;"></td>
      <td><input type="input" name="dua_amt[]"  class="form-control otherdue" style="width: 100px;"></td>
      <td><input type="input" name="remark[]"  class="form-control otherremark" style="width: 200px;"></td>
    </tr>
    <tr>
      <th scope="row" class="types" data-type="legalexpenses">Legal Expenses</th>
      <td><input type="date" name="dop[]"  class="form-control otherdop" style="width: 200px;"></td>
      <td><input type="input" name="duration[]"  class="form-control otherduration" style="width: 100px;"></td>
      <td><input type="input" name="amount[]"  class="form-control otheramount" style="width: 100px;"></td>
      <td><input type="input" name="paid_amt[]"  class="form-control otherpaid" style="width: 100px;"></td>
      <td><input type="input" name="dua_amt[]"  class="form-control otherdue" style="width: 100px;"></td>
      <td><input type="input" name="remark[]"  class="form-control otherremark" style="width: 200px;"></td>
    </tr>
  </tbody>
</table>
</div>
</section>
<h5 class="text-center">Client Visit</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    <th>Emp Code</th>
        <th>Client</th>
    	<th>Amount Paid</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer3" class="client">
</tbody>
<tfoot>
  <tr>
    <th colspan="6">
    <button id="btnAdd3" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
<h5 class="text-center">Supplier Visit</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    <th>Emp Code</th>
        <th>Supplier</th>
    	<th>Amount Paid</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer4" class="supplier">
</tbody>
<tfoot>
  <tr>
    <th colspan="6">
    <button id="btnAdd4" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
<h5 class="text-center">Purchase Expenses</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    <th>Purchase Person</th>
        <th>Amt Recieved</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Mode Of Payment</th>
        <th>for what</th>
        <th>Remarks</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer5" class="purchase">
</tbody>
<tfoot>
  <tr>
    <th colspan="8">
    <button id="btnAdd5" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
<h5 class="text-center">Dispatch Expenses</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    <th>Dispatch Person</th>
        <th>Amt Recieved</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Mode Of Payment</th>
        <th>for what</th>
        <th>Remarks</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer6" class="dispatch">
</tbody>
<tfoot>
  <tr>
    <th colspan="8">
    <button id="btnAdd6" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
<h5 class="text-center">Vehicle Expenses</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    <th> Person</th>
        <th>After Reading</th>
        <th>Before Reading</th>
        <th>Defference</th>
        <th>Unit Rate</th>
        <th>Amount</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer7" class="vehicle">
</tbody>
<tfoot>
  <tr>
    <th colspan="7">
    <button id="btnAdd7" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
<h5 class="text-center">Miscellanceses Expenses</h5>
<section class="container">
<div class="table table-responsive">
<table class="table table-responsive table-striped table-bordered">
<thead>
	<tr>
    <th>Person</th>
        <th>Amt Recieved</th>
        <th>Amt Spend</th>
        <th>Amt Return</th>
        <th>Mode Of Payment</th>
        <th>for what</th>
        <th>Remarks</th>
        <th>Action</th>
    </tr>
</thead>
<tbody id="TextBoxContainer8" class="misc">
</tbody>
<tfoot>
  <tr>
    <th colspan="8">
    <button id="btnAdd8" type="button" class="btn btn-primary" data-toggle="tooltip" data-original-title="Add more controls"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp; Add&nbsp;</button></th>
  </tr>
</tfoot>
</table>
</div>
</section>
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.dailyexpenses.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    <button type="button" data-id="" id="send" class="btn btn-success">Send</button>
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection