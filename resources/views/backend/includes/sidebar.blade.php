<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
            </li>
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}"><i class="icon-speedometer"></i> {{ __('menus.backend.sidebar.dashboard') }}</a>
            </li>
            @if(auth()->user()->can('Purchaser'))

            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.purchase.index') }}"><i class="icon-speedometer"></i> My Dashboard</a>
            </li>
            @endif
            @if(auth()->user()->can('Dispatcher'))

<li class="nav-item">
    <a class="nav-link" href="{{ route('admin.dispatch.index') }}"><i class="icon-speedometer"></i> My Dashboard</a>
</li>
@endif
            @if($logged_in_user->hasRole('Administrator'))
            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}"><i class="icon-speedometer"></i> Custome</a>
            </li>
                @endif
            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa fa-universal-access"></i> {{ __('menus.backend.access.title') }}
                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.index') }}">
                            <i class="fa fa-angle-right"></i>   {{ __('labels.backend.access.users.management') }}
                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}" href="{{ route('admin.auth.role.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.roles.management') }}
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
     
            
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa fa-archive"></i> {{ __('menus.backend.access.productmanagement.title') }} 
                    </a>

                    <ul class="nav-dropdown-items">
                    @if(auth()->user()->can('Products') || auth()->user()->roles[0]->name =="administrator")

                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer')) }}"  href="{{ route('admin.product.index') }}">
                        <i class="fa fa-angle-right"></i> {{ __('menus.backend.product-management.product') }}
                        </a>
                    </li>
                    @endif

                @if(auth()->user()->can('Category') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.category.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.categories.management') }}
                            </a>
                        </li>
                    @endif
                        
                    @if(auth()->user()->can('Category') || auth()->user()->roles[0]->name =="administrator")
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.subcategory.index') }}">
                        <i class="fa fa-angle-right"></i> {{ __('labels.backend.access.subcategories.management') }}
                        </a>
                    </li>
                    @endif

                @if(auth()->user()->can('catalogue') || auth()->user()->roles[0]->name =="administrator")
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.subcatalogue') }}">
                        <i class="fa fa-angle-right"></i> {{ __('labels.backend.access.subcatalogue.management') }}
                        </a>
                    </li>
                    @endif

                @if(auth()->user()->can('Brands') || auth()->user()->roles[0]->name =="administrator")
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('admin.brand.index') }}">
                        <i class="fa fa-angle-right"></i>   {{ __('menus.backend.product-management.brand') }}
                        </a>
                    </li>
                   @endif

               @if(auth()->user()->can('Uom') || auth()->user()->roles[0]->name =="administrator")
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('admin.uom.index') }}">
                        <i class="fa fa-angle-right"></i>   {{ __('menus.backend.product-management.uom') }}
                        </a>
                    </li>
                    @endif

                    @if(auth()->user()->can('Color') || auth()->user()->roles[0]->name =="administrator")
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('admin.color.index') }}">
                        <i class="fa fa-angle-right"></i>     {{ __('labels.backend.access.colors.management') }}
                        </a>
                    </li>
                    @endif
                    </ul>
                </li>
          
                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa fa-question-circle-o"></i> {{ __('menus.backend.access.querymanagement.title') }} 
                    </a>

                    <ul class="nav-dropdown-items">
                    @if(auth()->user()->can('Query Formating') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.query.index') }}">
                            <i class="fa fa-angle-right"></i>   {{ __('labels.backend.access.queries.management') }}
                            </a>
                        </li>
                    @endif
                         
                    @if(auth()->user()->can('Pricing Center') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.pricing.index') }}">
                            <i class="fa fa-angle-right"></i>   {{ __('labels.backend.access.pricing.management') }}
                            </a>
                        </li>
                        @endif

 @if(auth()->user()->can('Quotation Approval') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.quotation.index') }}">
                            <i class="fa fa-angle-right"></i>    {{ __('labels.backend.access.quotation.management') }}
                            </a>
                        </li> 
                        @endif
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.quotationpdf.index') }}">
                            <i class="fa fa-angle-right"></i> Quotation PDF
                            </a>
                        </li> 
                    </ul>
                </li>    

      <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                         <i class="fa fa-archive"></i> {{ __('menus.backend.access.master.title') }} 
                    </a>
                    <ul class="nav-dropdown-items"> 
                    @if(auth()->user()->can('Item List') || auth()->user()->roles[0]->name =="administrator")
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('admin.itemlist.index') }}">
                        <i class="fa fa-angle-right"></i>   {{ __('menus.backend.product-management.item') }}
                        </a>
                    </li>
                    @endif
                    @if(auth()->user()->can('ItemList Pricing') || auth()->user()->roles[0]->name =="administrator")
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/log-viewer/logs*')) }}" href="{{ route('admin.itemmapping.index') }}">
                        <i class="fa fa-angle-right"></i>   {{ __('menus.backend.product-management.itemmapping') }}
                        </a>
                    </li>
                    @endif
                    @if(auth()->user()->can('Client') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.client.index') }}">
                            <i class="fa fa-angle-right"></i>   {{ __('labels.backend.access.clients.management') }}
                            </a>
                        </li>
                        @endif

 @if(auth()->user()->can('Supplier') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.supplier.index') }}">
                            <i class="fa fa-angle-right"></i> {{ __('labels.backend.access.suppliers.management') }}
                            </a>
                        </li>
                        @endif

@if(auth()->user()->can('Client Type') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.clienttype.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.clienttypes.management') }}
                            </a>
                        </li>
                        @endif

@if(auth()->user()->can('Payment Mode') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.paymentmode.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.paymentmodes.management') }}
                            </a>
                        </li>
                        @endif

                        @if(auth()->user()->can('Payment Terms') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.paymentterm.index') }}">
                            <i class="fa fa-angle-right"></i> {{ __('labels.backend.access.paymentterm.management') }}
                            </a>
                        </li>
                        @endif

                    </ul>
                </li>
                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa fa-cog"></i> {{ __('menus.backend.access.purchasemanagement.title') }} 
                    </a>
                    <ul class="nav-dropdown-items">
                    @if(auth()->user()->can('PoFormatting') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.poformatting.index') }}">
                            <i class="fa fa-angle-right"></i>    {{ __('labels.backend.access.poformattings.management') }}
                            </a>
                        </li> 
                        @endif

@if(auth()->user()->can('PoPlanner') || auth()->user()->roles[0]->name =="administrator")                        
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.planner.index') }}">
                            <i class="fa fa-angle-right"></i>    {{ __('labels.backend.access.poformattings.planner') }}
                            </a>
                        </li> 
                        @endif
                        @if(auth()->user()->can('PurchaseAccount') || auth()->user()->roles[0]->name =="administrator")                        
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.poregister.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.purchaseaccount.management') }}
                            </a>
                        </li>
                        @endif
                        @if(auth()->user()->can('PurchasePO') || auth()->user()->roles[0]->name =="administrator")

                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.pogenerate.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.pogenerate.management') }}
                            </a>
                        </li> 
                        @endif
                        @if(auth()->user()->can('PurchaseChallan') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.purchasechallan.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.purchasechallan.management') }}
                            </a>
                        </li> 
                        @endif
                        @if(auth()->user()->can('DailyExpenses') || auth()->user()->roles[0]->name =="administrator")

                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.dailyexpenses.index') }}">
                            <i class="fa fa-angle-right"></i>   {{ __('labels.backend.access.dailyexpenses.management') }}
                            </a>
                        </li>
                        @endif

                    </ul>

                    </li>      
  <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa fa-cog"></i> {{ __('menus.backend.access.dispatchmanagement.title') }} 
                    </a>


                     <ul class="nav-dropdown-items">
                     @if(auth()->user()->can('DispatchPlanner') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.dispatchregister.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.purchaseaccount.dispatch') }}
                            </a>
                        </li>
                        @endif

                         @if(auth()->user()->can('DispatchAccount') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.dispatchaccount.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.purchaseaccount.dispatchaccount') }}
                            </a>
                        </li>
                        @endif
                        @if(auth()->user()->can('DispatchChallan') || auth()->user()->roles[0]->name =="administrator")

                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.dispatchchallan.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.dispatchchallan.management') }}
                            </a>
                        </li>
                        @endif  
                          @if(auth()->user()->can('PurchaseInventory') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.inventorydetails.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.purchaseaccount.inventory') }}
                            </a>
                        </li>
                        @endif 
                        @if(auth()->user()->can('DispatchInventory') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.inventorydispatch.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.purchaseaccount.inventor') }}
                            </a>
                        </li>
                        @endif 
                    </ul>

                    </li>      
                    <!-- <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa fa-cog"></i> {{ __('menus.backend.access.manualpurchase.title') }} 
                    </a>
                    <ul class="nav-dropdown-items">
                    @if(auth()->user()->can('OnCallPurchase') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.manualpurchase.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.manualpurchase.management') }}
                            </a>
                        </li> 
                        @endif 
                        @if(auth()->user()->can('OnCallPricing') || auth()->user()->roles[0]->name =="administrator")    
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.manualpricing.index') }}">
                            <i class="fa fa-angle-right"></i>   {{ __('labels.backend.access.manualpricing.management') }}
                            </a>
                        </li>
                        @endif 
                        @if(auth()->user()->can('OnCallQuotation') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.manualquotation.index') }}">
                            <i class="fa fa-angle-right"></i>   {{ __('labels.backend.access.manualquotation.management') }}
                            </a>
                        </li>
                        @endif 
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.manualquotationpdf.index') }}">
                            <i class="fa fa-angle-right"></i>On Call QuotationPDF
                            </a>
                        </li> 
                    </ul>

                    </li>     -->
                 <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa fa-cog"></i> {{ __('menus.backend.access.setting.title') }} 
                    </a>
                    <ul class="nav-dropdown-items">
                    @if(auth()->user()->can('Company Details') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.companydetail.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.companydetails.management') }}
                            </a>
                        </li>
                        @endif
                        </ul>
                        <ul class="nav-dropdown-items">

@if(auth()->user()->can('Freights') || auth()->user()->roles[0]->name =="administrator")
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.freight.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.freights.management') }}
                            </a>
                        </li>
                        @endif

                        </ul>

                        @if(auth()->user()->can('Transport Insurance') || auth()->user()->roles[0]->name =="administrator")
                        <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.transportinsurance.index') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.transportinsurances.management') }}
                            </a>
                        </li>
                        @endif
                        </ul>
                    </li>

                      <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="fa fa-cog"></i> {{ __('menus.backend.access.excelupload.title') }} 
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.productexcel.create') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.productexcelupload.management') }}
                            </a>
                        </li>
                        </ul>
                        <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.productimage.create') }}">
                            <i class="fa fa-angle-right"></i>  {{ __('labels.backend.access.productimageupload.management') }}
                            </a>
                        </li>
                        </ul>
                    </li> 
                    </ul>
        
    </nav>
</div>