@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')

<div class="container">
<h3 style="margin-top: 10px;">Dashboard</h3>

      <div class="cards">
     <div class="row">
    <div class="col-sm-3">
       <label> Start: </label>
       <input type="date" class="form-control sdate" name="sdate">
    </div>
       <div class="col-sm-3">
       <label> End: </label>
       <input type="date" class="form-control edate" name="edate">
        
    </div>
       <div class="col-sm-3">
        <label>Select User :</label>
         <select class="form-control user_name" name="user_name">
                                <option value="">Select</option>
                                @foreach($datainfo as $data)
                                        <option value="{{$data->user_name}}" data-id="{{$data->user_name}}">{{$data->user_name}}</option>
                                @endforeach

                                </select>
    </div>
    <div class="col-sm-3">
        <input type="button" value="Search" class="send btn btn-success btn-md" style=" margin-top: 30px;">
    </div>
</div>
       
 
        
       <table class='table'style=" margin-top: 30px;" >
       <thead>
                        <tr>
                            <th>Module</th>
                            <th>Menu</th>
                            <th>Activity</th>
                            <th>Total</th>
                        </tr>
                        </thead>

       <tbody id='show'>
@foreach ($logsdata as $data)
<tr>
<td>{{$data->module}}</td><td>{{$data->action}}</td><td>{{$data->activity}}</td><td>{{$data->total}}</td></tr>
@endforeach
</tbody>

</table>   
<div class="row">
            <div class="col-7">
                <div class="float-left">
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                {{ $logsdata->render() }}

                </div>
            </div><!--col-->
        </div><!--row-->
    

    <!-- second chart -->
    <div class="row">
    <div class="col-md-6 right">
<div class="card" style="padding: 0px">
    <canvas id="myChart"></canvas>
    </div> 
  </div>
  <div class="col-md-6 left">
<div class="card" style="padding: 0px">
    <canvas id="myQuotation"></canvas>
    </div>
    </div>      
      </div>
      <div class="row">
      <div class="col-md-6 left">
   <div class="card" style="padding: 0px">
    <canvas id="PO"></canvas>
    </div>
    </div>
    <div class="col-md-6 right">
   <div class="card" style="padding: 0px">
    <canvas id="supplier"></canvas>
    </div>
    </div>
      </div>
      <div class="row">
      <div class="col-md-6 left">
   <div class="card" style="padding: 0px">
    <canvas id="material"></canvas>
    </div>
    </div>
    <div class="col-md-6 right">
   <div class="card" style="padding: 0px">
    <canvas id="daily"></canvas>
    </div>
    </div>
      </div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript"></script>
  <script>
        
//second  chart

$(function(){
    let queryInfo = document.getElementById('myChart').getContext('2d');
        
        // Global Options
        Chart.defaults.global.defaultFontFamily = 'Lato';
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontColor = '#222d32';
        var label = [];
        var data = [];
        @foreach ($query_count as $data)
        {{$monthNum=$data->months}}
            label.push("{{  $monthName = date('F', mktime(0, 0, 0, $monthNum, 10))  }}"); 
        @endforeach
        @foreach ($query_count as $data)
            data.push("{{ $data->queries_count }}"); 
        @endforeach
        let massPopChart = new Chart(queryInfo, {
          type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
          data:{
            labels:label,
            datasets:[{
            //   label:'Revenue',
              data:data,
              backgroundColor:'#d73925',
            //   backgroundColor:[
            //     'rgba(255, 99, 132, 0.6)',
            //     'rgba(54, 162, 235, 0.6)',
            //     'rgba(255, 206, 86, 0.6)',
            //     'rgba(75, 192, 192, 0.6)',
            //     'rgba(153, 102, 255, 0.6)',
            //     'rgba(255, 159, 64, 0.6)',
            //     'rgba(255, 99, 132, 0.6)'
            //   ],
              borderWidth:1,
              borderColor:'#777',
              hoverBorderWidth:3,
              hoverBorderColor:'#000'
            }]
          },
          options:{
            title:{
              display:true,
              text:'Query',
              fontSize:22
            },
            legend:{
              display:false,
              position:'right',
              labels:{
                fontColor:'#777'
              }
            },
            layout:{
              padding:{
                left:30,
                right:0,
                bottom:0,
                top:0
              }
            },
            tooltips:{
              enabled:true
            }
          }
        });
//second chart

  let myQuotation = document.getElementById('myQuotation').getContext('2d');
        
        // Global Options
        Chart.defaults.global.defaultFontFamily = 'Lato';
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontColor = '#00a65a';
        var label = [];
        var data = [];
        @foreach ($quotation_data as $data)
        {{$monthNum=$data->months}}
            label.push("{{  $monthName = date('F', mktime(0, 0, 0, $monthNum, 10))  }}"); 
        @endforeach
        @foreach ($quotation_data as $data)
            data.push("{{ $data->queries_count }}"); 
        @endforeach
        let massPopChart_two = new Chart(myQuotation, {
          type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
          data:{
            labels:label,
            datasets:[{
            //   label:'Revenue',
              data:data,
              backgroundColor:'blue',
            //   backgroundColor:[
            //     'rgba(255, 99, 132, 0.6)',
            //     'rgba(54, 162, 235, 0.6)',
            //     'rgba(255, 206, 86, 0.6)',
            //     'rgba(75, 192, 192, 0.6)',
            //     'rgba(153, 102, 255, 0.6)',
            //     'rgba(255, 159, 64, 0.6)',
            //     'rgba(255, 99, 132, 0.6)'
            //   ],
              borderWidth:1,
              borderColor:'#777',
              hoverBorderWidth:3,
              hoverBorderColor:'#000'
            }]
          },
          options:{
            title:{
              display:true,
              text:'Quotation',
              fontSize:22
            },
            legend:{
              display:false,
              position:'left',
              labels:{
                fontColor:'#000'
              }
            },
            layout:{
              padding:{
                left:30,
                right:0,
                bottom:0,
                top:0
              }
            },
            tooltips:{
              enabled:true
            }
          }
        });
// third chart
let PO = document.getElementById('PO').getContext('2d');
        
        // Global Options
        Chart.defaults.global.defaultFontFamily = 'Lato';
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontColor = '#00a65a';
        var label = [];
        var data = [];
        @foreach ($po_data as $data)
        {{$monthNum=$data->months}}
            label.push("{{  $monthName = date('F', mktime(0, 0, 0, $monthNum, 10))  }}"); 
        @endforeach
        @foreach ($po_data as $data)
            data.push("{{ $data->po_count }}"); 
        @endforeach
        let massPopChart_three = new Chart(PO, {
          type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
          data:{
            labels:label,
            datasets:[{
            //   label:'Revenue',
              data:data,
              backgroundColor:'#00a65a',
            //   backgroundColor:[
            //     'rgba(255, 99, 132, 0.6)',
            //     'rgba(54, 162, 235, 0.6)',
            //     'rgba(255, 206, 86, 0.6)',
            //     'rgba(75, 192, 192, 0.6)',
            //     'rgba(153, 102, 255, 0.6)',
            //     'rgba(255, 159, 64, 0.6)',
            //     'rgba(255, 99, 132, 0.6)'
            //   ],
              borderWidth:1,
              borderColor:'#777',
              hoverBorderWidth:3,
              hoverBorderColor:'#000'
            }]
          },
          options:{
            title:{
              display:true,
              text:'PO',
              fontSize:22
            },
            legend:{
              display:false,
              position:'left',
              labels:{
                fontColor:'#000'
              }
            },
            layout:{
              padding:{
                left:30,
                right:0,
                bottom:0,
                top:0
              }
            },
            tooltips:{
              enabled:true
            }
          }
        });
// Forth chart
let supplier = document.getElementById('supplier').getContext('2d');
        
        // Global Options
        Chart.defaults.global.defaultFontFamily = 'Lato';
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontColor = '#00a65a';
        var label = [];
        var data = [];
        @foreach ($supplier_data as $data)
        {{$monthNum=$data->months}}
            label.push("{{  $monthName = date('F', mktime(0, 0, 0, $monthNum, 10))  }}"); 
        @endforeach
        @foreach ($supplier_data as $data)
            data.push("{{ $data->supplier_count }}"); 
        @endforeach
        let massPopChart_four = new Chart(supplier, {
          type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
          data:{
            labels:label,
            datasets:[{
            //   label:'Revenue',
              data:data,
              backgroundColor:'#00a65a',
            //   backgroundColor:[
            //     'rgba(255, 99, 132, 0.6)',
            //     'rgba(54, 162, 235, 0.6)',
            //     'rgba(255, 206, 86, 0.6)',
            //     'rgba(75, 192, 192, 0.6)',
            //     'rgba(153, 102, 255, 0.6)',
            //     'rgba(255, 159, 64, 0.6)',
            //     'rgba(255, 99, 132, 0.6)'
            //   ],
              borderWidth:1,
              borderColor:'#777',
              hoverBorderWidth:3,
              hoverBorderColor:'#000'
            }]
          },
          options:{
            title:{
              display:true,
              text:'Supplier',
              fontSize:22
            },
            legend:{
              display:false,
              position:'left',
              labels:{
                fontColor:'#000'
              }
            },
            layout:{
              padding:{
                left:30,
                right:0,
                bottom:0,
                top:0
              }
            },
            tooltips:{
              enabled:true
            }
          }
        });

        // Five chart
let material = document.getElementById('material').getContext('2d');
        
        // Global Options
        Chart.defaults.global.defaultFontFamily = 'Lato';
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontColor = '#00a65a';
        var label = [];
        var data = [];
        @foreach ($material_data as $data)
        {{$monthNum=$data->months}}
            label.push("{{  $monthName = date('F', mktime(0, 0, 0, $monthNum, 10))  }}"); 
        @endforeach
        @foreach ($material_data as $data)
            data.push("{{ $data->material_sum }}"); 
        @endforeach
        let massPopChart_five = new Chart(material, {
          type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
          data:{
            labels:label,
            datasets:[{
            //   label:'Revenue',
              data:data,
              backgroundColor:'#00a65a',
            //   backgroundColor:[
            //     'rgba(255, 99, 132, 0.6)',
            //     'rgba(54, 162, 235, 0.6)',
            //     'rgba(255, 206, 86, 0.6)',
            //     'rgba(75, 192, 192, 0.6)',
            //     'rgba(153, 102, 255, 0.6)',
            //     'rgba(255, 159, 64, 0.6)',
            //     'rgba(255, 99, 132, 0.6)'
            //   ],
              borderWidth:1,
              borderColor:'#777',
              hoverBorderWidth:3,
              hoverBorderColor:'#000'
            }]
          },
          options:{
            title:{
              display:true,
              text:'Material Expenses',
              fontSize:22
            },
            legend:{
              display:false,
              position:'left',
              labels:{
                fontColor:'#000'
              }
            },
            layout:{
              padding:{
                left:30,
                right:0,
                bottom:0,
                top:0
              }
            },
            tooltips:{
              enabled:true
            }
          }
        });

                // six chart
let daily = document.getElementById('daily').getContext('2d');
        
        // Global Options
        Chart.defaults.global.defaultFontFamily = 'Lato';
        Chart.defaults.global.defaultFontSize = 14;
        Chart.defaults.global.defaultFontColor = '#00a65a';
        var label = [];
        var data = [];
        @foreach ($tea_data as $data)
        {{$monthNum=$data->months}}
            label.push("{{  $monthName = date('F', mktime(0, 0, 0, $monthNum, 10))  }}"); 
        @endforeach
        @foreach ($info as $data)
            data.push("{{ $data->teas->sum('total')+$data->waters->sum('total')+$data->stationeries->sum('total')+$data->itPhones->sum('plan_value')+$data->salaryExpenses->sum('paid_amt')+$data->clientVisits->sum('amt_spend')+$data->supplierVisits->sum('amt_spend')+$data->purchaseExpenses->sum('amt_spend')+$data->dispatchExpenses->sum('amt_spend')+$data->vehicleExpenses->sum('amount')+$data->miscExpenses->sum('amt_spend')+$data->otherExpenses->sum('amt_spend') }}"); 
        @endforeach
        let massPopChart_six = new Chart(daily, {
          type:'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
          data:{
            labels:label,
            datasets:[{
            //   label:'Revenue',
              data:data,
              backgroundColor:'#00a65a',
            //   backgroundColor:[
            //     'rgba(255, 99, 132, 0.6)',
            //     'rgba(54, 162, 235, 0.6)',
            //     'rgba(255, 206, 86, 0.6)',
            //     'rgba(75, 192, 192, 0.6)',
            //     'rgba(153, 102, 255, 0.6)',
            //     'rgba(255, 159, 64, 0.6)',
            //     'rgba(255, 99, 132, 0.6)'
            //   ],
              borderWidth:1,
              borderColor:'#777',
              hoverBorderWidth:3,
              hoverBorderColor:'#000'
            }]
          },
          options:{
            title:{
              display:true,
              text:'Daily Expenses',
              fontSize:22
            },
            legend:{
              display:false,
              position:'left',
              labels:{
                fontColor:'#000'
              }
            },
            layout:{
              padding:{
                left:30,
                right:0,
                bottom:0,
                top:0
              }
            },
            tooltips:{
              enabled:true
            }
          }
        });
});
    
  </script>
     