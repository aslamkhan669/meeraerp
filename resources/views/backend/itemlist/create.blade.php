@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.item.management') . ' | ' . __('labels.backend.access.item.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.itemlist.store'))->class('form-horizontal')->attributes(array('enctype'=>'multipart/form-data'))->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.item.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.item.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="ItemManagement">
                    <input name="action" type="hidden" value="ItemList">
                    <input name="activity" type="hidden" value="create">
                    <div class="col">
                 
                </div><!--col-->
                
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.item.is_active'))->class('col-md-2 form-control-label')->for('is_active') }}

                    <div class="col-md-4">
                        <label class="switch switch-3d switch-primary">
                            {{ html()->checkbox('is_active', true, '1')->class('switch-input') }}
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div><!--col-->

                     
                     {{ html()->label(__('validation.attributes.backend.access.item.hsncode'))->class('col-md-2 form-control-label')->for('model_no') }}

                    <div class="col-md-4">
                        {{ html()->text('hsn_code')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.item.hsncode'))
                            ->attribute('maxlength', 191)
                            ->required()
                            ->autofocus() }}
                    </div><!--col-->
                        </div><!--form-group-->
                          <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.item.model_no'))->class('col-md-2 form-control-label')->for('model_no') }}

                            <div class="col-md-4">
                                {{ html()->text('model_no')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.model_no'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.item.product_name'))->class('col-md-2 form-control-label')->for('product_name') }}

                                <div class="col-md-4">
                                <select class="form-control category" name="product_id">
                                <option value="">Select</option>
                                @foreach($products as $stand)
                                <option value="{{$stand->id}}">{{$stand->product_name}}</option>
                                @endforeach

                                </select>
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">  
                        {{ html()->label(__('validation.attributes.backend.access.product.cat_name'))->class('col-sm-2 form-control-label')->for('cat_name') }}
        
                        <div class="col-sm-4 ">
                    <div class="form-group">
                    <select class="form-control category" name="category_id" id="cat">
                   

                    </select>
                </div><!--col-->
                </div> 
                <input type="hidden" name="prod_id" id="prod_id">     
             <input type="hidden" value="" name="cat_id" id="cat_id">
             {{ html()->label(__('validation.attributes.backend.access.product.subcat_name'))->class('col-sm-2 form-control-label')->for('mobile') }}

            <div class="col-sm-4 ">
                    <div class="form-group">
                    <select class="form-control category" name="subcategory_id" id="subcat">
                    

                    </select>
                </div><!--col-->
                </div>
                    </div><!--col-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.product.brand_name'))->class('col-sm-2 form-control-label')->for('cat_name') }}
        
        <div class="col-sm-4 ">
    <div class="form-group">
    <select class="form-control category" name="brand_id" id="brand">
   

    </select>
</div><!--col-->
</div> 
                      
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.item.unit'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                            <select class="form-control category" name="unit">
                            <option value="">Select</option>
                            @foreach($uoms as $stand)
                            <option value="{{$stand->id}}">{{$stand->unit}}</option>
                            @endforeach

                            </select>
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.item.color'))->class('col-md-2 form-control-label')->for('color') }}

                            <div class="col-md-4">
                            <select class="form-control category" name="color">
                            <option value="">Select</option>
                            @foreach($colors as $stand)
                            <option value="{{$stand->id}}">{{$stand->color}}</option>
                            @endforeach
                            </select>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.item.image'))->class('col-md-2 form-control-label')->for('specification') }}

<div class="col-md-4">
    {{ html()->file('image')
        ->class('form-control')
        ->attribute('maxlength', 191)
        ->required()
        ->autofocus() }}
</div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.item.description'))->class('col-md-2 form-control-label')->for('description') }}

                            <div class="col-md-4">
                                {{ html()->text('description')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.description'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.item.specification'))->class('col-md-2 form-control-label')->for('specification') }}

                            <div class="col-md-4">
                                {{ html()->text('specification')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.specification'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.item.remarks'))->class('col-md-2 form-control-label')->for('remarks') }}

                            <div class="col-md-4">
                                {{ html()->text('remarks')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.remarks'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                 
                    <div class="form-group row">
            
                            </div>
                </div><!--row-->
  
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.itemlist.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
 
    $('select[name="product_id"]').on('change', function() {
        var id = $(this).val();      
        debugger;
            if(id) {
                $.ajax({
                    url: '/admin/productcode/ajax/'+encodeURI(id),
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        for(var i=0;i<data.length;i++){ 
                         $('#prod_id').val(data[i].product_code);
                        }
                        }
                    
                });
            }
            });

  $('select[name="product_id"]').on('change', function() {
        var product_id = $(this).val();      
        $('#cat_id').val(product_id);
        debugger;
            if(product_id) {
                $.ajax({
                    url: '/admin/parentscategories/ajax/'+encodeURI(product_id),
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        var html = "<option>Select</option>";
                        for(var i=0;i<data.length;i++){ 
                            html+="<option value="+data[i].id+">"+data[i].category_name+"</option>";
                        }
                        $("#cat").html(html);
                        $('.unshowcat').show();
                    }
                });
            }
    });
//brand show
$('select[name="product_id"]').on('change', function() {
        var product_id = $(this).val();      
        debugger;
            if(product_id) {
                $.ajax({
                    url: '/admin/parentsbrands/ajax/'+encodeURI(product_id),
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        var html = "<option>Select</option>";
                        for(var i=0;i<data.length;i++){ 
                            html+="<option value="+data[i].id+">"+data[i].brand_name+"</option>";
                        }
                        $("#brand").html(html);
                    }
                });
            }
    });
//On category change
    $('select[name="category_id"]').on('change', function() {
        var category_id = $(this).val();
        $('#cat_id').val(category_id);
            if(category_id) {
                $.ajax({
                    url: '/admin/subscategories/ajax/'+encodeURI(category_id),
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        if ($.trim(data)){ 
                        var html = "<option>Select</option>";
                        for(var i=0;i<data.length;i++){ 
                            html+="<option value="+data[i].id+">"+data[i].category_name+"</option>";
                        }
                        $("#subcat").html(html);
                        $('.unshow').show();
                        }else{   
                        alert("Subcategory not Exist");
                        $('.unshow').hide();

                        }
                    }
                });     
            }
    });

    $('select[name="subcategory_id"]').on('change',function(){
        var cat_id=$(this).val();
        $('#cat_id').val(cat_id);
    })
});
</script>
