@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.purchaseaccount.management') . ' | ' . __('labels.backend.access.uom.create'))

@section('breadcrumb-links')
@endsection

@section('content')
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                          Purchase Challan                            
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />
                <div class="row mt-4 mb-4">
                <input name="user_name" id="userName" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="userId" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="Module" type="hidden" value="PurchaseManagement">
                    <input name="action" id="Action" type="hidden" value="PurchaseChallan">
                    <input name="activity" id="Activity" type="hidden" value="create">
                <input type="hidden" value="{{Auth::user()->id}}" name="pt_user_id" id="pt_user_id">
                <input type="hidden" value="{{$info[0]->id}}" name="supplier_id" id="supplier_id">
                <input type="hidden" value="{{$centerinfo[0]->id}}" name="center_id" id="center_id">
                <input type="hidden" value="{{$id}}" name="po_id" id="po_id">

                   <label class="col-md-1 form-control-label" for="lab"></label>

                            <div class="col-md-4">
                            </div><!--col-->

                             <div class="col-md-2"></div>
                            <label class="col-md-1 form-control-label" for="lab">Challan No.</label>

                            <div class="col-md-4 table-responsive">
                               <table class="table table-bordered"><tbody><tr><td>CH</td><td>{{ 'V'.$sa=substr(strtoupper($brandletters='V0'.$info[0]->id),-2)}}</td><td>{{$info[0]->pricing_center}}</td><td>{{substr(str_replace("-","",explode(" ",$info[0]->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$id),-2)}}</td></tr></tbody></table>
                            </div><!--col-->
                   
                 </div>
               

                <div class="row">

                    <div class="col-md-6  mb-4">
                         
                        <h4 class="card-title">Purchaser's Details</h4>
                        <h5>{{$centerinfo[0]->company_name}}</h5>
                    </div>
                    <div class="col-md-6 text-right  mb-4">
                       <h4 class="card-title">Supplier Details</h4>
                       <h5>{{$info[0]->supplier_name}}</h5>
                    </div>
                </div>
                <div class="row  mb-4">
                    <div class="col-md-12">
                             <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Address</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control" value="{{$centerinfo[0]->address}}" readonly>
                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">Address</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control" value="{{$info[0]->street}} {{$info[0]->sector}} {{$info[0]->city}} {{$info[0]->state}}" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                      
                      
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Email</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control"  value="{{$centerinfo[0]->email}}" readonly>
                            </div><!--col-->
                            
                            <label class="col-md-2 form-control-label" for="lab">Email</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control"  value="{{$info[0]->email}}" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">GST No.</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control"  value="{{$centerinfo[0]->gst_no}}" readonly>
                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">GST No.</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control"  value="{{$info[0]->gst_no}}" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        <label class="col-md-2 form-control-label" for="lab"></label>
                        <div class="col-md-4">
                            </div>
                        <label class="col-md-2 form-control-label" for="lab">Supplier Challan No.</label>
                        <div class="col-md-4">
                                <input type="text" class="form-control" id="supplier_challan"  value=""  required>
                            </div><!--col-->
                        </div><!--form-group-->

                    </div>

                </div>
                
                
                <input type="hidden" value="" name="challan_id[]" id="challan_id">


                <div class="row mt-4">
    <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Qty</th>
                        <th>GST</th>
                        <th>Total Amount</th>
                        
                        
                    </tr>
                    </thead>
                    <tbody class="data">
                    
                    <?php $i=1;?>

                    @foreach($data as $items)
                    <tr>
                    <td>{{$i++}}</td>
                    <input type="hidden"  class="form-control itemId " name="item_id[]"  value='{{$items->item_id}}'>
                    <td><button type='button' class='btn btn-info tdClick' data-id='{{$items->item_id}}' value='{{$items->item_id}}' data-toggle='modal' data-target='#ModalItem'>View</button></td>
                    <td class="image-hover"><img src="{{url('/uploads/'.$items->image)}}" width="50px" height="50px"/></td>   
                    <td><input type="input"  class="form-control quantity" name="quantity[]"  value='{{$items->qty}}' readonly></td>
                    <td><input type="input" class="form-control gst" name="gst[]" value='{{$items->gst}}' readonly></td>
                    <td><input type="input" class="form-control total" name="total[]"  value='{{$items->grand_total}}' readonly></td>                    
                    </tr>
                    @endforeach
     
                    </tbody>
                    </table>
                    </div>
</div>
 </div> 
 
          

 <div class="row">
 <div class="col-md-12">

 <div class="terms">
 <h4 class="card-title">Terms & Conditions</h4>
 <p class="text-muted">Tax as per GST Rules</p>
 <table>
     <tbody>
     <tr>
          <td><label class="form-control-label"> Payment Terms</label></td>
          <td><label class="form-control-label"> : {{$termsdata[0]->payment_days}} days </label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Freight</label></td>
          <td><label class="form-control-label"> : {{$freightinfo[0]->conditions}}</label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Transport Insurance</label></td>
          <td><label class="form-control-label"> : {{$insuranceinfo[0]->conditions}} </label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Validity</label></td>
          <td><label class="form-control-label"> : {{$termsdata[0]->validity_days}} days</label></td>
     <tr>
        
</tbody>

 </table>
 <p><b>{{$termsdata[0]->comment_one}}</b></p>
<p><b>{{$termsdata[0]->comment_two}}</b></p>
<p><b>{{$termsdata[0]->comment_three}}</b></p>
<p><b>{{$termsdata[0]->comment_four}}</b></p>
 </div>
 </div>
 </div>
                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                <div class="col text-left">
                  

                  <button type="button" class="btn btn-default" >Close</button>

                  </div><!--col-->

                    <div class="col text-right">

                    <input type="submit" class="btn btn-primary" id="challan" value="send">

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    </form>

        </div>
    
      </div>
    </div>
    <div class="modal fade" id="ModalItem" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="section">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

@endsection