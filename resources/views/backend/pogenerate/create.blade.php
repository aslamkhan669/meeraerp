@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.quotation.pomanagement') . ' | ' . __('labels.backend.access.quotation.createpo'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="module" type="hidden" value="PurchaseManagement">
                    <input name="action" id="action" type="hidden" value="PurchasePO">
                    <input name="activity" id="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
Purchase PO                            
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                 
                </div><!--col-->
                
                </div><!--form-group-->

                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                    </div>
                </div>
                <div class="row  mb-4">
                    <div class="col-md-12">
                    <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">PO No.</label>

                            <div class="col-md-4">
                            <select class="form-control po_no" name="po_no">
                                <option value="">Select</option>
                                @foreach($poinfo as $data)
                                        <option value="{{$data->id}}" data-id="{{$data->id}}">PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$data->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$data->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$data->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$data->id),-2)}}</option>
                                @endforeach

                                </select>
                            </div><!--col-->
                        </div><!--form-group-->

                       
                     
                        
                    </div>

                </div>
              

                <div class="row mt-4">
    <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                    <th></th>
                        <th>S.NO.</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Unit PP</th>
                        <th>GST</th>
                        <th>Unit SP</th>
                        <th>Qty</th>
                        <th>Grand Total</th>
                        <th>Select Item</th>
                        
                        
                    </tr>
                    </thead>
                    <tbody  class="data">
                    
                 
                    </tbody>
                    </table>
                    </div>
                    </div>
                    </div> 

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.pogenerate.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                  

                    <button type="button" data-id="" id=send class="btn btn-success send">Send</button>

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    
@endsection