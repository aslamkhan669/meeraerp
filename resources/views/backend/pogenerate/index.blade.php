@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.purchaseaccount.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Purchase Order <small class="text-muted"></small>
                </h4>
            </div><!--col-->
            <div class="col-sm-7">
                @include('backend.pogenerate.includes.header-buttons')
            </div><!--col-->
           
        </div><!--row-->
        <form action="searchpo" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Supplier PO No" style="width: 20%; display: inline;">

    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            
                        <th>{{ __('labels.backend.access.poformattings.table.min_po_no') }}</th>
                        <th>Suplier Name</th>
                        <th>Supplier PO NO</th>
                            <th>Date&Time</th>
                            <th>Term&condition</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($pogenerates as $poformat)
                            <tr>
                            <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$poformat->poFormatting->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$poformat->poFormatting->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$poformat->poFormatting->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poformat->poFormatting->id),-2)}}</td>
                               <td>{{$poformat->poFormatting->quotation->queries->queryItems[0]->priceMappings[0]->supplier->supplier_name}}</td>
                               <td>PO/{{ 'V'.$sa=substr(strtoupper($brandletters='V0'.$poformat->poFormatting->quotation->queries->queryItems[0]->priceMappings[0]->supplier_id),-2)}}/{{$poformat->poFormatting->quotation->queries->queryItems[0]->priceMappings[0]->supplier->pricing_center}}/{{substr(str_replace("-","",explode(" ",$poformat->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poformat->id),-2)}}</td>   
                                <td>{{$sa=substr(strtoupper($poformat->updated_at),0,-3)}}</td>
                                <td> <span><a class="btn btn-sm btn-success" href="{{ url('admin/pogenerate/show/') }}/<?=$poformat->poFormatting->id?>">VIEW</a></span>
                                <td><span><a class="btn btn-sm btn-info" href="{{ url('admin/pogenerate/pdf/') }}/<?=$poformat->poFormatting->id?>">Pdf</a></span></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $pogenerates->total() !!} {{ trans_choice('labels.backend.access.brand.table.total', $pogenerates->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $pogenerates->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
@endsection
