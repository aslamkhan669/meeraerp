@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.paymentmodes.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.paymentmodes.management') }} <small class="text-muted">{{ __('labels.backend.access.paymentmodes.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.paymentmode.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            
                            <th>{{ __('labels.backend.access.paymentmodes.table.mode') }}</th>
                            
                            <th>{{ __('labels.backend.access.paymentmodes.table.is_active') }}</th>
                            <th>Date&Time</th>

                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($paymentmodes as $client)
                            <tr>
                                
                                <td>{{ $client->mode }}</td>
                                
                                <td>{{ $client->is_active==1 ? 'Active' : 'Not Active' }}</td>
                                <td>{{$sa=substr(strtoupper($client->updated_at),0,-3)}}</td>
                                <td>{!! $client->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $paymentmodes->total() !!} {{ trans_choice('labels.backend.access.paymentmodes.table.total', $paymentmodes->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $paymentmodes->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
