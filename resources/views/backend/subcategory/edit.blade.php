@extends ('Frontend.user.layouts.app')

@section ('title', __('labels.frontend.access.categories.management') . ' | ' . __('labels.frontend.access.categories.edit'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="CategoryManagement">
                    <input name="action" type="hidden" value="SubCategory">
                    <input name="activity" type="hidden" value="update">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.subcategories.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.subcategories.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                  
                        <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.product.product_name'))->class('col-md-2 form-control-label')->for('product_name') }}

                        <div class="col-md-4">
                        <select class="form-control" name="product_id">
                        <option value="">Select</option>     
                        @foreach($productname as $product)
                                 <option value="{{$product->id}}">{{$product->product_name}}</option>
                                 @endforeach
                                </select>
                        </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.category'))->class('col-md-2 form-control-label')->for('category') }}
                            <div class="col-md-4">
                            <select class="form-control" name="parent_id" id="parentcat">
                            <option value="">Select</option>
                                 @foreach($categories as $category)
                                 <option value="{{$category->id}}">{{$category->category_name}}</option>
                                 @endforeach
              
                                     </select>
                                     </div>
                        </div><!--form-group--> 
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.subcategory'))->class('col-md-2 form-control-label')->for('subcategory') }}

                            <div class="col-md-4">
                                {{ html()->text('category_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.subcategories.subcategory'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.subcategory_code'))->class('col-md-2 form-control-label')->for('subcategory_code') }}

                            <div class="col-md-4">
                            <input name="code" type="text" class="form-control" value="{{category->id}}" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                       
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.hsn_code'))->class('col-md-2 form-control-label')->for('subcategory_code') }}

                            <div class="col-md-4">
                                {{ html()->text('hsn_code')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.subcategories.hsn_code'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.active'))->class('col-md-2 form-control-label')->for('active') }}

                                <div class="col-md-4">
                                <label class="switch switch-3d switch-primary">
                                        {{ html()->checkbox('is_active', true, '1')->class('switch-input') }}
                                        <span class="switch-label"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div><!--col-->
                        </div><!--form-group-->
                      
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.image'))->class('col-md-2 form-control-label')->for('image') }}

                            <div class="col-md-4">
                                {{ html()->file('image')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
 
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->
        <div class="card-footer">
            <div class="row">
                <div class="col">
                {{ form_cancel(route('admin.subcategory.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                     {{ form_submit(__('buttons.general.crud.update')) }} 
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
