@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.subcategories.management') . ' | ' . __('labels.backend.access.subcategories.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.subcategory.store'))->class('form-horizontal')->attributes(array('enctype'=>'multipart/form-data'))->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="CategoryManagement">
                    <input name="action" type="hidden" value="SubCategory">
                    <input name="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.subcategories.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.subcategories.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                  
                        <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('validation.attributes.backend.access.product.product_name'))->class('col-md-2 form-control-label')->for('product_name') }}

                        <div class="col-md-4">
                        <select class="form-control" name="product_id">
                        <option value="">Select</option>     
                        @foreach($productname as $product)
                                 <option value="{{$product->id}}">{{$product->product_name}}</option>
                                 @endforeach
                                </select>
                        </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.category'))->class('col-md-2 form-control-label')->for('category') }}
                            <div class="col-md-4">
                            <select class="form-control" name="parent_id" id="parentcat">
                            <option value="">Select</option>
                                 @foreach($categories as $category)
                                 <option value="{{$category->id}}">{{$category->category_name}}</option>
                                 @endforeach
              
                                     </select>
                                     </div>
                        </div><!--form-group--> 
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.subcategory'))->class('col-md-2 form-control-label')->for('subcategory') }}

                            <div class="col-md-4">
                                {{ html()->text('category_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.subcategories.subcategory'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.subcategory_code'))->class('col-md-2 form-control-label')->for('subcategory_code') }}

                            <div class="col-md-4">
                            <input name="code" type="hidden" value="<?=$code?>">
                                {{ html()->text('')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.subcategories.subcategory_code'))
                                    ->attribute('maxlength', 191)
                                    ->readonly()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                       
                        <div class="form-group row">
                    
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.active'))->class('col-md-2 form-control-label')->for('active') }}

                                <div class="col-md-4">
                                <label class="switch switch-3d switch-primary">
                                        {{ html()->checkbox('is_active', true, '1')->class('switch-input') }}
                                        <span class="switch-label"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div><!--col-->
                        </div><!--form-group-->
                      
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.subcategories.image'))->class('col-md-2 form-control-label')->for('image') }}

                            <div class="col-md-4">
                                {{ html()->file('image')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
 
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.subcategory.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    //hide on load
    $('.unshow').hide();

    $('select[name="product_id"]').on('change', function() {
        
        var product_id = $(this).val();
        
        var isparent = $("#isParent").val()=="1"?true:false;
        debugger;
            if(product_id && !isparent) {
                $.ajax({
                    url: '/admin/parentcategories/ajax/'+encodeURI(product_id),
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        var html = "<option>Select</option>";
                        for(var i=0;i<data.length;i++){ 
                            html+="<option value="+data[i].id+">"+data[i].category_name+"</option>";
                        }
                        $("#parentcat").html(html);
                        
                        
                    }
                });
            }
            else{
                $('select[name="state"]').empty();
            }
    });

    $('select[name="category_id"]').on('change',function(){
        
        alert('change2');
        var cat_id=$(this).val();
        $('#cat_id').val(cat_id);

    })
  
            
           
});
$(document).ready(function(){
    
    var clients = [
        { "Name": "Otto Clay", "Age": 25, "Country": 1, "Address": "Ap #897-1459 Quam Avenue", "Married": false },
        { "Name": "Connor Johnston", "Age": 45, "Country": 2, "Address": "Ap #370-4647 Dis Av.", "Married": true },
        { "Name": "Lacey Hess", "Age": 29, "Country": 3, "Address": "Ap #365-8835 Integer St.", "Married": false },
        { "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
        { "Name": "Ramona Benton", "Age": 32, "Country": 3, "Address": "Ap #614-689 Vehicula Street", "Married": false }
    ];
 
    var countries = [
        { Name: "", Id: 0 },
        { Name: "United States", Id: 1 },
        { Name: "Canada", Id: 2 },
        { Name: "United Kingdom", Id: 3 }
    ];
 
    $("#queryItems").jsGrid({
        width: "100%",
        height: "400px",
 
        inserting: true,
        editing: true,
        sorting: true,
        paging: true,
 
        data: clients,
 
        fields: [
            { name: "Name", type: "text", width: 150, validate: "required" },
            { name: "Age", type: "number", width: 50 },
            { name: "Address", type: "text", width: 200 },
            { name: "Country", type: "select", items: countries, valueField: "Id", textField: "Name" },
            { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
            { type: "control" }
        ]
    });
})
</script>