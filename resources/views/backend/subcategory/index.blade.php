@extends ('backend.layouts.app')


@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            @include('backend.subcategory.includes.header-buttons')

            </div><!--col-->
        </div><!--row-->
        <form action="searchsub" method="post">
@csrf
<input type="text" value="" class="form-control search-box"  autocomplete="off" placeholder="Enter category name" style="width: 30%; display: inline;">
    <input type="text" value="" class="form-control"  name="term" placeholder="Enter Subcat name" style="width: 30%; display: inline;">
    <div class="suggestion-box"></div>
                                <input type="hidden" class="parent_id" name="parent_id">
    <button type="submit" class="btn btn-md btn-success" value="Search" style="margin-top: -5px;"><i class="fa fa-search"></i></button>
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>{{ __('labels.backend.access.subcategories.table.product_name') }}</th>
                        <th>{{ __('labels.backend.access.subcategories.table.category') }}</th>
                            <th>{{ __('labels.backend.access.subcategories.table.subcategory') }}</th>
                            <th>{{ __('labels.backend.access.subcategories.table.subcategory_code') }}</th>
                            <th>{{ __('labels.backend.access.subcategories.table.image') }}</th>
                            <th>{{ __('labels.backend.access.subcategories.table.is_active') }}</th>
                            <th>Date&Time</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($subcategories as $subcategory)
                            <tr> 
                             <td>{{ $subcategory->product->product_name }}</td>
                            <td>{{ $subcategory->category->category_name }}</td> 
                                <td>{{ $subcategory->category_name }}</td> 
                                <td><?= 'S'.$sa=substr(strtoupper($brandletters='S000'.$subcategory->id),-4);?></td> 
                                <td class="image-hover"><img src="{{url($subcategory->image)}}" width="50px" height="50px"/></td>                                                                                                                        
                                <td>{{ $subcategory->is_active==1?'Active':'Not Active' }}</td> 
                                <td>{{$sa=substr(strtoupper($subcategory->updated_at),0,-3)}}</td>   
                                <td>{!! $subcategory->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
              <label class="control-label">{!! $subcategories->total() !!} {{ trans_choice('labels.backend.access.subcategories.table.total', $subcategories->total()) }}</label>  

                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                {!! $subcategories->render() !!}

                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function (){
    $('.search-box').on('keyup',function(){
        debugger;
        var keyword=$(this).val();
        if(keyword.length>2){	
        $.ajax({
            url:'/admin/parents/ajax/' + encodeURI(keyword),
            type:'get',
            dataType:'JSON',
            success:function(data){
           debugger; 
                $(".suggestion-box").show();
               var html='';
               html+='<ul id="pro-list">';
               for (var i = 0; i < data.length; i++) {
               html+='<li style="text-align: center;" class="selectCategory" data-id='+data[i].id+'>'+data[i].category_name+'</li>';
               }
               html+='</ul>';
               $(".suggestion-box").html(html);  
               $(".selectCategory").on("click",function(e){
                   $(".search-box").val($(this).text());
                   $(".parent_id").val($(this).attr('data-id'));
                   $(".suggestion-box").hide();
               })
               $(".search-box").css("background","#FFF");
                      }
        })
    }else{
        alert('3 keyword must entered.')
    }
        });



    });
        </script>
        