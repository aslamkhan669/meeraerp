@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.pricing.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.pricing.management') }} <small class="text-muted">{{ __('labels.backend.access.pricing.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
            <form action="searchpricing" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Query Number" style="width: 80%; display: inline;">
    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>{{ __('labels.general.actions') }}</th>
                        <th>{{ __('labels.backend.access.pricing.table.status') }}</th>             
                        <th>{{ __('labels.backend.access.queries.table.query_no') }}</th>
                            <th>{{ __('labels.backend.access.queries.table.center_code') }}</th>
                            <th>{{ __('labels.backend.access.pricing.table.query_center') }}</th>       
                            <th>{{ __('labels.backend.access.pricing.table.client_code') }}</th>
                            <th>{{ __('labels.backend.access.pricing.table.sales_center') }}</th> 
                            <th>Date&Time</th>
                            <th>Normal/On Call</th>
                        </tr>
                        </thead>
                        <tbody>
                        
                        @foreach ($queries as $query)
                            <tr>
                            <td><a class="btn btn-info" href="{{ url('admin/pricing/show/') }}/<?=$query->id?>">View</a></td>
                            @if(isset($query->quotations[0]))
                                @if($query->quotations[0]->is_confirmed =='0')
                                <td><span class="label label-info">Under Review</span></td>
                                @elseif($query->quotations[0]->is_confirmed =='1')
                                <td><span class="label label-success">Accepted</span></td>
                                @elseif($query->quotations[0]->is_confirmed =='2')
                                <td><span class="label label-warning">Rejected</span></td>
                               
                                @endif
                            @else
                                <td><span class="label label-danger">Not Priced</span></td>    
                            @endif
                            <td>QR/<?= 'M'.$sa=substr(strtoupper($brandletters='M0'.$query->client_id),-2);?>/<?= 'S'.$sa=substr(strtoupper($brandletters='S0'.$query->id),-2);?>/{{substr(str_replace("-","",explode(" ",$query->updated_at)[0]),2)}}-<?='0'.$sa=substr(strtoupper($brandletters='00'.$query->id),-2);?></td>
                            <td>{{'C'.$sa=substr(strtoupper($brandletters='C0'.$query->CompanyDetail->id),-2)}}</td>
                            <td>{{'QR'.$sa=substr(strtoupper($brandletters='QR00'.$query->query_center),-2)}}</td>
                            <td>{{ 'M'.$sa=substr(strtoupper($brandletters='M0'.$query->client_id),-2)}}</td>
                            <td>{{'S'.$sa=substr(strtoupper($brandletters='S0'.$query->client->sales_center),-2)}}</td>
                            <td>{{$sa=substr(strtoupper($query->updated_at),0,-3)}}</td>   
                            <td>{{ $query->normal_oncall }}</td>

                
                           
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                   <label class="control-label"> {!! $queries->total() !!} {{ trans_choice('labels.backend.access.queries.table.total', $queries->total()) }}</label>
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $queries->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
