@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.companydetails.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.companydetails.management') }} <small class="text-muted">{{ __('labels.backend.access.colors.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.company.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            
                            <th>{{ __('labels.backend.access.companydetails.table.center_name') }}</th>
                            <th>{{ __('labels.backend.access.companydetails.table.center_code') }}</th>
                            <th>{{ __('labels.backend.access.companydetails.table.contact_Person') }}</th>
                            <th>{{ __('labels.backend.access.companydetails.table.phone_no') }}</th>
                            <th>{{ __('labels.backend.access.companydetails.table.email_id') }}</th>
                            <th>{{ __('labels.backend.access.companydetails.table.is_active') }}</th>
                            <th>Date&Time</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($companydetails as $companydetail)
                            <tr>
                                
                                <td>{{ $companydetail->company_name }}</td>
                                <td>{{'C'.$sa=substr(strtoupper($brandletters='C0'.$companydetail->id),-2)}}</td>
                                <td>{{ $companydetail->contact_person }}</td>
                                <td>{{ $companydetail->phone_no }}</td>
                                <td>{{ $companydetail->email }}</td>

                                <td>{{ $companydetail->reg_type==1?'Registered Office':'Branch Office' }}</td>    
                                <td>{{$sa=substr(strtoupper($companydetail->updated_at),0,-3)}}</td>
                         
                                <td>{!! $companydetail->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $companydetails->total() !!} {{ trans_choice('labels.backend.access.brand.table.total', $companydetails->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $companydetails->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
