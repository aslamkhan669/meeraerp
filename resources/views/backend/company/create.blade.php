@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.companydetails.management') . ' | ' . __('labels.backend.access.colors.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.companydetail.store'))->class('form-horizontal')->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="Setting">
                    <input name="action" type="hidden" value="CompanyDetail">
                    <input name="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.companydetails.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.companydetails.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                 
                </div><!--col-->
                
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.center_name'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('company_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.center_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                      
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.contact_person'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.contact_person'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.phone_no'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('phone_no')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.phone_no'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.email'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.email'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.pan_no'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('pan_no')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.pan_no'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.mobile'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('mobile')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.mobile'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.delivery_address'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('delivery_address')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.delivery_address'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.address'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('address')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.address'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.aadhar_no'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('aadhar_no')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.aadhar_no'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.gst_no'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('gst_no')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.gst_no'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.state_code'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('state_code')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.companydetails.state_code'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                       
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.companydetails.reg_type'))->class('col-md-2 form-control-label')->for('is_active') }}

                                <div class="col-md-4">
                                    <label class="switch switch-3d switch-primary">
                                        {{ html()->checkbox('reg_type', true, '1')->class('switch-input') }}
                                        <span class="switch-label"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div><!--col-->
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.companydetail.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection