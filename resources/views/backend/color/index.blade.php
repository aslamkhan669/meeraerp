@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.colors.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.colors.management') }} <small class="text-muted">{{ __('labels.backend.access.colors.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.color.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            
                            <th>{{ __('labels.backend.access.colors.table.color') }}</th>
                            
                            <th>{{ __('labels.backend.access.colors.table.is_active') }}</th>
                            <th>Date&Time</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($colors as $col)
                            <tr>
                                
                                <td>{{ $col->color }}</td>
                                
                                <td>{{ $col->is_active==1 ? 'Active' : 'Not Active' }}</td>

                               

                                <td>{{$sa=substr(strtoupper($col->updated_at),0,-3)}}</td>
                                <td>{!! $col->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $colors->total() !!} {{ trans_choice('labels.backend.access.brand.table.total', $colors->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $colors->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
