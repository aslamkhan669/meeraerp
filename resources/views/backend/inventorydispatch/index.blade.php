@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.colors.inventorydata'))

@section('content')
<div class="modal fade" id="myModalItem" role="dialog">
    <div class="modal-dialog" style="max-width: 960px!important;">
    
      <!-- Modal content-->
<div class="modal-content" >
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                  Dispatch Inventory Management <small class="text-muted"></small>
                </h4>
            </div><!--col-->

            
        </div><!--row-->
        <form action="searchinvetorydispatch" method="post">
@csrf
    <input type="text" class="search-item"  autocomplete="off" placeholder="Enter Item No">
    <input type="text" class="search-box"  autocomplete="off" placeholder="Enter Description">
    <div class="suggestion-box"></div>
                                <input type="hidden" class="poregisterid" name="description">
                                <input type="hidden" class="poregister" name="term">

    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Product Owner</th>
                            <th>Reason</th>
                            <th>Qty</th>
                            <th>Unit PP</th>
                            <th>Inventory Amount Value</th>
                            <th>Bin No</th>
                            <th>Bin Location</th>
                            <th>Date&Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($inventory as $data)
                            <tr>
                            <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{$data->dispatchRegister->poItem->queryItem->item_id}}" data-toggle="modal" data-target="#myModalItem">View</button></td>
                            <td class="image-hover"><img src="{{url('/uploads/'.$data->dispatchRegister->poItem->queryItem->image)}}" width="50px" height="50px"/></td>   

                            <td>{{$data->dispatchRegister->user->first_name}}</td>
                                <td>{{ $data->reason }}</td>
                                <td>{{ $data->extra_qty }}</td>
                                <td>{{ $data->dispatchRegister->poItem->queryItem->priceMappings[0]->discounted_price}}</td>
                                <td>{{$data->extra_qty * $data->dispatchRegister->poItem->queryItem->priceMappings[0]->discounted_price}}</td>
                                <td>{{ $data->bill_no }}</td>
                                <td>{{ $data->location }}</td>
                                <td>{{ $data->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                   
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                   
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function (){
    $('.search-box').on('keyup',function(){
        debugger;
        var keyword=$(this).val();
        if(keyword.length>2){	
        $.ajax({
            url:'/admin/itemsearch/ajax/' + encodeURI(keyword),
            type:'get',
            dataType:'JSON',
            success:function(data){
                debugger;
                $(".suggestion-box").show();
               var html='';
               html+='<ul id="pro-list">';
               for (var i = 0; i < data.length; i++) {
               html+='<li style="text-align: center;" class="selectItem" data-id='+data[i].disregisterid+'>'+data[i].description+'</li>';
               }
               html+='</ul>';
               $(".suggestion-box").html(html);  
               $(".selectItem").on("click",function(e){
                   $(".search-box").val($(this).text());
                   $(".poregisterid").val($(this).attr('data-id'));
                   $(".suggestion-box").hide();
               })
               $(".search-box").css("background","#FFF");
                      }
        })
    }else{
        alert('3 keyword must entered.')
    }
        });
//item no
$('.search-item').on('keyup',function(){
        debugger;
        var keyword=$(this).val();
        if(keyword.length>0){	
        $.ajax({
            url:'/admin/itemnosearch/ajax/' + encodeURI(keyword),
            type:'get',
            dataType:'JSON',
            success:function(data){
                debugger;
                $(".suggestion-box").show();
               var html='';
               html+='<ul id="pro-list">';
               for (var i = 0; i < data.length; i++) {
               html+='<li style="text-align: center;" class="selectItemno" data-id='+data[i].disregisterid+'>'+data[i].item_id+'</li>';
               }
               html+='</ul>';
               $(".suggestion-box").html(html);  
               $(".selectItemno").on("click",function(e){
                   $(".search-item").val($(this).text());
                   $(".poregister").val($(this).attr('data-id'));
                   $(".suggestion-box").hide();
               })
               $(".search-box").css("background","#FFF");
                      }
        })
    }else{
        alert('1 keyword must entered.')
    }
        });
    });
        </script>
        