@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.purchaseaccount.management') . ' | ' . __('labels.backend.access.uom.create'))

@section('breadcrumb-links')
@endsection

@section('content')


  <!-- Transfer Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div> -->
        <div class="modal-body">
          <label>Reason(Why):-</label>
          <input type="text" class="form-control" id="reason" value="">
          <label>Extra QTY:-</label>
          <input type="text" class="form-control" id="qty" value="">
          <input type="submit" id="transfer" value="Transfer">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


<div class="modal fade" id="myModalItem" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width: 900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.purchaseaccount.management') }}
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                  
                <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                      
                      <th scope="col">Description</th>
                      <th scope="col">Specification</th>
                      <!-- <th scope="col">Color</th> -->
                      <th scope="col">Model No.</th>
                      <th scope="col">Brand</th>
                      <th scope="col">HSN Code</th>
                      <th scope="col">UOM</th>
                      <th scope="col">Remarks</th>
                    </tr>
                    </thead>
                    <tbody>

                 
                    <tr>
                    <td>{{$inventorydata[0]->dispatchRegister->poItem->queryItem->description}}</td>
                    <td class="image-hover"><img src="{{url($inventorydata[0]->dispatchRegister->poItem->queryItem->image)}}" width="50px" height="50px"/></td>                                                                                                                      
                    <td>{{$inventorydata[0]->dispatchRegister->poItem->queryItem->model_no}}</td>
                    <td>{{$inventorydata[0]->dispatchRegister->poItem->queryItem->brand->brand_name}}</td>
                    <td>{{$inventorydata[0]->dispatchRegister->poItem->queryItem->itemList->hsn_code}}</td>
                    <td>{{$inventorydata[0]->dispatchRegister->poItem->queryItem->uom->unit}}</td>
                    <td>{{$inventorydata[0]->dispatchRegister->poItem->queryItem->remarks}}</td>
                    
                    </tr>
                  
                    </tbody>

                    </table>
                    </div>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
              
</div><!--card-body-->
            <div class="card-footer clearfix">
                <div class="row">
                  

                   
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}

  </div>
@endsection