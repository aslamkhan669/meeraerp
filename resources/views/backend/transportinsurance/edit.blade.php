@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.transportinsurances.management') . ' | ' . __('labels.backend.access.colors.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($transportinsurance, 'PATCH', route('admin.transportinsurance.update', $transportinsurance->id))->class('form-horizontal')->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="Setting">
                    <input name="action" type="hidden" value="TransportInsurance">
                    <input name="activity" type="hidden" value="update">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.transportinsurances.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.transportinsurances.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                  
                </div><!--col-->
                
                        </div><!--form-group-->

                                       <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.freights.condition'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                            {{ html()->text('conditions')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.freights.condition'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.transportinsurance.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection