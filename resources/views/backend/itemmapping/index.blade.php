@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.item.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Item Pricing <small class="text-muted">{{ __('labels.backend.access.item.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->
        <form action="searchitems" method="post">
@csrf
    <input type="text" value=""  name="term" placeholder="Enter Description">
    <input type="text" value=""  name="specif" placeholder="Enter Specification">
    <input type="text" value=""  name="hsncode" placeholder="Enter HSN Code">
    <input type="text" value=""  name="modelno" placeholder="Enter Model No">
    <input type="text" class="search-box"  autocomplete="off" placeholder="Enter Product Name">
    <input type="text" class="search-brand"  autocomplete="off" placeholder="Enter Brand">
    <input type="text" value=""  name="itemno" placeholder="Enter Item No">
                                <div class="suggestion-box"></div>
                                <input type="hidden" class="product_id" name="product_id">
                                <input type="hidden" class="brand_id" name="brand_id">
                                
    <input type="submit" value="Search">
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            
                            
                            <th>{{ __('labels.backend.access.item.table.item_code') }}</th> 
                            <th>{{ __('labels.backend.access.item.table.product_name') }}</th>
                            <th>{{ __('labels.backend.access.item.table.product_owner') }}</th>
                            <th>{{ __('labels.backend.access.item.table.category_name') }}</th>
                            <th>SubCategory Name</th>
                             <th>{{ __('labels.backend.access.item.table.description') }}</th>
                             <th>{{ __('labels.backend.access.item.table.specification') }}</th>
                             <th>HSN Code</th>
                             <th>{{ __('labels.backend.access.item.table.image') }}</th> 
                             <th>Date&Time</th>
                            <th>{{ __('labels.backend.access.item.table.is_active') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($itemmapping as $standobj)
                            <tr>
                                
                                <td>{{'P'.$sa=substr(strtoupper($brandletters='P00'.$standobj->product_id),-2)}}{{'I'.$sa=substr(strtoupper($brandletters='I000000'.$standobj->id),-6)}}</td>
                                <td>{{ $standobj->product->product_name }}</td> 
                                <td>{{ $standobj->product->product_owner }}</td>
                                <td>{{ $standobj->category->category->category_name }}</td> 
                                <td>{{ $standobj->category->category_name }}</td>
                                <td>{{ $standobj->description }}</td> 
                                <td>{{ $standobj->specification }}</td> 
                                <td>{{ $standobj->hsn_code }}</td> 
                                <td class="image-hover"><img src="../../../{{$standobj->image }}" width="50px" height="50px"/></td>                          
                                <td>{{$sa=substr(strtoupper($standobj->updated_at),0,-3)}}</td>

                                @if(isset($standobj))
                                @if($standobj->status =='0')
                                <td><Button class="btn btn-danger">Not Priced</Button></td>
                                @else($standobj->status =='1')
                                <td><Button class="btn btn-success">Accepted</Button></td>
                                @endif
                            @else
                                <td><Button class="btn btn-danger">Not Priced</Button></td>    
                            @endif 
                            @if(isset($standobj))
                            @if($standobj->status =='1')

                            <td><a class="button" href="itemmapping/<?=$standobj->id?>/edit">View</a></td>                         
                            @else($standobj->status =='0')

                                 <td><a class="button" href="{{ url('admin/itemmapping/show/') }}/<?=$standobj->id?>">Create</a></td>
                                 @endif
                                 @else
                                <td><Button class="btn">Not Priced</Button></td>    
                            @endif 
                                </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $itemmapping->total() !!} {{ trans_choice('labels.backend.access.brand.table.total', $itemmapping->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $itemmapping->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function (){
    $('.search-box').on('keyup',function(){
        debugger;
        var keyword=$(this).val();
        if(keyword.length>2){	
        $.ajax({
            url:'/admin/products/ajax/' + encodeURI(keyword),
            type:'get',
            dataType:'JSON',
            success:function(data){
                $(".suggestion-box").show();
               var html='';
               html+='<ul id="pro-list">';
               for (var i = 0; i < data.length; i++) {
               html+='<li style="text-align: center;" class="selectProduct" data-id='+data[i].id+'>'+data[i].product_name+'</li>';
               }
               html+='</ul>';
               $(".suggestion-box").html(html);  
               $(".selectProduct").on("click",function(e){
                   $(".search-box").val($(this).text());
                   $(".product_id").val($(this).attr('data-id'));
                   $(".suggestion-box").hide();
               })
               $(".search-box").css("background","#FFF");
                      }
        })
    }else{
        alert('3 keyword must entered.')
    }
        });
        //brand search
$('.search-brand').on('keyup',function(){
        debugger;
        var keyword=$(this).val();
        if(keyword.length>2){	
        $.ajax({
            url:'/admin/brands/ajax/' + encodeURI(keyword),
            type:'get',
            dataType:'JSON',
            success:function(data){
                $(".suggestion-box").show();
               var html='';
               html+='<ul id="pro-list">';
               for (var i = 0; i < data.length; i++) {
                   debugger;
               html+='<li style="text-align: center;" class="selectBrand" data-id='+data[i].id+'>'+data[i].brand_name+'</li>';
               }
               html+='</ul>';
               $(".suggestion-box").html(html);  
               $(".selectBrand").on("click",function(e){
                   $(".search-brand").val($(this).text());
                   $(".brand_id").val($(this).attr('data-id'));
                   $(".suggestion-box").hide();
               })
               $(".search-brand").css("background","#FFF");
                      }
        })
    }else{
        alert('3 keyword must entered.')
    }
        });
    });
        </script>
        