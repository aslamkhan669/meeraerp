@extends ('backend.layouts.app')

@section ('title', __('Item List Price') . ' | ' . __('Item List Price'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.itemmapping.store'))->class('form-horizontal')->open() }}

<!-- Modal -->
<div class="modal fade" id="mysupplier" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 180%;">
        <div class="modal-header">
          <h4 class="modal-title"> Supplier</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
    <th scope="col">Supplier Type</th>
    <th scope="col">Description</th>
      <th scope="col">List price/cost</th>
      <th scope="col">Insurance&Shipping</th>
      <th scope="col">Custom&Clearance</th>
      <th scope="col">Discount</th>
      <th scope="col">PurchaseUnitPrice</th>
      <th scope="col">GST</th>
    </tr>
  </thead>
   
<tbody class="history" >
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <div class="modal fade" id="ModalItem" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="card-title mb-0">
                        <div>
                        </div>
                        Item Pricing                           
                        </h4>
                        <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Item No.</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Specification</th>
                            <th>Model No.</th>
                            <th>Color</th>
                            <th>Brand</th>
                            <th>HSN Code</th>
                            <th>UOM</th>
                            <th>Remarks</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($itemlists as $standobj)
                            <tr>
                                <td>{{'P'.$sa=substr(strtoupper($brandletters='P00'.$standobj->product_id),-2)}}{{'I'.$sa=substr(strtoupper($brandletters='I0000'.$standobj->id ),-4)}}</td>
                                <td class="image-hover"><img src="{{url($standobj->image)}}" width="50px" height="50px"/></td>                                                                                                                        
                                <td>{{ $standobj->description }}</td>
                                <td>{{ $standobj->specification }}</td>
                                <td>{{ $standobj->model_no }}</td>
                                <td>{{ $standobj->Color->color }}</td>
                                <td>{{ $standobj->Brand->brand_name }}</td>
                                <td>{{ $standobj->hsn_code }}</td>
                                <td>{{ $standobj->Uom->unit }}</td>
                                <td>{{ $standobj->remarks }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <input type='hidden' class='form-control ' name='Pid' value='{{$id}}'>
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="ItemManagement">
                    <input name="action" type="hidden" value="ItemPricing">
                    <input name="activity" type="hidden" value="create">
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Supplier/Code</th>
                            <th>Contact/Address</th>      
                            <th>Suppliers</th>      
                            <th>List Price/Cost</th>
                            <th>Insurance & Shipping</th>
                            <th>Custom & Clearance</th>
                            <th>Discount(%)</th>
                            <th>PurchaseUnitPrice</th>
                            <th>GST(%)</th>
                            <th>TotalAmount</th>
                            <th>Quantity</th>
                            <th>GrandAmount</th>
                            <th>Delivery Terms</th>
                            <th>Warranty</th>
                            <th>Std Packaging</th>
                        </tr>
                        </thead>
                        <tbody>
                        
                        @foreach($iteminfo as $standobj)
                       @if(strtolower($standobj->supplier->supplier_type) =='overseas')
                      
                                
                            <tr>
                            <input type='hidden' class="form-control" name='item_id[]' value='{{$id}}'>
                              <input type='hidden' class="form-control" name='supplier_id[]' value='{{$standobj->supplier_id}}'>
                                <td>{{$standobj->supplier->supplier_name}}/{{'V'.$sa=substr(strtoupper($brandletters='V00'.$standobj->supplier_id),-3)}}</td>
                                <td>{{$standobj->supplier->mobile}}/{{$standobj->supplier->street}}{{$standobj->supplier->landmark}}{{$standobj->supplier->sector}}{{$standobj->supplier->city}}</td>                                                                                                                        
                                <td><button type='button' class='form-control suppinfo' data-toggle='modal' supplierId='{{$standobj->supplier_id}}' data-target='#mysupplier' >History</button></td>
                                <td><input type='text' class="form-control listprice" name='listprice[]' value='{{$standobj->listprice}}'></td>
                                <td><input type='text' class="form-control insurance" name='insurance_shipping[]' value='{{$standobj->insurance_shipping}}'></td>
                                <td><input type='text' class="form-control clearance" name='custom_clearance[]' value='{{$standobj->custom_clearance}}'></td>
                                <td><input type='text' class="form-control discount" name='discount[]' value='{{$standobj->discount}}' readonly></td>
                                <td><input type='text' class="form-control discounted_price" name='discounted_price[]' value='{{$standobj->discounted_price}}' readonly></td>
                                <td><input type='text' class="form-control gst" name='gst[]' value='{{$standobj->gst}}'></td>
                                <td><input type='text' class="form-control total" name='total[]' value='{{$standobj->total}}' readonly></td>
                                <td><input type='text' class="form-control qty" name='quantity[]' value='{{$standobj->quantity}}'></td>
                                <td><input type='text' class="form-control grand_amount" name='grand_amount[]' value='{{$standobj->grand_amount}}'></td>
                                <td><input type='text' class="form-control delivery_term" name='delivery_term[]' value='{{$standobj->delivery_term}}'></td>
                                <td><input type='text' class="form-control warranty" name='warranty[]' value='{{$standobj->warranty}}'></td>
                                <td><input type='text' class="form-control standard_package" name='standard_package[]' value='{{$standobj->standard_package}}'></td>
                            </tr>
                        
                        @else
                            <tr>
                            <input type='hidden' class="form-control" name='item_id[]' value='{{$id}}'>
                              <input type='hidden' class="form-control" name='supplier_id[]' value='{{$standobj->supplier_id}}'>
                                <td>{{$standobj->supplier->supplier_name}}/{{'V'.$sa=substr(strtoupper($brandletters='V00'.$standobj->supplier_id),-3)}}</td>
                                <td>{{$standobj->supplier->mobile}}/{{$standobj->supplier->street}}{{$standobj->supplier->landmark}}{{$standobj->supplier->sector}}{{$standobj->supplier->city}}</td>                                                                                                                        
                                <td><button type='button' class='form-control suppinfo' data-toggle='modal' supplierId='{{$standobj->supplier_id}}' data-target='#mysupplier' >History</button></td>
                                <td><input type='text' class="form-control listprice" name='listprice[]' value='{{$standobj->listprice}}'></td>
                                <td><input type='text' class="form-control insurance_shipping" name='insurance_shipping[]' value='{{$standobj->insurance_shipping}}'readonly></td>
                                <td><input type='text' class="form-control custom_clearance" name='custom_clearance[]' value='{{$standobj->custom_clearance}}' readonly></td>
                                <td><input type='text' class="form-control discount" name='discount[]' value='{{$standobj->discount}}'></td>
                                <td><input type='text' class="form-control discounted_price" name='discounted_price[]' value='{{$standobj->discounted_price}}' readonly></td>
                                <td><input type='text' class="form-control gst" name='gst[]' value='{{$standobj->gst}}'></td>
                                <td><input type='text' class="form-control total" name='total[]' value='{{$standobj->total}}' readonly></td>
                                <td><input type='text' class="form-control qty" name='quantity[]' value='{{$standobj->quantity}}'></td>
                                <td><input type='text' class="form-control grand_amount" name='grand_amount[]' value='{{$standobj->grand_amount}}' ></td>
                                <td><input type='text' class="form-control delivery_term" name='delivery_term[]' value='{{$standobj->delivery_term}}' ></td>
                                <td><input type='text' class="form-control warranty" name='warranty[]' value='{{$standobj->warranty}}'></td>
                                <td><input type='text' class="form-control standard_package" name='standard_package[]' value='{{$standobj->standard_package}}'></td>
                            </tr>
                        @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div><!--col-->
            
        </div><!--row-->
             </div>
            </div><!--card-body-->
            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.itemmapping.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                    </div>
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection

