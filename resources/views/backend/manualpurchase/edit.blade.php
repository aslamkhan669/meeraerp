@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.queries.management') . ' | ' . __('labels.backend.access.queries.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($manualpurchase, 'PATCH', route('admin.manualpurchase.update', $manualpurchase->id))->class('form-horizontal')->attributes(array('enctype'=>'multipart/form-data'))->open() }}

<input type="hidden" value="" name="item_id" id="item_id">

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.queries.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.queries.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                 
                </div><!--col-->
                
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.queries.center_code'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                        <select class="form-control" name="center_id" id="centercode">
                        <option value="">Select</option>
                            @foreach($companydetails as $company)
                            <option value="{{$company->id}}"{{$company->id==$centerinfo->id?"selected":""}}>C{{$company->id}}</option>
                            @endforeach

                                </select>
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.queries.client_code'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                        <select class="form-control clientcode" name="client_id" id="clientcode">
                            <option value="">Select</option>
                                @foreach($clients as $client)
                                <option value="{{$client->id}}"{{$client->id==$clientinfo->id?"selected":""}}>{{'M'.$sa=substr(strtoupper($brandletters='M0'.$client->id),-2)}}  ({{$client->client_name}})</option>
                                @endforeach

                                    </select>
                        </div><!--col-->
                        </div><!--form-group--> 
             

                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.queries.meera_center_detail'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('meera_center_code')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.queries.meera_center_detail'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('id', 'meera_center_code')
                                    ->readonly()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.queries.client_detail'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('client_detail')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.queries.client_detail'))
                                    ->attribute('class', 'client_detail')
                                    ->readonly()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.queries.po_no'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                            <input type="text" name="po_no" value="PO{{auth()->user()->id}}" class="form-control" readonly>
                            </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.queries.query_person'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                        <select class="form-control query_person" name="query_person" >
                        @foreach($clients as $client)
                                <option value="{{$client->contact_person1}}"{{$client->id==$clientinfo->id?"selected":""}}>{{$client->contact_person1}}</option>
                                @endforeach
                    </select> 
                        </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">

                         {{ html()->label(__('validation.attributes.backend.access.queries.sales_center'))->class('col-md-2 form-control-label')->for('unit') }}

                                <div class="col-md-4">
                                {{ html()->text('sales_center')
                                                                ->class('form-control')
                                                                ->placeholder(__('validation.attributes.backend.access.queries.sales_center'))
                                                                ->attribute('maxlength', 191)
                                                                ->attribute('class', 'sales_center')
                                                                ->readonly()
                                                                ->autofocus() }}
                                </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.queries.contact_no'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                        <select class="form-control contact_no" name="contact_no" >
                        @foreach($clients as $client)
                                <option value="{{$client->contact_number1}}"{{$client->id==$clientinfo->id?"selected":""}}>{{$client->contact_number1}}</option>
                                @endforeach
                    </select>  
                        </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">

                            {{ html()->label(__('validation.attributes.backend.access.queries.query_no'))->class('col-md-2 form-control-label')->for('unit') }}
                            <div class="col-md-4">
                            <input name="query_no" type="text" class="form-control" value="QR/<?= 'M'.$sa=substr(strtoupper($brandletters='M0'.$manualpurchase->client_id),-2);?>/<?= 'S'.$sa=substr(strtoupper($brandletters='S0'.$manualpurchase->client_id),-2);?>/{{substr(str_replace("-","",explode(" ",$manualpurchase->updated_at)[0]),2)}}-<?='0'.$sa=substr(strtoupper($brandletters='00'.$manualpurchase->id),-2);?>" readonly>

    
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.queries.query_center'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                            <input type="text" class="form-control" value="<?='QR'.$sa=substr(strtoupper($brandletters='QR00'.Auth::user()->id),-2);?>" readonly/>
                            <input type="hidden" value="{{Auth::user()->id}}" name="query_center">
                            </div><!--col-->
                            </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.queries.query_date'))->class('col-md-2 form-control-label')->for('is_active') }}

                        <div class="col-md-4">
                        <input  type="text" class="form-control" value="<?=$sa=substr(strtoupper($manualpurchase->updated_at),0,-3);?>" readonly>

                        </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.queries.quote_no'))->class('col-md-2 form-control-label')->for('unit') }}
                           
                                <div class="col-md-4">
                                <input name="quote_no" type="text" class="form-control" value="QT/<?= 'M'.$sa=substr(strtoupper($brandletters='M0'.$manualpurchase->client_id),-2);?>/{{$salescenter}}/{{substr(str_replace("-","",explode(" ",$salesupdate)[0]),2)}}-<?='0'.$sa=substr(strtoupper($brandletters='00'.$manualpurchase->id),-2);?>" readonly>
                                </div><!--col-->
                        </div><!--form-group-->
                       
                        <div class="row table-responsive">
    <div class="col-md-12">
        <button type="button" class="btn btn-primary" id="addItemsModal" data-toggle="modal" data-target="#addItemsModal" style="margin-bottom: 15px;">
 Old Add Items
</button>
                    <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>

                        <th>Description</th>
                        <th>Specification</th>
                        <th>Model No</th>
                        <th>HSN</th>
                        <th>QTY</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="queryItems">
                    <?php $i=0; ?>
                    @foreach($manualitems as $data)
                    <input type="hidden" value='{{$data->id}}' name="id[]">
                    <tr class='{{$i++}}'>
        <td><input type='hidden' class='i_item_code' name='item_code[]' value='{{$data->item_code}}'><input type='hidden' class='i_cat_id' name='cat_id[]' value='{{$data->cat_id}}' ><input type='hidden' class='i_category' name='category[]' value='{{$data->category}}' >
        <input type='hidden' class='i_product_name' name='product_name[]' value='{{$data->product_name}}' ><input type='hidden' class='i_items_id' name='item_id[]' value='{{$data->item_id}}' ><input type='hidden' class='i_color' name='color[]' value='{{$data->color}}'>
        <input type='hidden'  class='i_model_no' name='model_no[]' value='{{$data->model_no}}'><input type='hidden' class='i_brand_id' name='brand_id[]' value='{{$data->brand_id}}'><input type='hidden' class='i_hsn_code' name='hsn_code[]' value='{{$data->hsn_code}}' >
        <input type='hidden' class='i_unit' name='unit[]' value='{{$data->unit}}'><input type='hidden' class='i_query_qty' name='query_qty[]' value='{{$data->query_qty}}'><input type='hidden' class='i_standard_package' name='standard_package[]' value='{{$data->standard_package}}'><input type='hidden' class='i_remarks' name='remarks[]' value='{{$data->remarks}}'>
        <input type='hidden' class='i_specification' name='specification[]' value='{{$data->specification}}'><input type='hidden' class='i_description' name='description[]' value='{{$data->description}}'><input type='hidden' class='i_image' name='image[]' value='{{$data->image}}'>
         {{$data->description}}</td>
        <td>{{$data->specification}}</td>
        <td>{{$data->model_no}}</td>
        <td>{{$data->hsn_code}}</td>
        <td>{{$data->query_qty}}</td>
        <td> <button class="btn btn-primary edit" >Edit</button>
        <span class='delete'><a id="product_list_cart" class="btn btn-danger btn-md" data-id="{{$data->id}}">Delete</a></span></td>
        </tr>
        @endforeach
                    </tbody>
                    </table>
                    </div>
</div>
 </div>   
</div>

     </div><!--col-->
     
                </div><!--row-->
                
                </div><!--card-body-->

<div class="card-footer clearfix">
    <div class="row">
        <div class="col">
            {{ form_cancel(route('admin.query.index'), __('buttons.general.cancel')) }}
        </div><!--col-->

        <div class="col text-right">
            {{ form_submit(__('buttons.general.crud.create')) }}
        </div><!--col-->
    </div><!--row-->
</div><!--card-footer-->
</div><!--card-->
                {{ html()->form()->close() }}

<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-width: 850px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Query Items</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form role="myForm" id="myForm" method="post" enctype="multipart/form-data" action='backend/query/create'>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <div class="form-group row">
      {{ html()->label(__('validation.attributes.backend.access.item_query.image'))->class('col-md-2 form-control-label')->for('remarks') }}

<div class="col-md-4">
<input type="file" id="fileinput" name='image' value="image"/>
<input name="saveimage" type="hidden" id="saveimage">
<input type="hidden"  name="id[]" value='<?=$id=0;?>'>
</div><!--col-->
<div class="col-md-4 image">

 </div><!--col-->        
</div><!--form-group-->
      <div class="form-group row">
                           
                                {{ html()->label(__('validation.attributes.backend.access.item_query.product_name'))->class('col-md-2 form-control-label')->for('product_name') }}

                            <div class="col-md-4">
                            <select class="form-control" name="product_id" id="product_id">
                            <option value="">Select</option>     
                            @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->product_name}}</option>
                            @endforeach
                                    </select>
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.product.cat_name'))->class('col-md-2 form-control-label')->for('cat_name') }}         
                        <div class="col-md-4 unshowcat">
                    <div class="form-group">
                    <select class="form-control category" name="category_id" id="cat">
                   

                    </select>
                </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row"> 
               
                </div>      
                {{ html()->label(__('validation.attributes.backend.access.product.subcat_name'))->class('col-md-2 form-control-label unshowcat')->for('mobile') }}

            <div class="col-md-4 unshow">
                    <div class="form-group">
                    <select class="form-control category" name="subcategory_id" id="subcat">
                    

                    </select>
                </div><!--col-->
                </div><!--row-->
                {{ html()->label(__('validation.attributes.backend.access.item_query.item_description'))->class('col-sm-2 form-control-label')->for('cat_name') }}

<div class="col-md-4">
     <div class="form-group">
<select class="form-control category" name="item_id" id="item">
</select>

</div>
</div>
            </div><!--card-body-->
            <!-- test -->
                 <div class="form-group row">
             
          
                      </div>
            <!-- test end -->
            <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.item_query.qty'))->class('col-md-2 form-control-label')->for('qty') }}

<div class="col-md-4">
    {{ html()->text('qty')
        ->class('form-control')
        ->placeholder(__('validation.attributes.backend.access.item_query.qty'))
        ->attribute('maxlength', 191)
        ->required()
        ->autofocus() }}
</div>
               
                      </div><!--form-group-->
                      <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.item_query.description'))->class('col-md-2 form-control-label')->for('description') }}

                        <div class="col-md-4">
                            {{ html()->text('description')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.item_query.description'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.item_query.specification'))->class('col-md-2 form-control-label')->for('specification') }}

                            <div class="col-md-4">
                                {{ html()->text('specifications')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item_query.specification'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('id', 'specification')
                                    ->autofocus() }}
                            </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.item_query.color'))->class('col-md-2 form-control-label')->for('color') }}

                        <div class="col-md-4">
                        <select class="form-control category" name="color" id="color">
                        <option value="">Select</option>
                        @foreach($colors as $color)
                            <option value="{{$color->id}}">{{$color->color}}</option>
                            @endforeach
                        </select>
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.item_query.model_no'))->class('col-md-2 form-control-label')->for('model_no') }}

                        <div class="col-md-4">
                            {{ html()->text('model_no')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.item_query.model_no'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.item_query.brand_name'))->class('col-md-2 form-control-label')->for('brand_name') }}
                    
                        <div class="col-md-4">
                        <select class="form-control category" name="brand_id" id="brand_id">
                        <option value="">Select</option>
                        @foreach($brands as $brand)
                            <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                            @endforeach
                        </select>
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.item_query.hsn_code'))->class('col-md-2 form-control-label')->for('hsn_code') }}

                        <div class="col-md-4">
                            {{ html()->text('hsn_code')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.item_query.hsn_code'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row">

{{ html()->label(__('validation.attributes.backend.access.item_query.remarks'))->class('col-md-2 form-control-label')->for('remarks') }}

  <div class="col-md-4">
      {{ html()->text('remarks')
          ->class('form-control')
          ->placeholder(__('validation.attributes.backend.access.item_query.remarks'))
          ->attribute('maxlength', 191)
          ->autofocus() }}
  </div><!--col-->
  {{ html()->label(__('validation.attributes.backend.access.item_query.unit'))->class('col-md-2 form-control-label')->for('unit') }}

<div class="col-md-4">
<select class="form-control category" name="unit" id="unit">
<option value="">Select</option>
@foreach($uoms as $uom)
    <option value="{{$uom->id}}">{{$uom->unit}}</option>
    @endforeach
</select>
</div><!--col-->
  </div><!--form-group-->

                 

      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <input type="submit" class="btn btn-primary" id="addItems" value="Add Item">
        <input type="submit" class="btn btn-primary" style="display:none;" id="editItem" value="Save changes">

        </form>
      </div>
    </div>
  </div>
  
</div>
@endsection


