@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.queries.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.manualpurchase.management') }} <small class="text-muted"></small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.manualpurchase.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>{{ __('labels.backend.access.queries.table.query_no') }}</th>
                            <th>{{ __('labels.backend.access.queries.table.client_name') }}</th>
                            <th>{{ __('labels.backend.access.queries.table.query_date') }}</th>
                            <th>{{ __('labels.backend.access.queries.table.meera_center_detail') }}</th>
                            <th>{{ __('labels.backend.access.queries.table.query_center') }}</th>
                            <th>{{ __('labels.backend.access.queries.table.sales_center') }}</th>
                            <th>{{ __('labels.backend.access.queries.table.status') }}</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($manualpurchases as $data)
                            <tr>
                           

                                <td>QR/<?= 'M'.$sa=substr(strtoupper($brandletters='M0'.$data->client_id),-2);?>/<?= 'S'.$sa=substr(strtoupper($brandletters='S0'.$data->client->sales_center),-2);?>/{{substr(str_replace("-","",explode(" ",$data->updated_at)[0]),2)}}-<?='0'.$sa=substr(strtoupper($brandletters='00'.$data->id),-2);?></td>
                                <td>{{ $data->client->client_name }}</td>
                    
                                <td>{{$sa=substr(strtoupper($data->updated_at),0,-3)}}</td>
                                <td>{{ $data->meera_center_code }}</td>
                                <td><?='QR'.$sa=substr(strtoupper($brandletters='QR00'.$data->query_center),-2);?></td>
                                <td><?= 'S'.$sa=substr(strtoupper($brandletters='S0'.$data->client->sales_center),-2);?></td>





                                @if(isset($data->quotations[0]))
                                @if($data->quotations[0]->is_confirmed =='0')
                                <td><span class="label label-info">Under Review</span></td>
                                @elseif($data->quotations[0]->is_confirmed =='1')
                                <td><span class="label label-success">Accepted</span></td>
                                @elseif($data->quotations[0]->is_confirmed =='2')
                                <td><span class="label label-warning">Rejected</span></td>
                               
                                @endif
                            @else
                                <td><span class="label label-danger">Not Priced</span></td>    
                            @endif
                                <td>{!! $data->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $manualpurchases->total() !!} 
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $manualpurchases->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
