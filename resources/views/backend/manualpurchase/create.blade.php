@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.manualpurchase.management') . ' | ' . __('labels.backend.access.queries.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.manualpurchase.store'))->class('form-horizontal query-form')->attributes(array('enctype'=>'multipart/form-data'))->open() }}

    <input type="hidden" value="" name="item_id" id="item_id">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.manualpurchase.management') }}
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                    <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="OnCall">
                    <input name="action" type="hidden" value="OnCallPurchase">
                    <input name="activity" type="hidden" value="create">
                </div><!--col-->
                
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.queries.center_code'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                        <select class="form-control " name="center_id" id="centercode">
                        <option value="">Select</option>
                            @foreach($companydetails as $company)
                            <option value="{{$company->id}}">C{{$company->id}}</option>
                            @endforeach

                                </select>
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.queries.client_code'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                        <select class="form-control clientcode" name="client_id" >
                            <option value="">Select</option>
                                @foreach($clients as $client)
                                <option value="{{$client->id}}">{{'M'.$sa=substr(strtoupper($brandletters='M0'.$client->id),-2)}}  ({{$client->client_name}})</option>
                                @endforeach

                                    </select>
                        </div><!--col-->
                        </div><!--form-group--> 

             

                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.queries.meera_center_detail'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('meera_center_code')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.queries.meera_center_detail'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('id', 'meera_center_code')
                                    ->readonly()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.queries.client_detail'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                                {{ html()->text('client_detail')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.queries.client_detail'))
                                    ->attribute('class', 'client_detail')
                                    ->readonly()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.queries.po_no'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                            <input type="text" name="po_no" value="PO{{auth()->user()->id}}" class="form-control" readonly>
                            </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.queries.query_person'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                        <select class="form-control query_person" name="query_person" >

                        </select> 
                        </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">

                         {{ html()->label(__('validation.attributes.backend.access.queries.sales_center'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4">
                            {{ html()->text('sales_center')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.queries.sales_center'))
                                ->attribute('maxlength', 191)
                                ->readonly()
                                ->attribute('class', 'sales_center')
                                ->autofocus() }}
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.queries.contact_no'))->class('col-md-2 form-control-label')->for('unit') }}

                        <div class="col-md-4 ">
                      
                           {{ html()->text('contact_no')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.queries.contact_no'))
                            ->attribute('maxlength', 191)
                            ->attribute('class', 'contact_no')
                            ->readonly()
                            ->autofocus() }}    
                        </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">

                            {{ html()->label(__('validation.attributes.backend.access.queries.query_no'))->class('col-md-2 form-control-label')->for('unit') }}
                            <div class="col-md-4">
                            {{ html()->text('query_no')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.queries.query_no'))
                            ->attribute('maxlength', 191)
                            ->readonly()
                            ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.queries.query_center'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">

                            <input type="text" class="form-control" value="<?='QR'.$sa=substr(strtoupper($brandletters='QR00'.Auth::user()->id),-2);?>" readonly/>
                            <input type="hidden" value="{{Auth::user()->id}}" name="query_center">
                            </div><!--col-->
                            </div><!--form-group-->
         
                        <div class="row table-responsive">
    <div class="col-md-12">

<button type="button" class="btn btn-primary" data-toggle="modal" style="margin-bottom: 15px;" data-target="#myModal">Old Add Items</button>
<button type="button" class="btn btn-primary" data-toggle="modal" style="margin-bottom: 15px;float: right;" data-target="#modalMy">Create New Item</button>

                    <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>Specification</th>
                        <th>Model No</th>
                        <th>HSN</th>
                        <th>QTY</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="queryItems"></tbody>
                    </table>
                    </div>
</div>
 </div>   
</div>

     </div><!--col-->
     
                </div><!--row-->
                
                </div><!--card-body-->

<div class="card-footer clearfix">
    <div class="row">
        <div class="col">
            {{ form_cancel(route('admin.query.index'), __('buttons.general.cancel')) }}
        </div><!--col-->

        <div class="col text-right">
            {{ form_submit(__('buttons.general.crud.create')) }}
        </div><!--col-->
    </div><!--row-->
</div><!--card-footer-->
</div><!--card-->
                {{ html()->form()->close() }}

   
   
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="    max-width: 850px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        	 <h4 class="card-title mb-0">
                            <small class="text-muted"></small>
                        </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal </h4> -->
        </div>
        <div class="modal-body">
        <form role="myForm" id="myForm" method="post" enctype="multipart/form-data" action='backend/query/create'>
      <meta name="csrf-token" content="{{ csrf_token() }}">
                 
                  <div class="form-group row">

           {{ html()->label(__('validation.attributes.backend.access.item_query.image'))->class('col-md-2 form-control-label')->for('remarks') }}
 

 <div class="col-md-4">
 <input type="file" id="fileinput" name='image' value="image" required>
 <input name="saveimage" type="hidden" class="checkimage" id="saveimage" >
 </div><!--col--> 
 <div class="col-md-4 image">
 
 </div><!--col--> 


</div>
      <div class="form-group row">
                          
                                {{ html()->label(__('validation.attributes.backend.access.item_query.product_name'))->class('col-md-2 form-control-label')->for('product_name') }}

                            <div class="col-md-4">
                            <select class="form-control product_id" name="product_id" id="product_id">
                            <option value="">Select</option>     
                            @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->product_name}}</option>
                            @endforeach
                                    </select>
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.product.cat_name'))->class('col-md-2 form-control-label')->for('cat_name') }}         
                        <div class="col-md-4 unshowcat">
                    <div class="form-group">
                    <select class="form-control category" name="category_id" id="cat">
                   

                    </select>
                </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row"> 
                 
                </div>      
                {{ html()->label(__('validation.attributes.backend.access.product.subcat_name'))->class('col-md-2 form-control-label unshowcat')->for('mobile') }}

            <div class="col-md-4 unshow">
                    <div class="form-group">
                    <select class="form-control category" name="subcategory_id" id="subcat">
                    

                    </select>
                </div><!--col-->
                </div><!--row-->

{{ html()->label(__('validation.attributes.backend.access.item_query.qty'))->class('col-md-2 form-control-label')->for('qty') }}

<div class="col-md-4">
    {{ html()->text('qty')
        ->class('form-control')
        ->placeholder(__('validation.attributes.backend.access.item_query.qty'))
        ->attribute('maxlength', 191)
        ->required()
        ->autofocus() }}
</div>

                </div>
                <div class="form-group row"> 
                {{ html()->label(__('validation.attributes.backend.access.item_query.item_description'))->class('col-sm-2 form-control-label')->for('cat_name') }}
                
<div class="col-md-10">
     <div class="form-group">
<select class="form-control category" name="item_id" id="item">
</select>
</div>
</div>

            </div><!--card-body-->

                      <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.item_query.description'))->class('col-md-2 form-control-label')->for('description') }}

                        <div class="col-md-4">
                            {{ html()->text('description')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.item_query.description'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.item_query.specification'))->class('col-md-2 form-control-label')->for('specification') }}

                            <div class="col-md-4">
                                {{ html()->text('specifications')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item_query.specification'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('id', 'specification')
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.item_query.color'))->class('col-md-2 form-control-label')->for('color') }}

                        <div class="col-md-4">
                        <select class="form-control category" name="color" id="color">
                        <option value="">Select</option>
                        @foreach($colors as $color)
                            <option value="{{$color->id}}">{{$color->color}}</option>
                            @endforeach
                        </select>
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.item_query.model_no'))->class('col-md-2 form-control-label')->for('model_no') }}

                        <div class="col-md-4">
                            {{ html()->text('model_no')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.item_query.model_no'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row">
                      {{ html()->label(__('validation.attributes.backend.access.item_query.brand_name'))->class('col-md-2 form-control-label')->for('brand_name') }}
                    
                        <div class="col-md-4">
                        <select class="form-control brand_id" name="brand_id" id="brand_id">
                       
                        </select>
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.item_query.hsn_code'))->class('col-md-2 form-control-label')->for('hsn_code') }}

                        <div class="col-md-4">
                            {{ html()->text('hsn_code')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.item_query.hsn_code'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                      </div><!--form-group-->
                      <div class="form-group row">

{{ html()->label(__('validation.attributes.backend.access.item_query.remarks'))->class('col-md-2 form-control-label')->for('remarks') }}

  <div class="col-md-4">
      {{ html()->text('remarks')
          ->class('form-control')
          ->placeholder(__('validation.attributes.backend.access.item_query.remarks'))
          ->attribute('maxlength', 191)
          ->required()
          ->autofocus() }}
  </div><!--col-->
  {{ html()->label(__('validation.attributes.backend.access.item_query.unit'))->class('col-md-2 form-control-label')->for('unit') }}

<div class="col-md-4">
<select class="form-control category" name="unit" id="unit">
<option value="">Select</option>
@foreach($uoms as $uom)
    <option value="{{$uom->id}}">{{$uom->unit}}</option>
    @endforeach
</select>
</div><!--col-->
  </div><!--form-group-->
  <div class="form-group row">

               
                      </div><!--form-group-->
                   

      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        
        <input type="submit" class="btn btn-primary" id="addItems" value="Add Item">
        <input type="submit" class="btn btn-primary" style="display:none;" id="editItem" value="Save changes">

        </form>
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- Modal Item create -->
  <div class="modal fade" id="modalMy" role="dialog">
    <div class="modal-dialog" style="    max-width: 850px;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        	 <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.item.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.item.create') }}</small>
                        </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <!-- <h4 class="modal-title">Modal </h4> -->
        </div>
        <div class="modal-body">
                   
        <form role="myForm" id="itemform" method="post" enctype="multipart/form-data" action='backend/query/create'>
      <meta name="csrf-token" content="{{ csrf_token() }}">
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.item.is_active'))->class('col-md-2 form-control-label')->for('is_active') }}

                    <div class="col-md-4">
                        <label class="switch switch-3d switch-primary">
                            {{ html()->checkbox('is_active', true, '1')->class('switch-input') }}
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div><!--col-->
                    {{ html()->label(__('validation.attributes.backend.access.item.hsn_code'))->class('col-md-2 form-control-label')->for('hsn_code') }}

<div class="col-md-4">
    {{ html()->text('hsn_code')
        ->class('form-control')
        ->placeholder(__('validation.attributes.backend.access.item.hsn_code'))
        ->attribute('id', 'hsnval')
        ->required()
        ->autofocus() }}
</div><!--col-->

                           
                        </div><!--form-group-->
                          <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.item.model_no'))->class('col-md-2 form-control-label')->for('model_no') }}

                            <div class="col-md-4">
                                {{ html()->text('modelno')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.model_no'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.item.product_name'))->class('col-md-2 form-control-label')->for('product_name') }}

                                <div class="col-md-4">
                                <select class="form-control category" name="product_id" id="productid">
                                <option value="">Select</option>
                                @foreach($products as $stand)
                                <option value="{{$stand->id}}">{{$stand->product_name}}</option>
                                @endforeach

                                </select>
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">  
                        {{ html()->label(__('validation.attributes.backend.access.product.cat_name'))->class('col-sm-2 form-control-label')->for('cat_name') }}
        
                        <div class="col-sm-4 ">
                    <div class="form-group">
                    <select class="form-control category" name="category_id" id="cat_new">
                   

                    </select>
                </div><!--col-->
                </div>      
             <input type="hidden" value="" name="cat_id" id="cat_id">
             {{ html()->label(__('validation.attributes.backend.access.product.subcat_name'))->class('col-sm-2 form-control-label')->for('mobile') }}

            <div class="col-sm-4 ">
                    <div class="form-group">
                    <select class="form-control category" name="subcategory_id" id="subcat_new">
                    

                    </select>
                </div><!--col-->
                </div>
                    </div><!--col-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.product.brand_name'))->class('col-sm-2 form-control-label')->for('cat_name') }}
        
        <div class="col-sm-4 ">
    <div class="form-group">
    <select class="form-control category" name="brand_id" id="brand">
   

    </select>
</div><!--col-->
</div> 
                           
                            {{ html()->label(__('validation.attributes.backend.access.item.package_qty'))->class('col-md-2 form-control-label')->for('package_qty') }}

                            <div class="col-md-4">
                                {{ html()->text('package_qty')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.package_qty'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.item.unit'))->class('col-md-2 form-control-label')->for('unit') }}

                            <div class="col-md-4">
                            <select class="form-control category" name="unit" id="units">
                            <option value="">Select</option>
                            @foreach($uoms as $stand)
                            <option value="{{$stand->id}}">{{$stand->unit}}</option>
                            @endforeach

                            </select>
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.item.color'))->class('col-md-2 form-control-label')->for('color') }}

                            <div class="col-md-4">
                            <select class="form-control category" name="color" id="colors">
                            <option value="">Select</option>
                            @foreach($colors as $stand)
                            <option value="{{$stand->id}}">{{$stand->color}}</option>
                            @endforeach
                            </select>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.item.image'))->class('col-md-2 form-control-label')->for('specification') }}

<div class="col-md-4">
<div class="col-md-4">
<input type="file" id="fileinputitem" name='image' value="image"/>
<input name="saveimages" type="hidden"  id="saveimages">
</div><!--col-->
</div>
                            {{ html()->label(__('validation.attributes.backend.access.item.description'))->class('col-md-2 form-control-label')->for('description') }}

                            <div class="col-md-4">
                                {{ html()->text('descriptions')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.description'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.item.specification'))->class('col-md-2 form-control-label')->for('specification') }}

                            <div class="col-md-4">
                                {{ html()->text('specifications')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.specification'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.item.remarks'))->class('col-md-2 form-control-label')->for('remarks') }}

                            <div class="col-md-4">
                                {{ html()->text('remark')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.item.remarks'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                 
                    <div class="form-group row">
              
                            </form>
                </div><!--row-->
        <div class="modal-footer">
        <input type="submit" class="btn btn-primary" id="addNewItems" value="Add Item">
        </div>
      </div>
      
    </div>
  </div>

@endsection



