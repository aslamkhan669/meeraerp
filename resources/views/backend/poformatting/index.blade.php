@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.poformattings.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.poformattings.management') }} <small class="text-muted"></small>
                </h4>
            </div><!--col-->
            <div class="col-sm-5">
            </div><!--col-->
            <div class="col-sm-2">
                @include('backend.poformatting.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->
        <form action="searchpoform" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Min PO NO" style="width: 20%; display: inline;">
    <input type="text" value=""  name="clientpono" class="form-control" placeholder="Enter Client PO NO" style="width: 20%; display: inline;">

    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>{{ __('labels.general.actions') }}</th>
                        <th>{{ __('labels.backend.access.poformattings.table.status') }}</th>
                        <th>{{ __('labels.backend.access.poformattings.table.min_po_no') }}</th>
                        <th>{{ __('labels.backend.access.poformattings.table.client') }}</th>
                            <th>{{ __('labels.backend.access.poformattings.table.client_po_no') }}</th>
                            <th>{{ __('labels.backend.access.poformattings.table.purchaser_name') }}</th>
                            <th>{{ __('labels.backend.access.poformattings.table.reciever_name') }}</th>
                            <th>{{ __('labels.backend.access.poformattings.table.quotation_id') }}</th>
                            <th>Normal/OnCall/Reject</th>
                            <th>Date&Time</th>


                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($poformattings as $poformat)
                            <tr>
                            <td>{!! $poformat->action_buttons !!}</td>
                            @if(isset($poformat->status))
                                @if($poformat->status =='0')
                                <td><Button class="btn btn-warning">Under Planning</Button></td>
                                @elseif($poformat->status =='1')
                                <td><Button class="btn btn-success">Planned</Button></td>   
                                @endif
                            @else
                                <td><Button class="btn btn-danger">Not Planned</Button></td>    
                            @endif  
                                <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$poformat->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$poformat->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$poformat->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poformat->id),-2)}}</td>
                                <td>{{$poformat->quotation->queries->client->client_name}}</td>
                                <td>{{ $poformat->c_p_owner_no }}</td>
                                <td>{{ $poformat->c_purchaser_name }}</td>
                                <td>{{ $poformat->c_reciever_name }}</td>
                                <td>QT/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$poformat->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$poformat->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$poformat->quotation->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poformat->quotation_id),-2)}}</td>
                                <td>{{$poformat->quotation->queries->normal_oncall}}</td>
                                <td>{{$sa=substr(strtoupper($poformat->updated_at),0,-3)}}</td>   
                       
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $poformattings->total() !!} {{ trans_choice('labels.backend.access.brand.table.total', $poformattings->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $poformattings->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
