@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.quotation.pomanagement') . ' | ' . __('labels.backend.access.quotation.createpo'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($poformatting, 'PATCH', route('admin.poformatting.update', $poformatting->id))->class('form-horizontal')->open() }}
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="PurchaseManagement">
                    <input name="action" type="hidden" value="POFormatting">
                    <input name="activity" type="hidden" value="update">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.quotation.pomanagement') }}
                            
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                 
                </div><!--col-->
                
                </div><!--form-group-->

                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                    </div>
                </div>
                <div class="row  mb-4">
                    <div class="col-md-12">
                    <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Quotation No.</label>

                            <div class="col-md-4">
                            <select class="form-control quotation_no" name="quotation_no">
                                <option value="">Select</option>
                                @foreach($quotationno as $data)
                                <option value="{{$data->query_id}}" {{$data->id==$qinfo->quotation_id?"selected":""}} data-id="{{$data->id}}">QT/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$data->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$data->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$data->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$data->id),-2)}}({{$data->queries->normal_oncall}})</option>
                                @endforeach
                                </select>
                            </div><!--col-->
                            
                            <label class="col-md-2 form-control-label" for="lab">MIN PO No.</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" value="PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$data->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$data->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$poformatting->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poformatting->id),-2)}}"  readonly>
                            </div><!--col-->
                        </div><!--form-group-->

                             <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Client PO No.</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="c_p_owner_no" value="{{$poformatting->c_p_owner_no}}">
                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">Client purchaser</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" name="c_purchaser_name" value="{{$poformatting->c_purchaser_name}}">
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Client reciever</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" name="c_reciever_name" value="{{$poformatting->c_reciever_name}}">
                            </div><!--col-->
                    
                        </div><!--form-group-->
                        
                    </div>

                </div>
              

                <div class="row mt-4">
    <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                        <th>S.NO.</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Unit PP</th>
                        <th>GST</th>
                        <th>Unit SP</th>
                        <th>Qty</th>
                        <th>Grand Total</th>
                        <th>Select Item</th>                
                    </tr>
                    </thead>
                    <tbody  class="data poformatting">
                    <?php $i=1; ?>
                    @foreach($qitems as $data)

                    <tr><th scope='row'>{{$i++}}</th>
                    <input type='hidden'  name='id[]' value='{{$data->id}}' >
                    <input type='hidden'  name='po_id[]' value='{{$data->po_id}}' >
                    <input type='hidden'  name='qitemid[]' value='{{$data->qitemid}}' >
                    <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{$data->item_id}}" data-toggle="modal" data-target="#myModal">View</button></td>
                    <td class="image-hover"><img src="{{url('/uploads/'.$data->image)}}" width="50px" height="50px"/></td>
                    <td>{{$data->discounted_price}}</td>
                    <td>{{$data->gst}}</td>
                    <td><input type='text' value='{{$data->total}}' class='form-control' readonly></td>
                    <td><input type='text' id='qty' class='form-control qty' name='qty[]' value='{{$data->qty}}'></td>
                     <td><input type='text' class='form-control grand_total' name='po_grand_total[]' value='{{$data->grand_total}}'></td>
                    <td><input type='checkbox' class='form-check-input qitemid' ischecked='false' value='' ></td>
                    </tr>

                         @endforeach

                    </tbody>
                    </table>
                    </div>
                    </div>
                    </div> 

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.poformatting.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                  

                        {{ form_submit(__('buttons.general.crud.create')) }}

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection