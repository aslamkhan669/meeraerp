@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.colors.dispatch'))

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Dispatch Planner <small class="text-muted"></small>
                </h4>
            </div><!--col-->

            
        </div><!--row-->
        <form action="searchdisplan" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Company Name" style="width: 20%; display: inline;">

    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>Status</th>
                        <th>Purchased/InPurchased</th>
                        <th>Reject Status</th>
                        <th>Action</th>
                        <th>Company Name</th>
                        <th>MIN PO No</th>
                        <th>Client PO No</th>
                            <th>Date&Time</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $po)
                            <tr>
                                @if($po->poItems[0]->dis_complete=='0')
                                <td><Button class="btn btn-success">Not Submit</Button></td>       
                                @elseif($po->poItems[0]->dis_complete =='1')
                                <td><Button class="btn btn-danger">Submit</Button></td> 
                                @else
                                <tr><td><Button class="btn btn-success">Not Submit</Button></td>       
                                @endif
                                <td>
                                @foreach ($po->poItems as $data)
                                @if($data->dis_complete=='1')
                                <span class="label label-warning">Dispatched</span>
       
                                @else
                                <span class="label label-success">In Dispatched</span>
                                @endif 

                                @endforeach
</td>
<td>
                                @foreach ($po->poItems as $data)
                                @foreach ($data->dispatchRegisters as $info)
                                @foreach ($info->dispatchedOrder as $a)

                                @if($a->pending_qty=='1')
                                <span class="label label-warning">Reject</span>
                                @elseif($a->pending_qty >'1')
                                <span class="label label-warning">Reject</span>
                                @else
                                <span class="label label-success">Accept</span>
                                @endif 
                                @endforeach
                                @endforeach
                                @endforeach
</td>
                                <td><span><a class="btn btn-sm btn-success" href="{{ url('admin/dispatch/show/') }}/<?=$po->id?>">Input</a></span></td>                              
                            <td>{{ $po->quotation->queries->client->client_name }}</td>
                            <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$po->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$po->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$po->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$po->id),-2)}}</td>
                               <td>{{$po->c_p_owner_no}}</td>
                                <td>{{$sa=substr(strtoupper($po->updated_at),0,-3)}}</td>   
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                   
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                   
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
