@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.purchaseaccount.management') . ' | ' . __('labels.backend.access.quotation.createpo'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width: 900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="module" type="hidden" value="DispatchManagement">
                    <input name="action" id="action" type="hidden" value="DispatchPlanner">
                    <input name="activity" id="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Dispatch Planner
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                    <div class="col">
                </div>
                </div>
                <div class="row mt-4">
    <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead id="data">
                    <tr>
                        <th>S.NO.</th>
                        <th>Status</th>
                        <th>PO No</th>
                        <th>Client PO No</th>
                        <th>Contact&Address  </th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Dispatch date</th>
                        <th>Day Shift</th>
                        <th>Dispatch Person</th>
                        <th>Transportation Mode</th>
                        <th>On Bill/Challan</th>
                        <th>Priority</th>
                        <th>Real Qty</th>
                        <th>Pending Qty</th>
                        <th>Qty</th>
                        <th>Amount</th>
                       
                    </tr>
                    </thead>
                    <tbody  class="data">
                    <tr>
                    @for($i=0; $i<sizeof($poitems[0]->poItems); $i++)
                    <th scope='row'>{{$i+1}}</th>
                    <input type='hidden' class='form-control pid'  value='{{$poitems[0]->id}}'>

       @if(isset($podata[0]->poItems[$i]->dispatchRegisters[0]))

                                @if($podata[0]->poItems[$i]->dispatchRegisters[0]->checked=='0')
                                <td><Button class="btn btn-success">Pending</Button></td>       
                                @elseif($podata[0]->poItems[$i]->dispatchRegisters[0]->checked =='1')
                                <td><Button class="btn btn-danger">planned</Button></td> 
                                @endif      
                                @else
                                <td><Button class="btn btn-success">Pending</Button></td>       
                                @endif

                    <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$poitems[0]->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$poitems[0]->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$poitems[0]->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poitems[0]->id),-2)}}</td>

                    <td>{{$poitems[0]->c_p_owner_no}}</td>
                    <td style="justify-content:normal;">{{$poitems[0]->quotation->queries->client->contact_number1}}/{{$poitems[0]->quotation->queries->client->street}},{{$poitems[0]->quotation->queries->client->landmark}},{{$poitems[0]->quotation->queries->client->sector}},{{$poitems[0]->quotation->queries->client->city}},{{$poitems[0]->quotation->queries->client->state}} </td>
                    <td class="image-hover"><img src="{{url('/uploads/'.$poitems[0]->poItems[$i]->queryItem->image)}}" width="50px" height="50px"/></td>   
                    <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{$poitems[0]->poItems[$i]->queryItem->item_id}}" data-toggle="modal" data-target="#myModal">View</button></td>
                    <td><input type='date' class='form-control purchase_date'  value=''></td>
                    <td>
                    <select name="iftoday" class="form-control dayshift" style="width: 118px;">
                    <option value=""> Select shift </option>
                    <option value="0"> First Half </option>
                    <option value="1"> Second Half </option>
                    </select>
                    </td>
                    <td>
                        <select name="iftoday" class="form-control pteam" style="width: 150px;">
                        <option value=""> Select DP </option>
                        @foreach ($users as $dp)
                            <option value="{{$dp->id}}">{{$dp->first_name}} </option>
                            @endforeach
                            </select>
                        </td>
                    <td>
                    <select name="transportation" class="form-control transportation_mode">
                    <option value=""> Select </option>
                    <option value="Bus"> Bus </option>
                        <option value="Metro"> Metro </option>
                        <option value="Car"> Car </option>
                        <option value="Auto"> Auto </option>
                        <option value="Other vedhicles"> Other vehicles </option>
                    </select>
                    </td>
                    <td>
                    <select name="on_bill_challan" class="form-control onbill" style="width: 118px;">
                     <option value="">Select</option>
                        <option value="bill">Bill</option>
                        <option value="challan">Challan</option>
                    </select>
                    </td>   
                     <td>
                    <select name="priority" class="form-control priority" style="width: 118px;">
                    <option value=""> Select level </option>
                        <option value="2"> High </option>
                        <option value="1"> Medium </option>
                        <option value="0"> Low </option>
                    </select>
                    </td>
                    <td><input type='text' value='{{$poitems[0]->poItems[$i]->qty}}'  class='form-control' style="width: 100px;" readonly></td>
                    <td><input type='text' value="{{ isset($poitems[0]->poItems[$i]->poRegisters[0]->purchasedOrders[0]->pending_qty) ? $poitems[0]->poItems[$i]->poRegisters[0]->purchasedOrders[0]->pending_qty : '' }}"  class='form-control' style="width: 100px;" readonly></td>
                    <td><input type='text' value='' data-id='{{$poitems[0]->poItems[$i]->qty}}' class='form-control qty' style="width: 100px;"></td>
                   <td><input type='text' class='form-control gtotal' data-io='{{$poitems[0]->poItems[$i]->grand_total}}'  style="width: 100px;" value='{{$poitems[0]->poItems[$i]->grand_total}}'></td>

                    </tr>
                    <input type="hidden" value="{{$poitems[0]->poItems[$i]->id}}" class="poitemid">
                    @endfor


                    </tbody>
                    </table>
                    </div>
                    </div>
                    </div> 

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.quotation.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                  

                    <button type="button" data-id="" class="btn btn-success send">Send</button>

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    
@endsection