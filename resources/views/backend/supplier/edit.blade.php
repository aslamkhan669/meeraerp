@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.suppliers.management') . ' | ' . __('labels.backend.access.suppliers.edit'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($supplier, 'PATCH', route('admin.supplier.update', $supplier->id))->class('form-horizontal')->attributes(array('enctype'=>'multipart/form-data'))->open() }}
<div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="ItemManagement">
                    <input name="action" type="hidden" value="Supplier">
                    <input name="activity" type="hidden" value="update">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.suppliers.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.suppliers.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                    <div class="form-group row col-md-4">
                        <h4>
                        Supplier Info
                        </h4>
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.suppliers.supplier_code'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                            <input type="text" class="form-control" value="{{'V'.$sa=substr(strtoupper($brandletters='V00'.$supplier->id),-3)}}" readonly>
                            
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row product-mapping">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.supplier_name'))->class('col-md-2 form-control-label')->for('category') }}
                            

                            <div class="col-md-4">
                                {{ html()->text('supplier_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.supplier_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.supplier_category'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                <select class="form-control" name="supplier_category">
                                <option value="">Select</option>
                                <option value="Manufacturer"{{"Manufacturer"==$suppliercat?"selected":""}}>Manufacturer</option>
                                <option value="Dealer"{{"Dealer"==$suppliercat?"selected":""}}>Dealer</option>
                                <option value="Trader"{{"Trader"==$suppliercat?"selected":""}}>Trader</option>
                                <option value="Importer"{{"Importer"==$suppliercat?"selected":""}}>Importer</option>
                                <option value="Exporter"{{"Exporter"==$suppliercat?"selected":""}}>Exporter</option>
                                    </select>
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.pricing_center'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('pricing_center')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.pricing_center'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.supplier_type'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                <select class="form-control" name="supplier_type">
                            <option value="">Select</option>
                                 @foreach($clienttypes as $client)
                                 <option value="{{$client->type}}"{{$client->type==$supplierinfo?"selected":""}}>{{$client->type}}</option>
                                 @endforeach
              
                                     </select>
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.mobile'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('mobile')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.mobile'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.landline'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('landline')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.suppliers.landline'))
                                        ->attribute('maxlength', 191)
                                        ->required()
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.email'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.email'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.rating'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4 stars">
                            <input class="star star-5" id="star-5" type="radio" value="5" name="rating" <?php  if($supplier->rating=='5'){ echo 'checked'; } ?>/>
                            <label class="star star-5" for="star-5"></label>
                            <input class="star star-4" id="star-4" type="radio" value="4" name="rating" <?php  if($supplier->rating=='4'){ echo 'checked'; } ?>/>
                            <label class="star star-4" for="star-4"></label>
                            <input class="star star-3" id="star-3" type="radio" value="3" name="rating" <?php  if($supplier->rating=='3'){ echo 'checked'; } ?>/>
                            <label class="star star-3" for="star-3"></label>
                            <input class="star star-2" id="star-2" type="radio" value="2" name="rating" <?php  if($supplier->rating=='2'){ echo 'checked'; } ?>/>
                            <label class="star star-2" for="star-2"></label>
                            <input class="star star-1" id="star-1" type="radio" value="1" name="rating" <?php  if($supplier->rating=='1'){ echo 'checked'; } ?>/>
                            <label class="star star-1" for="star-1"></label>

                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row col-md-4">
                        <h4>
                        Contact Info
                        </h4>
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.contact_person1'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person1')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.contact_person1'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.contact_number1'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('contact_number1')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.suppliers.contact_number1'))
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.contact_person2'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person2')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.contact_person2'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.contact_number2'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number2')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.contact_number2'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.contact_person3'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person3')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.contact_person3'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.contact_number3'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('contact_number3')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.suppliers.contact_number3'))
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                     
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.contact_person4'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person4')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.contact_person4'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.contact_number4'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('contact_number4')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.suppliers.contact_number4'))
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                   
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.supplier_catalog'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->file('supplier_catalog')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.street'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('street')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.suppliers.street'))
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.landmark'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('landmark')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.landmark'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.sector'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('sector')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.suppliers.sector'))
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.city'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('city')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.city'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.pincode'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('pincode')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.pincode'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.state'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('state')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.state'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.website'))->class('col-md-2 form-control-label')->for('category_code') }}

                                    <div class="col-md-4">
                                        {{ html()->text('website')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.suppliers.website'))
                                            ->attribute('maxlength', 191)
                                            ->autofocus() }}
                                    </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.zone'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                            <input type="checkbox" name="zone"  value="North" <?php  if($supplier->zone=='North'){ echo 'checked'; } ?>>
                            <label for="north">North</label>
                            <input type="checkbox" name="zone"  value="East" <?php  if($supplier->zone=='East'){ echo 'checked'; } ?>>
                            <label for="south">East</label>
                            <input type="checkbox" name="zone"  value="West" <?php  if($supplier->zone=='West'){ echo 'checked'; } ?>>
                            <label for="south">West</label>
                            <input type="checkbox" name="zone"  value="South" <?php  if($supplier->zone=='South'){ echo 'checked'; } ?>>
                            <label for="south">South</label>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row col-md-4">
                        <h4>
                        Account Info
                        </h4>
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.pancard'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('pancard')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.pancard'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.adharcard'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('adharcard')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.adharcard'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.gst_no'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('gst_no')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.gst_no'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.bank_name'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('bank_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.bank_name'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('validation.attributes.backend.access.suppliers.cancelled_cheque'))->class('col-md-2 form-control-label')->for('category_code') }}

                        <div class="col-md-4">
                            {{ html()->file('cancelled_cheque')
                                ->class('form-control')
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.ifsc_code'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('ifsc_code')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.ifsc_code'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.suppliers.account_no'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('account_no')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.suppliers.account_no'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                      
                      
                    </div><!--col-->
                </div><!--row-->
                <div class="row mt-4 mb-4">
                	<div class="col-md-6">

                    </div>
                	<div class="col-md-6">
                		<div class="cat-map" style="    background: #f6f6f7; padding: 15px;">
                			<h3>Product Cat-Mapping</h3>
                		<div class="form-group row">
                             <label class="col-md-3 form-control-label">Product</label>

                            <div class="col-md-7">
                            <select class="form-control product_id" name="product_id" >
                            <option value="">Select</option>     
                            @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->product_name}}</option>
                            @endforeach
                                    </select>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                             <label class="col-md-3 form-control-label">Category</label>

                            <div class="col-md-7">
                            <select class="form-control category cat" name="cat_id" >
                         </select>
                            </div><!--col-->
                            <div class="col-md-2"><button class="btn btn-primary addcategory">Add</button></div>
                        </div><!--form-group-->
                        <div class="table-responsive mt-4">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ProductName</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody class="supplierItems">
                        @foreach($queryitems as $data)
                        <tr class=''>
        <td><input type='hidden' class='i_product_name' name='product_name[]' value='{{$data->product_name}}' ><input type='hidden' class='i_cat_id' name='catId[]' value='{{$data->cat_id}}' >

         {{$data->category->product->product_name}}</td>
        <td>{{$data->category->category_name}}</td>
      
        <td><span class='delete'><a class="product_list_cart" data-id="{{$data->id}}">Delete</a></span></td>
        </tr>

        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>

                	</div>

                	
                </div>
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.supplier.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
