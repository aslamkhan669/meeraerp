@extends ('backend.layouts.app')


@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                </h4>
                <!-- <a href=""><img src="../../../uploads/excel-icon.ico" width="75px;"></a> -->

            </div><!--col-->
            <div class="col-sm-7">
            @include('backend.supplier.includes.header-buttons')

            </div><!--col-->
        </div><!--row-->
        <form action="searchsupplier" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Supplier name" style="width: 30%; display: inline;">
    <button type="submit" class="btn btn-success btn-md" value="Search" style="margin-top: -5px;"><i class="fa fa-search"></i></button>
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.access.suppliers.table.supplier_name') }}</th>
                            <th>{{ __('labels.backend.access.suppliers.table.supplier_code') }}</th>
                            <th>{{ __('labels.backend.access.suppliers.table.supplier_type') }}</th>
                            <th>{{ __('labels.backend.access.suppliers.table.pricing_center') }}</th>
                    
                            <th>{{ __('labels.backend.access.suppliers.table.rating') }}</th>
                            <th>Date&Time</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($suppliers as $supplier)
                            <tr> 
                                <td>{{ $supplier->supplier_name }}</td> 
                                <td>{{'V'.$sa=substr(strtoupper($brandletters='V00'.$supplier->id),-3)}}</td>                               
                                <td>{{ $supplier->supplier_type }}</td>
                                <td>{{ $supplier->pricing_center }}</td> 
                       
                                <td>{{ $supplier->rating }}</td> 
                                <td>{{$sa=substr(strtoupper($supplier->updated_at),0,-3)}}</td>   
                                <td>{!! $supplier->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                {!! $suppliers->total() !!} {{ trans_choice('labels.backend.access.suppliers.table.total', $suppliers->total()) }}

                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                {!! $suppliers->render() !!}

                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
