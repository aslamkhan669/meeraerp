@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.poformattings.planner'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.poformattings.planner') }} <small class="text-muted"></small>
                </h4>
            </div><!--col-->

           
        </div><!--row-->
        <form action="searchpoplan" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Min PO NO" style="width: 20%; display: inline;">
    <input type="text" value=""  name="clientpo" class="form-control" placeholder="Enter Client PO NO" style="width: 20%; display: inline;">

    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>{{ __('labels.general.actions') }}</th>
                        <th>Status</th>
                        <th>Purchased/InPurchasing</th>
                        <th>Pending/UnPending</th>
                            <th>Client Name</th>
                            <th>MIN PO NO</th>
                            <th>Client PO NO</th>
                            <th>Center Name</th>
                            <th>Purchase Person</th>
                            <th>Normal/OnCall/Reject</th>
                            <th>Date&Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($poformattings as $poformat)
                            <tr>
                            <td>{!! $poformat->action_buttons !!}</td>

                                 @if($poformat->poItems[0]->complete=='0')
                                <td><Button class="btn btn-success">Not Submit</Button></td>       
                                @elseif($poformat->poItems[0]->complete =='1')
                                <td><Button class="btn btn-danger">Submit</Button></td> 
                                @else
                                <td><Button class="btn btn-success">Not Submit</Button></td>       
                                @endif 
<td>
                                @foreach ($poformat->poItems as $data)
                                @if($data->complete=='1')
                                <span class="label label-warning">Purchased</span>
       
                                @else
                                <span class="label label-success">InPurchasing</span>
                                @endif 

                                @endforeach
</td>

<td>
                                @foreach ($poformat->poItems as $data)
                                @foreach ($data->poRegisters as $info)
                                @foreach ($info->purchasedOrders as $a)

                                @if($a->pending_qty=='1')
                                <span class="label label-warning">Pending</span>
                                @elseif($a->pending_qty >'1')
                                <span class="label label-warning">Pending</span>
                                @else
                                <span class="label label-success">UnPending</span>
                                @endif 
                                @endforeach
                                @endforeach
                                @endforeach
</td>
                                <td>{{ $poformat->quotation->queries->client->client_name}}</td>
                                <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$poformat->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$poformat->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$poformat->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poformat->id),-2)}}</td>
                                <td>{{ $poformat->c_p_owner_no }}</td>
                                <td>{{ $poformat->quotation->queries->companydetail->company_name}}</td>
                                <td>{{ $poformat->c_purchaser_name }}</td>
                                <td>{{$poformat->quotation->queries->normal_oncall}}</td>
                                <td>{{$sa=substr(strtoupper($poformat->updated_at),0,-3)}}</td>   
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $poformattings->total() !!} {{ trans_choice('labels.backend.access.brand.table.total', $poformattings->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $poformattings->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
