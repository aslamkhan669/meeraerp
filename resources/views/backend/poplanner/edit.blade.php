@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.quotation.pomanagement') . ' | ' . __('labels.backend.access.quotation.createpo'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width: 900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="module" type="hidden" value="PurchaseManagement">
                    <input name="action" id="action" type="hidden" value="POPlanner">
                    <input name="activity" id="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.poformattings.poregister') }}
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                <div class="col"> 
                </div><!--col-->
                </div><!--form-group-->
                <div class="row mt-4">
                <div class="col-md-12">
                    <div class="table-responsive" >
                    <table class="table">
                    <thead id="data">
                    <tr>
                        <th>S.NO.</th>
                        <th>Status</th>
                        <th>Client Name</th>
                        <th>PO No</th>
                        <th>Client PO No</th>
                        <th>Contact&Address  </th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Day Shift</th>
                        <th>Purchase date</th>
                        <th>Purchase Person</th>
                        <th>Transportation Mode</th>
                        <th>Criticality</th>
                        <th>Priority</th>
                        <th>Real Qty</th>
                        <th>Pending Qty</th>
                        <th>Qty</th>
                        <th>Payment Amount</th>
                        <th>Payment Mode</th>
                        <th>Bank</th> 
                       
                          
                    </tr>
                    </thead>
                    <tbody  class="data">
                    
                    
                    @for($i=0; $i<sizeof($podata[0]->poItems); $i++)
                    <tr>
                    <th scope='row'>{{$i+1}}</th>

                    <input type='hidden' class='form-control pid'  value='{{$podata[0]->id}}'>
                    
                    @if(isset($podata[0]->poItems[$i]))
                                @if($podata[0]->poItems[$i]->complete =='1')
                                <td><Button class="btn btn-danger">Purchased</Button></td>       
                                @elseif ($podata[0]->poItems[$i]->complete == '0')
                                <td><Button class="btn btn-danger">Purchased</Button></td> 
                                @else
                                <td><Button class="btn btn-success">In Purchased</Button></td>       
                                @endif      
                                @else
                                <td><Button class="btn btn-success">In Purchased</Button></td>       
                                @endif

      

                                <td>{{ $podata[0]->quotation->queries->client->client_name}}</td>
                                <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$podata[0]->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$podata[0]->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$podata[0]->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$podata[0]->id),-2)}}</td>
                    <td>{{$podata[0]->c_p_owner_no}}</td>
                    <td style="justify-content: normal;">{{$podata[0]->poItems[$i]->queryItem->priceMappings[0]->supplier->mobile}}/{{$podata[0]->poItems[$i]->queryItem->priceMappings[0]->supplier->street}},{{$podata[0]->poItems[$i]->queryItem->priceMappings[0]->supplier->landmark}},{{$podata[0]->poItems[$i]->queryItem->priceMappings[0]->supplier->sector}},{{$podata[0]->poItems[$i]->queryItem->priceMappings[0]->supplier->city}},{{$podata[0]->poItems[$i]->queryItem->priceMappings[0]->supplier->state}} </td>
                                <td class="image-hover"><img src="{{url('/uploads/'.$podata[0]->poItems[$i]->queryItem->image)}}" width="50px" height="50px"/></td>   
                      <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{$podata[0]->poItems[$i]->queryItem->item_id}}" data-toggle="modal" data-target="#myModal">View</button></td>
                      <td>
                    <select name="iftoday" class="form-control dayshift" style="width: 118px;">
                    <option value=""> Select shift </option>
                        <option value="0"> First Half </option>
                        <option value="1"> Second Half </option>
                    </select>
                    </td>
                    <td><input type='date' class='form-control purchase_date'  value=''></td>
                    <td>
                    <select name="iftoday" class="form-control pteam" style="width: 148px;">
                    <option value=""> Select PP </option>
                    @foreach ($users as $po)
                            <option value="{{$po->id}}">{{$po->first_name}} </option>
                            @endforeach
                     </select>
                    </td>
          
      
                    <td>
                    <select name="transportation" class="form-control transportation_mode" style="width: 128px;">
                    <option value=""> Select </option>
                    <option value="Bus"> Bus </option>
                        <option value="Metro"> Metro </option>
                        <option value="Car"> Car </option>
                        <option value="Auto"> Auto </option>
                        <option value="Other vedhicles"> Other vehicles </option>
                    </select>
                    </td>
                    <td>
                    <select name="criticality" class="form-control criticality" style="width: 118px;">
                     <option value="">Select</option>
                        <option value="Critical">Critical</option>
                        <option value="Normal">Normal</option>
                        <option value="Easily">Easily</option>
                    </select>
                    </td>
                    <td>
                    <select name="priority" class="form-control priority" style="width: 118px;">
                    <option value=""> Select level </option>
                        <option value="2"> High </option>
                        <option value="1"> Medium </option>
                        <option value="0"> Low </option>
                    </select>
                    </td>
                    <td><input type='text' value='{{$podata[0]->poItems[$i]->qty}}' data-id='{{$podata[0]->poItems[$i]->qty}}' class='form-control ' style="width: 120px;" readonly></td>
                    <td><input type='text' value="{{ isset($podata[0]->poItems[$i]->poRegisters[0]->purchasedOrders[0]->pending_qty) ? $podata[0]->poItems[$i]->poRegisters[0]->purchasedOrders[0]->pending_qty : '' }}"  class='form-control' style="width: 100px;" readonly></td>

                    <td><input type='text' value='' data-id='{{$podata[0]->poItems[$i]->qty}}' class='form-control qty' style="width: 120px;"></td>
                    <td><input type='text' class='form-control gtotal' data-io='{{$podata[0]->poItems[$i]->grand_total}}'  style="width: 100px;" value='{{$podata[0]->poItems[$i]->grand_total}}'></td>
                    <td>
                    <select name="cashmode" class="form-control payment_mode" style="width: 118px;">
                    <option value=""> Select </option>
                        <option value="Cash"> Cash </option>
                        <option value="Neft"> Neft </option>
                        <option value="Online"> Online </option>
                    </select>
                    </td>
                    <td>
                    <select name="bank" class="form-control bank" style="width: 118px;">
                    <option value="">Select </option>
                        <option value="PNB">PNB </option>
                        <option value="Kotek">Kotek </option>
                    </select>
                    </td>
           
                    </tr>
                    <input name="pending_qty" class="pending_qty" type="hidden" value="0">
                    <input type="hidden" value="{{$podata[0]->poItems[$i]->id}}" class="poitemid">
                    @endfor
                    </tbody>
                    </table>
                    </div>
                    </div>
                    </div> 

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.quotation.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                  

                    <button type="button" data-id="" id="send" class="btn btn-success">Send</button>

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    
@endsection