@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.quotation.management') . ' | ' . __('labels.backend.access.quotation.create'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width: 900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <input name="user_name" type="hidden" id="userName" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" id="userId" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" id="module" value="RFQManagement">
                    <input name="action" type="hidden" id="action" value="QuotationApproval">
                    <input name="activity" type="hidden" id="activity" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Quotation                
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                   <label class="col-md-1 form-control-label" for="lab">QT No.</label>
                            <div class="col-md-4">
                                <table class="table table-bordered"><tbody><tr><td>QT</td><td><?= 
                                   'M'.$sa=substr(strtoupper($brandletters='M0'.$querydata[0]->client_id),-2);
                                    ?></td><td><?= 
                                    'S'.$sa=substr(strtoupper($brandletters='S0'.$querydata[0]->client_id),-2);
                                     ?></td><td>{{substr(str_replace("-","",explode(" ",$qdate[0]->updated_at)[0]),2)}}-<?='0'.$sa=substr(strtoupper($brandletters='00'.$qid),-2);?> (On Call) </td></tr></tbody></table>
                            </div><!--col-->
                             <div class="col-md-2"></div>
                            <label class="col-md-1 form-control-label" for="lab">QR No.</label>
                            <div class="col-md-4 table-responsive">
                               <table class="table table-bordered"><tbody><tr><td>QR</td><td><?= 
                                   'M'.$sa=substr(strtoupper($brandletters='M0'.$querydata[0]->client_id),-2);
                                    ?></td><td><?= 
                                    'S'.$sa=substr(strtoupper($brandletters='S0'.$querydata[0]->client_id),-2);
                                     ?></td><td>{{substr(str_replace("-","",explode(" ",$querydata[0]->updated_at)[0]),2)}}-<?='0'.$sa=substr(strtoupper($brandletters='00'.$querydata[0]->id),-2);?></td></tr></tbody></table>
                            </div><!--col-->
                 </div>

                <div class="row">
                    <div class="col-md-6  mb-4">
                        <h4 class="card-title">Supplier Details</h4>
                        <h5>{{$querydata[0]->CompanyDetail->company_name}}</h5>
                    </div>
                    <div class="col-md-6 text-right  mb-4">
                       <h4 class="card-title">Client Details</h4>
                        <h5>{{$querydata[0]->Client->client_name}}</h5>
                    </div>
                </div>
                <div class="row  mb-4">
                    <div class="col-md-12">
                             <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Address</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" value="{{$querydata[0]->CompanyDetail->address}}" readonly>
                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">Address</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" value="{{$querydata[0]->Client->street}}{{$querydata[0]->Client->sector}}{{$querydata[0]->Client->city}}{{$querydata[0]->Client->state}}" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Contact No.</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" value="{{$querydata[0]->CompanyDetail->phone_no}}" readonly>
                            </div><!--col-->
                            <label class="col-md-2 form-control-label" for="lab">Query Person</label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" value="{{$querydata[0]->Client->contact_person1}}" readonly>
                        </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                       <label class="col-md-2 form-control-label" for="lab">Query Center</label>
<div class="col-md-4">
    <input type="text" class="form-control"  value="QR0{{$querydata[0]->query_center}}" readonly>
</div><!--col-->
                            <label class="col-md-2 form-control-label" for="lab">Contact No.</label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" value="{{$querydata[0]->Client->contact_number1}}" readonly>
                        </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Email</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control"  value="{{$querydata[0]->CompanyDetail->email}}" readonly>
                            </div><!--col-->
                            <label class="col-md-2 form-control-label" for="lab">Email</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" value="{{$querydata[0]->Client->email}}" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">GST No.</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control"  value="{{$querydata[0]->CompanyDetail->gst_no}}" readonly>
                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">GST No.</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" value="{{$querydata[0]->Client->gst_no}}" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Sales Center</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control"  value="S0{{$querydata[0]->center_id}}" readonly>
                            </div><!--col-->
                            <div class="col-md-6"></div>                           
                        </div><!--form-group-->
                    </div>
                </div>

                <div class="row mt-4">
    <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table input-boxs">
                    <thead>
                    <tr>
                        <th>Description</th>
                        <th>Image</th>
                        <th>HSN Code</th>
                        <th>Unit PP</th>
                        <th>Profit(%)</th>
                        <th>Unit SP</th>
                        <th>GST(%)</th>
                        <th>GST Amt.</th>
                        <th>Total</th>
                        <th>Qty</th>
                        <th>Grand Total</th>
                        <th>Delivery Term</th>
                        <th>Warranty</th>
                        <th>Standard Package</th>
                    </tr>
                    </thead>
                    <tbody  id="data">
                    @foreach($qitems as $items)
                    <tr>
                    <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{$items->item_id}}" data-toggle="modal" data-target="#myModal">View</button></td>
                    <td class="image-hover"><img src="{{url('/uploads/'.$items->image)}}" width="50px" height="50px"/></td>                                                                                                                        
                    <td><input type="text"  id="hsn_code" class="form-control hsn_code"  value='{{$items->hsn_code}}' readonly></td>
                    <form id="test">
                    <td><input type="text"  id="pup" class="form-control pup" size="300" value='{{$items->discounted_price}}'></td>
                    <td><input type="text" class="form-control profit" value=''></td>
                    <td><input type="text" class="form-control net" value='' size="300" readonly></td>
                    <td><input type="text"  id="gst" class="form-control" value='{{$items->gst}}' readonly></td>
                    <td><input type="text" id="gstamount" class="form-control gstamount" size="300" value='' readonly></td>
                    <td><input type="text"  id="total" class="form-control total" size="300" value='{{$items->total}}' readonly></td>
                    <td><input type="text"  id="quantity" class="form-control quantity" size="200" value='{{$items->quantity}}' ></td>
                    <td><input type="text"  id="grand_total" class="form-control grand_total" size="300" value='{{$items->grand_total}}'></td>
                    <td><input type="text"  id="delivery_term" class="form-control delivery_term"  value='{{$items->delivery_term}}' readonly></td>
                    <td><input type="text"  id="warranty" class="form-control warranty" size="200" value='{{$items->warranty}}' readonly></td>
                    <td><input type="text"  id="standard_package" class="form-control standard_package" size="200" value='{{$items->standard_package}}' readonly></td>
                    <input type="hidden"   class="form-control qitemid" value='{{$items->item_id}}'>
                    </form>
                    </tr>
                            @endforeach
                            
                    </tbody>
                    </table>
                    </div>
</div>
 </div> 
 
          

 <div class="row">
 <div class="col-md-12">

 <div class="terms">
 <h4 class="card-title">Terms & Conditions</h4>
 <p class="text-muted">Tax as per GST Rules</p>
 <table>
     <tbody>
     <tr>
          <td><label class="form-control-label"> Payment Terms</label></td>
          <td><label class="form-control-label"> : {{$termsdata[0]->payment_days}} days </label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Freight</label></td>
          <td><label class="form-control-label"> : {{$termsdata[0]->Freight->conditions}} </label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Transport Insurance</label></td>
          <td><label class="form-control-label"> : {{$termsdata[0]->TransportInsurance->conditions}}</label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Validity</label></td>
          <td><label class="form-control-label"> : {{$termsdata[0]->validity_days}} days</label></td>
     <tr>
        
</tbody>

 </table>
 <p><b>{{$termsdata[0]->comment_one}}</b></p>
<p><b>{{$termsdata[0]->comment_two}}</b></p>
<p><b>{{$termsdata[0]->comment_three}}</b></p>
<p><b>{{$termsdata[0]->comment_four}}</b></p>
 </div>
 </div>
 </div>
                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                <div class="col text-left">
                  <button type="button" data-id="{{$qid}}" class="btn btn-primary reject">Reject</button>
                  </div><!--col-->
                  <div class="col text-left">
                  <button type="button" data-id="{{$qid}}" class="btn btn-danger cancel">Cancel</button>
                  </div><!--col-->
                  <div class="col text-left">
                  <button type="button" data-id="{{$qid}}" class="btn btn-warning hold">Hold</button>
                  </div><!--col-->
                    <div class="col text-right">
                    <button type="button" data-id="{{$qid}}" class="btn btn-success send">Send</button>

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    
@endsection
