@extends ('backend.layouts.app')
@section ('title', __('labels.backend.access.uom.management') . ' | ' . __('labels.backend.access.uom.create'))
@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($quotation, 'PATCH', route('admin.quotation.update', $quotation->id))->class('form-horizontal')->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.quotation.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.quotation.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                    <div class="col">
                </div>
                    </div>
                    <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.quotation.supplier_id'))->class('col-md-2 form-control-label')->for('supplier_id') }}
                            <div class="col-md-4">
                    <div class="form-group">
                    <select class="form-control" name="supplier_id">
                    @foreach($suppliers as $stand)
                    <option value="{{$stand->id}}"{{$stand->id==$suppliername->id?"selected":""}}>{{$stand->supplier_name}}</option>
                    @endforeach
                    </select>
                </div><!--col-->
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.quotation.request_no'))->class('col-md-2 form-control-label')->for('request_no') }}
                            <div class="col-md-4">
                                {{ html()->text('request_no')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.quotation.request_no'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        
                           <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.quotation.delivery_term_id'))->class('col-md-2 form-control-label')->for('delivery_term_id') }}

                            <div class="col-md-4">
                            <div class="form-group">
                    <select class="form-control" name="delivery_term_id">
                    @foreach($paymentterms as $stand)
                    <option value="{{$stand->id}}"{{$stand->id==$paymenttermname->id?"selected":""}}>{{$stand->term_name}}</option>
                    @endforeach

                    </select>
                    </div>
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.quotation.customer_request_ref'))->class('col-md-2 form-control-label')->for('customer_request_ref') }}

                            <div class="col-md-4">
                                {{ html()->text('customer_request_ref')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.quotation.customer_request_ref'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.quotation.remark'))->class('col-md-2 form-control-label')->for('remark') }}

                            <div class="col-md-4">
                                {{ html()->text('remark')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.quotation.remark'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.quotation.validate_date'))->class('col-md-2 form-control-label')->for('validate_date') }}

                            <div class="col-md-4">
                                {{ html()->date('validate_date')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.quotation.validate_date'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->


                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.quotation.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection