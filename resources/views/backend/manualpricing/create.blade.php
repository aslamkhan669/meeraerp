@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.manualpurchase.management'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.manualpurchase.store'))->class('form-horizontal')->open() }}

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 180%;">
        <div class="modal-header">
          <h4 class="modal-title">Available Supplier</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
      <th scope="col">Sr No.</th>
      <th scope="col">Name</th>
      <th scope="col">Code</th>
      <th scope="col">Contact no</th>
      <th scope="col">Email</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- Modal -->
<div class="modal fade" id="mysupplier" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 180%;">
        <div class="modal-header">
          <h4 class="modal-title"> Supplier</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
    <th scope="col">Supplier Type</th>
    <th scope="col">Description</th>
      <th scope="col">List price/cost</th>
      <th scope="col">Insurance&Shipping</th>
      <th scope="col">Custom&Clearance</th>
      <th scope="col">Discount</th>
      <th scope="col">PurchaseUnitPrice</th>
      <th scope="col">GST</th>
    </tr>
  </thead>
   
<tbody class="history" >
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <div class="modal fade" id="ModalItem" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody class="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" id="userName" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" id="userId" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" id="module" value="OnCall">
                    <input name="action" type="hidden" id="action" value="OnCallPricing">
                    <input name="activity" type="hidden" id="activity" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                        <div id="msg">
                        </div>
Manual Items                            
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Item No.</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Specification</th>
                            <th>Model No.</th>
                            <th>Brand</th>
                            <th>HSN Code</th>
                            <th>QTY</th>
                            <th>UOM</th>
                            <th>Remarks</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($itemlist as $standobj)
                            <tr>
                                <td>{{'P'.$sa=substr(strtoupper($brandletters='P00'.$standobj->itemList->product_id),-2)}}{{'I'.$sa=substr(strtoupper($brandletters='I0000'.$standobj->itemList->id ),-4)}}</td>
                                <td class="image-hover"><img src="{{url('/uploads/'.$standobj->image)}}" width="50px" height="50px"/></td>                                                                                                                        
                                <td>{{ $standobj->description }}</td>
                                <td>{{ $standobj->specification }}</td>
                                <td>{{ $standobj->Color->color }}</td>
                                <td>{{ $standobj->Brand->brand_name }}</td>
                                <td>{{ $standobj->hsn_code }}</td>
                                <td>{{ $standobj->query_qty }}</td>
                                <td>{{ $standobj->Uom->unit }}</td>
                                <td>{{ $standobj->remarks }}</td>
                                <input type='hidden' class='itemid' value='{{$standobj->item_id}}'>
                                <td> <button type="button" data-item-id="{{ $catid=$standobj->ItemList->id==0?$standobj->ItemList->Category->id:$standobj->ItemList->id }}" data-id="{{ $catid=$standobj->ItemList->Category->parent_id==0?$standobj->ItemList->Category->id:$standobj->ItemList->Category->parent_id }}" value="{{ $standobj->id }}" class="btn btn-info btn-md onpmap sub" data-toggle="modal" data-target="#myModal">Supplier</button></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Supplier/Code</th>
                            <th>Contact/Address</th>  
                            <th>Suppliers</th>          
                            <th>List Price/Cost</th>
                            <th>Insurance & Shipping</th>
                            <th>Custom & Clearance</th>
                            <th>Discount(%)</th>
                            <th>Purchase UnitPrice</th>
                            <th>GST(%)</th>
                            <th>Unit SP</th>
                            <th>Qty.</th>
                            <th>Grand Amt</th>
                            <th>Delivery Terms</th>
                            <th>Warranty</th>
                            <th>Standard Package</th>
                            <th>Is Selected</th>
                            <th>Data</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="onpmap">
                        </tbody>
                    </table>
                </div>
<br/>
<br/>
<br/>
                <div class="row">
 <div class="col-md-12">
 <div class="terms">
 <h4 class="card-title">Terms & Conditions</h4>
 <p class="text-muted"><b>Taxes are per applicable sd per GST Rules.</b></p>
 

 

 <table>
     <tbody>
         <tr>
             <td><label class="form-control-label"> Payment Terms</label></td>
             <td><input type="text" class="form-control" value="100% Advance" name="payment_days" ></td>
         </tr>
         <tr>
             <td><label class="form-control-label"> Freight Charges </label></td>
             <td> <select class="form-control freight_id" name="freight_id">
                                <option value="">Select</option>
                                @foreach($freights as $freight)
                                <option value="{{$freight->id}}">{{$freight->conditions}}</option>
                                @endforeach
                                </select></td>                    
         </tr>
         <tr>
             <td><label class="form-control-label"> Material Insurance</label></td>
             <td><select class="form-control insurance_id" name="insurance_id">
                                <option value="">Select</option>
                                @foreach($insurances as $insurance)
                                <option value="{{$insurance->id}}">{{$insurance->conditions}}</option>
                                @endforeach
                                </select></td>
         </tr>
         <tr>
             <td><label class="form-control-label">Quotation Validity</label></td>
             <td><input type="text" class="form-control" value="Seven Days" name="validity_days" ></td>
         </tr>
     </tbody>
 </table>
 <br/>
 <input type="text" class="line" placeholder="enter any comment" name="comment_one">   <input type="text" class="line" placeholder="enter another comment" name="comment_two"></br>
 <input type="text" class="line" placeholder="enter any comment" name="comment_three">   <input type="text" class="line" placeholder="enter another comment" name="comment_four">
 </div>
 </div>
 </div>
            </div><!--col-->
            
        </div><!--row-->
             </div>
            </div><!--card-body-->
<input type="hidden" value="<?=$id?>" id="qid">
            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.query.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    <button type="button" id="q" class="btn btn-primary btn-sm">Convert to Oncall Quotation</button>
                    </div>
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection

