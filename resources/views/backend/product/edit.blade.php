@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.product.management') . ' | ' . __('labels.backend.access.product.create'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($product, 'PATCH', route('admin.product.update', $product->id))->class('form-horizontal')->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.product.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.product.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                    <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="CategoryManagement">
                    <input name="action" type="hidden" value="Product">
                    <input name="activity" type="hidden" value="update">
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.product.product_name'))->class('col-md-2 form-control-label')->for('product_name') }}

                            <div class="col-md-4">
                                {{ html()->text('product_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.product.product_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.product.product_code'))->class('col-md-2 form-control-label')->for('product_code') }}

                            <div class="col-md-4">
                            <input name="product_code" type="text" class="form-control" value="<?=$code?>" readonly>

                       
                            </div><!--col-->
                        </div><!--form-group-->

                      
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.product.product_owner'))->class('col-md-2 form-control-label')->for('product_owner') }}

                            <div class="col-md-4">
                                {{ html()->text('product_owner')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.product.product_owner'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.product.is_active'))->class('col-md-2 form-control-label')->for('is_active') }}

                        <div class="col-md-4">
                            <label class="switch switch-3d switch-primary">
                                {{ html()->checkbox('is_active',$product->is_active)->class('switch-input') }}
                                <span class="switch-label"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div><!--col-->
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.product.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection

