@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.product.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.product.management') }} <small class="text-muted">{{ __('labels.backend.access.product.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.product.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->
<form action="searchproduct" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter product name" style="width: 30%; display: inline;">
    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.access.product.table.product_name') }}</th>
                            <th>{{ __('labels.backend.access.product.table.product_code') }}</th>    
                            <th>{{ __('labels.backend.access.product.table.product_owner') }}</th>
                            <th>{{ __('labels.backend.access.product.table.is_active') }}</th>
                            <th>Date&Time</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($product as $standobj)
                            <tr>
                                
                                <td>{{ $standobj->product_name }}</td>
                                <td>{{'P'.$sa=substr(strtoupper($brandletters='P0'.$standobj->id),-2)}}</td>
                                <td>{{ $standobj->product_owner }}</td>
                                <td>{{ $standobj->is_active==1?'Active':'Not Active' }}</td>    
                                <td>{{$sa=substr(strtoupper($standobj->updated_at),0,-3)}}</td>
                                <td>{!! $standobj->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><label class="control-label"> {!! $product->total() !!} {{ trans_choice('labels.backend.access.product.table.total', $product->total()) }} </label></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>  <label>  {!! $product->render() !!}</label></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
             
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                {!! $product->render() !!}

                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
