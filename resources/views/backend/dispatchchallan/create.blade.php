@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.quotation.pomanagement') . ' | ' . __('labels.backend.access.quotation.createpo'))

@section('breadcrumb-links')
@endsection

@section('content')
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="module" type="hidden" value="DispatchManagement">
                    <input name="action" id="action" type="hidden" value="DispachChallan">
                    <input name="activity" id="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
Dispatch Challan                           
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                 
                </div><!--col-->
                
                </div><!--form-group-->

                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6 text-right">
                    </div>
                </div>
                <div class="row  mb-4">
                    <div class="col-md-12">
                    <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">PO No.</label>

                            <div class="col-md-4">
                            <!-- <select class="form-control po_no" name="po_no">
                                <option value="">Select</option>
                                @foreach($poinfo as $data)
                                        <option value="{{$data->id}}" data-id="{{$data->id}}">PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$data->poItem->poFormatting->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$data->poItem->poFormatting->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$data->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$data->poItem->poFormatting->id),-2)}}</option>
                                @endforeach

                                </select> -->
                                <form action="searchitem" method="post">
@csrf
 
    <input type="text" class="form-control search-box"  autocomplete="off" placeholder="Enter Product Name" style="width: 80%; display: inline;">
                                <div class="suggestion-box"></div>

                                <input type="hidden" class="po_id" name="po_no">
</form>
                            </div><!--col-->
                        </div><!--form-group-->

                    </div>

                </div>
              

                <div class="row mt-4">
    <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                    <th></th>
                        <th>S.NO.</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Unit PP</th>
                        <th>GST(%)</th>
                        <th>Unit SP</th>
                        <th>Qty</th>
                        <th>Grand Total</th>
                        <th>Select Item</th>
                        
                        
                    </tr>
                    </thead>
                    <tbody  class="data">
                    
                 
                    </tbody>
                    </table>
                    </div>
                    </div>
                    </div> 

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.dispatchchallan.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                  

                    <button type="button" data-id="" id=senddispatch class="btn btn-success send">Send</button>

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $('.search-box').on('keyup',function(){
        debugger;
        var keyword=$(this).val();
        if(keyword.length>0){	
        $.ajax({
            url:'/admin/dispatchchallan/ajax/' + encodeURI(keyword),
            type:'get',
            dataType:'JSON',
            success:function(data){
                debugger;
                $(".suggestion-box").show();
               var html='';
               html+='<ul id="pro-list">';
               for (var i = 0; i < data.length; i++) {
               html+='<li class="selectProduct" data-id='+data[i].id+'>PO/'+data[i].id+'</li>';
               }
               html+='</ul>';
               $(".suggestion-box").html(html);  
               $(".selectProduct").on("click",function(e){
                   $(".search-box").val($(this).text());
                   $(".po_id").val($(this).attr('data-id'));
                   $(".suggestion-box").hide();
               })
               $(".search-box").css("background","#FFF");
                      }
        })
    }else{
        alert('1 keyword must entered.')
    }
        });
           
});
</script>