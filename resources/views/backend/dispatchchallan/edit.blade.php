@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.purchaseaccount.management') )

@section('breadcrumb-links')
@endsection

@section('content')
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="userName" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="userId" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="Module" type="hidden" value="DispatchManagement">
                    <input name="action" id="Action" type="hidden" value="DispatchChallan">
                    <input name="activity" id="Activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                          Dispatch Challan                            
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />
                <div class="row mt-4 mb-4">
                <input type="hidden" value="{{Auth::user()->id}}" name="dt_user_id" id="dt_user_id">
                <input type="hidden" value="{{$data[0]->poFormatting->quotation->queries->center_id}}" name="center_id" id="center_id">
                <input type="hidden" value="{{$data[0]->poFormatting->quotation->queries->client_id}}" name="client_id" id="client_id">
                <input type="hidden" value="{{$id}}" name="po_id" id="po_id">

                   <label class="col-md-1 form-control-label" for="lab"></label>

                            <div class="col-md-4">
                            </div><!--col-->

                             <div class="col-md-2"></div>
                            <label class="col-md-1 form-control-label" for="lab">Challan No.</label>

                            <div class="col-md-4 table-responsive">
                            <table class="table table-bordered"><tbody><tr><td>CH</td><td>{{ 'M'.$sa=substr(strtoupper($brandletters='M0'.$data[0]->poFormatting->quotation->queries->client_id),-2)}}</td><td>{{$data[0]->poFormatting->quotation->queries->client->sales_center}}</td><td>{{substr(str_replace("-","",explode(" ",$data[0]->poFormatting->quotation->queries->client->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$data[0]->poFormatting->id),-2)}}</td></tr></tbody></table>

                            </div><!--col-->
                   
                 </div>
               

                <div class="row">

                    <div class="col-md-6  mb-4">
                    <h4 class="card-title">Purchaser's Details</h4> 
                        <h5>{{$data[0]->poFormatting->quotation->queries->client->client_name}}</h5>
                    </div>
                    <div class="col-md-6 text-right  mb-4">
                    <h4 class="card-title">Supplier Details</h4>
                    <h5>{{$data[0]->poFormatting->quotation->queries->companyDetail->company_name}}</h5>

                    </div>
                </div>
                <div class="row  mb-4">
                    <div class="col-md-12">
                             <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Address</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control" value="{{$data[0]->poFormatting->quotation->queries->client->street}} {{$data[0]->poFormatting->quotation->queries->client->sector}}{{$data[0]->poFormatting->quotation->queries->client->city}}{{$data[0]->poFormatting->quotation->queries->client->state}}" readonly>

                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">Address</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control" value="{{$data[0]->poFormatting->quotation->queries->companyDetail->address}}" readonly>

                            </div><!--col-->
                        </div><!--form-group-->
                      
                      
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Email</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control"  value="{{$data[0]->poFormatting->quotation->queries->client->email}}" readonly>

                            </div><!--col-->
                            
                            <label class="col-md-2 form-control-label" for="lab">Email</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control"  value="{{$data[0]->poFormatting->quotation->queries->companyDetail->email}}" readonly>

                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">GST No.</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control"  value="{{$data[0]->poFormatting->quotation->queries->client->gst_no}}" readonly>

                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">GST No.</label>

                            <div class="col-md-4">
                            <input type="text" class="form-control"  value="{{$data[0]->poFormatting->quotation->queries->companyDetail->gst_no}}" readonly>

                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                 
                        </div><!--form-group-->

                    </div>

                </div>
                
                
                <input type="hidden" value="" name="challan_id[]" id="challan_id">


                <div class="row mt-4">
    <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Qty</th>
                        <th>GST(%)</th>
                        
                        
                    </tr>
                    </thead>
                    <tbody class="data">
                    <?php $i=1;?>

                        @foreach($data as $items)
                        <tr>
                        <td>{{$i++}}</td>
                        <input type="hidden"  class="form-control itemId " name="item_id[]"  value='{{$items->queryItem->item_id}}'>
                        <td><button type='button' class='btn btn-info tClick' data-id='{{$items->queryItem->item_id}}' value='{{$items->queryItem->item_id}}' data-toggle='modal' data-target='#ModalItem'>View</button></td>
                        <td class="image-hover"><img src="{{url($items->queryItem->itemList->image)}}" width="50px" height="50px"/></td>   
                        <td><input type="input"  class="form-control quantity" name="quantity[]"  value='{{$items->qty}}' readonly></td>
                        <td><input type="input" class="form-control gst" name="gst[]" value='{{$items->queryItem->priceMappings[0]->gst}}' readonly></td>
                        </tr>
                        @endforeach
        
     
                    </tbody>
                    </table>
                    </div>
</div>
 </div> 
 
  <div class="row">
 <div class="col-md-12">
 <div class="terms">
 <h4 class="card-title">Terms & Conditions</h4>
 <p class="text-muted"><b>Taxes are per applicable sd per GST Rules.</b></p>
 

 

 <table>
     <tbody>
         <tr>
             <td><label class="form-control-label"> Payment Terms</label></td>
             <td><input type="text" class="form-control payment_days" value="100% Advance" name="payment_days" ></td>
         </tr>
         <tr>
             <td><label class="form-control-label"> Freight Charges </label></td>
             <td> <select class="form-control freight_id" name="freight_id">
                                <option value="">Select</option>
                                @foreach($freightinfo as $freight)
                                <option value="{{$freight->id}}">{{$freight->conditions}}</option>
                                @endforeach
                                </select></td>                    
         </tr>
         <tr>
             <td><label class="form-control-label"> Material Insurance</label></td>
             <td><select class="form-control insurance_id" name="insurance_id">
                                <option value="">Select</option>
                                @foreach($insuranceinfo as $insurance)
                                <option value="{{$insurance->id}}">{{$insurance->conditions}}</option>
                                @endforeach
                                </select></td>
         </tr>
         <tr>
             <td><label class="form-control-label">PO Validity</label></td>
             <td><input type="text" class="form-control validity" value="Seven Days" name="validity_days" ></td>
         </tr>
     </tbody>
 </table>
 <br/>
 <input type="text" class="line1" placeholder="enter any comment" name="comment_one">   <input type="text" class="line2" placeholder="enter another comment" name="comment_two"></br>
 <input type="text" class="line3" placeholder="enter any comment" name="comment_three">   <input type="text" class="line4" placeholder="enter another comment" name="comment_four">
 </div>
 </div>
 </div>
                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                <div class="col text-left">
                  

                  <button type="button" class="btn btn-default" >Close</button>

                  </div><!--col-->

                    <div class="col text-right">

                    <input type="submit" class="btn btn-primary" id="dischallan" value="send">

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    </form>

        </div>
    
      </div>
    </div>
    <div class="modal fade" id="ModalItem" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="section">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

@endsection