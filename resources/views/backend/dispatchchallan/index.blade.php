@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.purchaseaccount.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.dispatchchallan.management') }} <small class="text-muted"></small>
                </h4>
            </div><!--col-->
            <div class="col-sm-7">
                @include('backend.dispatchchallan.includes.header-buttons')
            </div><!--col-->
           
        </div><!--row-->
        <form action="searchdischallan" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Challan No" style="width: 20%; display: inline;">
    <!-- <input type="text" value=""  name="clientpono" class="form-control" placeholder="Enter Client PO No" style="width: 20%; display: inline;"> -->
    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>MIN PO No</th>
                            <th>Client PO NO</th>
                            <th>Challan No</th>
                            <th>Date&Time</th>
                            <th>Action</th>
                            <th>PDF</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($dispatchchallans as $poformat)
                            <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$poformat->PoFormatting->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$poformat->PoFormatting->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$poformat->PoFormatting->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poformat->PoFormatting->id),-2)}}</td>
                            <td>{{$poformat->PoFormatting->c_p_owner_no}}</td> 
                            <td>CH/{{ 'M'.$sa=substr(strtoupper($brandletters='M0'.$poformat->PoFormatting->quotation->queries->client_id),-2)}}/{{$poformat->PoFormatting->quotation->queries->client->sales_center}}/{{substr(str_replace("-","",explode(" ",$poformat->PoFormatting->quotation->queries->client->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$poformat->PoFormatting->id),-2)}}</td>   
                                <td>{{$sa=substr(strtoupper($poformat->updated_at),0,-3)}}</td> 
                                <td><a class="btn btn-sm  btn-success" href="{{ url('admin/dispatchchallan/edit/') }}/<?=$poformat->po_id?>">Generate Challan</a></td>
                                <td><span><a class="btn btn-sm btn-info" href="{{ url('admin/dispatchchallan/pdf/') }}/<?=$poformat->po_id?>">Pdf</a></span></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm" style="max-width: 850px;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
        <form role="myForm" id="itemform" method="post"  action='backend/poregister/data'>
      <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                          Purchase Challan                            
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />
                <div class="row mt-4 mb-4">
                <input type="hidden" value="" name="supplier_id" id="supplier_id">
                <input type="hidden" value="" name="client_id" id="client_id">
                <input type="hidden" value=""  name="po_reg_id">
                   <label class="col-md-1 form-control-label" for="lab">ChallanNo.</label>

                            <div class="col-md-4">
                                <table class="table table-bordered"><tbody><tr><td>CH</td><td></td><td></td><td></td></tr></tbody></table>
                            </div><!--col-->

                             <div class="col-md-2"></div>
                            <label class="col-md-1 form-control-label" for="lab">ChallanNo.</label>

                            <div class="col-md-4 table-responsive">
                               <table class="table table-bordered"><tbody><tr><td>CH</td><td></td><td></td><td></td></tr></tbody></table>
                            </div><!--col-->
                   
                 </div>
               

                <div class="row">

                    <div class="col-md-6  mb-4">
                         
                        <h4 class="card-title">Supplier Details</h4>
                        <h5></h5>
                    </div>
                    <div class="col-md-6 text-right  mb-4">
                       <h4 class="card-title">Client Details</h4>
                        <h5></h5>
                    </div>
                </div>
                <div class="row  mb-4">
                    <div class="col-md-12">
                             <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Address</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" value="" readonly>
                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">Address</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" value="" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                      
                      
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">Email</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control"  value="" readonly>
                            </div><!--col-->
                            
                            <label class="col-md-2 form-control-label" for="lab">Email</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" value="" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label class="col-md-2 form-control-label" for="lab">GST No.</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control"  value="" readonly>
                            </div><!--col-->
                             <label class="col-md-2 form-control-label" for="lab">GST No.</label>

                            <div class="col-md-4">
                                <input type="text" class="form-control" value="" readonly>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        <label class="col-md-2 form-control-label" for="lab"></label>
                        <label class="col-md-2 form-control-label" for="lab">Supplier Challan.</label>
                        <div class="col-md-4">
                                <input type="text" class="form-control" id="supplier_challan"  value="" >
                            </div><!--col-->
                        </div><!--form-group-->

                    </div>

                </div>
                
                
                <input type="hidden" value="" name="challan_id[]" id="challan_id">


                <div class="row mt-4">
    <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Unit price</th>
                        <th>GST</th>
                        <th>Total Amount</th>
                        
                        
                    </tr>
                    </thead>
                    <tbody id="data">
                    
                    
                  
                    <tr>
                    <td></td>
                    <td><button type='button' class='btn btn-info tdClick' data-id='' value='' data-toggle='modal' data-target='#ModalItem'>View</button></td>
                    <td><input type="input"  class="form-control quantity" name="quantity[]" id="quantity" value=''></td>
                    <td><input type="input" class="form-control" name="total[]" id="total" value=''></td>
                    <td><input type="text" class="form-control" name="gst[]" id="gst" value='' readonly></td>
                    <td><input type="text" class="form-control" name="grand_total[]" id="grand_total" value='' readonly></td>
                    
                    </tr>
                            
                    </tbody>
                    </table>
                    </div>
</div>
 </div> 
 
          

 <div class="row">
 <div class="col-md-12">

 <div class="terms">
 <h4 class="card-title">Terms & Conditions</h4>
 <p class="text-muted">Tax as per GST Rules</p>
 <table>
     <tbody>
     <tr>
          <td><label class="form-control-label"> Payment Terms</label></td>
          <td><label class="form-control-label"> :  days </label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Freight</label></td>
          <td><label class="form-control-label"> : </label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Transport Insurance</label></td>
          <td><label class="form-control-label"> :  </label></td>
     <tr>
     <tr>
          <td><label class="form-control-label"> Validity</label></td>
          <td><label class="form-control-label"> :  days</label></td>
     <tr>
        
</tbody>

 </table>
 <p><b></b></p>
<p><b></b></p>
<p><b></b></p>
<p><b></b></p>
 </div>
 </div>
 </div>
                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                <div class="col text-left">
                  

                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div><!--col-->

                    <div class="col text-right">

                    <input type="submit" class="btn btn-primary" id="challan" value="send">

                    </div><!--col-->
                </div><!--row-->
                
            </div><!--card-footer-->
        </div><!--card-->
    </form>

        </div>
    
      </div>
    </div>
    <div class="modal fade" id="ModalItem" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width:900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="section">
  
    
  </tbody>
</table>        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</div><!--card-->
@endsection
