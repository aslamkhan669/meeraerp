@extends ('backend.layouts.app')
@section ('title', __('labels.backend.access.categories.management') . ' | ' . __('labels.backend.access.categories.create'))

@section('breadcrumb-links')
@endsection

@section('content')


<form action="subcatalogue" method="POST">
{{ csrf_field() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                             Catalogue
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                 <div class="row mt-4 mb-4">
                 <div class="col">
                    <div class="form-group row" >
                            {{ html()->label(__('validation.attributes.backend.access.categories.product_name'))->class('col-md-2 form-control-label')->for('category_code') }}

                        <div class="col-md-4">
                       <input type="text" class="form-control searchprod"  autocomplete="off">
                                <div class="suggestion-box"></div>
                                <input type="hidden" class="product_id" name="product_id">

                               
                        </div><!--col-->
                        
                        </div><!--form-group-->
                       
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.category.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection
