@extends ('backend.layouts.app')


@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            @include('backend.category.includes.header-buttons')

            </div><!--col-->
        </div><!--row-->
        <form action="searchcategory" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Category name" style="width: 30%; display: inline;">
    <button type="submit" class="btn btn-success btn-md" value="Search" style="margin-top: -5px;"><i class="fa fa-search"></i></button>
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>{{ __('labels.backend.access.categories.table.product_name') }}</th>
                            <th>{{ __('labels.backend.access.categories.table.category') }}</th>
                            <th>{{ __('labels.backend.access.categories.table.category_code') }}</th>
                            <th>{{ __('labels.backend.access.categories.table.is_active') }}</th>
                            <th>Date&Time</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($categories as $category)
                            <tr> 
                            <td>{{ $category->product->product_name }}</td> 
                                <td>{{ $category->category_name }}</td> 
                                <td> {{'C'.$sa=substr(strtoupper($brandletters='C00'.$category->id),-3)}}</td>                               
                                <td>{{ $category->is_active==1?'Active':'Not Active' }}</td>    

                                <td>{{$sa=substr(strtoupper($category->updated_at),0,-3)}}</td>

                                <td>{!! $category->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><label class="control-label">{!! $categories->total() !!} {{ trans_choice('labels.backend.access.categories.table.total', $categories->total()) }}
</label></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                {!! $categories->render() !!}

                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
