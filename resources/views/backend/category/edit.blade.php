@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.categories.management') . ' | ' . __('labels.backend.access.categories.edit'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($category, 'PATCH', route('admin.category.update', $category->id))->class('form-horizontal')->attributes(array('enctype'=>'multipart/form-data'))->open() }}
<div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="CategoryManagement">
                    <input name="action" type="hidden" value="Category">
                    <input name="activity" type="hidden" value="update">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.categories.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.categories.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                    <div class="form-group row">
                         
                            {{ html()->label(__('validation.attributes.backend.access.categories.product_name'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                            <select class="form-control" name="product_id">
                            @foreach($productname as $product)
                                    <option value="{{$product->id}}"{{$product->id==$productinfo->id?"selected":""}}>{{$product->product_name}}</option>
                                    @endforeach
                                    </select>
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.categories.category_code'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                            <input name="code" type="text" class="form-control" value="    <?= 
                                   'C'.$sa=substr(strtoupper($brandletters='C00'.$category->id),-3);
                                    ?>" readonly>


                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row parentcat"
                         @if($isParent) 
                            style="display:none";
                         @endif   
                         >
                            {{ html()->label(__('validation.attributes.backend.access.categories.select_category'))->class('col-md-2 form-control-label')->for('category') }}

                            <div class="col-md-4" >
                            <select class="form-control" name="parent_id" id="parentcat">
                                 @foreach($parentCategory as $parentcategory)
                     <option value="{{$parentcategory->id}}"{{($parentcategory->id==$category->parent_id)?'selected':''}} >{{$parentcategory->category_name}}</option>
                                 @endforeach             
                            </select>
                        
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.categories.category'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('category_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.categories.category'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.categories.active'))->class('col-md-2 form-control-label')->for('active') }}

                            <div class="col-md-4">
                            <label class="switch switch-3d switch-primary">
                                    {{ html()->checkbox('is_active',$category->is_active)->class('switch-input') }}
                                    <span class="switch-label"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row parentcat"  @if($isParent) 
                            style="display:none";
                         @endif   >
                            {{ html()->label(__('validation.attributes.backend.access.categories.image'))->class('col-md-2 form-control-label')->for('image') }}

                            <div class="col-md-4">
                                <!-- <img src="/storage/{{$category->image }}" width="50px" height="50px"/> -->
                               <img src="../../../{{$category->image }}" width="50px" height="50px"/>
                                <input type="file" id="image" name="image" value="{{$category->image}}" />
                            </div><!--col-->

                             </div><!--form-group-->
                             <div class="form-group row">
               
                             </div><!--form-group-->

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                {{ form_cancel(route('admin.category.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                     {{ form_submit(__('buttons.general.crud.update')) }} 
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    //hide on load
    $('.unshow').hide();

    $('select[name="product_id"]').on('change', function() {
        
        var product_id = $(this).val();
        
        var isparent = $("#isParent").val()=="1"?true:false;
        debugger;
            if(product_id && !isparent) {
                $.ajax({
                    url: '/admin/parentcategories/ajax/'+encodeURI(product_id),
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        var html = "<option>Select</option>";
                        for(var i=0;i<data.length;i++){ 
                            html+="<option value="+data[i].id+">"+data[i].category_name+"</option>";
                        }
                        $("#parentcat").html(html);
                        
                        
                    }
                });
            }
            else{
                $('select[name="state"]').empty();
            }
    });

    $('select[name="category_id"]').on('change',function(){
        
        alert('change2');
        var cat_id=$(this).val();
        $('#cat_id').val(cat_id);

    });
    
    $('.search-box').on('keyup',function(){
        debugger;
        var keyword=$(this).val();
        if(keyword.length>2){	
        $.ajax({
            url:'/admin/products/ajax/' + encodeURI(keyword),
            type:'get',
            dataType:'JSON',
            success:function(data){
                $("#suggestion-box").show();
               var html='';
               html+='<ul id="pro-list">';
               for (var i = 0; i < data.length; i++) {
               html+='<li class="selectProduct" data-id='+data[i].id+'>'+data[i].product_name+'</li>';
               }
               html+='</ul>';
               $("#suggestion-box").html(html);  
               $(".selectProduct").on("click",function(e){
                   $(".search-box").val($(this).text());
                   $(".product_id").val($(this).attr('data-id'));
                   $("#suggestion-box").hide();
               })
               $(".search-box").css("background","#FFF");
                      }
        })
    }else{
        alert('3 keyword must entered.')
    }
        });
           
});
</script>