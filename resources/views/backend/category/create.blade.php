@extends ('backend.layouts.app')
@section ('title', __('labels.backend.access.categories.management') . ' | ' . __('labels.backend.access.categories.create'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.category.store'))->class('form-horizontal')->attributes(array('enctype'=>'multipart/form-data'))->open()}}

        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="CategoryManagement">
                    <input name="action" type="hidden" value="Category">
                    <input name="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.categories.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.categories.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                    <div class="form-group row" >
                            {{ html()->label(__('validation.attributes.backend.access.categories.product_name'))->class('col-md-2 form-control-label')->for('category_code') }}

                      
                      <div class="col-md-4">
                        <select class="form-control" name="product_id">
                        <option value="">Select</option>     
                        @foreach($productname as $product)
                                 <option value="{{$product->id}}">{{$product->product_name}}</option>
                                 @endforeach
                                </select>
                        </div><!--col-->
                        {{ html()->label(__('validation.attributes.backend.access.categories.category'))->class('col-md-2 form-control-label')->for('category_code') }}

                        <div class="col-md-4">
                            {{ html()->text('category_name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.backend.access.categories.category'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                        </div><!--form-group-->
                        <input name="code" type="hidden" value="<?=$code?>">
                        <div class="form-group row">

                            {{ html()->label(__('validation.attributes.backend.access.categories.category_code'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                            {{ html()->text('')

                            ->class('form-control')
                            ->placeholder(__('validation.attributes.backend.access.categories.category_code'))
                            ->attribute('maxlength', 191)
                            ->autofocus()
                            ->readonly() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.categories.active'))->class('col-md-2 form-control-label')->for('active') }}

                            <div class="col-md-4">
                            <label class="switch switch-3d switch-primary">
                                    {{ html()->checkbox('is_active', true, '1')->class('switch-input') }}
                                    <span class="switch-label"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.category.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection
