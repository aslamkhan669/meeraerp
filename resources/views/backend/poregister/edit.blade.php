@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.purchaseaccount.management') . ' | ' . __('labels.backend.access.uom.create'))

@section('breadcrumb-links')
@endsection

@section('content')


  <!-- Transfer Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div> -->
        <div class="modal-body">
          <label>Reason(Why):-</label>
          <input type="text" class="form-control" id="reason" value="">
          <label>Rejected QTY:-</label>
          <input type="text" class="form-control" id="qty" value="">
          <label>Bin No:-</label>
          <input type="text" class="form-control" id="bill_no" value="">
          <label>Bin Location:-</label>
          <input type="text" class="form-control" id="location" value="">
          <input type="submit" id="transfer" class="btn btn-success" value="Transfer">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


<div class="modal fade" id="myModalItem" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width: 900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="sec">
  
    
  </tbody>
</table></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="module" type="hidden" value="PurchaseManagement">
                    <input name="action" id="action" type="hidden" value="PurchaseAccount">
                    <input name="activity" id="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.purchaseaccount.management') }}
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                        <th>Status</th>
                          <th>Client Name</th>
                            <th>MIN PO NO</th>
                            <th>Client PO NO</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Day Shift</th>
                        <th>Criticality</th>
                        <th>Priority</th>
                        <th>Qty</th>
                        <th>Pending Qty</th>
                        <th>Payment Amount</th>
                        <th>Payment Mode</th>
                        <th>M/L Purchased</th>
                        <th>M/L Recieved</th>
                        <th>Purchase Receipt</th>
                        <th>Bill/Challan Status</th>
                        <th>Bill/Challan No.</th>
                        <th>Remarks</th>
                        <th>E-wayBill No</th>
                        <th>Inventory</th>
                    </tr>
                    </thead>
<tbody class="data">
@foreach ($data as $post)
      <input type="hidden" value="{{$post->pt_user_id}}"  class='form-control pt_user' style="width: 88px;">
      <tr>
  

             @if(isset($post->purchasedOrders[0]))
                       @if ($post->purchasedOrders[0]->pending_qty == '1')
                       <td><Button class="btn btn-danger">Pending</Button></td>
                     @elseif ($post->purchasedOrders[0]->pending_qty > '1')
                <td><Button class="btn btn-danger">Pending</Button></td> 
                 @else
               <td><Button class="btn btn-success"> UnPending</Button></td> 
                     @endif      
                 @else
                       <td><Button class="btn btn-success">UnPending</Button></td>    
                   @endif                  
      <td><input type="text" value="{{$post->poItem->poFormatting->quotation->queries->client->client_name}}"  class='form-control' style="width: 250px;" readonly></td>
      <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$post->poItem->poFormatting->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$post->poItem->poFormatting->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$post->poItem->poFormatting->quotation->queries->client->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$post->poItem->poFormatting->id),-2)}}</td>
      <td><input type="text" value="{{$post->poItem->poFormatting->c_p_owner_no}}"  class='form-control' style="width: 120px;" readonly></td>

      <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{$post->poItem->queryItem->item_id}}" data-toggle="modal" data-target="#myModalItem">View</button></td>
      <td class="image-hover"><img src="{{url('/uploads/'.$post->poItem->queryItem->image)}}" width="50px" height="50px"/></td>   
                    <td><input type="text" value="{{$post->day_shift==0?'First Half':'Second Half'}}"  class='form-control' style="width: 120px;" readonly></td>
                    <td><input type="text" value="{{$post->criticality}}"  class='form-control' style="width: 88px;" readonly></td>

     <!-- <td>
                    <input type="radio" name="payment_mode" value="Cash" <?php echo ($post->criticality=='Critical')?'checked':'' ?>> Cash<br>
                    <input type="radio" name="payment_mode" value="Bill" <?php echo ($post->criticality=='Normal')?'checked':'' ?>> Bill<br>
                    <input type="radio" name="payment_mode" value="Challan" <?php echo ($post->criticality=='Easily')?'checked':'' ?>> Challan  
                    </td> -->
                                @if($post->priority =='0')
                                <td><input type="button" value="Low" class="btn btn-info btn-sm"></td>
                                @elseif($post->priority =='1')
                                <td><input type="button" value="Medium" class="btn btn-warning btn-sm"></td> 
                                @elseif($post->priority =='2')
                                <td><input type="button" value="High" class="btn btn-danger btn-sm"></td>        
                                @else
                                <td><Button class="btn btn-danger">High</Button></td>    
                                @endif
                    <td><input type="input" value="{{$post->qty}}" data-id='{{$post->qty}}' class="form-control qty" style="width: 80px;"></td>
                    <td><input type="input" value="{{ isset($post->purchasedOrders[0]->pending_qty) ? $post->purchasedOrders[0]->pending_qty : '' }}"  class="form-control pendingqty" style="width: 80px;" readonly></td>
                    <td><input type="input" value="{{$post->payment_amount}}"  class='form-control gtotal' style="width: 88px;"></td>
                    <td><input type="text" value="{{$post->payment_mode}}"  class='form-control payment_mode' style="width: 88px;" readonly></td>

        
                    @if(isset($post->purchasedOrders[0]->ml_purchased))
                                @if($post->purchasedOrders[0]->ml_purchased==1)
                                <td><input type="button" value="Purchased" class="btn btn-success btn-sm"></td>    
                                @elseif($post->purchasedOrders[0]->ml_purchased==0)
                                <td><input type="button" value="NOT Purchased" class="btn btn-info btn-sm"></td>    
                                @endif
                                @else
                                <td><input type="button" value="NOT Purchased" class="btn btn-info btn-sm"></td>    
                            @endif
                    <td><input type="checkbox" class="ml_recieved" name="ml_recieved" {{ (isset($post->purchasedOrders[0]->ml_recieved) && $post->purchasedOrders[0]->ml_recieved==1) ? 'checked' : '' }}  value=""></td>
                    <td><input type="checkbox" class="recieved_doc" name="recieved_doc" {{ (isset($post->purchasedOrders[0]->recieved_doc) && $post->purchasedOrders[0]->recieved_doc==1) ? 'checked' : '' }} value=""></td>
                    <td><input type="text" class="form-control bill_challan_amt" value="{{ isset($post->purchasedOrders[0]->payment_mode) ? $post->purchasedOrders[0]->payment_mode : '' }}" style="width: 100px" readonly></td>
                    <td><input type="text" class="form-control bill_challan_no" value="{{ isset($post->purchasedOrders[0]->bill_challan_no) ? $post->purchasedOrders[0]->bill_challan_no : '' }}" style="width: 120px;"></td>
                    
                    <input type="hidden" value="{{$post->id}}" class="poregisterid" name="poregisterid">
                    <td><input type="text" value="{{ isset($post->purchasedOrders[0]->remarks) ? $post->purchasedOrders[0]->remarks : '' }}"  class='form-control ' style="width: 200px;"  readonly></td>
                    <td><input type="text" name="ewaybill_no"  class='form-control ewaybillno ' style="width: 150px;" ></td>
                       <td>  <button type="button" class="btn btn-sm  btn-dark" data-toggle="modal" data-target="#myModal">Transfer</button></td>
                    </tr>

@endforeach

                    </tbody>

                    </table>
                    </div>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
                <div class="form-group row">
                <label class="col-md-2 form-control-label" for="lab"></label>
                <h4>Material Expenses</h4>

                            <div class="col-md-2">

                                <input type="text" class="form-control"  style="width: 200px; value="{{$expensesamt}}"  readonly>
                            </div><!--col-->
                            <div class="col-md-6"></div>                           
                        </div><!--form-group-->
</div><!--card-body-->
            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.poregister.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    <input class="btn btn-success btn-sm" id="sub" data-id="{{$id}}" type="button" value="Submit">
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}


  </div>
@endsection