@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.purchaseaccount.management') . ' | ' . __('labels.backend.access.uom.create'))

@section('breadcrumb-links')
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
Purchase Account                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                <div class="col-md-12">
                       <div class="table-responsive" >

        <table class="table">
                        <thead>
                        <tr>
                            
                        <th>min PO NO</th>
                        <th>Purchaser Name</th>
                        <th>Pending Qty</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($info as $data)
                            <tr>
 <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$data->poRegister->poItem->poFormatting->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$data->poRegister->poItem->poFormatting->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$data->poRegister->poItem->poFormatting->quotation->queries->client->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$data->poRegister->poItem->po_id),-2)}}</td>

                                <td>{{ $data->user->first_name }} </td>
                                <td>{{ $data->pending_qty }} </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
      </div>
                    </div>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
</div><!--card-body-->
            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.poregister.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}


  </div>
@endsection