@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.purchaseaccount.management') . ' | ' . __('labels.backend.access.uom.create'))

@section('breadcrumb-links')
@endsection

@section('content')

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
Purchase Account                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                <div class="col-md-12">
                       <div class="table-responsive" >

        <table class="table">
                        <thead>
                        <tr>
                            
                        <th>Bill/Challan NO</th>
                        <th>Total Amount</th>
                        <th>e-way bill</th>
                        <th>e-way bill No</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $da)
                        @if($da->bill_challan_no)
                            <tr>
                                <td>{{ $da->bill_challan_no }} </td>
                                <td>{{ $da->p }} </td>
                                <td>{{$da->p>=50000?'Required':'Not Required'}}</td>
                                <td>{{ $da->ewaybill_no }} </td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
      </div>
                    </div>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
</div><!--card-body-->
            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.poregister.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}


  </div>
@endsection