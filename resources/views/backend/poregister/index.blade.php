@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.purchaseaccount.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.purchaseaccount.management') }} <small class="text-muted"></small>
                </h4>
            </div><!--col-->
            <div class="col-sm-7">

                <h4>
                    Pending Qty
            </h4>
            <a class="button" href="{{ url('admin/poregister/create') }}">info</a>
            </div>

           
        </div><!--row-->
        <form action="searchporegister" method="post">
@csrf
    <input type="text" class="search-box"  autocomplete="off" placeholder="Enter Client PO NO">
                                <div class="suggestion-box"></div>
                                <input type="hidden" class="poitemid" name="poitemid">

    <input type="submit" value="Search">
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>Status</th>
                        <th>Purchased/InPurchasing</th>
                        <!-- <th>Client Name</th>
                            <th>Client PO NO</th> -->
                            <th>Purchaser Name</th>
                            <th>Purchaser Date</th>
                            <th>Total Amount</th>

                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($poregisters as $poformat)
                            <tr>
                    
                                @if(isset($poformat->purchasedOrders[0]->complete))
                                @if($poformat->purchasedOrders[0]->complete=='0')
                                <td><Button class="btn btn-success">Not Submit</Button></td>       
                                @elseif($poformat->purchasedOrders[0]->complete =='1')
                                <td><Button class="btn btn-danger">Submit</Button></td> 
                                @endif      
                                @else
                                 <td><Button class="btn btn-success">Not Submit</Button></td>       
                                @endif 
                                @if(isset($poformat->purchasedOrders[0]->ml_purchased))
                                @if($poformat->purchasedOrders[0]->ml_purchased=='0')
                                <td><Button class="btn btn-success">in purchasing</Button></td>       
                                @elseif($poformat->purchasedOrders[0]->ml_purchased =='1')
                                <td><Button class="btn btn-danger"> purchased</Button></td> 
                                @endif      
                                @else
                                 <td><Button class="btn btn-success">in purchasing</Button></td>       
                                @endif
                            <td>{{$poformat->user->first_name}}</td>
                                <td>{{$poformat->purchase_date}}</td>
                                <td><a class="button" href="{{ url('admin/poregister/show/') }}/<?=$poformat->pt_user_id?>/<?=$poformat->purchase_date?>">info</a></td>
                                <td><a class="button" href="{{ url('admin/poregister/edit/') }}/<?=$poformat->pt_user_id?>/<?=$poformat->purchase_date?>">View</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $poregisters->total() !!} {{ trans_choice('labels.backend.access.brand.table.total', $poregisters->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $poregisters->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

</div><!--card-->
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function (){
    $('.search-box').on('keyup',function(){
        debugger;
        var keyword=$(this).val();
        if(keyword.length>2){	
        $.ajax({
            url:'/admin/pogenerate/ajax/' + encodeURI(keyword),
            type:'get',
            dataType:'JSON',
            success:function(data){
                debugger;
                $(".suggestion-box").show();
               var html='';
               html+='<ul id="pro-list">';
               for (var i = 0; i < data.length; i++) {
               html+='<li style="text-align: center;" class="selectClientPONO" data-id='+data[i].poitemid+'>'+data[i].c_p_owner_no+'</li>';
               }
               html+='</ul>';
               $(".suggestion-box").html(html);  
               $(".selectClientPONO").on("click",function(e){
                   $(".search-box").val($(this).text());
                   $(".poitemid").val($(this).attr('data-id'));
                   $(".suggestion-box").hide();
               })
               $(".search-box").css("background","#FFF");
                      }
        })
    }else{
        alert('3 keyword must entered.')
    }
        });

    });
        </script>