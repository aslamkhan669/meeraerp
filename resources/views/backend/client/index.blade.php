@extends ('backend.layouts.app')


@section('breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            @include('backend.client.includes.header-buttons')

            </div><!--col-->
        </div><!--row-->
        <form action="searchclient" method="post">
@csrf
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Client name" style="width: 30%; display: inline;">
    <button type="submit" class="btn btn-success btn-md" value="Search" style="margin-top: -5px;"><i class="fa fa-search"></i></button>
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.access.clients.table.client_name') }}</th>
                            <th>{{ __('labels.backend.access.clients.table.client_code') }}</th>
                            <th>{{ __('labels.backend.access.clients.table.rating') }}</th>
                            <th>{{ __('labels.backend.access.clients.table.purchase_category') }}</th>
                            <th>{{ __('labels.backend.access.clients.table.sales_center') }}</th>
                         
                            <th>Date&Time</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($clients as $client)
                            <tr> 
                                <td>{{ $client->client_name }}</td> 
                                <td>{{'M'.$sa=substr(strtoupper($brandletters='M0'.$client->id),-2)}}</td>                               
                                <td>{{ $client->rating }}</td> 
                                <td>{{ $client->purchase_category }}</td>   
                                <td><?='S'.$sa=substr(strtoupper($brandletters='S0'.$client->sales_center),-2);?></td>                                                           
                           
                                <td>{{$sa=substr(strtoupper($client->updated_at),0,-3)}}</td>
                                <td>{!! $client->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                {!! $clients->total() !!} {{ trans_choice('labels.backend.access.clients.table.total', $clients->total()) }}

                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                {!! $clients->render() !!}

                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
