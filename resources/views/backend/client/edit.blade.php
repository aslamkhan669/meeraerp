@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.clients.management') . ' | ' . __('labels.backend.access.clients.edit'))

@section('breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($client, 'PATCH', route('admin.client.update', $client->id))->class('form-horizontal')->attributes(array('enctype'=>'multipart/form-data'))->open() }}

<div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="ItemManagement">
                    <input name="action" type="hidden" value="Client">
                    <input name="activity" type="hidden" value="update">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.clients.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.clients.edit') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                    <div class="form-group row col-md-4">
                        <h4>
                        Client Info
                        </h4>
                        </div><!--form-group-->
                        <div class="form-group row">

<div class="col-md-4">

</div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.client_name'))->class('col-md-2 form-control-label')->for('category') }}

                            <div class="col-md-4">
                                {{ html()->text('client_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.client_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.client_code'))->class('col-md-2 form-control-label')->for('category_code') }}

<div class="col-md-4">
<input type="text" class="form-control" value="{{'M'.$sa=substr(strtoupper($brandletters='M0'.$client->id),-2)}}" readonly>

</div><!--col-->
                         
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.client_type'))->class('col-md-2 form-control-label')->for('category_code') }}

                             <div class="col-md-4">
                            <select class="form-control" name="client_type">
                                @foreach($clienttypes as $cl)
                                <option value="{{$client->client_type}}"{{$cl->id==$clientinfo->id?"selected":""}}>{{$cl->type}}</option>
                                @endforeach
                                    </select>
                            </div>
                            {{ html()->label(__('validation.attributes.backend.access.clients.rating'))->class('col-md-2 form-control-label')->for('rating') }}

                          <div class="col-md-4 stars">
                            <input class="star star-5" id="star-5" type="radio" value="5" name="rating" <?php  if($client->rating=="5"){ echo "checked"; } ?>/>
                            <label class="star star-5" for="star-5"></label>
                            <input class="star star-4" id="star-4" type="radio" value="4" name="rating" <?php  if($client->rating=="4"){ echo "checked"; } ?>/>
                            <label class="star star-4" for="star-4"></label>
                            <input class="star star-3" id="star-3" type="radio" value="3" name="rating" <?php  if($client->rating=="3"){ echo "checked"; } ?>/>
                            <label class="star star-3" for="star-3"></label>
                            <input class="star star-2" id="star-2" type="radio" value="2" name="rating" <?php  if($client->rating=="2"){ echo "checked"; } ?>/>
                            <label class="star star-2" for="star-2"></label>
                            <input class="star star-1" id="star-1" type="radio" value="1" name="rating" <?php  if($client->rating=="1"){ echo "checked"; } ?>/>
                            <label class="star star-1" for="star-1"></label>

                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.purchase_category'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('purchase_category')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.purchase_category'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.sales_center'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('sales_center')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.sales_center'))
                                    ->attribute('maxlength', 191)
                                    ->readonly()
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row col-md-4">
                        <h4>
                        Contact Info
                        </h4>
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.email'))->class('col-md-2 form-control-label')->for('category') }}

                            <div class="col-md-4">
                                {{ html()->text('email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.email'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.email1'))->class('col-md-2 form-control-label') }}

                            <div class="col-md-4">
                                {{ html()->text('email1')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.email1'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.email2'))->class('col-md-2 form-control-label')->for('category') }}

                            <div class="col-md-4">
                                {{ html()->text('email2')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.email2'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.email3'))->class('col-md-2 form-control-label') }}

                            <div class="col-md-4">
                                {{ html()->text('email3')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.email3'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.email4'))->class('col-md-2 form-control-label')->for('category') }}

                            <div class="col-md-4">
                                {{ html()->text('email4')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.email4'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.email5'))->class('col-md-2 form-control-label') }}

                            <div class="col-md-4">
                                {{ html()->text('email5')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.email5'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_person1'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person1')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_person1'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_number1'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number1')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_number1'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                      
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_person2'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person2')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_person2'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_number2'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number2')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_number2'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_person3'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person3')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_person3'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_number3'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number3')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_number3'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                     
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_person4'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person4')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_person4'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_number4'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number4')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_number4'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_person5'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person5')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_person5'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_number5'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number5')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_number5'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_person6'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person6')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_person6'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_number6'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number6')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_number6'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_person7'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person7')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_person7'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_number7'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number7')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_number7'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_person8'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_person8')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_person8'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.contact_number8'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('contact_number8')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.contact_number8'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.street'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('street')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.street'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.landmark'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('landmark')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.landmark'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.sector'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('sector')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.sector'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.city'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('city')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.city'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.pincode'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('pincode')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.pincode'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.state'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('state')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.clients.state'))
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.zone'))->class('col-md-2 form-control-label')->for('category_code') }}
                            <div class="col-md-4">
                            <input type="checkbox" name="zone"  value="North" <?php if($client->zone=="North"){ echo 'checked'; } ?>>
                            <label for="north">North</label>
                            <input type="checkbox" name="zone"  value="East" <?php if($client->zone=="East"){ echo 'checked'; } ?>>
                            <label for="south">East</label>
                            <input type="checkbox" name="zone"  value="West" <?php if($client->zone=="West"){ echo 'checked'; } ?>>
                            <label for="south">West</label>
                            <input type="checkbox" name="zone"  value="South" <?php if($client->zone=="South"){ echo 'checked'; } ?>>
                            <label for="south">South</label>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row col-md-4">
                        <h4>
                        Account Info
                        </h4>
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.adharcard'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('adharcard')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.adharcard'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.gst_no'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('gst_no')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.clients.gst_no'))
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.pancard'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                                {{ html()->text('pancard')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.clients.pancard'))
                                    ->attribute('maxlength', 191)
                                    ->autofocus() }}
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.demand_frequency'))->class('col-md-2 form-control-label')->for('category_code') }}

                                <div class="col-md-4">
                                    {{ html()->text('demand_frequency')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.backend.access.clients.demand_frequency'))
                                        ->attribute('maxlength', 191)
                                        ->autofocus() }}
                                </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.payment_term'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                            <select class="form-control" name="payment_term">
                                 @foreach($payterm as $payterms)
                                 <option value="{{$payterms->id}}"{{$payterms->id==$payterminfo->id?"selected":""}}>{{$payterms->term_name}}</option>
                                 @endforeach
              
                                     </select>
                            </div><!--col-->
                            {{ html()->label(__('validation.attributes.backend.access.clients.payment_mode'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                            <select class="form-control" name="payment_mode">
                                @foreach($paymode as $paymodes)
                                <option value="{{$paymodes->id}}"{{$paymodes->id==$paymodeinfo->id?"selected":""}}>{{$paymodes->mode}}</option>
                                @endforeach

                                    </select>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.clients.excisable'))->class('col-md-2 form-control-label')->for('category_code') }}

                            <div class="col-md-4">
                              <label class="switch switch-3d switch-primary">
                                    {{ html()->checkbox('excisable',$client->excisable)->class('switch-input')}}
                                    <span class="switch-label"></span>
                                    <span class="switch-handle"></span>
                                </label> 
                        
                            </div><!--col-->
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                {{ form_cancel(route('admin.client.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                     {{ form_submit(__('buttons.general.crud.update')) }} 
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
