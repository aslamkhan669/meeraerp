@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.purchaseaccount.management') . ' | ' . __('labels.backend.access.uom.create'))

@section('breadcrumb-links')
@endsection

@section('content')


  <!-- Transfer Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
  
        <div class="modal-body">
          <label>Reason(Why):-</label>
          <input type="text" class="form-control" id="reason" value="">
          <label>Rejected QTY:-</label>
          <input type="text" class="form-control" id="qty" value="">
          <label>Bin No:-</label>
          <input type="text" class="form-control" id="bill_no" value="">
          <label>Bin Location:-</label>
          <input type="text" class="form-control" id="location" value="">
          <input type="submit" id="dtransfer" class="btn btn-success" value="Transfer">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


<div class="modal fade" id="myModalItem" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
<div class="modal-content" style="width: 900px;">
        <div class="modal-header">
          <h4 class="modal-title">Details</h4>
          
          
        </div>
        <div class="modal-body">
        <table class="table table-hover table-bordered" id="sometable">
        <thead style="background: #efeeee;">
    <tr>
            <th scope="col">Item No.</th>
            <th scope="col">Description</th>
            <th scope="col">Specification</th>
            <th scope="col">Color</th>
            <th scope="col">Model No.</th>
            <th scope="col">Brand</th>
            <th scope="col">HSN Code</th>
            <th scope="col">UOM</th>
            <th scope="col">Remarks</th>
    </tr>
  </thead>
   
<tbody id="secc">
  
    
  </tbody>
</table></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="close-button" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" id="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" id="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" id="module" type="hidden" value="DispatchManagement">
                    <input name="action" id="action" type="hidden" value="DispatchAccount">
                    <input name="activity" id="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
Dispatch Account                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr />
                <div class="row mt-4 mb-4">
                <div class="col-md-12">
                       <div class="table-responsive" >
                    <table class="table">
                    <thead>
                    <tr>
                        <th>Status</th>
                        <th>Reject/Not Reject</th>
                        <th>Company Name</th>
                        <th>PO No</th>
                        <th>Client PO No</th>
                        <th>Contact&Address  </th>
                        <th>Description</th>
                        <th>image</th>
                        <th>Dispatch date</th>
                        <th>Day Shift</th>
                        <th>Dispatch Person</th>
                        <th>Mode of Travel</th>
                        <th>Priority</th>
                        <th>Qty</th>
                        <th>Reject Qty</th>
                        <th>Dispatch Amount</th>
                        <th>M/L Dispatch</th>
                        <th>M/L delivered</th>
                        <th>On Bill/Challan</th>
                        <th>Bill/Challan No.</th>
                        <th>Receipt Slip</th>
                        <th>PO Items Purchaser</th>
                        <th>PO Items Reciever</th>
                        <th>Remark</th>
                        <th>Inventory</th>

                    </tr>
                    </thead>
                    <tbody  class="data">
                    @foreach ($data as $post)
                    @if(isset($post->dispatchedOrder[0]->ml_recieved))
                        @if($post->dispatchedOrder[0]->ml_recieved =='0')
                        <tr><td><Button class="btn btn-danger">Non Dispatch</Button></td>       
                                @elseif($post->dispatchedOrder[0]->ml_recieved =='1')
                                <tr><td><Button class="btn btn-danger">Dispatch</Button></td>       
                                @endif
                                @else
                                <tr><td><Button class="btn btn-danger">Non Dispatch</Button></td>       
                                @endif


       @if(isset($post->dispatchedOrder[0]))
                       @if ($post->dispatchedOrder[0]->pending_qty == '1')
                       <td><Button class="btn btn-danger">Reject</Button></td>
                     @elseif ($post->dispatchedOrder[0]->pending_qty > '1')
                <td><Button class="btn btn-danger">Reject</Button></td> 
                 @else
               <td><Button class="btn btn-success">Non Reject</Button></td> 
                     @endif      
                 @else
                       <td><Button class="btn btn-success">Non Reject</Button></td>    
                   @endif 

                                <td><input type="text" value="{{$post->poItem->poFormatting->quotation->queries->client->client_name}}"  class='form-control' style="width: 250px;" readonly></td>
                                <td>PO/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$post->poItem->poFormatting->quotation->queries->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$post->poItem->poFormatting->quotation->queries->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$post->poItem->poFormatting->quotation->queries->client->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$post->poItem->poFormatting->id),-2)}}</td>
                                <td>{{$post->poItem->poFormatting->c_p_owner_no}}</td>
                                <td style="justify-content:normal;">{{$post->poItem->poFormatting->quotation->queries->client->contact_number1}}/{{$post->poItem->poFormatting->quotation->queries->client->street}},{{$post->poItem->poFormatting->quotation->queries->client->landmark}},{{$post->poItem->poFormatting->quotation->queries->client->sector}},{{$post->poItem->poFormatting->quotation->queries->client->city}},{{$post->poItem->poFormatting->quotation->queries->client->state}} </td>
                 <td><button type="button" class="btn btn-info btn-md td-click" data-id="{{ isset($post->poItem->queryItem->item_id) ? $post->poItem->queryItem->item_id : '' }}" data-toggle="modal" data-target="#myModalItem">View</button></td>
                 <td class="image-hover"><img src="{{url(isset($post->poItem->queryItem->itemList->image) ? $post->poItem->queryItem->itemList->image : '')}}" width="50px" height="50px"/></td>   
                 <td><input type="text" class="form-control date"  value="{{$post->purchase_date}}" style="width: 100px;" readonly></td>
                 @if(isset($post->day_shift))
                                @if($post->day_shift =='0')
                                <td><Button class="btn btn-info">First Half</Button></td>
                                @elseif($post->day_shift =='1')
                                <td><Button class="btn btn-success">Second Half</Button></td>         
                                @endif
                                @else
                                <td><Button class="btn btn-danger">Not Priced</Button></td>    
                            @endif
                 <td><input type="text" class="form-control person"  value="{{$post->user->first_name}}" style="width: 150px;" readonly></td>
                 <td><input type="text" class="form-control transportation"  value="{{$post->transportation_mode}}" style="width: 100px;" readonly></td>

                                @if($post->priority =='0')
                                <td><input type="button" value="Low" class="btn btn-info btn-sm"></td>
                                @elseif($post->priority =='1')
                                <td><input type="button" value="Medium" class="btn btn-warning btn-sm"></td> 
                                @elseif($post->priority =='2')
                                <td><input type="button" value="High" class="btn btn-danger btn-sm"></td>        
                                @else
                                <td><Button class="btn btn-danger">High</Button></td>    
                                @endif
                    <td><input type="input" value="{{$post->dt_qty}}"  class="form-control qty" style="width: 80px;"></td>
                    <td><input type="input" value="{{ isset($post->dispatchedOrder[0]->pending_qty) ? $post->dispatchedOrder[0]->pending_qty : '' }}"  class="form-control rejectqty" style="width: 80px;" readonly></td>
                    <td><input type="input" value="{{$post->payment_amount}}" class="form-control amount" style="width: 100px;"></td>
                    <td><input type="checkbox" class="ml_recieved" name="ml_recieved" {{ (isset($post->dispatchedOrder[0]->ml_recieved) && $post->dispatchedOrder[0]->ml_recieved == 1) ? 'checked' : '' }} value=""></td>
                    

             @if(isset($post->dispatchedOrder[0]->ml_purchased))
                                @if($post->dispatchedOrder[0]->ml_purchased =='1')
                                <td><input type="button" value="Dispatched" class="btn btn-success btn-sm"></td>    
                                @elseif($post->dispatchedOrder[0]->ml_purchased =='0')
                                <td><input type="button" value="NOT Dispatched" class="btn btn-info btn-sm"></td>    
                                @endif
                                @else
                                <td><input type="button" value="NOT Dispatched" class="btn btn-info btn-sm"></td>    
                            @endif
                    <!-- <td>
                    <input type="radio" name="payment_mode" value="Cash" <?php echo ($post->payment_mode=='Cash')?'checked':'' ?>> Cash<br>
                    <input type="radio" name="payment_mode" value="Bill" <?php echo ($post->payment_mode=='Bill')?'checked':'' ?>> Bill<br>
                    <input type="radio" name="payment_mode" value="Challan" <?php echo ($post->payment_mode=='Challan')?'checked':'' ?>> Challan  
                    </td> -->
                    <td><input type="text" value="{{$post->on_bill_challan}}"  class='form-control bill_challan' style="width: 88px;" readonly></td>
     <!-- <td>
                    <input type="radio" name="payment_mode" value="Bill" <?php echo ($post->on_bill_challan=='challan')?'checked':'' ?>> Bill<br>
                    <input type="radio" name="payment_mode" value="Challan" <?php echo ($post->on_bill_challan=='bill')?'checked':'' ?>> Challan  
                    </td> -->
                    <td><input type="text" class="form-control bill_challan_no" value="{{ isset($post->dispatchedOrder[0]->bill_challan_no) ? $post->dispatchedOrder[0]->bill_challan_no : '' }}" style="width: 130%"></td>
                    <td><input type="checkbox" class="recieved_doc" style="width: 100%" name="recieved_doc" {{ (isset($post->dispatchedOrder[0]->recieved_doc) && $post->dispatchedOrder[0]->recieved_doc == 1) ? 'checked' : '' }} value=""></td>
                    <td><input type="text" class="form-control poitem_purchaser" style="width: 130%" name="poitem_purchaser"  value="{{ isset($post->poItem->poFormatting->c_purchaser_name) ? $post->poItem->poFormatting->c_purchaser_name : '' }}" readonly></td>
                    <td><input type="text" class="poitem_reciever" name="poitem_reciever"  value="{{ isset($post->poItem->poFormatting->c_reciever_name) ? $post->poItem->poFormatting->c_reciever_name : '' }}"></td>
                    <td><input type="text" class="form-control remarks"   style="width: 200px;" value="{{ isset($post->dispatchedOrder[0]->remarks) ? $post->dispatchedOrder[0]->remarks : '' }}" readonly></td>

                    <input type="hidden" value="{{$post->id}}" class="disregisterid" name="disregisterid">
                    <td>  <button type="button" class="btn btn-sm  btn-dark" data-toggle="modal" data-target="#myModal">Transfer</button></td>

                    </tr>
                    @endforeach
                    </tbody>

                    </table>
                    </div>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
</div><!--card-body-->
            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.poregister.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                    <input class="btn btn-success btn-sm" id="dis" data-id="{{$id}}" type="button" value="Submit">
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}


  </div>
@endsection