@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.purchaseaccount.management'))



@section('content')
  <!-- Transfer Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
  
        <div class="modal-body">
          <label>Total Amount:-</label>
          <input type="text" class="form-control" id="reason" value="" style="width: 200px;" readonly>
          <label>e-way bill:-</label>
          <input type="text" class="form-control" id="qty" value="" style="width: 200px;" readonly>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Dispatch Account <small class="text-muted"></small>
                </h4>
            </div><!--col-->
            <div class="col-sm-7">

<h4>
    Reject Qty
</h4>

            <a class="button" href="{{ url('admin/dispatchaccount/create') }}">info</a>

</div>
           
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                        <th>status</th>
                        <th>Dispatch Account</th>
                            <th>Dispatcher Name</th>
                            <th>Dispatcher Date</th>
                            <th>Total Amount</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($poregisters as $poformat)
                            <tr>
                            @if(isset($poformat->dispatchedOrder[0]->ml_recieved))
                                @if($poformat->dispatchedOrder[0]->ml_recieved=='0')
                                <td><Button class="btn btn-success">Not Dispatch</Button></td>       
                                @elseif($poformat->dispatchedOrder[0]->ml_recieved =='1')
                                <td><Button class="btn btn-danger"> Dispatched</Button></td> 
                                @endif      
                                @else
                                 <td><Button class="btn btn-success">Not Dispatch</Button></td>       
                                @endif

                                @if(isset($poformat->dispatchedOrder[0]->complete))
                                @if($poformat->dispatchedOrder[0]->complete=='0')
                                <td><Button class="btn btn-success">Not Submit</Button></td>       
                                @elseif($poformat->dispatchedOrder[0]->complete =='1')
                                <td><Button class="btn btn-danger">Submit</Button></td> 
                                @endif      
                                @else
                                 <td><Button class="btn btn-success">Not Submit</Button></td>       
                                @endif
                                <td>{{ $poformat->user->first_name }}</td>
                                <td>{{$poformat->purchase_date}}</td>
                                <td><a class="button" href="{{ url('admin/dispatchaccount/show/') }}/<?=$poformat->dt_user_id?>/<?=$poformat->purchase_date?>">info</a></td>
                                <td><a class="button" href="{{ url('admin/dispatchaccount/edit/') }}/<?=$poformat->dt_user_id?>/<?=$poformat->purchase_date?>">View</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

</div><!--card-->
@endsection
