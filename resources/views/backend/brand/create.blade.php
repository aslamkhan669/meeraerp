@extends ('backend.layouts.app')

@section ('title', __('labels.backend.access.brand.management') . ' | ' . __('labels.backend.access.brand.create'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.brand.store'))->class('form-horizontal')->open() }}

        <div class="card">
            <div class="card-body">
                <div class="row">
                <input name="user_name" type="hidden" value="{{Auth::user()->first_name}}">
                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                    <input name="module" type="hidden" value="CategoryManagement">
                    <input name="action" type="hidden" value="Brand">
                    <input name="activity" type="hidden" value="create">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('labels.backend.access.brand.management') }}
                            <small class="text-muted">{{ __('labels.backend.access.brand.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                 
                </div><!--col-->
                
                        </div><!--form-group-->

                        <div class="form-group row">

                              {{ html()->label(__('validation.attributes.backend.access.brand.brand_name'))->class('col-md-2 form-control-label')->for('brand_name') }}

<div class="col-md-4">
    {{ html()->text('brand_name')
        ->class('form-control')
        ->placeholder(__('validation.attributes.backend.access.brand.brand_name'))
        ->attribute('maxlength', 191)
        ->required()
        ->autofocus() }}
</div><!--col-->
          
<input name="brand_code" type="hidden" value="<?=$brand_code?>">



                             {{ html()->label(__('validation.attributes.backend.access.brand.product_id'))->class('col-md-2 form-control-label')->for('product_id') }}

                        <div class="col-md-4">
                        <select class="form-control category" name="product_id">
                                <option value="">Select</option>
                                @foreach($products as $stand)
                                <option value="{{$stand->id}}">{{$stand->product_name}}</option>
                                @endforeach

                                </select>
                        </div><!--col-->



                          


                           
                        </div><!--form-group-->
                        <div class="form-group row">
                            {{ html()->label(__('validation.attributes.backend.access.brand.is_active'))->class('col-md-2 form-control-label')->for('is_active') }}

                            <div class="col-md-4">
                                <label class="switch switch-3d switch-primary">
                                    {{ html()->checkbox('is_active', true, '1')->class('switch-input') }}
                                    <span class="switch-label"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div><!--col-->

                             {{ html()->label(__('validation.attributes.backend.access.brand.brand_code'))->class('col-md-2 form-control-label')->for('brand_code') }}

                <div class="col-md-4">
                    {{ html()->text('')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.backend.access.brand.brand_code'))
                        ->attribute('maxlength', 191)
                        ->required()
                        ->readonly()
                    
                        ->autofocus() }}
                </div><!--col-->
                        </div><!--form-group-->

                        

                       

    

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.brand.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->form()->close() }}
@endsection