@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.brand.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.access.brand.management') }} <small class="text-muted">{{ __('labels.backend.access.brand.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.brand.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            
                            <th>{{ __('labels.backend.access.brand.table.brand_name') }}</th>
                            <th>{{ __('labels.backend.access.brand.table.brand_code') }}</th>    
                            <th>{{ __('labels.backend.access.brand.table.product_name') }}</th>    
                            <th>{{ __('labels.backend.access.brand.table.is_active') }}</th>
                            <th>Date&Time</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($brand as $standobj)
                            <tr>
                                
                                <td>{{ $standobj->brand_name }}</td>
                                <td>{{'B'.$sa=substr(strtoupper($brandletters='B000'.$standobj->id),-4)}}</td>
                                <td>{{ $standobj->product->product_name }}</td>
                                <td>{{ $standobj->is_active==1 ? 'Active' : 'Not Active' }}</td>
                                <td>{{$sa=substr(strtoupper($standobj->updated_at),0,-3)}}</td>

                                <td>{!! $standobj->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                   <label class="control-label"> {!! $brand->total() !!} {{ trans_choice('labels.backend.access.brand.table.total', $brand->total()) }}</label>
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $brand->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
