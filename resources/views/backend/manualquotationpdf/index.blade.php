@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.access.quotation.management'))



@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
Quotation PDF
                </h4>
            </div><!--col-->


        </div><!--row-->
        <form action="searchmanualquotation" method="post">
@csrf
<input type="text" value=""  name="quata" class="form-control" placeholder="Enter Quotation Number" style="width: 20%; display: inline;">
    <input type="text" value=""  name="term" class="form-control" placeholder="Enter Query Number" style="width: 20%; display: inline;">

    <button type="submit" value="Search" class="btn btn-md btn-success" style="margin-top: -5px;"><i class="fa fa-search"></i></button> 
</form>
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            
                           
                        <th>Quotation No</th>
                            <th>Query No</th>
                            <th>Client name</th>
                            <th>Center Name</th>
                            <th>Query date</th>
                            <th>Status</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                       @foreach($info as $data)
                       <tr>
                  
                       <td>QT/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$data->manualPurchase->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$data->manualPurchase->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$data->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$data->id),-2)}}(On Call Quotation)</td>
                        <td>QR/{{'M'.$sa=substr(strtoupper($brandletters='M0'.$data->manualPurchase->client_id),-2)}}/{{'S'.$sa=substr(strtoupper($brandletters='S0'.$data->manualPurchase->client->sales_center),-2)}}/{{substr(str_replace("-","",explode(" ",$data->updated_at)[0]),2)}}-{{'0'.$sa=substr(strtoupper($brandletters='00'.$data->manual_id),-2)}}</td>
                        <td>{{ $data->manualPurchase->client->client_name }}</td>
                        <td>{{ $data->manualPurchase->companyDetail->company_name }}</td>
                        <td>{{ $sa=substr(strtoupper($data->manualPurchase->updated_at),0,-3)}}</td>
                        @if(isset($data->status))
                                @if($data->status =='0')
                                <td><Button class="btn btn-warning">Hold</Button></td>
                                @elseif($data->status =='1')
                                <td><Button class="btn btn-success">Accepted</Button></td>
                                @elseif($data->status =='2')
                                <td><Button class="btn btn-danger">Cancel</Button></td>
                               
                                @endif
                            @else
                                <td><Button class="btn btn-danger">Not Priced</Button></td>    
                            @endif
                        <td> 
                       <span><a class="btn btn-sm btn-info" href="{{ url('admin/manualquotationpdf/pdf/') }}/<?=$data->manual_id?>">Pdf</a></span></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
