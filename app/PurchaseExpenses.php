<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $daily_id
 * @property string $purchase_person
 * @property string $amt_recieved
 * @property string $amt_spent
 * @property string $amt_returned
 * @property string $mode_payment
 * @property string $remarks
 * @property string $for_what
 * @property string $created_at
 * @property string $updated_at
 * @property DailyExpense $dailyExpense
 */
class PurchaseExpenses extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id', 'purchase_person', 'amt_recieved', 'amt_spent', 'amt_returned', 'mode_payment', 'remarks', 'for_what', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dailyExpense()
    {
        return $this->belongsTo('App\DailyExpense', 'daily_id');
    }
}
