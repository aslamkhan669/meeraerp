<?php

namespace App\Repositories\Backend\Setting;

use Illuminate\Database\Eloquent\Model;

class AppLog extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'query_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function query()
    {
        return $this->belongsTo('App\Query');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
