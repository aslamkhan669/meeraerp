<?php

namespace App\Repositories\Backend\Setting;

use App\Models\Setting\CompanyDetail;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class CompanyDetailRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return CompanyDetail::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : CompanyDetail
    {
        
        return DB::transaction(function () use ($data) {
       
            $company = parent::create([
                'company_name'=>$data['company_name'], 
                'contact_person'=>$data['contact_person'], 
                'phone_no'=>$data['phone_no'], 
                'email'=>$data['email'], 
                'pan_no'=>$data['pan_no'], 
                'mobile'=>$data['mobile'], 
                'delivery_address'=>$data['delivery_address'], 
                'address'=>$data['address'], 
                'aadhar_no'=>$data['aadhar_no'], 
                'gst_no'=>$data['gst_no'], 
                'state_code'=>$data['state_code'], 
                'reg_type'=>isset($data['reg_type']) && $data['reg_type'] == '1' ? 1 : 0
                ]);
            return $company;
        });
    }



    public function update(CompanyDetail $company, array $data) : CompanyDetail
    {    
        return DB::transaction(function () use ($company, $data) {
            if ($company->update([
                'company_name'=>$data['company_name'], 
                'company_code'=>$data['company_code'], 
                'contact_person'=>$data['contact_person'], 
                'phone_no'=>$data['phone_no'], 
                'email'=>$data['email'], 
                'pan_no'=>$data['pan_no'], 
                'mobile'=>$data['mobile'], 
                'delivery_address'=>$data['delivery_address'], 
                'address'=>$data['address'], 
                'aadhar_no'=>$data['aadhar_no'], 
                'gst_no'=>$data['gst_no'], 
                'state_code'=>$data['state_code'], 
                'reg_type'=>isset($data['reg_type']) && $data['reg_type'] == '1' ? 1 : 0
                ])); 
            return $company;

        });
    }
    

}