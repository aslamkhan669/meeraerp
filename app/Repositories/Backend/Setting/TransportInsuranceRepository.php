<?php

namespace App\Repositories\Backend\Setting;

use App\Models\Setting\TransportInsurance;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class TransportInsuranceRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return TransportInsurance::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : TransportInsurance
    {
        
        return DB::transaction(function () use ($data) {
            $transportinsurance = parent::create([
                'conditions'=>$data['conditions']
                ]);
            return $transportinsurance;
        });
    }



    public function update(TransportInsurance $transportinsurance, array $data) : TransportInsurance
    {    

        return DB::transaction(function () use ($transportinsurance, $data) {
            if ($transportinsurance->update([
                'conditions'=>$data['conditions']
                ])); 
            return $transportinsurance;

        });
    }


}