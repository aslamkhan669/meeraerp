<?php
namespace App\Repositories\Backend\ProductManagement;

use App\Models\ProductManagement\Product;              
use App\Models\ProductManagement\Brand;
use App\Models\ProductManagement\Category;          
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Events\Backend\ProductManagement\ProductCreated;
use App\Events\Backend\ProductManagement\ProductUpdated;
use App\Events\Backend\ProductManagement\ProductDeleted;
use App\Models\LogManagement\Log;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class StandardRepository.
 */
class ProductRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getSearchedPaginated($paged = 2500, $orderBy = 'created_at', $sort = 'desc',$term) : LengthAwarePaginator
    {
        return $this->model
        ->where('product_name','like','%'.$term.'%')
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return User
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Product
    {
        return DB::transaction(function () use ($data) {
           
            $pro = new Log;
            $pro->name=$data['product_name'];
            $pro->user_name=$data['user_name'];
             $pro->user_id=$data['user_id'];
             $pro->module=$data['module'];
             $pro->action=$data['action'];
             $pro->activity=$data['activity'];
             $pro->save();

            $brandletters='P0';
            $str=strtoupper($brandletters).$data['product_code'];
            $sa=substr($str,-2);
            $code='P'.$sa;
            $product = parent::create([
                'product_code'=>$code,
                        'product_name'=>$data['product_name'],
                        'product_owner'=>$data['product_owner'],
                        'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
            ]);
       
            
            return $product;
        });
    }


    public function update(Product $product, array $data) : Product
    {
        $pro = new Log;
        $pro->name=$data['product_name'];
        $pro->user_name=$data['user_name'];
         $pro->user_id=$data['user_id'];
         $pro->module=$data['module'];
         $pro->action=$data['action'];
         $pro->activity=$data['activity'];
         $pro->save();
        return DB::transaction(function () use ($product, $data) {
            if ($product->update([            
                'product_name'=>$data['product_name'],
                'product_owner'=>$data['product_owner'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
            ])); 
                return $product;
            });
    }

    public function updatePassword(User $user, $input) : User
    {
        $user->password = Hash::make($input['password']);
        if ($user->save()) {
            event(new UserPasswordChanged($user));
            return $user;
        }
        throw new GeneralException(__('exceptions.backend.access.users.update_password_error'));
    }

    public function mark(User $user, $status) : User
    {
        if (auth()->id() == $user->id && $status == 0) {
            throw new GeneralException(__('exceptions.backend.access.users.cant_deactivate_self'));
        }
        $user->active = $status;
        switch ($status) {
            case 0:
                event(new UserDeactivated($user));
            break;
            case 1:
                event(new UserReactivated($user));
            break;
        }
        if ($user->save()) {
            return $user;
        }
        throw new GeneralException(__('exceptions.backend.access.users.mark_error'));
    }

    public function confirm(User $user) : User
    {
        if ($user->confirmed) {
            throw new GeneralException(__('exceptions.backend.access.users.already_confirmed'));
        }
        $user->confirmed = 1;
        $confirmed = $user->save();
        if ($confirmed) {
            event(new UserConfirmed($user));
            // Let user know their account was approved
            if (config('access.users.requires_approval')) {
                $user->notify(new UserAccountActive);
            }

            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.cant_confirm'));
    }

    /**
     * @param User $user
     *
     * @return User
     * @throws GeneralException
     */
    public function unconfirm(User $user) : User
    {
        if (! $user->confirmed) {
            throw new GeneralException(__('exceptions.backend.access.users.not_confirmed'));
        }

        if ($user->id == 1) {
            // Cant un-confirm admin
            throw new GeneralException(__('exceptions.backend.access.users.cant_unconfirm_admin'));
        }

        if ($user->id == auth()->id()) {
            // Cant un-confirm self
            throw new GeneralException(__('exceptions.backend.access.users.cant_unconfirm_self'));
        }

        $user->confirmed = 0;
        $unconfirmed = $user->save();

        if ($unconfirmed) {
            event(new UserUnconfirmed($user));

            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.cant_unconfirm'));
    }

    /**
     * @param User $user
     *
     * @return User
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(User $user) : User
    {
        if (is_null($user->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.users.delete_first'));
        }

        return DB::transaction(function () use ($user) {
            // Delete associated relationships
            $user->providers()->delete();

            if ($user->forceDelete()) {
                event(new UserPermanentlyDeleted($user));

                return $user;
            }

            throw new GeneralException(__('exceptions.backend.access.users.delete_error'));
        });
    }

    /**
     * @param User $user
     *
     * @return User
     * @throws GeneralException
     */
    public function restore(User $user) : User
    {
        if (is_null($user->deleted_at)) {
            throw new GeneralException(__('exceptions.backend.access.users.cant_restore'));
        }

        if ($user->restore()) {
            event(new UserRestored($user));

            return $user;
        }

        throw new GeneralException(__('exceptions.backend.access.users.restore_error'));
    }

    public function parentStandard(){
		return $this->model->get();
}
    /**
     * @param User $user
     * @param      $email
     *
     * @throws GeneralException
     */
    protected function checkUserByEmail(User $user, $email)
    {
        //Figure out if email is not the same
        if ($user->email != $email) {
            //Check to see if email exists
            if ($this->model->where('email', '=', $email)->first()) {
                throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
            }
        }
    }
    public function parentProduct(){
		return $this->model->get();
}
}
