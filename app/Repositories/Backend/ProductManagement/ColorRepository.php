<?php

namespace App\Repositories\Backend\ProductManagement;

use App\Models\ProductManagement\Color;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\LogManagement\Log;


/**
 * Class CustomerRepository.
 */
class ColorRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Color::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : Color
    {
        $pro = new Log;
        $pro->name=$data['color'];
        $pro->user_name=$data['user_name'];
         $pro->user_id=$data['user_id'];
         $pro->module=$data['module'];
         $pro->action=$data['action'];
         $pro->activity=$data['activity']; 
        return DB::transaction(function () use ($data) {
            $color = parent::create([
                'color'=>$data['color'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
                ]);
            return $color;
        });
    }



    public function update(Color $color, array $data) : Color
    {    
        $pro = new Log;
        $pro->name=$data['color'];
        $pro->user_name=$data['user_name'];
         $pro->user_id=$data['user_id'];
         $pro->module=$data['module'];
         $pro->action=$data['action'];
         $pro->activity=$data['activity'];
        return DB::transaction(function () use ($color, $data) {
            if ($color->update([
                'color'=>$data['color'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
                ])); 
            return $color;

        });
    }


}