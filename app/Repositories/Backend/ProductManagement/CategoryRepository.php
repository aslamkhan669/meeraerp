<?php

namespace App\Repositories\Backend\ProductManagement;

use App\Models\ProductManagement\Category;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Events\Backend\Category\CategoryDeleted;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\LogManagement\Log;


/**
 * Class CustomerRepository.
 */
class CategoryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
         ->where('parent_id', '=', 0)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function getSubActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->where('parent_id', '!=', 0)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term) : LengthAwarePaginator
    {
        return $this->model
        ->where([['parent_id', '=', 0],['category_name','LIKE','%'.$term.'%']])
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    public function getSubSearched($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$cat) : LengthAwarePaginator
    {



            if($term!=null && $cat!=null ){
                return $this->model
                ->where([['parent_id','=',$cat],['category_name','like','%'.$term.'%']])
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);
            }
                elseif($term!=null){
                    return $this->model
                    ->where([['parent_id', '!=', 0],['category_name','like','%'.$term.'%']])
                    ->orderBy($orderBy, $sort)
                        ->paginate($paged);                
                }
        
                else{
                    return $this->model
                    ->where('parent_id','like','%'.$cat.'%')
                        ->orderBy($orderBy, $sort)
                        ->paginate($paged);                
    
                }
    }


    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : Category
    {
        $pro = new Log;
        $pro->name=$data['category_name'];
        $pro->user_name=$data['user_name'];
         $pro->user_id=$data['user_id'];
         $pro->module=$data['module'];
         $pro->action=$data['action'];
         $pro->activity=$data['activity'];
         $pro->save();

        return DB::transaction(function () use ($data) {
            $brandletters='C00';
            $str=strtoupper($brandletters).$data['code'];
            $sa=substr($str,-3);
            $code='C'.$sa;
            $category = parent::create([
                'product_id' => $data['product_id'],
                'category_name' => $data['category_name'],
                'code' => $code,
                'is_active' => $data['is_active'],
                'parent_id' => 0
             
            ]);

      
            return $category;
        });
    }

    public function createNew(array $data, $image) : Category
    {
        $pro = new Log;
        $pro->name=$data['category_name'];
        $pro->user_name=$data['user_name'];
         $pro->user_id=$data['user_id'];
         $pro->module=$data['module'];
         $pro->action=$data['action'];
         $pro->activity=$data['activity'];
         $pro->save();

        $brandletters='S000';
        $str=strtoupper($brandletters).$data['code'];
        $sa=substr($str,-4);
        $code='S'.$sa;
        $create = [
            'product_id' => $data['product_id'],
            'category_name' => $data['category_name'],
            'code' => $code,
            'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0,
            'image' => $image,
            'parent_id' => $data['parent_id']
         
        ];
        if($image!=null){
            $create['image']=$image;
        }
        return DB::transaction(function () use ($create,$image) {
            
            $category = parent::create($create);
            return $category;
        });
    }

    public function update(Category $category, array $data,$image) : Category
    {   
         $pro = new Log;
        $pro->name=$data['category_name'];
        $pro->user_name=$data['user_name'];
         $pro->user_id=$data['user_id'];
         $pro->module=$data['module'];
         $pro->action=$data['action'];
         $pro->activity=$data['activity'];
         $pro->save(); 
       $update =  [
        'product_id' => $data['product_id'],
            'category_name' => $data['category_name'],
            'code' => $data['code'],
            'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0,
            'parent_id' => $data['parent_id']
       ];
       if($image!=null){
           $update['image']=$image;
       }
        return DB::transaction(function () use ($category, $update) {
            if ($category->update($update)); 
            return $category;

        });
    }


}