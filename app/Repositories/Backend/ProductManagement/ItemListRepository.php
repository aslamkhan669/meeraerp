<?php
namespace App\Repositories\Backend\ProductManagement;

use App\Models\ProductManagement\ItemList;              
use App\Models\ProductManagement\Product;        
use App\Models\ProductManagement\Category;        
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Models\LogManagement\Log;
use Illuminate\Pagination\LengthAwarePaginator;


class ItemListRepository extends BaseRepository
{
   
    private $cateogry;
    public function __construct(Category $category){
        $this->makeModel();
        $this->category = $category;
    }

    public function model()
    {
        return ItemList::class;
    }

    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$specif,$hsncode,$modelno,$product_id,$brand_id,$itemno) : LengthAwarePaginator
    {
        if($product_id!=null && $brand_id!=null && $term!=null && $specif!=null && $hsncode=null && $modelno=null && $itemno=null){
            return $this->model
            ->where([['product_id','=',$product_id],['brand_id','=',$brand_id],['description','like','%'.$term.'%'],['specification','like','%'.$specif.'%'],['hsn_code','like','%'.$hsncode.'%'],['model_no','like','%'.$modelno.'%'],['id','like','%'.$itemno.'%']])
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
        }elseif ($brand_id!=null){
            return $this->model
            ->where('brand_id','=',$brand_id)
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
            }
            elseif ($product_id!=null){
                return $this->model
                ->where('product_id','=',$product_id)
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);
                }
                elseif ($itemno!=null){
                    return $this->model
                  //  ->where('id','=',$itemno)
                    ->where('id', 'like', '%'.$itemno.'%')
                        ->orderBy($orderBy, $sort)
                        ->paginate($paged);
                    }
            elseif($specif!=null){
                return $this->model
                ->where('specification','like','%'.$specif.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                
            }
            elseif($hsncode!=null){
                return $this->model
                ->where('hsn_code','like','%'.$hsncode.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                
            }
            elseif($modelno!=null){
                return $this->model
                ->where('model_no','like','%'.$modelno.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                
            }
            else{
                return $this->model
                ->where('description','like','%'.$term.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                

            }
    }
    
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
           
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

   
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function newcreate(array $data, string $image) : Itemlist
    {
        return DB::transaction(function () use ($data,$image) {
           
            $pro = new Log;
            $pro->name=$data['description'];
            $pro->user_name=$data['user_name'];
             $pro->user_id=$data['user_id'];
             $pro->module=$data['module'];
             $pro->action=$data['action'];
             $pro->activity=$data['activity'];
             $pro->save();

            $itemlist = parent::create([
                        'product_id'=>$data['product_id'],
                        'cat_id'=>$data['cat_id'],
                        'brand_id'=>$data['brand_id'],
                        'model_no'=>$data['model_no'],
                        'hsn_code'=>$data['hsn_code'],
                        'unit'=>$data['unit'],
                        'color'=>$data['color'],
                        'description'=>$data['description'],
                        'remarks'=>$data['remarks'],
                        'specification'=>$data['specification'],
                        'image'=>$image,
                        'status'=> 0,
                        'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
            ]);
            return $itemlist;
        });
    }


   
    public function update(ItemList $itemlist, array $data,$image) : ItemList
    {
    

       $update=[
        'product_id'=>$data['product_id'],
        'cat_id'=>$data['cat_id'],
        'brand_id'=>$data['brand_id'],
        'model_no'=>$data['model_no'],
        'hsn_code'=>$data['hsn_code'],
        'unit'=>$data['unit'],
        'color'=>$data['color'],
        'description'=>$data['description'],
        'remarks'=>$data['remarks'],
        'specification'=>$data['specification'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
       ];
            $canChequeCount = count($image);
         if($canChequeCount != 0){
            $update['image']=$image;
        }
        return DB::transaction(function () use ($itemlist,$update) {
            if ($itemlist->update($update)); 
            return $itemlist;

        });
    }

    public function parentStandard(){
        return $this->model->get();
    }

    public function parentProduct(){
        return $this->model->get();
    }
    public function createitem(array $data) : Itemlist
    {
        return DB::transaction(function () use ($data) {
        
            $itemlist = parent::create([
                        'product_id'=>$data['product_id'],
                        'brand_id'=>$data['brand_id'],
                        'cat_id'=>$data['cat_id'],
                        'model_no'=>$data['model_no'],
                        'hsn_code'=>$data['hsn_code'],
                        'unit'=>$data['unit'],
                        'color'=>$data['color'],
                        'description'=>$data['description'],
                        'remarks'=>$data['remarks'],
                        'specification'=>$data['specification'],
                        'image'=>$data['image'],
                        'status'=> 0,
                        'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
            ]);
            return $itemlist;
        });
    }
}
