<?php
namespace App\Repositories\Backend\ProductManagement;

use App\Models\ProductManagement\ItemList;              
use App\Models\ProductManagement\Product;        
use App\Models\ProductManagement\Category;        
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;


class ItemMappingRepository extends BaseRepository
{
   
    public function model()
    {
        return ItemList::class;
    }

    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
           
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    
    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$specif,$hsncode,$modelno,$product_id,$brand_id,$itemno) : LengthAwarePaginator
    {
        if($product_id!=null && $brand_id!=null && $term!=null && $specif!=null && $hsncode=null && $modelno=null && $itemno=null){
            return $this->model
            ->where([['product_id','=',$product_id],['brand_id','=',$brand_id],['description','like','%'.$term.'%'],['specification','like','%'.$specif.'%'],['hsn_code','like','%'.$hsncode.'%'],['model_no','like','%'.$modelno.'%'],['id','like','%'.$itemno.'%']])
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
        }elseif ($brand_id!=null){
            return $this->model
            ->where('brand_id','=',$brand_id)
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
            }
            elseif ($product_id!=null){
                return $this->model
                ->where('product_id','=',$product_id)
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);
                }
                elseif ($itemno!=null){
                    return $this->model
                    ->where('id', 'like', '%'.$itemno.'%')
                        ->orderBy($orderBy, $sort)
                        ->paginate($paged);
                    }
            elseif($specif!=null){
                return $this->model
                ->where('specification','like','%'.$specif.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                
            }
            elseif($hsncode!=null){
                return $this->model
                ->where('hsn_code','like','%'.$hsncode.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                
            }
            elseif($modelno!=null){
                return $this->model
                ->where('model_no','like','%'.$modelno.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                
            }
            else{
                return $this->model
                ->where('description','like','%'.$term.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                

            }
    }

}
