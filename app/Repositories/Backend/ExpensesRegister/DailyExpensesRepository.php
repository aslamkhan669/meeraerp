<?php

namespace App\Repositories\Backend\ExpensesRegister;

use App\Models\ExpensesRegister\Tea;
use App\Models\ExpensesRegister\Water;
use App\Models\ExpensesRegister\Stationery;
use App\Models\ExpensesRegister\ItPhone;
use App\Models\ExpensesRegister\SalaryExpense;
use App\Models\ExpensesRegister\OtherExpense;
use App\Models\ExpensesRegister\SupplierVisit;
use App\Models\ExpensesRegister\ClientVisit;
use App\Models\ExpensesRegister\DailyExpense;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class DailyExpensesRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return DailyExpense::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term) : LengthAwarePaginator
    {
        return $this->model
        ->where([['updated_at','LIKE','%'.$term.'%']])
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }



}