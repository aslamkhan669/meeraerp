<?php

namespace App\Repositories\Backend\Master;

use App\Models\Master\Client;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Events\Backend\Client\ClientDeleted;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\LogManagement\Log;


/**
 * Class CustomerRepository.
 */
class ClientRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Client::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 250000, $orderBy = 'updated_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term) : LengthAwarePaginator
    {
        return $this->model
        ->where([['client_name','LIKE','%'.$term.'%']])
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : Client
    {
        
        return DB::transaction(function () use ($data) {
        

            $salesletters='S0';
            $str1=strtoupper($salesletters).$data['sales_center'];
            $sal=substr($str1,-2);
            $salescenter='S'.$sal;
            $client = parent::create([
                'client_name'=>$data['client_name'],
                'client_type'=>$data['client_type'], 
                'rating'=>$data['rating'], 
                'email'=>$data['email'], 
                'email1'=>$data['email1'], 
                'email2'=>$data['email2'], 
                'email3'=>$data['email3'], 
                'email4'=>$data['email4'], 
                'email5'=>$data['email5'], 
                'purchase_category'=>$data['purchase_category'], 
                'sales_center'=>$salescenter, 
                'contact_person1'=>$data['contact_person1'], 
                'contact_number1'=>$data['contact_number1'], 
                'contact_person2'=>$data['contact_person2'], 
                'contact_number2'=>$data['contact_number2'], 
                'contact_person3'=>$data['contact_person3'], 
                'contact_number3'=>$data['contact_number3'], 
                'contact_person4'=>$data['contact_person4'], 
                'contact_number4'=>$data['contact_number4'], 
                'contact_person5'=>$data['contact_person5'], 
                'contact_number5'=>$data['contact_number5'], 
                'contact_person6'=>$data['contact_person6'], 
                'contact_number6'=>$data['contact_number6'], 
                'contact_person7'=>$data['contact_person7'], 
                'contact_number7'=>$data['contact_number7'], 
                'contact_person8'=>$data['contact_person8'], 
                'contact_number8'=>$data['contact_number8'], 
                'street'=>$data['street'], 
                'landmark'=>$data['landmark'], 
                'sector'=>$data['sector'], 
                'city'=>$data['city'], 
                'pincode'=>$data['pincode'], 
                'state'=>$data['state'], 
                'zone'=>$data['zone'], 
                'pancard'=>$data['pancard'], 
                'adharcard'=>$data['adharcard'], 
                'gst_no'=>$data['gst_no'], 
                'payment_term'=>$data['payment_term'], 
                'payment_mode'=>$data['payment_mode'],
                'excisable' => isset($data['excisable']) && $data['excisable'] == '1' ? 1 : 0,
                'demand_frequency'=>$data['demand_frequency'],
  
             
            ]);

      
            return $client;
        });
    }



    public function update(Client $client, array $data) : Client
    {    

        return DB::transaction(function () use ($client, $data) {
            if ($client->update([
                'client_name'=>$data['client_name'],
                'client_type'=>$data['client_type'], 
                'rating'=>$data['rating'], 
                'email'=>$data['email'], 
                'email1'=>$data['email1'], 
                'email2'=>$data['email2'], 
                'email3'=>$data['email3'], 
                'email4'=>$data['email4'], 
                'email5'=>$data['email5'], 
                'purchase_category'=>$data['purchase_category'], 
                'sales_center'=>$data['sales_center'], 
                'contact_person1'=>$data['contact_person1'], 
                'contact_number1'=>$data['contact_number1'], 
                'contact_person2'=>$data['contact_person2'], 
                'contact_number2'=>$data['contact_number2'], 
                'contact_person3'=>$data['contact_person3'], 
                'contact_number3'=>$data['contact_number3'], 
                'contact_person4'=>$data['contact_person4'], 
                'contact_number4'=>$data['contact_number4'], 
                'contact_person5'=>$data['contact_person5'], 
                'contact_number5'=>$data['contact_number5'], 
                'contact_person6'=>$data['contact_person6'], 
                'contact_number6'=>$data['contact_number6'], 
                'contact_person7'=>$data['contact_person7'], 
                'contact_number7'=>$data['contact_number7'], 
                'contact_person8'=>$data['contact_person8'], 
                'contact_number8'=>$data['contact_number8'], 
                'street'=>$data['street'], 
                'landmark'=>$data['landmark'], 
                'sector'=>$data['sector'], 
                'city'=>$data['city'], 
                'pincode'=>$data['pincode'], 
                'state'=>$data['state'], 
                'zone'=>$data['zone'], 
                'pancard'=>$data['pancard'], 
                'adharcard'=>$data['adharcard'], 
                'gst_no'=>$data['gst_no'], 
                'payment_term'=>$data['payment_term'], 
                'payment_mode'=>$data['payment_mode'],
                'excisable' => isset($data['excisable']) && $data['excisable'] == '1' ? 1 : 0,
                'demand_frequency'=>$data['demand_frequency'],
            ])); 
            return $client;

        });
    }


}