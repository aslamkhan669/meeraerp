<?php

namespace App\Repositories\Backend\Master;

use App\Models\Master\ClientType;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
// use App\Events\Backend\Client\ClientDeleted;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class ClientTypeRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ClientType::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : ClientType
    {
        
        return DB::transaction(function () use ($data) {
            $clienttype = parent::create([
                'type'=>$data['type'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
                ]);
            return $clienttype;
        });
    }



    public function update(ClientType $clienttype, array $data) : ClientType
    {    

        return DB::transaction(function () use ($clienttype, $data) {
            if ($clienttype->update([
                'type'=>$data['type'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
                ])); 
            return $clienttype;

        });
    }


}