<?php

namespace App\Repositories\Backend\Master;

use App\Models\Master\Supplier;
use App\Models\Master\CategorySupplier;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Events\Backend\Supplier\SupplierDeleted;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class SupplierRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Supplier::class;
    }
    public function __construct(CategorySupplier $categorysupplier)
    {
        parent::__construct();
        $this->categorysupplier = $categorysupplier;
    }
    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getSearchedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc',$term) : LengthAwarePaginator
    {
        return $this->model
        ->where('supplier_name','like','%'.$term.'%')
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function createnew(array $data) : Supplier
    {
        $supplier =   $data['supplier'];
        $qi = [];
        for($i=0;$i<sizeof($data['supplierItems']);$i++){
            $item = new CategorySupplier;
            array_push($qi,new CategorySupplier($data['supplierItems'][$i]));
        }   
        return DB::transaction(function () use ($supplier,$qi) {
          
            $supplier = parent::create([
                'supplier_name'=>$supplier['supplier_name'],
                'pricing_center'=>$supplier['pricing_center'], 
                'supplier_type'=>$supplier['supplier_type'],
                'supplier_category'=>$supplier['supplier_category'],  
                'mobile'=>$supplier['mobile'], 
                'landline'=>$supplier['landline'], 
                'email'=>$supplier['email'], 
                'rating'=>$supplier['rating'], 
                'contact_person1'=>$supplier['contact_person1'], 
                'contact_number1'=>$supplier['contact_number1'], 
                'contact_person2'=>$supplier['contact_person2'], 
                'contact_number2'=>$supplier['contact_number2'], 
                'contact_person3'=>$supplier['contact_person3'], 
                'contact_number3'=>$supplier['contact_number3'], 
                'contact_person4'=>$supplier['contact_person4'], 
                'contact_number4'=>$supplier['contact_number4'], 
                'supplier_catalog'=>$supplier['supplier_catalog'], 
                'street'=>$supplier['street'], 
                'landmark'=>$supplier['landmark'], 
                'sector'=>$supplier['sector'], 
                'city'=>$supplier['city'], 
                'pincode'=>$supplier['pincode'], 
                'state'=>$supplier['state'], 
                'website'=>$supplier['website'], 
                'zone'=>$supplier['zone'], 
                'pancard'=>$supplier['pancard'], 
                'adharcard'=>$supplier['adharcard'], 
                'gst_no'=>$supplier['gst_no'], 
                'bank_name'=>$supplier['bank_name'], 
                'ifsc_code'=>$supplier['ifsc_code'], 
                'account_no'=>$supplier['account_no'], 
                'cancelled_cheque'=>$supplier['cancelled_cheque'],
  
             
            ]);
            $supplier->categorysuppliers()->saveMany($qi);

      
            return $supplier;
        });
    }



    public function update(Supplier $supplier, array $data, $cancelled_cheque, $supplier_catalog) : Supplier
    {  
         $update= [
        'supplier_name'=>$data['supplier_name'],
        'pricing_center'=>$data['pricing_center'], 
        'supplier_type'=>$data['supplier_type'], 
        'supplier_category'=>$data['supplier_category'], 
        'mobile'=>$data['mobile'], 
        'landline'=>$data['landline'], 
        'email'=>$data['email'], 
        'rating'=>$data['rating'], 
        'contact_person1'=>$data['contact_person1'], 
        'contact_number1'=>$data['contact_number1'], 
        'contact_person2'=>$data['contact_person2'], 
        'contact_number2'=>$data['contact_number2'], 
        'contact_person3'=>$data['contact_person3'], 
        'contact_number3'=>$data['contact_number3'], 
        'contact_person4'=>$data['contact_person4'], 
        'contact_number4'=>$data['contact_number4'], 
        'street'=>$data['street'], 
        'landmark'=>$data['landmark'], 
        'sector'=>$data['sector'], 
        'city'=>$data['city'], 
        'pincode'=>$data['pincode'], 
        'state'=>$data['state'], 
        'website'=>$data['website'], 
        'zone'=>$data['zone'], 
        'pancard'=>$data['pancard'], 
        'adharcard'=>$data['adharcard'], 
        'gst_no'=>$data['gst_no'], 
        'bank_name'=>$data['bank_name'], 
        'ifsc_code'=>$data['ifsc_code'], 
        'account_no'=>$data['account_no'], 
         ];
         $canChequeCount = count($cancelled_cheque);
         if($canChequeCount != 0){
            $update['cancelled_cheque']=$cancelled_cheque;
        }
        $supCatalogCount = count($supplier_catalog);
        if($supCatalogCount != 0){
            $update['supplier_catalog']=$supplier_catalog;
        }
        return DB::transaction(function () use ($supplier,$update) {
            if ($supplier->update($update)); 
            return $supplier;

        });
    }

    public function deleteItem($id)
    {
        $item = $this->categorysupplier->find($id);
        return ($item!=null?$item->delete():false);      
        
    }
}