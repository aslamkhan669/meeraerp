<?php

namespace App\Repositories\Backend\PurchaseTeam;

use App\Models\PoFormatting\PoFormatting;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Models\PoPlanner\PoRegister;
// use App\Events\Backend\Client\ClientDeleted;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class PurchaseTeamRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return PoRegister::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        $id=auth()->user()->id;
        return $this->model
            ->orderBy($orderBy, $sort)
            ->where([['pt_user_id','=',$id],['checked','=',1]])
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : Color
    {
        
        return DB::transaction(function () use ($data) {
            $color = parent::create([
                'color'=>$data['color'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
                ]);
            return $color;
        });
    }



    public function update(Color $color, array $data) : Color
    {    

        return DB::transaction(function () use ($color, $data) {
            if ($color->update([
                'color'=>$data['color'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
                ])); 
            return $color;

        });
    }


}