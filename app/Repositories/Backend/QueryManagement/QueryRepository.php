<?php

namespace App\Repositories\Backend\QueryManagement;

use App\Models\QueryManagement\Query;
use App\Models\QueryManagement\QueryItem;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Models\LogManagement\Log;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class QueryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Query::class;
    }
    public function items(){

        return QueryItem::class;
    }
    public function __construct(QueryItem $queryItem)
    {
        parent::__construct();
        $this->queryItem = $queryItem;
    }
    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'updated_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : Query
    {
      
        $query =   $data['query'];
        $qi = [];
        for($i=0;$i<sizeof($data['queryItems']);$i++){
            $item = new QueryItem;
            array_push($qi,new QueryItem($data['queryItems'][$i]));
        }    
       
        return DB::transaction(function () use ($query,$qi) {

            $pro = new Log;
            $pro->name=$query['client_id'];
            $pro->user_name=$query['user_name'];
             $pro->user_id=$query['user_id'];
             $pro->module=$query['module'];
             $pro->action=$query['action'];
             $pro->activity=$query['activity'];
             $pro->save();

            $query = parent::create([
                'client_id'=>$query['client_id'],
                'normal_oncall'=>$query['normal_oncall'],
                // 'query_date'=>$query['query_date'],
                'center_id'=>$query['center_id'],
                'meera_center_code'=>$query['meera_center_code'],
                'query_center'=>$query['query_center'],
                'po_no'=>$query['po_no'],
                'sales_center'=>$query['sales_center'],
               // 'quote_no'=>$query['quote_no'],
                'client_detail'=>$query['client_detail'],
                'query_person'=>$query['query_person'],
                'contact_no'=>$query['contact_no']
                ]); 
            $query->queryItems()->saveMany($qi);

            return $query;
        });
    }


public function update(Query $query, array $data) : Query
{    

    return DB::transaction(function () use ($query, $data) {
        if ($query->update([
            'client_id'=>$data['client_id'],
            'normal_oncall'=>$data['normal_oncall'],
            // 'query_date'=>$data['query_date'],
            'center_id'=>$data['center_id'],
            'meera_center_code'=>$data['meera_center_code'],
            'query_center'=>$data['query_center'],
            'po_no'=>$data['po_no'],
            'sales_center'=>$data['sales_center'],
           // 'quote_no'=>$data['quote_no'],
            'client_detail'=>$data['client_detail']
            ])); 
        return $query;

    });
}
public function deleteItem($id)
{
    $item = $this->queryItem->find($id);
    return ($item!=null?$item->delete():false);      
    
}
public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term) : LengthAwarePaginator
{
    return $this->model
    ->where([['id','LIKE','%'.$term.'%']])
        ->orderBy($orderBy, $sort)
        ->paginate($paged);
}

}