<?php

namespace App\Repositories\Backend\PoPlanner;

use App\Models\PoFormatting\PoFormatting;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;


class PoPlannerRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return PoFormatting::class;
    }

    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$clientpo) : LengthAwarePaginator
    {
        if($term!=null && $clientpo!=null){
            return $this->model
            ->where([['id','=',$term],['c_p_owner_no','like','%'.$clientpo.'%']])
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
        }elseif ($term!=null){
            return $this->model
            ->where('id','=',$term)
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
            }
        
            else{
                return $this->model
                ->where('c_p_owner_no','like','%'.$clientpo.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                

            }
    }
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }





}