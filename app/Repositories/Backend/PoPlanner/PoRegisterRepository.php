<?php

namespace App\Repositories\Backend\PoPlanner;

use App\Models\PoFormatting\PoFormatting;
use App\Models\PoPlanner\PoRegister;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Validation\Rules\Unique;


/**
 * Class CustomerRepository.
 */
class PoRegisterRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return PoRegister::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        // $picks = DB::table('po_registers')->distinct()->select('pt_user_id')->groupBy('pt_user_id')->get();
        return 
        $this->model  
        // ->where('status','=','0')
        ->select('pt_user_id','poitemid','id','updated_at','purchase_date','qty')
        ->groupBy('pt_user_id','purchase_date')
        ->orderBy($orderBy, $sort)
        ->paginate($paged);
    }

    public function getActPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return 
        $this->model  
        ->select('pt_user_id','poitemid','id','updated_at','purchase_date','qty')
        ->orderBy($orderBy, $sort)
        ->paginate($paged);
    }
    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : Color
    {
        
        return DB::transaction(function () use ($data) {
            $color = parent::create([
                'color'=>$data['color'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
                ]);
            return $color;
        });
    }
    public function getSearchedPaginated($paged = 2500, $orderBy = 'created_at', $sort = 'desc',$term) : LengthAwarePaginator
    {
        return $this->model
        ->where('poitemid','like','%'.$term.'%')
        ->where('status','=','0')
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    public function update(Color $color, array $data) : Color
    {    

        return DB::transaction(function () use ($color, $data) {
            if ($color->update([
                'color'=>$data['color'],
                'is_active' => isset($data['is_active']) && $data['is_active'] == '1' ? 1 : 0
                ])); 
            return $color;

        });
    }


}