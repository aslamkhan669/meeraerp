<?php

namespace App\Repositories\Backend\PoPlanner;

use App\Models\PurchaseOrder\PO;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Validation\Rules\Unique;
use App\Models\PurchaseOrder\PoFormatting;


/**
 * Class CustomerRepository.
 */
class PoGenerateRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return PO::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'updated_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model 
        ->groupBy('po_id')
        ->orderBy($orderBy, $sort)
        ->paginate($paged);
    }
    public function getSearchedPaginated($paged = 2500, $orderBy = 'created_at', $sort = 'desc',$term) : LengthAwarePaginator
    {
        return $this->model
        ->where('id','like','%'.$term.'%')
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
}