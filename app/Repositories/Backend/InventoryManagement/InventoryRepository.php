<?php

namespace App\Repositories\Backend\InventoryManagement;

use App\Models\InventoryManagement\InventoryProduct;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class InventoryRepository.
 */
class InventoryRepository extends BaseRepository
{
   

    public function model()
    {
        return InventoryProduct::class;
    }

   
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }


    public function getActivePaginated($paged = 25, $orderBy = 'updated_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


   
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$description) : LengthAwarePaginator
    {
        if($term!=null && $description!=null){
            return $this->model
            ->where([['id','=',$term],['description','like','%'.$description.'%']])
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
        }elseif ($term!=null){
            return $this->model
            ->where('poregisterid','=',$term)
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
            }
        
            else{
                return $this->model
                ->where('poregisterid','like','%'.$description.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                

            }
    }

    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function create(array $data) : Client
    {
        return DB::transaction(function () use ($data) {
            $salesletters='S0';
            $str1=strtoupper($salesletters).$data['sales_center'];
            $sal=substr($str1,-2);
            $salescenter='S'.$sal;
            $client = parent::create([
                'client_name'=>$data['client_name'],
                'client_type'=>$data['client_type'], 
                'rating'=>$data['rating'], 
                'purchase_category'=>$data['purchase_category'], 
                'sales_center'=>$salescenter, 
                'contact_person1'=>$data['contact_person1'], 
                'contact_number1'=>$data['contact_number1'], 
                'contact_person2'=>$data['contact_person2'], 
                'contact_number2'=>$data['contact_number2'], 
                'contact_person3'=>$data['contact_person3'], 
                'contact_number3'=>$data['contact_number3'], 
                'contact_person4'=>$data['contact_person4'], 
                'contact_number4'=>$data['contact_number4'], 
                'street'=>$data['street'], 
                'landmark'=>$data['landmark'], 
                'sector'=>$data['sector'], 
                'city'=>$data['city'], 
                'pincode'=>$data['pincode'], 
                'state'=>$data['state'], 
                'zone'=>$data['zone'], 
                'pancard'=>$data['pancard'], 
                'adharcard'=>$data['adharcard'], 
                'gst_no'=>$data['gst_no'], 
                'payment_term'=>$data['payment_term'], 
                'payment_mode'=>$data['payment_mode'],
                'excisable' => isset($data['excisable']) && $data['excisable'] == '1' ? 1 : 0,
                'demand_frequency'=>$data['demand_frequency'],
            ]);
            return $client;
        });
    }

    public function update(Client $client, array $data) : Client
    {    
        return DB::transaction(function () use ($client, $data) {
            if ($client->update([
                'client_name'=>$data['client_name'],
                'client_type'=>$data['client_type'], 
                'rating'=>$data['rating'], 
                'purchase_category'=>$data['purchase_category'], 
                'sales_center'=>$data['sales_center'], 
                'contact_person1'=>$data['contact_person1'], 
                'contact_number1'=>$data['contact_number1'], 
                'contact_person2'=>$data['contact_person2'], 
                'contact_number2'=>$data['contact_number2'], 
                'contact_person3'=>$data['contact_person3'], 
                'contact_number3'=>$data['contact_number3'], 
                'contact_person4'=>$data['contact_person4'], 
                'contact_number4'=>$data['contact_number4'], 
                'street'=>$data['street'], 
                'landmark'=>$data['landmark'], 
                'sector'=>$data['sector'], 
                'city'=>$data['city'], 
                'pincode'=>$data['pincode'], 
                'state'=>$data['state'], 
                'zone'=>$data['zone'], 
                'pancard'=>$data['pancard'], 
                'adharcard'=>$data['adharcard'], 
                'gst_no'=>$data['gst_no'], 
                'payment_term'=>$data['payment_term'], 
                'payment_mode'=>$data['payment_mode'],
                'excisable' => isset($data['excisable']) && $data['excisable'] == '1' ? 1 : 0,
                'demand_frequency'=>$data['demand_frequency'],
            ])); 
            return $client;

        });
    }


}