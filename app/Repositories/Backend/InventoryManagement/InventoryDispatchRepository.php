<?php

namespace App\Repositories\Backend\InventoryManagement;

use App\Models\InventoryManagement\InventoryDispatch;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\DispatchRegister\DispatchRegister;


/**
 * Class InventoryRepository.
 */
class InventoryDispatchRepository extends BaseRepository
{
   

    public function model()
    {
        return InventoryDispatch::class;
    }

   
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }


    public function getActivePaginated($paged = 25, $orderBy = 'updated_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


   
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

  

    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$description) : LengthAwarePaginator
    {
        if($term!=null && $description!=null){
            return $this->model
            ->where([['id','=',$term],['description','like','%'.$description.'%']])
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
        }elseif ($term!=null){
            return $this->model
            ->where('disregisterid','=',$term)
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
            }
        
            else{
                return $this->model
                ->where('disregisterid','like','%'.$description.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                

            }
    }


 


}