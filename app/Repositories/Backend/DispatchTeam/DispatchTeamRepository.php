<?php

namespace App\Repositories\Backend\DispatchTeam;

use App\Models\PoFormatting\PoFormatting;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use App\Models\DispatchRegister\DispatchRegister;
use App\Models\DispatchRegister\DispatchedOrder ;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class DispatchTeamRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return DispatchRegister::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        $id=auth()->user()->id;
        return $this->model
            ->orderBy($orderBy, $sort)
            ->where('dt_user_id','=',$id)
            ->select('dt_user_id','poitemid','id','payment_amount','updated_at','purchase_date')
            ->groupBy('purchase_date')
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }






}