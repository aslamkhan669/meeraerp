<?php

namespace App\Repositories\Backend\ManualPurchasing;

use App\Models\ManualPurchasing\ManualPurchase;
use App\Models\ManualPurchasing\ManualItem;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\LogManagement\Log;


/**
 * Class CustomerRepository.
 */
class ManualPurchaseRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return ManualPurchase::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'updated_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }


    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function create(array $data) : ManualPurchase
    {
 
        $query =   $data['query'];
        $qi = [];
        for($i=0;$i<sizeof($data['queryItems']);$i++){
            $item = new ManualItem;
            array_push($qi,new ManualItem($data['queryItems'][$i]));
        }    
       
        return DB::transaction(function () use ($query,$qi) {

            $pro = new Log;
            $pro->name=$query['client_id'];
            $pro->user_name=$query['user_name'];
             $pro->user_id=$query['user_id'];
             $pro->module=$query['module'];
             $pro->action=$query['action'];
             $pro->activity=$query['activity'];
             $pro->save();

            $query = parent::create([
                'client_id'=>$query['client_id'],
              
                'center_id'=>$query['center_id'],
                'meera_center_code'=>$query['meera_center_code'],
                'query_center'=>$query['query_center'],
                'po_no'=>$query['po_no'],
                'sales_center'=>$query['sales_center'],
                'client_detail'=>$query['client_detail'],
                'query_person'=>$query['query_person'],
                'contact_no'=>$query['contact_no']
                ]); 
            $query->manualItems()->saveMany($qi);

            return $query;
        });
    }
    public function update(ManualPurchase $manualpurchase, array $data) : ManualPurchase
    {    
    
        return DB::transaction(function () use ($manualpurchase, $data) {
            if ($manualpurchase->update([
                'client_id'=>$data['client_id'],
                'center_id'=>$data['center_id'],
                'meera_center_code'=>$data['meera_center_code'],
                'query_center'=>$data['query_center'],
                'po_no'=>$data['po_no'],
                'sales_center'=>$data['sales_center'],
                'client_detail'=>$data['client_detail']
                ])); 
            return $manualpurchase;
    
        });
    }

public function deleteItem($id)
{
    $item = $this->queryItem->find($id);
    return ($item!=null?$item->delete():false);      
    
}
}