<?php
namespace App\Repositories\Backend\ManualPurchasing;

use App\Models\ManualPurchasing\OncallQuotation;
use App\Models\QueryManagement\Query;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;



/**
 * Class StandardRepository.
 */
class ManualQuotationRepository extends BaseRepository
{
    /**
     * @return string
     */

    public function model()
    {
        return OncallQuotation::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 2500, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return User
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Quotation
    {
        return DB::transaction(function () use ($data) {
            $quotation = parent::create([
                        
                        'delivery_term_id'=>$data['delivery_term_id'],
                        'supplier_id'=>$data['supplier_id'],
                        'request_no'=>$data['request_no'],
                        'customer_request_ref'=>$data['customer_request_ref'],
                        'validate_date'=>$data['validate_date'],
                        'remark'=>$data['remark']
                        
                        
   
            ]);

      
            return $quotation;
        });
    }


    public function update(Quotation $quotation, array $data) : Quotation
    {
        return DB::transaction(function () use ($quotation, $data) {
            if ($quotation->update([
                'delivery_term_id'=>$data['delivery_term_id'],
                'supplier_id'=>$data['supplier_id'],
                'request_no'=>$data['request_no'],
                'customer_request_ref'=>$data['customer_request_ref'],
                'validate_date'=>$data['validate_date'],
                'remark'=>$data['remark']
                ])); 
        
                return $quotation;
            });
    }
    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$quata,$client) : LengthAwarePaginator
    {
        if($term!=null && $quata!=null && $client!=null){
            return $this->model
            ->where([['manual_id','=',$term],['id','=',$quata],['description','like','%'.$client.'%']])
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
        }elseif ($term!=null){
            return $this->model
            ->where('manual_id','=',$term)
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
            }
            elseif ($quata!=null){
                return $this->model
                ->where('id','=',$quata)
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);
                }
            else{
                return $this->model
                ->where('client_name','like','%'.$client.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                

            }
    }

}
