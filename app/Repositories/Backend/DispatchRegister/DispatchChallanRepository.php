<?php

namespace App\Repositories\Backend\DispatchRegister;

use App\Models\DispatchRegister\DispatchedOrder;
use App\Models\DispatchRegister\DisChallan;
use App\Models\DispatchRegister\DispatchChallanItem;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Validation\Rules\Unique;
use App\Models\DispatchRegister\DispatchRegister;


/**
 * Class CustomerRepository.
 */
class DispatchChallanRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return DisChallan::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'updated_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model 
        ->groupBy('po_id')
        ->orderBy($orderBy, $sort)
        ->paginate($paged);
    }
    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$clientpono) : LengthAwarePaginator
    {
        if($term!=null && $clientpono!=null){
            return $this->model
            ->where([['id','=',$term],['c_p_owner_no','like','%'.$clientpono.'%']])
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
        }elseif ($term!=null){
            return $this->model
            ->where('id','=',$term)
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
            }
        
            else{
                return $this->model
                ->where('c_p_owner_no','like','%'.$clientpono.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                

            }
    }
}