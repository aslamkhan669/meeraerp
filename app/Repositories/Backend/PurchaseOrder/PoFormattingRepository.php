<?php

namespace App\Repositories\Backend\PurchaseOrder;

use App\Models\PurchaseOrder\PoFormatting;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Hash;
// use App\Events\Backend\Client\ClientDeleted;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class CustomerRepository.
 */
class PoFormattingRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return PoFormatting::class;
    }

    /**
     * @return mixed
     */
    public function getUnconfirmedCount() : int
    {
        return $this->model
            ->where('confirmed', 0)
            ->count();
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getSearchedPaginated($paged = 25000, $orderBy = 'created_at', $sort = 'desc',$term,$clientpono) : LengthAwarePaginator
    {
        if($term!=null && $clientpono!=null){
            return $this->model
            ->where([['id','=',$term],['c_p_owner_no','like','%'.$clientpono.'%']])
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
        }elseif ($term!=null){
            return $this->model
            ->where('id','=',$term)
                ->orderBy($orderBy, $sort)
                ->paginate($paged);
            }
        
            else{
                return $this->model
                ->where('c_p_owner_no','like','%'.$clientpono.'%')
                    ->orderBy($orderBy, $sort)
                    ->paginate($paged);                

            }
    }
    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getInactivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->active(false)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->with('roles', 'permissions', 'providers')
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function update(PoFormatting $poformatting, array $data) : PoFormatting
    {    

        return DB::transaction(function () use ($poformatting, $data) {
            if ($poformatting->update([
                'c_p_owner_no'=>$data['c_p_owner_no'],
                'c_purchaser_name'=>$data['c_purchaser_name'],
                'c_reciever_name'=>$data['c_reciever_name']

                ])); 
            return $poformatting;

        });
    }


}