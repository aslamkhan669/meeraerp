<?php

namespace App\Http\Requests\Backend\Subcategory;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreUserRequest.
 */
class StoreSubcategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_id'     => 'required|max:191',
            'category_name'     => 'required|max:191',
            'code'     => 'required|max:191',
            'image'     => 'required|max:191',           
        ];
    }
}
