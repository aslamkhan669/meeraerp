<?php

namespace App\Http\Requests\Backend\ProductManagement;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateStandardRequest.
 */
class UpdateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'brand_name'      => 'required|max:191',
            'brand_code',
            'is_active',
            
            
        ];
    }
}
