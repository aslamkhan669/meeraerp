<?php

namespace App\Http\Requests\Backend\ProductManagement;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateStandardRequest.
 */
class UpdateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            
            'product_id'        => 'required|max:191',
            'cat_id'        => 'required|max:191',
            'model_no'          => 'required|max:191',
            'unit'              => 'required|max:191',
            'package_qty'       => 'required|max:191',
            'color'             => 'required|max:191',
            'remarks'           => 'required',
            'description',       
            'specification',    
            'is_active',
            
            
        ];
    }
}
