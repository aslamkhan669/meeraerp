<?php

namespace App\Http\Requests\Backend\ProductManagement;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreUserRequest.
 */
class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'product_name'      => 'required|max:191',
            'product_owner'     => 'required|max:191',
            'is_active',
            'brand_id',
            'category_id',
        ];
    }
}
