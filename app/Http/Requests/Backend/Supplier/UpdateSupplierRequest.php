<?php

namespace App\Http\Requests\Backend\Supplier;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRoleRequest.
 */
class UpdateSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supplier_name'     => 'required|max:191',
            'pricing_center' => 'required|max:191',
             'supplier_type' => 'required|max:191', 
             'mobile' => 'required|max:191', 
             'landline' => 'required|max:191', 
             'email' => 'required|max:191', 
             'rating' => 'required|max:191',
        ];
    }
}
