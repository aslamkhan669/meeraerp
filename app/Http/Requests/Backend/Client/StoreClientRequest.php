<?php

namespace App\Http\Requests\Backend\Client;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class StoreUserRequest.
 */
class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_name'     => 'required|max:191',
            'client_type'     => 'required|max:191',
            'purchase_category'     => 'required|max:191',

           
        ];
    }
}
