<?php

namespace App\Http\Requests\Backend\QuotationManagement;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateStandardRequest.
 */
class UpdateQuotationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'delivery_term_id'=> 'required|max:191',
            'supplier_id'=> 'required|max:191',
            'request_no'=>'required|max:191',
            'customer_request_ref'=>'required|max:191',
            'validate_date'=>'required|max:191',
            'remark'=>'required|max:191',
            
        ];
    }
}
