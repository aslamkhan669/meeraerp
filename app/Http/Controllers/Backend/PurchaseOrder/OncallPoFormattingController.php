<?php

namespace App\Http\Controllers\Backend\PurchaseOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PurchaseOrder\PoFormatting;
use App\Models\QuotationManagement\Quotation;
use App\Models\QueryManagement\QueryItem;
use App\Models\QueryManagement\PriceMapping;
use App\Models\QueryManagement\Query;
use App\Models\PurchaseOrder\PoItem;
use App\Repositories\Backend\PurchaseOrder\PoFormattingRepository;
use App\Models\ManualPurchasing\ManualPurchase;
use App\Models\ManualPurchasing\ManualItem;
use Illuminate\Support\Facades\DB;
use App\Models\LogManagement\Log;
use App\Models\ManualPurchasing\OncallQuotation;
use App\Models\ProductManagement\ItemList;


class OncallPoFormattingController extends Controller
{
   

    protected $poformattingRepository;
    
    public function __construct(PoFormattingRepository $poformattingRepository)
    {
        $this->poformattingRepository = $poformattingRepository;
        $this->middleware(['permission:PoFormatting']);   
    }


    public function index()
    {
       
        $poformattings = $this->poformattingRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        return view('backend.poformatting.index',compact('poformattings'));
    }
    public function search(Request $request)
    {
        $term=$request->term;
        $clientpono=$request->clientpono;
        $poformattings = $this->poformattingRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$clientpono); 
        return view('backend.poformatting.index',compact('poformattings'));
    }
    
    public function create(Quotation $quotation,Query $query,OncallQuotation $oncallquotation)
    {
        // $quotationno=$quotation->where('is_confirmed','=',1)->where('status','=',1)->get();
        $oncallquotationno=$oncallquotation->where('is_confirmed','=',1)->where('status','=',1)->get();
        $qid=$oncallquotationno[0]->manual_id;
        $companyinfo=DB::SELECT("SELECT * FROM manual_purchases q INNER JOIN company_details c ON q.center_id=c.id WHERE q.id=$qid");

        return view('backend.oncallpoformatting.create',compact('quotationno','oncallquotationno','companyinfo'));
    }


    public function store(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->Quotation_id;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();

        $quote = PoFormatting::where('oncallquotation_id', '=', $request->Quotation_id)->first();
        if ($quote === null) {
        $poformatting = new PoFormatting();
        $poformatting->oncallquotation_id=$request->Quotation_id;
        $poformatting->c_p_owner_no=$request->client_purchase_owner;
        $poformatting->c_purchaser_name=$request->client_purchaser_name;
        $poformatting->c_reciever_name=$request->client_reciever_name;
        $poformatting->status=0;
        if($poformatting->save()) {
            $lid=$poformatting->id;
            return $lid;
        }else{
            return 'false';
        }
    }   
    
    }

    public function queryitems(QueryItem $qitem,$id){
       $qitems=DB::SELECT("SELECT * FROM query_items INNER JOIN price_mappings ON query_items.id=price_mappings.item_id WHERE query_id=$id AND is_selected=1");
       return $qitems;
    }
    public function manualitems(ManualItem $manualitem,$id){
        $qitems=DB::SELECT("SELECT * FROM manual_items INNER JOIN oncall_pricemaps ON manual_items.id=oncall_pricemaps.item_id WHERE manual_id=$id AND is_selected=1");
        return $qitems;
     }
    public function poitems(Request $request){
        for($i=0;$i<sizeof($request->dataArray);$i++){
        $poformatting = new PoItem();
        $poformatting->oncallpoitemid=$request->dataArray[$i]['QueryItemId'];
        $poformatting->oncallpo_id =$request->poid;
        $poformatting->qty=$request->dataArray[$i]['Qty'];
        $poformatting->grand_total =$request->dataArray[$i]['Gtotal'];
        if(!$poformatting->save()) {
            return 'false';
        }
    }
        return 'true';
    }

    public function edit(PoFormatting $poformatting, Quotation $quotation,QueryItem $queryitem,PoItem $poitem,PriceMapping $pricemapping)
    {
        $quotationno=$quotation->where('is_confirmed','=',1)->where('status','=',1)->get();
        $qinfo=$poformatting->first();
        $sid=$poformatting->quotation->query_id;
        $qid=$quotationno[0]->query_id;
        $companyinfo=DB::SELECT("SELECT * FROM queries q INNER JOIN company_details c ON q.center_id=c.id WHERE q.id=$qid");
        $qitems=DB::SELECT("SELECT * FROM ((query_items INNER JOIN price_mappings ON query_items.id=price_mappings.item_id)INNER JOIN po_items ON query_items.id=po_items.qitemid) WHERE query_id=$sid AND is_selected=1");
        return view('backend.poformatting.edit', compact('companyinfo','poformatting','quotationno','poitems','qinfo','qitems'));
    }
    public function update(PoFormatting $poformatting ,Request $request ,PoItem $poitem)
    {
        $pro = new Log;
        $pro->name=$request->quotation_no;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();

        $data = $request->all();
        $sitems = $poformatting->poItems()->delete();
        for($i=0;$i<sizeof($data['qty']);$i++){
        $poItems = PoItem::updateOrcreate([
            'id'=>$data['id'][$i], 
            'qty'=>$data['qty'][$i], 
            'grand_total'=>$data['po_grand_total'][$i], 
            'qitemid'=>$data['qitemid'][$i], 
            'po_id'=>$data['po_id'][$i]
            ]);
        }
        $this->poformattingRepository->update($poformatting, $request->only(
            'c_p_owner_no', 
            'c_purchaser_name', 
            'c_reciever_name'
    ));
        return redirect()->route('admin.poformatting.index')->withFlashSuccess(__('alerts.backend.poformat.updated'));
    }
    public function infoitems($id){
        $items=itemlist::with('brand','color','uom','product')->where('id','=',$id)->get()->toJson();  
        return $items;
    
    }  
    public function infositems($id){
        $iteminfo=itemlist::with('brand','color','uom','product')->where('id','=',$id)->get()->toJson();  
        return $iteminfo;
    
    }  
}
