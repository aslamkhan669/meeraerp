<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\ProductManagement\Product;
use App\Models\ProductManagement\Category;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Brand;
use App\Models\QueryManagement\Query;
use Illuminate\Http\Request;
use App\Models\LogManagement\Log;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use App\Models\ExpensesRegister\DailyExpense;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{

    public function __construct()
    {
       $this->middleware(['permission:dashboard']);
    }

    public function index(Log $log,DailyExpense $daily)
    { 
      $datainfo = $log->groupBy('user_name')
      ->selectRaw('count(user_name) as total,user_name')
      ->get();
      
    $logsdata = Log::whereDate('created_at', Carbon::today())->groupBy('user_name')->groupBy('module')->groupBy('action')->groupBy('activity')
    ->selectRaw('count(activity) as total,user_name,module,action,activity')->orderBy('created_at', 'desc')
    ->paginate(5);
    

    $now = Carbon::now();
  
    $query_count=DB::SELECT('SELECT MONTH(created_at) as months, COUNT(id) AS queries_count FROM queries WHERE YEAR(created_at)="'.$now->year.'" GROUP BY YEAR(created_at), MONTH(created_at) ORDER BY MONTH(created_at) ASC');
    $quotation_data=DB::SELECT('SELECT MONTH(created_at) as months, COUNT(id) AS queries_count FROM quotations WHERE YEAR(created_at)="'.$now->year.'" GROUP BY YEAR(created_at), MONTH(created_at) ORDER BY MONTH(created_at) ASC');
    $po_data=DB::SELECT('SELECT MONTH(created_at) as months, COUNT(id) AS po_count FROM pos WHERE YEAR(created_at)="'.$now->year.'" GROUP BY YEAR(created_at), MONTH(created_at) ORDER BY MONTH(created_at) ASC');
    $supplier_data=DB::SELECT('SELECT MONTH(created_at) as months, COUNT(id) AS supplier_count FROM suppliers WHERE YEAR(created_at)="'.$now->year.'" GROUP BY YEAR(created_at), MONTH(created_at) ORDER BY MONTH(created_at) ASC');
    $material_data=DB::SELECT('SELECT MONTH(created_at) as months, SUM(payment_amount) AS material_sum FROM po_registers WHERE YEAR(created_at)="'.$now->year.'" GROUP BY YEAR(created_at), MONTH(created_at) ORDER BY MONTH(created_at) ASC');
  
    $tea_data=DB::SELECT('SELECT MONTH(created_at) as months FROM daily_expenses WHERE YEAR(created_at)="'.$now->year.'" ORDER BY MONTH(created_at) ASC');
    $info=$daily->get();
     return view('backend.dashboard',compact('info','tea_data','datainfo','logsdata','query_count','quotation_data','po_data','supplier_data','material_data'));
    }

    public function filter(Request $request,Log $log)
    {  
      $sdate=$request->sDate;
      $edate=$request->eDate;
      $user=$request->userName;
      $info=Log::whereBetween('created_at',[$sdate,$edate])->where('user_name','=',$user)->groupBy('user_name')->groupBy('module')->groupBy('action')->groupBy('activity')
      ->selectRaw('count(activity) as total,user_name,module,action,activity')->get();
      

     return $info;
}

}
