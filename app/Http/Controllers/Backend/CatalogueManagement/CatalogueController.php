<?php

namespace App\Http\Controllers\Backend\CatalogueManagement;

use Illuminate\Http\Request;
use App\Models\ProductManagement\Category;
use App\Http\Controllers\Controller;
use App\Models\ProductManagement\Product;
use DB;
use PDF;
use App\Repositories\Backend\ProductManagement\CategoryRepository;




class CatalogueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->middleware(['permission:catalogue']);   
        $this->categoryRepository = $categoryRepository;
       
    }


    public function index(Product $prodobj)
    {
        $products=$prodobj->get();
        return view('backend.catalogue.index');
    }

    public function products($key,Product $product){

        $data=$product->where('product_name','LIKE','%'.$key.'%')->select('id','product_name')->get()->toJson();
        return $data;
    }

    public function getpdf(Request $request,Category $cat){
        $data = $this->categoryRepository->getSubActivePaginated(100000, 'id', 'asc')->Where('product_id','=',$request->product_id);
         $pdf = PDF::loadView('backend.catalogue.pdf', compact('data'));
        return $pdf->inline();
       }
 
}
