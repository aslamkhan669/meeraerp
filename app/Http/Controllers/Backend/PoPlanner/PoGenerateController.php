<?php

namespace App\Http\Controllers\Backend\PoPlanner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PoPlanner\PoRegister;
use App\Repositories\Backend\PoPlanner\PoGenerateRepository;
use App\Models\PoPlanner\PurchasedOrder;
use App\Models\Account\Challan;
use App\Models\Account\ChallanItem;
use App\Models\Setting\Freight;
use App\Models\Setting\CompanyDetail;
use App\Models\Setting\TransportInsurance;
use Illuminate\Support\Facades\DB;
use App\Models\PoPlanner\PurchaseExpense;
use App\Models\PurchaseOrder\PoFormatting;
use App\Models\PurchaseOrder\PoTermcondition;
use App\Models\Master\Client;
use App\Models\Master\Supplier;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand; 
use App\Models\ProductManagement\Color;
use PDF;
use App\Models\LogManagement\Log;
use App\Models\QuotationManagement\Quotation;
use App\Models\PurchaseOrder\PoItem;
use App\Models\QueryManagement\QueryItem;
use App\Models\QueryManagement\PriceMapping;
use App\Models\PurchaseOrder\PO;

class PoGenerateController extends Controller
{

   //PoAccount controller 
    protected $poGenerateRepository;
    
    public function __construct(PoGenerateRepository $poGenerateRepository)
    {
        $this->poGenerateRepository = $poGenerateRepository;
        $this->middleware(['permission:PurchasePO']); 

    }


    public function index()
    {   
        $pogenerates = $this->poGenerateRepository->getActivePaginated(25, 'updated_at', 'desc');    
 
        return view('backend.pogenerate.index',compact('pogenerates'));
    }
    public function search(Request $request)
    {
        $term=$request->term;
        $pogenerates = $this->poGenerateRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term); 
        return view('backend.pogenerate.index',compact('pogenerates'));
    }
    public function create(PoFormatting $poform)
    {
        $poinfo=$poform->get();

        return view('backend.pogenerate.create',compact('poinfo'));
    }
    public function show($id,CompanyDetail $company,Challan $challan,ChallanItem $challanitem,Supplier $supplier,Client $client,TransportInsurance $insurance,Freight $freight)
    {
        $freights=$freight->get();
        $insurances=$insurance->get();
    
        $data=DB::SELECT("SELECT * FROM (((po_formattings pf INNER JOIN po_items pi ON pf.id=pi.po_id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN pos ps ON ps.qitemid=qi.id INNER JOIN price_mappings pm ON qi.id=pm.item_id) WHERE pf.id=$id");
        $termsdata=DB::SELECT("SELECT * FROM (((( po_items pi INNER JOIN po_formattings pf ON pi.po_id=pf.id)INNER JOIN quotations q ON pf.quotation_id=q.id)INNER JOIN queries qu ON q.query_id=qu.id)INNER JOIN termsconditions t ON qu.id=t.query_id) WHERE pf.id=$id");
        $info=$supplier->where('id','=',$data[0]->supplier_id)->get();
       $centerinfo=$company->where('id','=',$termsdata[0]->center_id)->get();
      $fid= $termsdata[0]->freight_id;
     $freightinfo=$freight->where('id','=',$fid)->get();
      $tid= $termsdata[0]->freight_id;
      $insuranceinfo=$insurance->where('id','=',$tid)->get();
        
        return view('backend.pogenerate.show',compact('id','challanitem','freights','freightinfo','insurances','insuranceinfo','data','centerinfo','info','termsdata')); 
  
    }

public function streamPDF($id,PoTermcondition $potermcondition,CompanyDetail $company,Challan $challan,ChallanItem $challanitem,Supplier $supplier,Client $client,TransportInsurance $insurance,Freight $freight)
{
    $freights=$freight->get();
    $insurances=$insurance->get();

    $data=DB::SELECT("SELECT * FROM (((po_formattings pf INNER JOIN po_items pi ON pf.id=pi.po_id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN pos ps ON ps.qitemid=qi.id INNER JOIN price_mappings pm ON qi.id=pm.item_id) WHERE pf.id=$id");
    $termsdata=DB::SELECT("SELECT * FROM (((( po_items pi INNER JOIN po_formattings pf ON pi.po_id=pf.id)INNER JOIN quotations q ON pf.quotation_id=q.id)INNER JOIN queries qu ON q.query_id=qu.id)INNER JOIN termsconditions t ON qu.id=t.query_id) WHERE pf.id=$id");
   $term=$potermcondition->where('poform_id','=',$id)->get();
   $info=$supplier->where('id','=',$data[0]->supplier_id)->get();
   $centerinfo=$company->where('id','=',$termsdata[0]->center_id)->get();
  $fid= $term[0]->freight_charges;
 $freightinfo=$freight->where('id','=',$fid)->get();
  $tid= $term[0]->material_insurance;
  $insuranceinfo=$insurance->where('id','=',$tid)->get();
$pdf = PDF::loadview('backend.pogenerate.pdf', compact('id','term','challanitem','freightinfo','insuranceinfo','data','centerinfo','info','termsdata'))->setPaper('A4', 'landscape');
  return $pdf->inline();
// return view('backend.pogenerate.pdf', compact('id','challanitem','term','freightinfo','insuranceinfo','data','centerinfo','info','termsdata'));

}


public function poitems($id){
    $qitems=DB::SELECT("SELECT * FROM (( po_items pi INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN price_mappings pm ON qi.id=pm.item_id) WHERE pi.po_id=$id");
    return $qitems;
 }

 public function poitem(Request $request){
    for($i=0;$i<sizeof($request->dataArray);$i++){
    $poformatting = new PO();
    $poformatting->qitemid=$request->dataArray[$i]['QueryItemId'];
    $poformatting->po_id=$request->dataArray[$i]['Poid'];
    $poformatting->qty=$request->dataArray[$i]['Qty'];
    $poformatting->grand_total =$request->dataArray[$i]['Gtotal'];
    if(!$poformatting->save()) {
        return 'false';
    }
}
    return 'true';
}

public function pogeneratedata(Request $request){

    $log = new Log;
    $log->user_name=$request->data[0]['userName'];
    $log->user_id=$request->data[0]['userId'];
    $log->module=$request->data[0]['module'];
    $log->action=$request->data[0]['action'];
    $log->activity=$request->data[0]['activity'];
    $log->name=$request->data[0]['Qid'];
    $log->save();
    $qid = (int)$request->data[0]['Qid'];
    $qitems =  PoTermcondition::firstOrNew(['poform_id'=>$request->data[0]['Qid']]);
    $qitems->poform_id=$request->data[0]['Qid'];
    $qitems->payment_term=$request->data[0]['paymentDays'];
    $qitems->freight_charges=$request->data[0]['freightId'];
    $qitems->material_insurance=$request->data[0]['insuranceId'];
    $qitems->po_validity=$request->data[0]['Validity'];
    $qitems->comment1=$request->data[0]['Line1'];
    $qitems->comment2=$request->data[0]['Line2'];
    $qitems->comment3=$request->data[0]['Line3'];
    $qitems->comment4=$request->data[0]['Line4'];
    $qitems->save();
    $data='true';
    return $data;

}
public function queryitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
    $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
    return $items;

}
}
