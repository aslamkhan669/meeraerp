<?php

namespace App\Http\Controllers\Backend\PoPlanner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PoPlanner\PoRegister;
use App\Repositories\Backend\PoPlanner\PoRegisterRepository;
use App\Models\PoPlanner\PurchasedOrder;
use App\Models\Account\Challan;
use App\Models\Account\ChallanItem;
use App\Models\Setting\Freight;
use App\Models\Setting\TransportInsurance;
use Illuminate\Support\Facades\DB;
use App\Models\PoPlanner\PurchaseExpense;
use App\Models\PurchaseOrder\PoFormatting;
use App\Models\PurchaseOrder\PoItem;
use App\Models\Master\Client;
use App\Models\Master\Supplier;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand; 
use App\Models\ProductManagement\Color;
use PDF;
use App\Models\LogManagement\Log;
use Carbon\Carbon;
use App\Models\InventoryManagement\InventoryProduct;


class PoRegisterController extends Controller
{

   //PoAccount controller 
    protected $poregisterRepository;
    
    public function __construct(PoRegisterRepository $poregisterRepository)
    {
        $this->poregisterRepository = $poregisterRepository;
        $this->middleware(['permission:PurchaseAccount']);   
    }


    public function index(PoRegister $poregister)
    {   
        $poregisters = $this->poregisterRepository->getActivePaginated(25, 'updated_at', 'desc');  
        
        // $data = PoRegister::whereDate('created_at', Carbon::today())->groupBy('pt_user_id')
        // ->selectRaw('payment_amount,pt_user_id')
        // ->get();
        $info=PoRegister::with('purchasedOrders')->get();
        return view('backend.poregister.index',compact('poregisters','info'));
    }
    public function search(Request $request)
    {
        $term =$request->poitemid;
    $poregisters = $this->poregisterRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term); 
    return view('backend.poregister.index',compact('poregisters'));
}
    public function edit($id,$date,TransportInsurance $insurance,Freight $freight,PoItem $poitem,PoRegister $poregister,PoFormatting $po)
    {
        $data=$poregister->where([['pt_user_id','=',$id],['purchase_date','=',$date]])->get();
      //$data=$poitem->with('poRegisters')->where('po_id','=',$id)->get();
      $amt1 =DB::SELECT("SELECT sum(po.payment_amount) p FROM po_registers po where po.pt_user_id=$id AND po.purchase_date='$date'");
      $amt2 =DB::SELECT("SELECT sum(po.payment_amount) p FROM po_registers po INNER JOIN purchased_orders pu on po.id=pu.poregisterid where  po.pt_user_id=$id AND pu.ml_purchased=1 AND po.purchase_date='$date' "); 
       $expensesamt=$amt1[0]->p-$amt2[0]->p;
      return view('backend.poregister.edit', compact('data','id','expensesamt'));
    }
    
    public function create()
    {
     
    }

    public function poaccounts(Request $request){

       $pro = new Log;
       $pro->name=$request->dataArray[0]['PoRegisterId'];
       $pro->user_name=$request->dataArray[0]['user_name'];
        $pro->user_id=$request->dataArray[0]['user_id'];
        $pro->module=$request->dataArray[0]['module'];
        $pro->action=$request->dataArray[0]['action'];
        $pro->activity=$request->dataArray[0]['activity'];
        $pro->save();

       for($i=0;$i<sizeof($request->dataArray);$i++){   
        $terms = PurchasedOrder::firstOrNew(['poregisterid'=>$request->dataArray[$i]['PoRegisterId']]);
        $terms->poRegister()->update(['qty'=>$request->dataArray[$i]['Qty']]);
        $terms->poRegister()->update(['payment_amount'=>$request->dataArray[$i]['Gtotal']]);
        $terms->poregisterid=$request->dataArray[$i]['PoRegisterId'];
        $terms->ml_recieved=$request->dataArray[$i]['MLrecieved'];
        $terms->pt_user_id=$request->dataArray[$i]['PtUser'];    
        $terms->challan_recieved=$request->dataArray[$i]['Challan'];    
        $terms->po_generate=$request->dataArray[$i]['PoGenerate'];  
        $terms->bill_challan_no=$request->dataArray[$i]['BillNo'];
        $terms->ewaybill_no=$request->dataArray[$i]['ewaybillNO'];
        $terms->recieved_doc=$request->dataArray[$i]['recievedDoc'];
        $terms->complete="1";
        $terms->save();
        $terms->poRegister()->update(['checked'=>1]);
        }
        if($terms->save()) {
            return 'true';
        }
     }





    Public function storebychallan(Request $request) 
    {
        $ch = Challan::firstOrNew(['supplier_challan'=>$request->data1['supplier_challan']]);
        $ch->client_id=$request->data1['client_id'];
        $ch->supplier_id=$request->data1['supplier_id'];
        $ch->supplier_challan=$request->data1['supplier_challan'];
        $ch->po_reg_id=$request->data1['po_reg_id'];
        $ch->save();
        $cid= $ch->id;
        for($i=0;$i<sizeof($request->data[$i]['item_id']);$i++){
            $qitems = new ChallanItem();
               $qitems->challan_id=$cid;     
       $qitems->item_id=$request->data[$i]['item_id'];
       $qitems->qty=$request->data[$i]['quantity'];
       $qitems->total=$request->data[$i]['total'];
       $qitems->gst=$request->data[$i]['gst'];
       $qitems->grand_total=$request->data[$i]['grand_total'];
       $qitems->save();
       $data='true';
       return $data;
        }
   }

   public function streamPDF($id,Challan $challan,ChallanItem $challanitem,Supplier $supplier,Client $client,TransportInsurance $insurance,Freight $freight)
   {
    $data=challan::find($id);
    $info=$supplier->where('id','=',$data->supplier_id)->get();
    $clientinfo=$client->where('id','=',$data->client_id)->get();
    $challanitem=$challanitem->where('id','=',$data->id)->get();
    $termsdata=DB::SELECT("SELECT * FROM ((((( po_registers pr INNER JOIN po_items pi ON pr.poitemid=pi.id)INNER JOIN po_formattings pf ON pi.po_id=pf.id)INNER JOIN quotations q ON pf.quotation_id=q.id)INNER JOIN queries qu ON q.query_id=qu.id)INNER JOIN termsconditions t ON qu.id=t.query_id) WHERE pr.id=$id");
    $fid= $termsdata[0]->freight_id;
    $freightinfo=$freight->where('id','=',$fid)->get();
    $tid= $termsdata[0]->freight_id;
    $insuranceinfo=$insurance->where('id','=',$tid)->get();
    $pdf = PDF::loadview('backend.poregister.pdf', compact('id','challanitem','freightinfo','insuranceinfo','data','clientinfo','info','termsdata'))->setPaper('A4', 'landscape');
    return $pdf->inline();
    //return view('backend.poregister.pdf', compact('id','challanitem','freightinfo','insuranceinfo','data','clientinfo','info','termsdata'));
   }
   public function infoitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
    $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
    return $items;

}
public function show($id,$info)
{
    $data =DB::SELECT("SELECT sum(po.payment_amount) p,pu.bill_challan_no,pu.ewaybill_no FROM po_registers po INNER JOIN purchased_orders pu on po.id=pu.poregisterid where po.pt_user_id=$id AND po.purchase_date='$info' GROUP BY pu.bill_challan_no"); 
    return view('backend.poregister.show', compact('data'));
}
public function combodata($key,PoRegister $poregister,PoItem $poitem,PoFormatting $poformat){
    $data = DB::SELECT("SELECT * FROM (( po_registers pr INNER JOIN po_items pi ON pr.poitemid=pi.id)INNER JOIN po_formattings pf ON pi.po_id=pf.id) WHERE pf.c_p_owner_no LIKE '%$key%'"); 

    return $data;
}
public function comboinfo($key){

    $data = DB::SELECT("SELECT * FROM ((((inventory_products ip INNER JOIN po_registers pr ON pr.id=ip.poregisterid) INNER JOIN po_items pi ON pr.poitemid=pi.id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN item_lists il ON qi.item_id=il.id) WHERE il.description LIKE '%$key%'"); 
    return $data;
}
public function comboiteminfo($key){

    $data = DB::SELECT("SELECT * FROM ((((inventory_products ip INNER JOIN po_registers pr ON pr.id=ip.poregisterid) INNER JOIN po_items pi ON pr.poitemid=pi.id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN item_lists il ON qi.item_id=il.id) WHERE il.id LIKE '%$key%'"); 
    return $data;
}
public function storeinv(Request $request)
{
    $ch = InventoryProduct::firstOrNew(['poregisterid'=>$request->POID]);
    $ch->reason=$request->Reason;
    $ch->extra_qty=$request->QNTY;
    $ch->poregisterid=$request->POID;
    $ch->bill_no=$request->billNo;
    $ch->location=$request->location;

    $ch->save();
    DB::table('po_registers')
        ->where('id', $request->POID)
        ->update(['status' => '1']);
   return 'true';
}

    public function indexinfo(PoRegister $poregister)
{   
 
    $info=PurchasedOrder::where('pending_qty','>',0)->get();
    return view('backend.poregister.indexpending',compact('info'));
}

}
