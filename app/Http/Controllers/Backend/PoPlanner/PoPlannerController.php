<?php

namespace App\Http\Controllers\Backend\PoPlanner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PurchaseOrder\PoFormatting;
use App\Models\PoPlanner\PoRegister;
use App\Models\QuotationManagement\Quotation;
use App\Models\QueryManagement\QueryItem;
use App\Models\PurchaseOrder\PoItem;
use App\Models\Auth\User;
use App\Repositories\Backend\PoPlanner\PoPlannerRepository;
use App\Models\LogManagement\Log;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand; 
use App\Models\ProductManagement\Color;
use Illuminate\Support\Facades\DB;


class PoPlannerController extends Controller
{
    protected $poplannerRepository;   
    public function __construct(PoPlannerRepository $poplannerRepository)
    {
        $this->poplannerRepository = $poplannerRepository;
        $this->middleware(['permission:PoPlanner']);   
    }


    public function index()
    {
        $poformattings = $this->poplannerRepository->getActivePaginated(25, 'updated_at', 'desc');    
        return view('backend.poplanner.index',compact('poformattings'));
    }
    public function search(Request $request)
    {
        $term=$request->term;
        $clientpo=$request->clientpo;
        $poformattings = $this->poplannerRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$clientpo); 
        return view('backend.poplanner.index',compact('poformattings'));
    }
    public function edit($id,PoFormatting $po,Quotation $quotation)
    {
        $users=DB::table('users')
        ->select('users.id','users.first_name')
        ->join('model_has_roles','model_has_roles.model_id','=','users.id')
        ->where(['model_has_roles.role_id'=>43])
        ->get();
        $podata=PoFormatting::with('poItems')->where('id','=',$id)->get();
        // $info= $podata[0]->poItems[0]->poRegisters[0]->$purchasedOrders[0]->ml_recieved;
        return view('backend.poplanner.edit',compact('podata','users'));
    }
    
    public function create(Quotation $quotation)
    {
        $quotationno=$quotation->where('is_confirmed','=',1)->get();
        return view('backend.poformat.create',compact('quotationno'));
    }

    public function store(Request $request)
    {

        $pro = new Log;
        $pro->name=$request->data[0]['Pid'];
        $pro->user_name=$request->data[0]['user_name'];
         $pro->user_id=$request->data[0]['user_id'];
         $pro->module=$request->data[0]['module'];
         $pro->action=$request->data[0]['action'];
         $pro->activity=$request->data[0]['activity'];
         $pro->save();

        $pid = (int)$request->data[0]['Pid'];
        for($i=0;$i<sizeof($request->data);$i++){
        DB::statement("UPDATE po_formattings SET status ='1' where id = $pid");
        $qitems = PoRegister::firstOrNew(['poitemid'=>$request->data[$i]['PoId']]);
        $qitems->purchasedOrders()->update(['pending_qty'=>$request->data[$i]['Panding']]);
        $qitems->pt_user_id=$request->data[$i]['PurchaseTeam'];     
        $qitems->poitemid=$request->data[$i]['PoId'];
        $qitems->qty=$request->data[$i]['Qty'];
        $qitems->purchase_date=$request->data[$i]['PurchaseDate'];
        $qitems->day_shift=$request->data[$i]['DayShift'];
        $qitems->criticality=$request->data[$i]['Criticality'];
        $qitems->priority=$request->data[$i]['Priority'];
        $qitems->transportation_mode=$request->data[$i]['TransportationMode'];
        $qitems->payment_amount=$request->data[$i]['Gtotal'];
        $qitems->payment_mode=$request->data[$i]['PaymentMode'];
        $qitems->bank=$request->data[$i]['Bank'];
        $qitems->save();
        $qitems->poItem()->update(['complete'=>1]);

   }
   return 'true';
    }

    public function queryitems(QueryItem $qitem,$id){
       $qitems=DB::SELECT("SELECT * FROM query_items INNER JOIN price_mappings ON query_items.id=price_mappings.item_id WHERE query_id=$id AND is_selected=1");
       return $qitems;
    }

    public function poitems(Request $request){

        $var='abad';
        for($i=0;$i<sizeof($request->dataArray);$i++){
        $poformatting = new PoItem();
        $poformatting->qitemid=$request->dataArray[$i]['QueryItemId'];
        $poformatting->po_id =$request->poid;
        $poformatting->qty=$request->dataArray[$i]['Qty'];
        $poformatting->grand_total =$request->dataArray[$i]['Gtotal'];
        if($poformatting->save()) {
            return 'true';
        }
    }
    }

    public function infoitems($id){
        $items=itemlist::with('brand','color','uom','product')->where('id','=',$id)->get()->toJson();  
        return $items;
    
    }  

}
