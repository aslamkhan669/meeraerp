<?php

namespace App\Http\Controllers\Backend\PoPlanner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PoPlanner\PoRegister;
use App\Repositories\Backend\PoPlanner\PurchaseChallanRepository;
use App\Models\PoPlanner\PurchasedOrder;
use App\Models\Account\PoChallan;
use App\Models\Account\Challan;
use App\Models\Account\ChallanItem;
use App\Models\Setting\Freight;
use App\Models\Setting\CompanyDetail;
use App\Models\Setting\TransportInsurance;
use Illuminate\Support\Facades\DB;
use App\Models\PoPlanner\PurchaseExpense;
use App\Models\PurchaseOrder\PoFormatting;
use App\Models\Master\Client;
use App\Models\Master\Supplier;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand; 
use App\Models\ProductManagement\Color;
use PDF;
use App\Models\LogManagement\Log;
use App\Models\QueryManagement\Query;
use App\Models\QuotationManagement\Quotation;
use App\Models\QueryManagement\QueryItem;


class PurchaseChallanController extends Controller
{

   //PoAccount controller 
    protected $purchasechallanRepository;
    
    public function __construct(PurchaseChallanRepository $purchasechallanRepository)
    {
        $this->purchasechallanRepository = $purchasechallanRepository;
        $this->middleware(['permission:PurchaseChallan']); 

    }


    public function index()
    {   
        $purchasechallans = $this->purchasechallanRepository->getActivePaginated(25000, 'updated_at', 'desc');    
 
        return view('backend.purchasechallan.index',compact('purchasechallans'));
    }
    public function search(Request $request)
    {
        $term=$request->term;
        $clientpono=$request->clientpono;
        $purchasechallans = $this->purchasechallanRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$clientpono); 
        return view('backend.purchasechallan.index',compact('purchasechallans'));
    }
    public function create(PoFormatting $poform)
    {
        $poinfo=$poform->get();

        return view('backend.purchasechallan.create',compact('poinfo'));
    }
    public function edit($id,Supplier $supplier,CompanyDetail $companyname,PurchasedOrder $purchasedorder,TransportInsurance $insurance,Freight $freight,PoRegister $poregister)
    {
        $data=DB::SELECT("SELECT * FROM (((po_formattings pf INNER JOIN po_items pi ON pf.id=pi.po_id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN po_challans ps ON ps.qitemid=qi.id INNER JOIN price_mappings pm ON qi.id=pm.item_id) WHERE pf.id=$id");
        // $centerinfo=DB::SELECT("SELECT * FROM ((((((purchased_orders po INNER JOIN po_registers pr ON po.poregisterid=pr.id)INNER JOIN po_items pi ON pr.poitemid=pi.id)INNER JOIN po_formattings pf ON pi.po_id=pf.id)INNER JOIN quotations q ON pf.quotation_id=q.id)INNER JOIN queries qu ON q.query_id=qu.id)INNER JOIN company_details c ON qu.center_id=c.id) WHERE pr.pt_user_id=$id");

    //   $info=DB::SELECT("SELECT * FROM (((((po_registers pr INNER JOIN purchased_orders pu ON pr.id=pu.poregisterid)INNER JOIN po_items pi ON pr.poitemid=pi.id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN price_mappings pm ON qi.id=pm.item_id)INNER JOIN suppliers s ON pm.supplier_id=s.id) WHERE pr.pt_user_id=$id");

     $termsdata=DB::SELECT("SELECT * FROM (((( po_items pi INNER JOIN po_formattings pf ON pi.po_id=pf.id)INNER JOIN quotations q ON pf.quotation_id=q.id)INNER JOIN queries qu ON q.query_id=qu.id)INNER JOIN termsconditions t ON qu.id=t.query_id) WHERE pf.id=$id");
      $fid= $termsdata[0]->freight_id;
      $freightinfo=$freight->where('id','=',$fid)->get();
      $tid= $termsdata[0]->freight_id;
      $insuranceinfo=$insurance->where('id','=',$tid)->get();
      $cid= $termsdata[0]->center_id;
      $centerinfo=$companyname->where('id','=',$cid)->get();
      $sid= $data[0]->supplier_id;
      $info=$supplier->where('id','=',$sid)->get();
        return view('backend.purchasechallan.edit', compact('data','id','info','centerinfo','termsdata','freightinfo','insuranceinfo'));
    }
    Public function storebychallan(Request $request) 
    {

        $pro = new Log;
        $pro->name=$request->data1['supplier_challan'];
        $pro->user_name=$request->data1['username'];
         $pro->user_id=$request->data1['userid'];
         $pro->module=$request->data1['mode'];
         $pro->action=$request->data1['action'];
         $pro->activity=$request->data1['activity'];
         $pro->save();

        $ch = Challan::firstOrNew(['supplier_challan'=>$request->data1['supplier_challan']]);
        $ch->center_id=$request->data1['center_id'];
        $ch->supplier_id=$request->data1['supplier_id'];
        $ch->supplier_challan=$request->data1['supplier_challan'];
        $ch->pt_user_id=$request->data1['pt_user_id'];
        $ch->po_id=$request->data1['po_id'];
        $ch->save();
        $cid= $ch->id;
        for($i=0;$i<sizeof($request->data);$i++){
            $qitems =new ChallanItem;
               $qitems->challan_id=$cid;     
       $qitems->item_id=$request->data[$i]['item_id'];
       $qitems->qty=$request->data[$i]['quantity'];
       $qitems->total=$request->data[$i]['total'];
       $qitems->gst=$request->data[$i]['gst'];
       $qitems->save();
        }
       $data='true';
       return $data;
    
   }
   public function infoitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
    $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
    return $items;

}

public function streamPDF($id,CompanyDetail $company,Challan $challan,ChallanItem $challanitem,Supplier $supplier,Client $client,TransportInsurance $insurance,Freight $freight)
{
    $freights=$freight->get();
    $insurances=$insurance->get();

    $data=$challan->where('po_id','=',$id)->get();

    $info=$supplier->where('id','=',$data[0]->supplier_id)->get();
   $centerinfo=$company->where('id','=',$data[0]->center_id)->get();
  $challanitem=$challanitem->where('challan_id','=',$data[0]->id)->get();
$termsdata=DB::SELECT("SELECT * FROM (((( po_items pi INNER JOIN po_formattings pf ON pi.po_id=pf.id)INNER JOIN quotations q ON pf.quotation_id=q.id)INNER JOIN queries qu ON q.query_id=qu.id)INNER JOIN termsconditions t ON qu.id=t.query_id) WHERE pf.id=$id");
$fid= $termsdata[0]->freight_id;
 $freightinfo=$freight->where('id','=',$fid)->get();
  $tid= $termsdata[0]->freight_id;
  $insuranceinfo=$insurance->where('id','=',$tid)->get();
  $pdf = PDF::loadview('backend.purchasechallan.pdf', compact('id','challanitem','freightinfo','insuranceinfo','data','centerinfo','info','termsdata'))->setPaper('A4', 'landscape');
  return $pdf->inline();
 // return view('backend.purchasechallan.pdf', compact('id','challanitem','freightinfo','insuranceinfo','data','centerinfo','info','termsdata'));

   
 
}



public function poitems($id){
    $qitems=DB::SELECT("SELECT * FROM (( po_items pi INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN price_mappings pm ON qi.id=pm.item_id) WHERE pi.po_id=$id");
    return $qitems;
 }
 public function poitem(Request $request){
    for($i=0;$i<sizeof($request->dataArray);$i++){
    $poformatting = PoChallan::firstOrNew(['poitemid'=>$request->dataArray[$i]['PItemId']]);
    $poformatting->poitemid=$request->dataArray[$i]['PItemId'];
    $poformatting->po_id=$request->dataArray[$i]['Poid'];
    $poformatting->qitemid=$request->dataArray[$i]['QueryItemId'];
    $poformatting->qty=$request->dataArray[$i]['Qty'];
    $poformatting->grand_total =$request->dataArray[$i]['Gtotal'];
    if(!$poformatting->save()) {
        return 'false';
    }
}
    return 'true';
}

public function combodata($key,PoFormatting $poform ,Client $client,Quotation $quo,Query $query){

    $data=$poform->where('id','LIKE','%'.$key.'%')->select('id','updated_at')->get()->toJson();
    return $data;
}
public function queryitems($id){
    $qitems=QueryItem::with('brand','color','uom','itemlist')->where('item_id','=',$id)->get()->toJson();  
    return $qitems;
 }
}
