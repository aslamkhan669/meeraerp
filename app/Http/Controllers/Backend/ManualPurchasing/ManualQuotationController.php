<?php

namespace App\Http\Controllers\Backend\ManualPurchasing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QuotationManagement\Quotation;
use App\Models\QuotationManagement\Quotationitem;
use App\Models\ManualPurchasing\ManualPurchase;
use App\Models\ManualPurchasing\ManualItem;
use App\Models\ManualPurchasing\OncallQuotation;
use App\Models\ManualPurchasing\OncallQuotationitem;
use App\Models\ManualPurchasing\OncallPricemap;
use App\Models\QueryManagement\PriceMapping;
use App\Models\ProductManagement\Color;
use App\Models\Master\Supplier;
use App\Models\Auth\User;
use App\Models\Master\PaymentTerm;
use App\Models\Setting\Freight;
use App\Models\Setting\TransportInsurance;
use App\Models\Setting\TermCondition;
use App\Models\Setting\CompanyDetail;
use App\Repositories\Backend\ManualPurchasing\ManualQuotationRepository;
use DB;
use PDF;
use App\Models\LogManagement\Log;



class ManualQuotationController extends Controller
{

    protected $quotationRepository;
    
    public function __construct(ManualQuotationRepository $quotationRepository)
    {
        $this->quotationRepository = $quotationRepository;
        $this->middleware(['permission:OnCallQuotation']); 
    }


 public function index(CompanyDetail $company,User $user)
    {
        // $data=auth()->user();
        $info=$this->quotationRepository->getActivePaginated(250000, 'updated_at', 'desc');

        return view('backend.manualquotation.index',compact('info'));
     // ->withquotation($this->quotationRepository->getQuotationWithQueries());

    }
    public function searchinfo(Request $request)
    {
        $term =$request->term;
        $quata =$request->quata;
        $client=$request->client;
    $info = $this->quotationRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$quata,$client); 
    return view('backend.manualquotation.index',compact('info'));
}
    public function show($id,ManualPurchase $manualpurchase,ManualItem $manualitem,TermCondition $termcondition,PriceMapping $price,OncallQuotation $oncallquotation,TransportInsurance $insurance,Freight $freight)
    {
        $freights=$freight->get();
        $insurances=$insurance->get();
        $qdate=DB::SELECT("Select updated_at from oncall_quotations where manual_id='$id'");
        $querydata=$manualpurchase->where('id','=',$id)->get();
        $termsdata=$termcondition->where('query_id','=',$id)->get();
        $quotationid=$oncallquotation->where('manual_id','=',$id)->get();
        $qid=$quotationid[0]->id;
        $qitems=DB::SELECT("SELECT * FROM manual_items INNER JOIN oncall_pricemaps ON manual_items.id=oncall_pricemaps.item_id WHERE manual_id=$id AND is_selected=1");
        
        return view('backend.manualquotation.create',compact('qdate','querydata','qitems','qid','freights','insurances','termsdata')); 

    }
    

    public function showprofit($id,ManualPurchase $manualpurchase,ManualItem $manualitem,TermCondition $termcondition,PriceMapping $price,OncallQuotation $oncallquotation,TransportInsurance $insurance,Freight $freight)
    {
        $freights=$freight->get();
        $insurances=$insurance->get();
        $qdate=DB::SELECT("Select updated_at from oncall_quotations where manual_id='$id'");
        $querydata=$manualpurchase->where('id','=',$id)->get();
        $termsdata=$termcondition->where('query_id','=',$id)->get();
        $quotationid=$oncallquotation->where('manual_id','=',$id)->get();
        $qid=$quotationid[0]->id;
      //  $qitems=DB::SELECT("SELECT * FROM manual_items INNER JOIN oncall_pricemaps ON manual_items.id=oncall_pricemaps.item_id WHERE manual_id=$id AND is_selected=1");
        $qitems=DB::SELECT("SELECT * FROM manual_items INNER JOIN oncall_quotationitems ON oncall_quotationitems.manualitem_id=manual_items.id WHERE manual_items.manual_id=$id");
        return view('backend.manualquotation.showprofit',compact('qdate','querydata','qitems','qid','freights','insurances','termsdata')); 

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->quotationRepository->create($request->all());
        return redirect()->route('admin.manualquotation.index')->withFlashSuccess(__('alerts.backend.quotation.created'));
    }
    public function edit(Quotation $quotation,Supplier $supplier,PaymentTerm $paymentterm)
    {
        $suppliers=$supplier->get();
        $paymentterms=$paymentterm->get();
        $suppliername=$quotation->supplier()->first();
        $paymenttermname=$quotation->paymentTerm()->first();
        return view('backend.manualquotation.edit', compact('quotation','suppliers','paymentterms','suppliername','paymenttermname'));
    }
    public function update(Quotation $quotation,Request $request)
    {
        $this->quotationRepository->update($quotation, $request->only(
            'delivery_term_id', 
            'supplier_id', 
            'request_no', 
            'customer_request_ref', 
            'validate_date', 
            'remark'
            
        ));

        return redirect()->route('admin.manualquotation.index')->withFlashSuccess(__('alerts.backend.quotation.updated'));
    }

    public function quotationdata(Request $request){

        $log = new Log;
        $log->user_name=$request->data[0]['userName'];
        $log->user_id=$request->data[0]['userId'];
        $log->module=$request->data[0]['module'];
        $log->action=$request->data[0]['action'];
        $log->activity=$request->data[0]['activity'];
        $log->name=$request->data[0]['Qid'];
        $log->save();
        $qid = (int)$request->data[0]['Qid'];
        for($i=0;$i<sizeof($request->data);$i++){
        DB::statement("UPDATE oncall_quotations SET is_confirmed = '1',status ='1' where id = $qid");
        $qitems = new OncallQuotationitem;
        $qitems->pup=$request->data[$i]['PurchasePrice'];
        $qitems->gstamount=$request->data[$i]['GST'];
        $qitems->manualitem_id=$request->data[$i]['QueryItemId'];
        $qitems->mtotal=$request->data[$i]['Total'];
        $qitems->profit=$request->data[$i]['Profit'];
        $qitems->profit_amount=$request->data[$i]['ProfitAmount'];
        $qitems->oncallquotation_id=$request->data[$i]['Qid'];
        $qitems->mqty=$request->data[$i]['Qty'];
        $qitems->mgrand_total=$request->data[$i]['Gtotal'];
        $qitems->save();
    }
        $data='true';
        return $data;

    }
    public function destroy(Quotation $quotation)
    {
        $this->quotationRepository->deleteById($quotation->id);
        return redirect()->route('admin.quotation.index')->withFlashSuccess(__('alerts.backend.quotation.deleted'));
    }



     public function streamPDF($id,ManualPurchase $manualpurchase,ManualItem $manualitem,TermCondition $termcondition,PriceMapping $price,OncallQuotation $oncallquotation,TransportInsurance $insurance,Freight $freight,Color $color)
     {
        $colors=$color->get();
        $freights=$freight->get();
        $insurances=$insurance->get();
        $qdate=DB::SELECT("Select updated_at from oncall_quotations where manual_id='$id'");
  
     $querydata=$manualpurchase->where('id','=',$id)->get();
        $termsdata=$termcondition->where('query_id','=',$id)->get();
        $quotationid=$oncallquotation->where('manual_id','=',$id)->get();
        $qid=$quotationid[0]->id;
        $qitems=DB::SELECT("SELECT * FROM (((manual_items INNER JOIN oncall_quotationitems ON oncall_quotationitems.manualitem_id=manual_items.id INNER JOIN oncall_pricemaps ON manual_items.id=oncall_pricemaps.item_id)INNER JOIN colors ON manual_items.color=colors.id)INNER JOIN brands ON manual_items.brand_id=brands.id) WHERE manual_items.manual_id=$id ");
        $pdf = PDF::loadView('backend.manualquotation.pdf', compact('qdate','querydata','qitems','qid','freights','insurances','termsdata','colors'))->setPaper('A4', 'landscape');
        return $pdf->inline();
         
     // return view('backend.manualquotation.pdf',compact('qdate','querydata','qitems','qid','freights','insurances','termsdata'));
     } 
     Public function reject($cid,Quotation $quotation){
        $tdata=DB::statement("UPDATE oncall_quotations SET is_confirmed = '0'where id = $cid");   
        return $tdata;
    }
     Public function hold($cid,Quotation $quotation){
        $data=DB::statement("UPDATE oncall_quotations SET status = '0'where id = $cid");   
        return $data;
    }
    Public function cancel($cid,Quotation $quotation){
        $data=DB::statement("UPDATE oncall_quotations SET status = '2'where id = $cid");   
        return $data;
    }
    public function queryitems($id){
        $items=ManualItem::with('brand','color','itemList','uom')->where('id','=',$id)->get()->toJson();  
        return $items;

    }
    }


