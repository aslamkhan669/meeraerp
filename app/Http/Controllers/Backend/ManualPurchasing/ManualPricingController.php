<?php
namespace App\Http\Controllers\Backend\ManualPurchasing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ManualPurchasing\ManualPurchase;
use App\Models\ManualPurchasing\ManualItem;
use App\Models\ManualPurchasing\OncallPricemap;
use App\Models\ManualPurchasing\OncallQuotation;
use App\Models\ManualPurchasing\OncallQuotationitem;
use App\Models\QueryManagement\PriceMapping;
use App\Models\QuotationManagement\Quotation;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\ItemMapping;
use App\Models\ProductManagement\CategorySupplier;                                                    
use App\Models\Master\Supplier;                    
use App\Models\Setting\Freight;
use App\Models\Setting\TermCondition;
use App\Models\Setting\TransportInsurance; 
use App\Models\ProductManagement\Brand;                               
use App\Repositories\Backend\ManualPurchasing\ManualPurchaseRepository;
use App\Models\Master\Client;
use App\Models\ProductManagement\Color;
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Product; 
use App\Models\LogManagement\Log;

class ManualPricingController extends Controller
{
   

    protected $manualPurchaseRepository;
    
    public function __construct(ManualPurchaseRepository $manualPurchaseRepository)
    {
        $this->manualPurchaseRepository = $manualPurchaseRepository;
        $this->middleware(['permission:OnCallPricing']); 

    }


    public function index()
    {
        $manualpurchases = $this->manualPurchaseRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        return view('backend.manualpricing.index',compact('manualpurchases'));
    }

    
    public function show($id,ManualItem $manualitems,TransportInsurance $insurance,Freight $freight,TermCondition $termcondition)
    {
        $freights=$freight->get();
        $insurances=$insurance->get();
        $itemlist=$manualitems->where('manual_id','=',$id)->get();
        $term=$termcondition->where('query_id','=',$id)->get();                                                                       
        return view('backend.manualpricing.create',compact('itemlist','id','freights','insurances','term'));   
    }

    

    public function suppliers($id,CategorySupplier $isupplier){
        $data=CategorySupplier::with('supplier')->where('cat_id','=',$id)->get()->tojson();
    return $data;
    }
    public function changecolor($id){
        $data=ItemMapping::where('item_id','=',$id)->get()->tojson();
    return $data;
    }

    public function iteminfo(Request $request){
        $data=ItemMapping::with('supplier')->where('supplier_id','=',$request->suppid)->where('item_id','=',$request->itemid)->get()->tojson();
    return $data;
    }

    public function pricemapping(Request $request, OncallPricemap $price){
        
        $match=['item_id'=>$request->item_id, 'supplier_id'=>$request->supplier_id,'total'=>$request->total];
        $quote = OncallPricemap::where($match)->first();
        if ($quote === null) {
        $itemid=$request->item_id;
        $price->item_id=$request->item_id;
        $price->supplier_id=$request->supplier_id;
        $price->listprice=$request->listprice;
        $price->insurance_shipping=$request->insurance;
        $price->custom_clearance=$request->clearance;
        $price->discount=$request->discount;
        $price->discounted_price=$request->discounted_price;
        $price->gst=$request->gst;
        $price->total=$request->total;
        $price->quantity=$request->Quantity;
        $price->grand_total=$request->grandamount;
        $price->delivery_term=$request->DeliveryTerm;
        $price->warranty=$request->Warranty;
        $price->standard_package=$request->Std_package;
        $price->is_selected= $request->is_selected;
        $price->save();
        $data='true';
        return $data;
    }else
        {
            $res='false';
        return $res;
        }
    }

    public function mappingupdate(Request $request)
    {
        if($request->id){
       $price=OncallPricemap::find($request->id);
       $price->item_id=$request->item_id;
       $price->listprice=$request->listprice;
       $price->discount=$request->discount;
       $price->discounted_price=$request->discounted_price;
       $price->gst=$request->gst;
       $price->quantity=$request->Quantity;
       $price->total=$request->total;
       $price->is_selected= $request->is_selected;
       $price->save();
       $data='true';
       return $data;
   }else
       {
           $res='false';
       return $res;
       }

    }

    public function mappings($id,PriceMapping $pricemap){
        $data=OncallPricemap::with('queryItem.itemList','supplier')->where('item_id','=',$id)->get()->tojson();        
        return $data;
    }

    public function queryId(Request $request,OncallQuotation $quotation){
        $quote = OncallQuotation::where('manual_id', '=', $request->queryid)->first();
        if ($quote === null) {
            $quotation->manual_id=$request->queryid;
            $quotation->is_confirmed=1;
            $quotation->status=1;
            $quotation->save();

            $log = new Log;
            $log->user_name=$request->userName;
            $log->user_id=$request->userId;
            $log->module=$request->module;
            $log->action=$request->action;
            $log->activity=$request->activity;
            $log->name=$request->queryid;
            $log->save();
            
            return 'true';
        }else{
            return 'false';
        }
      
    }
   
    public function queryitems($id,ManualItem $qitems,Brand $brand){
        $items=ManualItem::with('brand','color','itemList','uom')->where('id','=',$id)->get()->toJson();  
        return $items;

    }
    public function historyinfo($id){
        $data=OncallPricemap::with('supplier')->where('supplier_id','=',$id)->get()->tojson();
    return $data;
    }
    public function infoitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
        $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
        return $items;

    }



    public function termsdata(Request $request){
        $terms = TermCondition::firstOrNew(['query_id'=>$request->Query_id]);
        $terms->query_id=$request->Query_id;    
        $terms->freight_id=$request->Freight_id;
        $terms->insurance_id=$request->Insurance_id;
        $terms->payment_days=$request->Payment_days;
        $terms->validity_days=$request->Validity_days;
        $terms->comment_one=$request->Comment_one;
        $terms->comment_two=$request->Comment_two;
        $terms->comment_three=$request->Comment_three;
        $terms->comment_four=$request->comment_four;
        $terms->save();
       
        $data='true';
        return $data;
     }
}
