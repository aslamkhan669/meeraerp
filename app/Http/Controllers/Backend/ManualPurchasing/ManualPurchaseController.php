<?php
namespace App\Http\Controllers\Backend\ManualPurchasing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ManualPurchasing\ManualPurchase;
use App\Models\ManualPurchasing\ManualItem;
use App\Models\ProductManagement\Product;
use App\Models\ProductManagement\Category;
use App\Models\ProductManagement\Color;
use App\Models\ProductManagement\Uom;
use App\Models\ProductManagement\Brand;
use App\Repositories\Backend\ManualPurchasing\ManualPurchaseRepository;
use App\Models\Master\Client;
use App\Models\Setting\CompanyDetail;
use App\Models\QueryManagement\QueryItem;
use App\Models\ProductManagement\ItemList;
use Illuminate\Support\Facades\DB;


class ManualPurchaseController extends Controller
{
   

    protected $manualPurchaseRepository;
    
    public function __construct(ManualPurchaseRepository $manualPurchaseRepository)
    {
        $this->manualPurchaseRepository = $manualPurchaseRepository;
        $this->middleware(['permission:OnCallPurchase']); 

    }


    public function index()
    {
       
        $manualpurchases = $this->manualPurchaseRepository->getActivePaginated(250000, 'updated_at', 'desc');  
        return view('backend.manualpurchase.index',compact('manualpurchases'));
    }

    
    public function create(Client $client,CompanyDetail $companydetail,ItemList $itemlist,Product $product,Color $color,Brand $brand,Uom $uom)
    {
   
  
        $itemlists=$itemlist->get();
        $companydetails=$companydetail->get();
        $clients=$client->get();
        $products=$product->get();
        $colors=$color->get();
        $brands=$brand->get();
        $uoms=$uom->get();

        return view('backend.manualpurchase.create',compact('item_code','clients','companydetails','itemlists','products','colors','brands','uoms'));
   
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $queryItems = [];

        for($i=0;$i<sizeof($data['item_id']);$i++){

        $arr =array(
           
            'item_id'=> $data['item_id'][$i],
            'description'=> $data['description'][$i],
            'specification'=> $data['specification'][$i],
            'color'=>$data['color'][$i],
            'model_no'=>$data['model_no'][$i],
            'brand_id'=>$data['brand_id'][$i],
            'hsn_code'=>$data['hsn_code'][$i],
            'unit'=>$data['unit'][$i],
            'query_qty'=>$data['qty'][$i],
            'remarks'=>$data['remarks'][$i],
            'image'=>$data['image'][$i],

            );
    
            array_push($queryItems,$arr);
        }
        $x = $queryItems;
        $query = [
            
            'client_id'=>$data['client_id'], 
            'center_id'=>$data['center_id'], 
            'user_name'=>$data['user_name'],
             'user_id'=>$data['user_id'], 
             'module'=>$data['module'], 
             'action'=>$data['action'], 
             'activity'=>$data['activity'], 
 
            'meera_center_code'=>$data['meera_center_code'] ,
            'query_center'=>$data['query_center'] ,
            'po_no'=>$data['po_no'] ,
            'sales_center'=>$data['sales_center'], 
            'client_detail'=>$data['client_detail'],
            'query_person'=>$data['query_person'],
            'contact_no'=>$data['contact_no'],
     
        ];
        $record['query'] = $query;
        $record['queryItems'] = $queryItems;
        $this->manualPurchaseRepository->create($record);

        return redirect()->route('admin.manualpurchase.index')->withFlashSuccess(__('alerts.backend.manualpurchase.created'));
    }
    public function edit(ManualPurchase $manualpurchase,Client $client,ItemList $itemlist,CompanyDetail $companydetail,Category $categ, Product $product,Color $color,Brand $brand,Uom $uom)
    {      
        $pid=$manualpurchase->id;

         $companydetails=$companydetail->get();
         $clients=$client->get();
         $products=$product->get();
         $colors=$color->get();
         $brands=$brand->get();
         $uoms=$uom->get();
         $manualitems=$manualpurchase->manualItems()->get();
         $qid=$manualitems->first();
        $sid=$qid->item_id;
        $clientinfo=$manualpurchase->client()->first();
        $centerinfo=$manualpurchase->companyDetail()->first();
         
        $info = DB::table('products as p')
        ->join('categories as c', 'c.product_id','=','p.id')
        ->join('categories as s', 's.parent_id','=','c.product_id')
        ->join('item_lists as i','i.cat_id','=','s.parent_id')
        ->join('query_items as q','q.item_id','=','i.cat_id')
        ->select('p.product_name','i.description')
        ->where('cat_id','=',$sid)->get();
        $salesinfo=DB::SELECT("SELECT *,qt.updated_at FROM (quotations qt INNER JOIN queries qu ON qt.query_id=qu.id) WHERE qu.id=$pid");
       if(sizeof($salesinfo)>0){
           $salescenter=$salesinfo[0]->sales_center;
           $salesupdate=$salesinfo[0]->updated_at;

       }else{
           $salescenter = "";
           $salesupdate = "";
       }
         return view('backend.manualpurchase.edit', compact('salesupdate','manualitems','manualpurchase','salescenter','code','scode','manual','info','productinfo','queryitems','clients','clientinfo','centerinfo','companydetails','itemlists','products','colors','uoms','brands'));
    }

    public function update(ManualPurchase $manualpurchase,Request $request)
    {
        $data = $request->all();

        for($i=0;$i<sizeof($data['item_id']);$i++){
            if(isset($data['id'][$i])){
                $check=[
                    'id'=>$data['id'][$i]
                ];
            }else{
                $check = [ 'id'=>0];
            }
        $queryItems = ManualItem::updateOrcreate($check,[
               
            'manual_id'=>$manualpurchase->id, 
            'item_id'=>$data['item_id'][$i], 
            'color'=>$data['color'][$i], 
            'brand_id'=>$data['brand_id'][$i], 
            'unit'=>$data['unit'][$i], 
            'description'=>$data['description'][$i], 
            'specification'=>$data['specification'][$i], 
            'model_no'=>$data['model_no'][$i], 
            'hsn_code'=>$data['hsn_code'][$i],
            'query_qty'=>$data['qty'][$i], 
            'remarks'=>$data['remarks'][$i], 
            'image'=>$data['image'][$i]==null?'default.jpg':$data['image'][$i]

            ]);
        }
        $this->manualPurchaseRepository->update($manualpurchase, $request->only(
            'client_id', 
            'center_id', 
            'query_no',  
            'meera_center_code', 
            'query_center', 
            'po_no', 
            'sales_center', 
            'client_detail'
    ));
        return redirect()->route('admin.manualpurchase.index')->withFlashSuccess(__('alerts.backend.queries.updated'));
    }

    Public function queries($cid,Query $query){
        $subcats_data=$query->where('id','=',$cid)->get()->toJson();
       
        return $subcats_data;
    }
    public function newItem(Request $request){
        $item = new QueryItem;
        $item->name = $request->name;
        $item->save();
        return true;
    }
    

    Public function fileupload(Request $request,QueryItem $queryitem){
        $file = $request->file('image');
        
        $img = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path();
        $file->move($destinationPath, $img);
       
        return $img;
    }
    public function deleteItem($id){
        $status = $this->queryRepository->deleteItem($id);
        $string = ($status?'true':'false');
          return $string;
    }
    public function show()
    {
    }
    
    public function getByClient($clientId,Client $client){
        $data= $client->where('id','=',$clientId)->get()->tojson();
        return $data;
    }

    public function getByCompany($centerId,CompanyDetail $companydetail){
        $data= $companydetail->where('id','=',$centerId)->get()->tojson();
        return $data;
    }

    public function getbyitemdesc($item_id,ItemList $itemlist){
        $data=$itemlist->where('id','=',$item_id)->get()->tojson();  
        return $data;
    }

    Public function parentcategories($cid,Category $category){
        $parentcats_data=$category->where('product_id','=',$cid)->where('parent_id','=',0)->get()->toJson();   
        return $parentcats_data;
    }

    Public function parentbrands($cid,Brand $brand){
        $parentcats_data=$brand->where('product_id','=',$cid)->get()->toJson();     
        return $parentcats_data;
    }
    Public function subcategories($cid,Category $category){
        $subcats_data=$category->where('parent_id','=',$cid)->get()->toJson();
        return $subcats_data;
    }
    public function getByCategory($id,ItemList $itemlist){
        return $itemlist->where('cat_id','=',$id)->get();
    }
    public function storebyitem(Request $request)
    {
        $item = new ItemList;
        $item->hsn_code = $request->hsn_code;
        $item->product_id = $request->product_id;
        $item->brand_id = $request->brand_id;
        $item->cat_id = $request->cat_id;
        $item->description = $request->description;
        $item->specification = $request->specification;
        $item->color = $request->color;
        $item->model_no = $request->model_no;
        $item->unit = $request->unit;
        $item->remarks = $request->remarks;
        $item->image = $request->image;
        $item->is_active = $request->is_active;
        $item->package_qty = $request->package_qty;
        $item->status =0;
        $item->save();
        $data='true';
        return $data;
    }
}
