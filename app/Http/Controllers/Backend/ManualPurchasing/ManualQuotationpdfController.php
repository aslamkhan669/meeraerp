<?php

namespace App\Http\Controllers\Backend\ManualPurchasing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QuotationManagement\Quotation;
use App\Models\QuotationManagement\Quotationitem;
use App\Models\ManualPurchasing\ManualPurchase;
use App\Models\ManualPurchasing\ManualItem;
use App\Models\ManualPurchasing\OncallQuotation;
use App\Models\ManualPurchasing\OncallQuotationitem;
use App\Models\ManualPurchasing\OncallPricemap;
use App\Models\QueryManagement\PriceMapping;
use App\Models\ProductManagement\Color;
use App\Models\Master\Supplier;
use App\Models\Auth\User;
use App\Models\Master\PaymentTerm;
use App\Models\Setting\Freight;
use App\Models\Setting\TransportInsurance;
use App\Models\Setting\TermCondition;
use App\Models\Setting\CompanyDetail;
use App\Repositories\Backend\ManualPurchasing\ManualQuotationRepository;
use DB;
use PDF;
use App\Models\LogManagement\Log;



class ManualQuotationpdfController extends Controller
{

    protected $quotationRepository;
    
    public function __construct(ManualQuotationRepository $quotationRepository)
    {
        $this->quotationRepository = $quotationRepository;
       $this->middleware(['permission:oncall pdf']);
    }


    public function index(CompanyDetail $company,User $user)
    {
        // $data=auth()->user();
        $info=$this->quotationRepository->getActivePaginated(250000, 'updated_at', 'desc');

        return view('backend.manualquotationpdf.index',compact('info'));
     // ->withquotation($this->quotationRepository->getQuotationWithQueries());

    }
    public function searchinfo(Request $request)
    {
        $term =$request->term;
        $quata =$request->quata;
        $client=$request->client;
    $info = $this->quotationRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$quata,$client); 
    return view('backend.manualquotationpdf.index',compact('info'));
}
public function streamPDF($id,ManualPurchase $manualpurchase,ManualItem $manualitem,TermCondition $termcondition,PriceMapping $price,OncallQuotation $oncallquotation,TransportInsurance $insurance,Freight $freight,Color $color)
{
   $colors=$color->get();
   $freights=$freight->get();
   $insurances=$insurance->get();
   $qdate=DB::SELECT("Select updated_at from oncall_quotations where manual_id='$id'");

$querydata=$manualpurchase->where('id','=',$id)->get();
   $termsdata=$termcondition->where('query_id','=',$id)->get();
   $quotationid=$oncallquotation->where('manual_id','=',$id)->get();
   $qid=$quotationid[0]->id;
   $qitems=DB::SELECT("SELECT * FROM (((manual_items INNER JOIN oncall_quotationitems ON oncall_quotationitems.manualitem_id=manual_items.id INNER JOIN oncall_pricemaps ON manual_items.id=oncall_pricemaps.item_id)INNER JOIN colors ON manual_items.color=colors.id)INNER JOIN brands ON manual_items.brand_id=brands.id) WHERE manual_items.manual_id=$id");
   $pdf = PDF::loadView('backend.manualquotationpdf.pdf', compact('qdate','querydata','qitems','qid','freights','insurances','termsdata','colors'))->setPaper('A4', 'landscape');
   return $pdf->inline();
    
// return view('backend.manualquotation.pdf',compact('qdate','querydata','qitems','qid','freights','insurances','termsdata'));
} 
public function queryitems($id){
    $items=ManualItem::with('brand','color','itemList','uom')->where('id','=',$id)->get()->toJson();  
    return $items;

}
    }


