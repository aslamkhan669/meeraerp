<?php

namespace App\Http\Controllers\Backend\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting\Freight;
use App\Repositories\Backend\Setting\FreightRepository;
use App\Models\LogManagement\Log;




class FreightController extends Controller
{
   

    protected $freightRepository;
    
    public function __construct(FreightRepository $freightRepository)
    {
        $this->freightRepository = $freightRepository;
    }


    public function index(Freight $freight)
    {
        $this->middleware(['permission:Freights']);

        $freights = $this->freightRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        return view('backend.freight.index',compact('freights'));
    }

    
    public function create()
    {
        
        return view('backend.freight.create');
   
    }

    
    public function store(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->conditions;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->freightRepository->create($request->all());
        return redirect()->route('admin.freight.index')->withFlashSuccess(__('alerts.backend.freights.created'));
    }

    


   
    public function edit(Freight $freight)
    {
        
        return view('backend.freight.edit', compact('freight'));
    }

   
    public function update(Freight $freight,Request $request)
    {
        $pro = new Log;
        $pro->name=$request->conditions;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->freightRepository->update($freight, $request->only(
                
                'conditions'
            
        ));

        return redirect()->route('admin.freight.index')->withFlashSuccess(__('alerts.backend.freights.updated'));
    }

  
    public function destroy(Freight $freight)
    {
        $this->freightRepository->deleteById($freight->id);
        return redirect()->route('admin.freight.index')->withFlashSuccess(__('alerts.backend.freights.deleted'));
    }
}
