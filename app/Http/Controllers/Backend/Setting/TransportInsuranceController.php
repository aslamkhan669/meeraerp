<?php

namespace App\Http\Controllers\Backend\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting\TransportInsurance;
use App\Repositories\Backend\Setting\TransportInsuranceRepository;
use App\Models\LogManagement\Log;




class TransportInsuranceController extends Controller
{
   

    protected $transportinsuranceRepository;
    
    public function __construct(TransportInsuranceRepository $transportinsuranceRepository)
    {
        $this->middleware(['permission:Transport Insurance']);

        $this->transportinsuranceRepository = $transportinsuranceRepository;
    }


    public function index(TransportInsurance $transportinsurance)
    {
       
        $transportinsurances = $this->transportinsuranceRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        return view('backend.transportinsurance.index',compact('transportinsurances'));
    }

    
    public function create()
    {
        
        return view('backend.transportinsurance.create');
   
    }

    
    public function store(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->conditions;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->transportinsuranceRepository->create($request->all());
        return redirect()->route('admin.transportinsurance.index')->withFlashSuccess(__('alerts.backend.transportinsurances.created'));
    }

    


   
    public function edit(TransportInsurance $transportinsurance)
    {
        
        return view('backend.transportinsurance.edit', compact('transportinsurance'));
    }

   
    public function update(TransportInsurance $transportinsurance,Request $request)
    {
        $pro = new Log;
        $pro->name=$request->conditions;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->transportinsuranceRepository->update($transportinsurance, $request->only(
                
            'conditions'

            
        ));

        return redirect()->route('admin.transportinsurance.index')->withFlashSuccess(__('alerts.backend.transportinsurances.updated'));
    }

  
    public function destroy(TransportInsurance $transportinsurance)
    {
        $this->transportinsuranceRepository->deleteById($transportinsurance->id);
        return redirect()->route('admin.transportinsurance.index')->withFlashSuccess(__('alerts.backend.transportinsurances.deleted'));
    }
}
