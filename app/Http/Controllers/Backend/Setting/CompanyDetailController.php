<?php

namespace App\Http\Controllers\Backend\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting\CompanyDetail;
use App\Repositories\Backend\Setting\CompanyDetailRepository;
use Illuminate\Support\Facades\DB;
use App\Models\LogManagement\Log;


class CompanyDetailController extends Controller
{
   

    protected $companydetailRepository;
    
    public function __construct(CompanyDetailRepository $companydetailRepository)
    {
        $this->companydetailRepository = $companydetailRepository;
       $this->middleware(['permission:Company Details']);
    }


    public function index(CompanyDetail $companydetail)
    {
       
        $companydetails = $this->companydetailRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        return view('backend.company.index',compact('companydetails'));
    }

    
    public function create()
    {
        $b_code = DB::table('products')->orderBy('id', 'desc')->select('id')->first();
        if(empty($b_code)){
            $code=1;
            }else{
            $code=$b_code->id+1;
        }
        return view('backend.company.create',compact('code'));
   
    }

    
    public function store(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->company_name;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->companydetailRepository->create($request->all());
        return redirect()->route('admin.companydetail.index')->withFlashSuccess(__('alerts.backend.company.created'));
    }

    


   
    public function edit(CompanyDetail $companydetail)
    {
        $pid=$companydetail->id;
    
        $brandletters='C0'.$pid;
        $str=strtoupper($brandletters);
        $sa=substr($str,-2);
        $code='C'.$sa;  
        return view('backend.company.edit', compact('companydetail','code'));
    }

   
    public function update(CompanyDetail  $companydetail,Request $request)
    {
        $pro = new Log;
        $pro->name=$request->company_name;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->companydetailRepository->update($companydetail, $request->only(
                
            'company_name', 
            'company_code', 
            'contact_person', 
            'phone_no', 
            'email', 
            'pan_no', 
            'mobile', 
            'delivery_address', 
            'address', 
            'aadhar_no', 
            'gst_no', 
            'state_code', 
            'reg_type'
            
        ));

        return redirect()->route('admin.companydetail.index')->withFlashSuccess(__('alerts.backend.company.updated'));
    }

  
    public function destroy(CompanyDetail $companydetail)
    {
        $this->companydetailRepository->deleteById($companydetail->id);
        return redirect()->route('admin.companydetail.index')->withFlashSuccess(__('alerts.backend.company.deleted'));
    }
    
}
