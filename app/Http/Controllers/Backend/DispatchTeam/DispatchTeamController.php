<?php

namespace App\Http\Controllers\Backend\DispatchTeam;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PoFormatting\PoFormatting;
use App\Models\PoPlanner\PoRegister;
use App\Models\QuotationManagement\Quotation;
use App\Models\QueryManagement\QueryItem;
use App\Models\PurchaseOrder\PoItem;
use App\Models\PoPlanner\PurchasedOrder;
use App\Repositories\Backend\DispatchTeam\DispatchTeamRepository;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Models\DispatchRegister\DispatchRegister;
use App\Models\DispatchRegister\DispatchedOrder;
use App\Models\DispatchRegister\DispatchInfo;

class DispatchTeamController extends Controller
{
    protected $dispatchteamRepository;    
    public function __construct(DispatchTeamRepository $dispatchteamRepository)
    {
        $this->dispatchteamRepository = $dispatchteamRepository;
        $this->middleware(['permission:Dispatcher']);

    }

    public function index()
    {
      $data = $this->dispatchteamRepository->getActivePaginated(25, 'id', 'desc');    
      //$data =DB::Select(DB::raw("SELECT cs.contact_number1,cs.client_name,cs.street,cs.sector,cs.city,cs.state,cs.zone,pf.c_p_owner_no,pf.c_purchaser_name,dr.dt_user_id,d.updated_at,qs.client_id,cs.sales_center,cs.created_at,d.bill_challan_no,d.bill_challan,d.poitem_reciever,d.pending_qty FROM dispatch_registers dr INNER JOIN dispatched_orders d on dr.id=d.disregisterid INNER JOIN po_items pi on dr.poitemid=pi.qty INNER JOIN po_formattings pf on pi.po_id=pf.id INNER JOIN quotations qu on pf.quotation_id=qu.id INNER JOIN queries qs on qu.query_id=qs.id INNER JOIN clients cs on qs.client_id=cs.id GROUP BY d.bill_challan_no")); 
        return view('backend.dispatchteam.index',compact('data'));
    }

    public function edit($id,$date,DispatchRegister $dispatchRegister)
    {       
         $data=$dispatchRegister->where([['dt_user_id','=',$id],['purchase_date','=',$date]])->get();
        $totalamount = DB::SELECT(" SELECT sum(payment_amount) total FROM `dispatch_registers` WHERE dt_user_id=$id AND purchase_date='$date'");

        //$data=DispatchedOrder::where('bill_challan_no','=',$id)->get();
        return view('backend.dispatchteam.edit',compact('id','data','totalamount'));
    }
    
    public function create(Quotation $quotation)
    {
        $quotationno=$quotation->where('is_confirmed','=',1)->get();
        return view('backend.poformat.create',compact('quotationno'));
    }

 

    public function store(Request $request)
    {
       for($i=0;$i<sizeof($request->data);$i++){
            $qitems = PoRegister::firstOrNew(['poitemid'=>$request->data[$i]['PoId']]);
       $qitems->pt_user_id=$request->data[$i]['PurchaseTeam'];     
       $qitems->poitemid=$request->data[$i]['PoId'];
       $qitems->qty=$request->data[$i]['Qty'];
       $qitems->purchase_date=$request->data[$i]['PurchaseDate'];
       $qitems->day_shift=$request->data[$i]['DayShift'];
       $qitems->priority=$request->data[$i]['Priority'];
       $qitems->transportation_mode=$request->data[$i]['TransportationMode'];
       $qitems->payment_amount=$request->data[$i]['Gtotal'];
       $qitems->payment_mode=$request->data[$i]['PaymentMode'];
       $qitems->bank=$request->data[$i]['Bank'];
       $qitems->save();
       $data='true';
       return $data;
   }
    }

    public function dispatchorder(Request $request){
        for($i=0;$i<sizeof($request->data);$i++){
        $term = DispatchedOrder::firstOrNew(['id'=>$request->data[$i]['Id']]);
        $term->ml_purchased=$request->data[$i]['materialRec'];
        $term->pending_qty=$request->data[$i]['rejectQty'];
        $term->remarks=$request->data[$i]['Remark'];
        $check=$term->save();
        }
        if($check){
        return 'true';
        }else
        {
            return 'false';
        }
        
    

     }

    public function purchaseritems(QueryItem $qitem,$id){
        $items=QueryItem::with('brand','color','itemList','uom')->where('item_id','=',$id)->get()->toJson();  
        return $items;
    }

    public function poitems(Request $request){
        for($i=0;$i<sizeof($request->dataArray);$i++){
        $poformatting = new PoItem();
        $poformatting->qitemid=$request->dataArray[$i]['QueryItemId'];
        $poformatting->po_id =$request->poid;
        $poformatting->qty=$request->dataArray[$i]['Qty'];
        $poformatting->grand_total =$request->dataArray[$i]['Gtotal'];
        if($poformatting->save()) {
            return 'true';
        }
    }
    }

    public function queryitems($id){
        $qitems=QueryItem::with('brand','color','uom','itemlist')->where('item_id','=',$id)->get()->toJson();  
        return $qitems;
     }

}
