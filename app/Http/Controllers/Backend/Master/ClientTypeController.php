<?php

namespace App\Http\Controllers\Backend\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\ClientType;
use App\Repositories\Backend\Master\ClientTypeRepository;
use App\Models\LogManagement\Log;

class ClientTypeController extends Controller
{
   

    protected $clienttypeRepository;
    
    public function __construct(ClientTypeRepository $clienttypeRepository)
    {
        $this->clienttypeRepository = $clienttypeRepository;
        $this->middleware(['permission:Client Type']);
    }


    public function index(ClientType $clienttype)
    {
       
        $clienttypes = $this->clienttypeRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        return view('backend.clienttype.index',compact('clienttypes'));
    }

    
    public function create()
    {
        
        return view('backend.clienttype.create');
   
    }

    
    public function store(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->type;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->clienttypeRepository->create($request->all());
        return redirect()->route('admin.clienttype.index')->withFlashSuccess(__('alerts.backend.clienttypes.created'));
    }

    


   
    public function edit(ClientType $clienttype)
    {
        
        return view('backend.clienttype.edit', compact('clienttype'));
    }

   
    public function update(ClientType $clienttype,Request $request)
    {
        $pro = new Log;
        $pro->name=$request->type;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->clienttypeRepository->update($clienttype, $request->only(
                
                'type',
                'is_active'
            
        ));

        return redirect()->route('admin.clienttype.index')->withFlashSuccess(__('alerts.backend.clienttypes.updated'));
    }

  
    public function destroy(ClientType $clienttype)
    {
        $this->clienttypeRepository->deleteById($clienttype->id);

        // event(new CustomerDeleted($customer));

        return redirect()->route('admin.clienttype.index')->withFlashSuccess(__('alerts.backend.clienttypes.deleted'));
    }
}
