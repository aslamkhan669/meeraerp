<?php

namespace App\Http\Controllers\Backend\Master;

use Illuminate\Http\Request;
use App\Models\Master\Client;
use App\Models\Master\ClientType;
use App\Models\Master\PaymentMode;
use App\Models\Master\PaymentTerm;
use App\Http\Controllers\Controller;
 use App\Events\Backend\Client\ClientDeleted;
 use App\Repositories\Backend\Master\ClientRepository;
use App\Http\Requests\Backend\Client\StoreClientRequest;
use App\Http\Requests\Backend\Client\ManageClientRequest;
use App\Http\Requests\Backend\Client\UpdateClientRequest;
use Illuminate\Support\Facades\DB;
use App\Models\LogManagement\Log;


/**
 * Class UserController.
 */
class ClientController extends Controller
{
    /**
     * @var ClientRepository
     */
    protected $clientRepository;

    /**
     * UserController constructor.
     *
     * @param ClientRepository $userRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
       $this->middleware(['permission:Client']);
    }




 
    public function index()
    {
        $clients = $this->clientRepository->getActivePaginated(250000, 'updated_at', 'desc');    
            return view('backend.client.index',compact('clients'));
    }
    public function searchclient(Request $request)
    {
        $term=$request->term;
        $clients = $this->clientRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term); 
        return view('backend.client.index',compact('clients'));
    }
    public function create(ClientType $clienttype,PaymentMode $paymentmode,PaymentTerm $term)
    {    
       $b_code = DB::table('clients')->orderBy('id', 'desc')->select('id')->first();
        if(empty($b_code)){
            $code=1;
            }else{
            $code=$b_code->id+1;
        }
        if(empty($b_code)){
            $salescenter=1;
            }else{
            $salescenter=$b_code->id+1;
        }
        $clienttypes=$clienttype->get();
        $paymode=$paymentmode->get();
       $payterm=$term->get();
        return view('backend.client.create',compact('clienttypes','paymode','payterm','code','salescenter'));
    }

    public function store(StoreClientRequest $request)
    {
        $pro = new Log;
        $pro->name=$request->client_name;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();

        $this->clientRepository->create($request->only(
            'client_name', 
            'client_type', 
            'rating', 
            'email', 
            'email1', 
            'email2', 
            'email3', 
            'email4', 
            'email5', 
            'purchase_category', 
            'sales_center', 
            'contact_person1', 
            'contact_number1', 
            'contact_person2', 
            'contact_number2', 
            'contact_person3', 
            'contact_number3', 
            'contact_person4', 
            'contact_number4', 
            'contact_person5', 
            'contact_number5', 
            'contact_person6', 
            'contact_number6', 
            'contact_person7', 
            'contact_number7', 
            'contact_person8', 
            'contact_number8', 
            'street', 
            'landmark', 
            'sector', 
            'city', 
            'pincode', 
            'state', 
            'zone', 
            'pancard', 
            'adharcard', 
            'gst_no', 
            'payment_term', 
            'payment_mode', 
            'excisable', 
            'demand_frequency'
                  
        )
          );
     
        return redirect()->route('admin.client.index')->withFlashSuccess(__('alerts.backend.clients.created'));
    }
    public function edit(Client $client,ClientType $clienttype,PaymentMode $paymentmode,PaymentTerm $term)
    {
        $pid=$client->id;
    $client = $client;
        $brandletters='M0'.$pid;
        $str=strtoupper($brandletters);
        $sa=substr($str,-2);
        $code='M'.$sa;
        $clienttypes=$clienttype->get();
        $paymode=$paymentmode->get();
       $payterm=$term->get();
       $clientinfo=$client->clienttype()->first();
       $payterminfo=$client->paymentterm()->first();
       $paymodeinfo=$client->paymentmode()->first();
       
        return view('backend.client.edit',compact('code','client','clienttypes','paymode','payterm','clientinfo','payterminfo','paymodeinfo'));

    }
    public function update(Client $client,UpdateClientRequest $request)
    {
        $pro = new Log;
        $pro->name=$request->client_name;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
         
        $this->clientRepository->update($client,$request->only(
            'client_name', 
            'client_type', 
            'rating', 
            'email',
            'email1', 
            'email2', 
            'email3', 
            'email4', 
            'email5',  
            'purchase_category', 
            'sales_center', 
            'contact_person1', 
            'contact_number1', 
            'contact_person2', 
            'contact_number2', 
            'contact_person3', 
            'contact_number3', 
            'contact_person4', 
            'contact_number4', 
            'contact_person5', 
            'contact_number5', 
            'contact_person6', 
            'contact_number6', 
            'contact_person7', 
            'contact_number7', 
            'contact_person8', 
            'contact_number8', 
            'street', 
            'landmark', 
            'sector', 
            'city', 
            'pincode', 
            'state', 
            'zone', 
            'pancard', 
            'adharcard', 
            'gst_no', 
            'payment_term', 
            'payment_mode', 
            'excisable', 
            'demand_frequency'
                  
        ));

        return redirect()->route('admin.client.index')->withFlashSuccess(__('alerts.backend.clients.updated'));
    }
    public function destroy(Client $client)
    {

        $this->clientRepository->deleteById($client->id);

        event(new ClientDeleted($client));

        return redirect()->route('admin.client.index')->withFlashSuccess(__('alerts.backend.clients.deleted'));
    }


    


}