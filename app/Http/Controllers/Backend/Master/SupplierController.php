<?php

namespace App\Http\Controllers\Backend\Master;

use Illuminate\Http\Request;
use App\Models\Master\Supplier;
use App\Models\Master\ClientType;
use App\Models\Master\CategorySupplier;
use App\Http\Controllers\Controller;
use App\Models\ProductManagement\Product;
use App\Models\ProductManagement\Category;
 use App\Events\Backend\Supplier\SupplierDeleted;
 use App\Repositories\Backend\Master\SupplierRepository;
use App\Http\Requests\Backend\Supplier\StoreSupplierRequest;
use App\Http\Requests\Backend\Supplier\ManageSupplierRequest;
use App\Http\Requests\Backend\Supplier\UpdateSupplierRequest;
use Illuminate\Support\Facades\DB;
use App\Models\LogManagement\Log;


/**
 * Class UserController.
 */
class SupplierController extends Controller
{
    /**
     * @var SupplierRepository
     */
    protected $supplierRepository;

    /**
     * UserController constructor.
     *
     * @param SupplierRepository $userRepository
     */
    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
       $this->middleware(['permission:Supplier']);
    }

    public function index()
    {
        $suppliers = $this->supplierRepository->getActivePaginated(25, 'updated_at', 'desc');    
            return view('backend.supplier.index',compact('suppliers'));
    }

    public function searchsupplier(Request $request)
    {
        $term=$request->term;
        $suppliers = $this->supplierRepository->getSearchedPaginated(25, 'updated_at', 'desc',$term); 
        return view('backend.supplier.index',compact('suppliers'));
    }

    public function create(Product $product,ClientType $clienttype)
    {
        $b_code = DB::table('suppliers')->orderBy('id', 'desc')->select('id')->first();
        if(empty($b_code)){
            $code=1;
            }else{
            $code=$b_code->id+1;
        }
        $clienttypes=$clienttype->get();
        $products=$product->get();
        return view('backend.supplier.create',compact('products','clienttypes','code'));
    }


    public function store(StoreSupplierRequest $request,Supplier $supplier)
    {
        $pro = new Log;
        $pro->name=$request->supplier_name;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();

        $file = $request->file('cancelled_cheque');
        if($file==null){
            $img='default.jpg';
            $image='uploads/'.$img; 
        }
        else{
        $img = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('/uploads');
        $file->move($destinationPath, $img);
         $image='uploads/'.$img; 
        }
         $file_supp = $request->file('supplier_catalog');
         if($file_supp==null){
            $img='default.jpg';
            $image_supp='uploads/'.$img; 
        }
        else{
         $img_supp = time().'.'.$file_supp->getClientOriginalExtension();
         $destinationPath = public_path('/uploads');
         $file_supp->move($destinationPath, $img_supp);
          $image_supp='uploads/'.$img_supp; 
        }
          $data = $request->all();
        $supplierItems = [];
        if (array_key_exists("catId",$data))
        {

        for($i=0;$i<sizeof($data['catId']);$i++){

        $arr =array(
            'cat_id' => $data['catId'][$i]
        
            );
    
            array_push($supplierItems,$arr);
        }
        $supplier = [
            
            'supplier_name'=>$data['supplier_name'], 
            'pricing_center'=>$data['pricing_center'], 
            'supplier_type'=>$data['supplier_type'], 
            'supplier_category'=>$data['supplier_category'], 
            'mobile'=>$data['mobile'], 
            'landline'=>$data['landline'], 
            'email'=>$data['email'], 
            'rating'=>$data['rating'], 
            'contact_person1'=>$data['contact_person1'], 
            'contact_number1'=>$data['contact_number1'], 
            'contact_person2'=>$data['contact_person2'], 
            'contact_number2'=>$data['contact_number2'], 
            'contact_person3'=>$data['contact_person3'], 
            'contact_number3'=>$data['contact_number3'], 
            'contact_person4'=>$data['contact_person4'], 
            'contact_number4'=>$data['contact_number4'], 
            'supplier_catalog'=>$image_supp, 
            'street'=>$data['street'], 
            'landmark'=>$data['landmark'], 
            'sector'=>$data['sector'], 
            'city'=>$data['city'], 
            'pincode'=>$data['pincode'], 
            'state'=>$data['state'], 
            'website'=>$data['website'], 
            'zone'=>$data['zone'], 
            'pancard'=>$data['pancard'], 
            'adharcard'=>$data['adharcard'], 
            'gst_no'=>$data['gst_no'], 
            'bank_name'=>$data['bank_name'], 
            'ifsc_code'=>$data['ifsc_code'], 
            'account_no'=>$data['account_no'], 
            'cancelled_cheque'=>$image
      
         ];
         $record['supplier'] = $supplier;
         $record['supplierItems'] = $supplierItems;
         $this->supplierRepository->createnew($record);  
        }else{
            $supp = new Supplier;
            $supp->supplier_name=$request->supplier_name;
            $supp->pricing_center=$request->pricing_center;
             $supp->supplier_type=$request->supplier_type;
             $supp->supplier_category=$request->supplier_category;
             $supp->mobile=$request->mobile;
             $supp->landline=$request->landline;
             $supp->email=$request->email;
             $supp->rating=$request->rating;
             $supp->contact_person1=$request->contact_person1;
             $supp->contact_number1=$request->contact_number1;
             $supp->contact_person2=$request->contact_person2;
             $supp->contact_number2=$request->contact_number2; 
             $supp->contact_person3=$request->contact_person3;
             $supp->contact_number3=$request->contact_number3;
             $supp->contact_person4=$request->contact_person4;
             $supp->contact_number4=$request->contact_number4;
             $supp->supplier_catalog=$image_supp;
             $supp->street=$request->street;
             $supp->landmark=$request->landmark;
             $supp->sector=$request->sector;
             $supp->city=$request->city;
             $supp->pincode=$request->pincode;
             $supp->state=$request->state;
             $supp->website=$request->website;
             $supp->zone=$request->zone;
             $supp->pancard=$request->pancard;
             $supp->adharcard=$request->adharcard;
             $supp->gst_no=$request->gst_no;
             $supp->bank_name=$request->bank_name;
             $supp->ifsc_code=$request->ifsc_code;
             $supp->account_no=$request->account_no;
             $supp->cancelled_cheque=$image;
             $supp->save();
        }
        return redirect()->route('admin.supplier.index')->withFlashSuccess(__('alerts.backend.suppliers.created'));
    }
    public function edit(Supplier $supplier,Product $product,Category $category,CategorySupplier $categorysuppliers,ClientType $clienttype)
    {
        $supplierinfo=$supplier->supplier_type;
        $suppliercat=$supplier->supplier_category;
        $clienttypes=$clienttype->get();
        $products=$product->get();
        $queryitems=$supplier->categorysuppliers()->get(); 

        return view('backend.supplier.edit',compact('products','queryitems','clienttypes','supplierinfo','suppliercat'))
           ->withSupplier($supplier);
    
    }

    public function update(Supplier $supplier,UpdateSupplierRequest $request)
    {
        $pro = new Log;
        $pro->name=$request->supplier_name;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();

        $cancelled_cheque = array();
        $supplier_catalog =array();
        $arraylenght = count($cancelled_cheque);
      
        $file = $request->file('cancelled_cheque');
        $file_supp = $request->file('supplier_catalog');
        $data = $request->all();

        for($i=0;$i<sizeof($data['catId']);$i++){
      
        $supplierItems = CategorySupplier::updateOrcreate([
               
            'supplier_id'=>$supplier->id, 
           'cat_id'=>$data['catId'][$i]


            ]);
        }
       $data= $request->only(
            'supplier_name', 
            'pricing_center', 
            'supplier_type', 
            'supplier_category',
            'mobile', 
            'landline', 
            'email', 
            'rating', 
            'contact_person1', 
            'contact_number1', 
            'contact_person2', 
            'contact_number2', 
            'contact_person3', 
            'contact_number3', 
            'contact_person4', 
            'contact_number4', 
            'street', 
            'landmark', 
            'sector', 
            'city', 
            'pincode', 
            'state', 
            'website', 
            'zone', 
            'pancard', 
            'adharcard', 
            'gst_no', 
            'bank_name', 
            'ifsc_code', 
            'account_no'
                  
       );

       if($file!=null){
        $img = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('/uploads');
        $file->move($destinationPath, $img);
         $cancelled_cheque='uploads/'.$img;
         $data['cancelled_cheque'] = $cancelled_cheque;
       }
       if($file_supp!=null){
         $img_supp = time().'.'.$file_supp->getClientOriginalExtension();
         $destinationPath = public_path('/uploads');
         $file_supp->move($destinationPath, $img_supp);
          $supplier_catalog='uploads/'.$img_supp;
          $data['supplier_catalog'] = $supplier_catalog;
 
       }
        $this->supplierRepository->update($supplier,$data,$cancelled_cheque,$supplier_catalog);
        return redirect()->route('admin.supplier.index')->withFlashSuccess(__('alerts.backend.suppliers.updated'));
    }

    public function destroy(Supplier $supplier)
    {
        $this->supplierRepository->deleteById($supplier->id);
        event(new SupplierDeleted($supplier));
        return redirect()->route('admin.supplier.index')->withFlashSuccess(__('alerts.backend.suppliers.deleted'));
    }


    public function deleteItem($id){
        $status = $this->supplierRepository->deleteItem($id);
        $string = ($status?'true':'false');
          return $string;
    }

    Public function parentcategories($cid,Category $category){
        $parentcats_data=$category->where('product_id','=',$cid)->where('parent_id','=',0)->get()->toJson();   
        return $parentcats_data;
    }
}