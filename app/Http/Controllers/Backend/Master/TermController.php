<?php

namespace App\Http\Controllers\Backend\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\PaymentTerm;
use App\Events\Backend\Term\TermDeleted;
use App\Repositories\Backend\Master\TermRepository;
use App\Http\Requests\Backend\Term\UpdateTermRequest;
use App\Http\Requests\Backend\Term\ManageTermRequest;
use App\Models\LogManagement\Log;



class TermController extends Controller
{
   

    protected $termRepository;
    
    public function __construct(TermRepository $termRepository)
    {
        $this->termRepository = $termRepository;
        $this->middleware(['permission:Payment Terms']);
    }


    public function index(PaymentTerm $paymentterm)
    {
       
        return view('backend.term.index')
      ->withterm($this->termRepository->getActivePaginated(250000, 'updated_at', 'desc'));
    }

    
    public function create()
    {
        
        return view('backend.term.create');
   
    }

    
    public function store(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->term_name;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->termRepository->create($request->all());
        return redirect()->route('admin.paymentterm.index')->withFlashSuccess(__('alerts.backend.paymentterm.created'));
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit(PaymentTerm $paymentterm)
    {
        return view('backend.term.edit', compact('paymentterm'));
    }

   
    public function update(PaymentTerm $paymentterm,Request $request)
    {
        $pro = new Log;
        $pro->name=$request->term_name;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->termRepository->update($paymentterm, $request->only(
                
                'term_name',
                
                'is_active'
            
        ));

        return redirect()->route('admin.paymentterm.index')->withFlashSuccess(__('alerts.backend.paymentterm.updated'));
    }

  
    public function destroy(PaymentTerm $paymentterm)
    {
        $id=$paymentterm->id;
        $this->termRepository->deleteById($paymentterm->id);

        // event(new UomDeleted($paymentterm));

        return redirect()->route('admin.paymentterm.index')->withFlashSuccess(__('alerts.backend.paymentterm.deleted'));
    }
}
