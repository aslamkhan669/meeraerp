<?php

namespace App\Http\Controllers\Backend\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\PaymentMode;
use App\Repositories\Backend\Master\PaymentModeRepository;
use App\Models\LogManagement\Log;

class PaymentModeController extends Controller
{
   

    protected $paymentmodeRepository;
    
    public function __construct(PaymentModeRepository $paymentmodeRepository)
    {
        $this->paymentmodeRepository = $paymentmodeRepository;
        $this->middleware(['permission:Payment Mode']);
    }


    public function index(PaymentMode $paymentmode)
    {
       
        $paymentmodes = $this->paymentmodeRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        return view('backend.paymentmode.index',compact('paymentmodes'));
    }

    
    public function create()
    {
        
        return view('backend.paymentmode.create');
   
    }

    
    public function store(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->mode;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->paymentmodeRepository->create($request->all());
        return redirect()->route('admin.paymentmode.index')->withFlashSuccess(__('alerts.backend.paymentmodes.created'));
    }

    


   
    public function edit(PaymentMode $paymentmode)
    {
        
        return view('backend.paymentmode.edit', compact('paymentmode'));
    }

   
    public function update(PaymentMode $paymentmode,Request $request)
    {
        $pro = new Log;
        $pro->name=$request->mode;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();
        $this->paymentmodeRepository->update($paymentmode, $request->only(
                
                'mode',
                'is_active'
            
        ));

        return redirect()->route('admin.paymentmode.index')->withFlashSuccess(__('alerts.backend.paymentmodes.updated'));
    }

  
    public function destroy(PaymentMode $paymentmode)
    {
        $this->paymentmodeRepository->deleteById($paymentmode->id);
        return redirect()->route('admin.paymentmode.index')->withFlashSuccess(__('alerts.backend.paymentmodes.deleted'));
    }
}
