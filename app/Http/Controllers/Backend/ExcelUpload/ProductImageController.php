<?php

namespace App\Http\Controllers\Backend\ExcelUpload;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use ZipArchive;

class ProductImageController extends Controller
{


    public function  __construct()
    {  
        $this->middleware(['permission:Product Image']);   
    }

    public function create()
    {
        return view('backend.productimage.create');
    }
    public function store(Request $request)
    {
     
        if(isset($_POST["btn_zip"]))  
        {  
             $output = '';  
             if($_FILES['zip_file']['name'] != '')  
             {  
                  $file_name = $_FILES['zip_file']['name'];  
                  $array = explode(".", $file_name);  
                  $name = $array[0];  
                  $ext = $array[1];  
                  if($ext == 'zip')  
                  {  
                       $path = 'uploads/';  
                       $location = $path . $file_name;  
               if(move_uploaded_file($_FILES['zip_file']['tmp_name'], $location))  
               
               {  
                            $zip = new ZipArchive;  
                            if($zip->open($location))  
                            {  
                                 $zip->extractTo($path);  
                                 $zip->close();  
                            }  
                  
                       }  
                  }  
             }  
        }  
    
        

    return redirect()->route('admin.productimage.create')->withFlashSuccess(__('alerts.backend.productinformation.uploded'));
}
}