<?php

namespace App\Http\Controllers\Backend\ExcelUpload;

use App\Http\Controllers\Controller;
use App\Models\ProductManagement\Brand;
use App\Models\ProductManagement\Product;
use App\Models\ProductManagement\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Excel;
use File;
use Illuminate\Support\Facades\Input;
use App\Models\LogManagement\Log;

class ProductExcelController extends Controller
{
    protected $brand;
    protected $product;
    protected $category;

    public function  __construct(Product $product,Category $category,Brand $brand)
    {
        $this->brand = $brand;
        $this->product = $product;
        $this->category = $category;
        $this->middleware(['permission:Product Excel']);   


    }

    public function create()
    {
        return view('backend.productexcel.create');
    }

    public function store(Request $request)
    {
        if($request->hasFile('import_file')){
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $recordMap = [];
                $currentBrand = null;
                $currentProduct = null;
                $currentCategory = null;
                $currentSubCategory = null;
                $brandId = null;
                $productId = null;
                $categoryId=null;
                foreach ($reader->toArray() as $key => $row) {

                    $pro = new Log;
                    $pro->name=$row['product_name'];
                    $pro->user_name="Admin";
                     $pro->user_id=1;
                     $pro->module="ProductManagement";
                     $pro->action="Product";
                     $pro->activity="create";
                     $pro->save();
                    //check if current product exists in db and get its ID
                     if($currentProduct != $row['product_name']){
                        $product = $this->product->where('product_name','=',$row['product_name'])->where('product_owner','=',$row['product_owner'])->first();
                        if($product == null){
                            $data['product_name'] = $row['product_name'];
                            $data['product_owner'] = $row['product_owner'];
                            $data['is_active'] = 1;
                            $product = $this->product->create($data);
                        }
                        $currentproduct = $product->product_name;
                        $productId= $product->id;
                    }
                    $pro = new Log;
                    $pro->name=$row['brand_name'];
                    $pro->user_name="Admin";
                    $pro->user_id=1;
                     $pro->module="ProductManagement";
                     $pro->action="Brand";
                     $pro->activity="create";
                     $pro->save();
                                                   //check if current brands exists in db
                                                   if($currentBrand != $row['brand_name']){
                                                    $brand = $this->brand->where('product_id','=',$productId)->where("brand_name","=",$row['brand_name'])->first();
                                                      if($brand == null){
                                                         $data['product_id'] = $productId;
                                                         $data['brand_name'] = $row['brand_name'];
                                                         $data['is_active'] = 1;
                                                          $brand = $this->brand->create($data);
                                                      }
                                                     $currentBrand = $brand->brand_name;
                                                 } 
                                                 $pro = new Log;
                                                 $pro->name=$row['category_name'];
                                                 $pro->user_name="Admin";
                                                 $pro->user_id=1;
                                                  $pro->module="ProductManagement";
                                                  $pro->action="Category";
                                                  $pro->activity="create";
                                                  $pro->save();
                               //check if current category exists in db
                    if($currentCategory != $row['category_name']){
                       $category = $this->category->where('product_id','=',$productId)->where("category_name","=",$row['category_name'])->first();
                         if($category == null){
                            $data['product_id'] = $productId;
                            $data['category_name'] = $row['category_name'];
                            $data['parent_id'] = 0;
                            $data['is_active'] = 1;
                             $category = $this->category->create($data);
                         }
                        $currentCategory = $category->category_name;
                        $categoryId= $category->id;
                        $productId= $category->product_id;

                    } 
                        

                    $pro = new Log;
                    $pro->name=$row['subcategory_name'];
                    $pro->user_name="Admin";
                    $pro->user_id=1;
                     $pro->module="ProductManagement";
                     $pro->action="SubCategory";
                     $pro->activity="create";
                     $pro->save();
                              //check if current subcategory exists in db
                              if($currentSubCategory != $row['subcategory_name']){
                                $subcategory = $this->category->where('parent_id','=',$categoryId)->where('product_id','=',$productId)->where("category_name","=",$row['subcategory_name'])->first();
                              if($subcategory == null){
                                $data['parent_id'] = $categoryId;
                                $data['product_id'] = $productId;
                                $data['category_name'] = $row['subcategory_name'];
                                $data['is_active'] = 1;
                                $data['image'] = $row['image'];
                                $subcategory = $this->category->create($data);
                              }
                            
                            }
                        
                }
            });
        }

        Session::put('success', 'Your file successfully import in database!!!');

        return redirect()->route('admin.productexcel.create')->withFlashSuccess(__('alerts.backend.productinformation.uploded'));
    }

}
