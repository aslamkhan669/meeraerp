<?php

namespace App\Http\Controllers\Backend\ExcelUpload;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\ProductManagement\CategoryRepository;
use App\Models\ProductManagement\Brand;
use App\Models\ProductManagement\Product;
use App\Models\ProductManagement\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Session;
use Excel;
use File;
use Illuminate\Support\Facades\Input;

class ExcelController extends Controller
{
    protected $categoryRepository;

    /**
     * UserController constructor.
     *
     * @param CategoryRepository $userRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function create(Request $request)
    {
        $data = $this->categoryRepository->getSubActivePaginated(100000, 'id', 'asc');

        $dir = "/uploads";

        // Open a directory, and read its contents
        if (is_dir($dir)){
          if ($dh = opendir($dir)){
            while (($file = readdir($dh)) !== false){
              echo "filename:" . $file . "<br>";
            }
            closedir($dh);
          }
        }
        
        return view('backend.excel.create',compact('data'));
    }

}