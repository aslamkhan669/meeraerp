<?php

namespace App\Http\Controllers\Backend\QuotationManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QuotationManagement\Quotation;
use App\Models\QuotationManagement\Quotationitem;
use App\Models\QueryManagement\Query;
use App\Models\QueryManagement\QueryItem;
use App\Models\QueryManagement\PriceMapping;
use App\Models\ProductManagement\Color;
use App\Models\Master\Supplier;
use App\Models\Auth\User;
use App\Models\Master\PaymentTerm;
use App\Models\Setting\Freight;
use App\Models\Setting\TransportInsurance;
use App\Models\Setting\TermCondition;
use App\Models\Setting\CompanyDetail;
use App\Repositories\Backend\QuotationManagement\QuotationRepository;
use App\Http\Requests\Backend\QuotationManagement\UpdateQuotationRequest;
use App\Http\Requests\Backend\QuotationManagement\ManageQuotationRequest;
use DB;
use PDF;
use App\Models\LogManagement\Log;



class QuotationController extends Controller
{

    protected $quotationRepository;
    
    public function __construct(QuotationRepository $quotationRepository)
    {
        $this->quotationRepository = $quotationRepository;
       $this->middleware(['permission:Quotation Approval']);
    }


 public function index(Quotation $quotation,CompanyDetail $company,User $user)
    {

        $info=$this->quotationRepository->getActivePaginated(25000, 'updated_at', 'desc');

        return view('backend.proposal.index',compact('info'));

    }
    public function searchinfo(Request $request)
    {
        $term =$request->term;
        $quata =$request->quata;
        $client=$request->client;
    $info = $this->quotationRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$quata,$client); 
    return view('backend.proposal.index',compact('info'));
}
    public function show($id,Query $query,QueryItem $qitem,TermCondition $termcondition,PriceMapping $price,Quotation $quotation,TransportInsurance $insurance,Freight $freight)
    {
        $freights=$freight->get();
        $insurances=$insurance->get();
        $qdate=DB::SELECT("Select updated_at from quotations where query_id='$id'");
        $querydata=$query->where('id','=',$id)->get();
        $termsdata=$termcondition->where('query_id','=',$id)->get();
        $quotationid=$quotation->where('query_id','=',$id)->get();
        $qid=$quotationid[0]->id;
        $qitems=DB::SELECT("SELECT * FROM query_items INNER JOIN price_mappings ON query_items.id=price_mappings.item_id WHERE query_id=$id AND is_selected=1");
        
        return view('backend.proposal.create',compact('qdate','querydata','qitems','qid','freights','insurances','termsdata')); 
  
    }
    public function showprofit($id,Query $query,QueryItem $qitem,TermCondition $termcondition,PriceMapping $price,Quotation $quotation,TransportInsurance $insurance,Freight $freight)
    {
        $freights=$freight->get();
        $insurances=$insurance->get();
        $qdate=DB::SELECT("Select updated_at from quotations where query_id='$id'");
        $querydata=$query->where('id','=',$id)->get();
        $termsdata=$termcondition->where('query_id','=',$id)->get();
        $quotationid=$quotation->where('query_id','=',$id)->get();
        $qid=$quotationid[0]->id;
        $qitems=DB::SELECT("SELECT * FROM query_items INNER JOIN quotationitems ON quotationitems.qitem_id=query_items.id WHERE query_items.query_id=$id");
        
        return view('backend.proposal.showprofit',compact('qdate','querydata','qitems','qid','freights','insurances','termsdata')); 
  
    }
    public function create(User $user, Quotation $quotation)
    {
        if(auth()->user()->can('create quotation')){

            return view('backend.proposal.create',compact('suppliers','paymentterms'));
        }else{
            return redirect()->back();
             }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->quotationRepository->create($request->all());
        return redirect()->route('admin.quotation.index')->withFlashSuccess(__('alerts.backend.quotation.created'));
    }
    public function edit(Quotation $quotation,Supplier $supplier,PaymentTerm $paymentterm)
    {
        $suppliers=$supplier->get();
        $paymentterms=$paymentterm->get();
        $suppliername=$quotation->supplier()->first();
        $paymenttermname=$quotation->paymentTerm()->first();
        return view('backend.proposal.edit', compact('quotation','suppliers','paymentterms','suppliername','paymenttermname'));
    }
    public function update(Quotation $quotation,Request $request)
    {
        $this->quotationRepository->update($quotation, $request->only(
            'delivery_term_id', 
            'supplier_id', 
            'request_no', 
            'customer_request_ref', 
            'validate_date', 
            'remark'
            
        ));

        return redirect()->route('admin.quotation.index')->withFlashSuccess(__('alerts.backend.quotation.updated'));
    }

    public function quotationdata(Request $request){

        $log = new Log;
        $log->user_name=$request->data[0]['userName'];
        $log->user_id=$request->data[0]['userId'];
        $log->module=$request->data[0]['module'];
        $log->action=$request->data[0]['action'];
        $log->activity=$request->data[0]['activity'];
        $log->name=$request->data[0]['Qid'];
        $log->save();
        $qid = (int)$request->data[0]['Qid'];
        for($i=0;$i<sizeof($request->data);$i++){
        DB::statement("UPDATE quotations SET is_confirmed = '1',status ='1' where id = $qid");
        $qitems = new Quotationitem;
        $qitems->pup=$request->data[$i]['PurchasePrice'];
        $qitems->gstamount=$request->data[$i]['GST'];
        $qitems->qitem_id=$request->data[$i]['QueryItemId'];
        $qitems->qtotal=$request->data[$i]['Total'];
        $qitems->profit=$request->data[$i]['Profit'];
        $qitems->profit_amount=$request->data[$i]['ProfitAmount'];
        $qitems->quotation_id=$request->data[$i]['Qid'];
        $qitems->qqty=$request->data[$i]['Qty'];
        $qitems->qgrand_total=$request->data[$i]['Gtotal'];
        $qitems->save();

    }
        $data='true';
        return $data;

    }
    public function destroy(Quotation $quotation)
    {
        $this->quotationRepository->deleteById($quotation->id);
        return redirect()->route('admin.quotation.index')->withFlashSuccess(__('alerts.backend.quotation.deleted'));
    }



     public function streamPDF($id,Query $query,QueryItem $qitem,TermCondition $termcondition,PriceMapping $price,Quotation $quotation,TransportInsurance $insurance,Freight $freight,Color $color)
     {
        $colors=$color->get();
        $freights=$freight->get();
        $insurances=$insurance->get();
     //   $data=DB::SELECT('select * from quotations q, queries qu, company_details c where q.query_id = qu.id and qu.center_id = c.id');
     $qdate=DB::SELECT("Select updated_at from quotations where query_id='$id'");
  
     $querydata=$query->where('id','=',$id)->get();
        $termsdata=$termcondition->where('query_id','=',$id)->get();
        $quotationid=$quotation->where('query_id','=',$id)->get();
        $qid=$quotationid[0]->id;
        $qitems=DB::SELECT("SELECT * FROM (((query_items INNER JOIN quotationitems ON quotationitems.qitem_id=query_items.id INNER JOIN price_mappings ON query_items.id=price_mappings.item_id)INNER JOIN colors ON query_items.color=colors.id)INNER JOIN brands ON query_items.brand_id=brands.id) WHERE query_items.query_id=$id ");
        $pdf = PDF::loadView('backend.proposal.pdf', compact('qdate','querydata','qitems','qid','freights','insurances','termsdata','colors'))->setPaper('A4', 'landscape');
        return $pdf->inline();
         
     // return view('backend.proposal.pdf',compact('qdate','querydata','qitems','qid','freights','insurances','termsdata'));
     } 
     Public function reject($cid,Quotation $quotation){
        $tdata=DB::statement("UPDATE quotations SET is_confirmed = '0'where id = $cid");   
        return $tdata;
    }
     Public function hold($cid,Quotation $quotation){
        $data=DB::statement("UPDATE quotations SET status = '0'where id = $cid");   
        return $data;
    }
    Public function cancel($cid,Quotation $quotation){
        $data=DB::statement("UPDATE quotations SET status = '2'where id = $cid");   
        return $data;
    }
    public function queryitems($id){
        $items=QueryItem::with('brand','color','itemList','uom')->where('id','=',$id)->get()->toJson();  
        return $items;

    }
    }


