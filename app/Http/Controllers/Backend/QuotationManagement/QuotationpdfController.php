<?php

namespace App\Http\Controllers\Backend\QuotationManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QuotationManagement\Quotation;
use App\Models\QuotationManagement\Quotationitem;
use App\Models\QueryManagement\Query;
use App\Models\QueryManagement\QueryItem;
use App\Models\QueryManagement\PriceMapping;
use App\Models\ProductManagement\Color;
use App\Models\Master\Supplier;
use App\Models\Auth\User;
use App\Models\Master\PaymentTerm;
use App\Models\Setting\Freight;
use App\Models\Setting\TransportInsurance;
use App\Models\Setting\TermCondition;
use App\Models\Setting\CompanyDetail;
use App\Repositories\Backend\QuotationManagement\QuotationRepository;
use App\Http\Requests\Backend\QuotationManagement\UpdateQuotationRequest;
use App\Http\Requests\Backend\QuotationManagement\ManageQuotationRequest;
use DB;
use PDF;
use App\Models\LogManagement\Log;



class QuotationpdfController extends Controller
{

    protected $quotationRepository;
    
    public function __construct(QuotationRepository $quotationRepository)
    {
        $this->quotationRepository = $quotationRepository;
       $this->middleware(['permission:quotation pdf']);
    }


 public function index(Quotation $quotation,CompanyDetail $company,User $user)
    {

        $info=$this->quotationRepository->getActivePaginated(25000, 'updated_at', 'desc');

        return view('backend.quotationpdf.index',compact('info'));

    }
    public function searchinfo(Request $request)
    {
        $term =$request->term;
        $quata =$request->quata;
        $client=$request->client;
    $info = $this->quotationRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$quata,$client); 
    return view('backend.proposal.index',compact('info'));
}

     public function streamPDF($id,Query $query,QueryItem $qitem,TermCondition $termcondition,PriceMapping $price,Quotation $quotation,TransportInsurance $insurance,Freight $freight,Color $color)
     {
        $colors=$color->get();
        $freights=$freight->get();
        $insurances=$insurance->get();
     $qdate=DB::SELECT("Select updated_at from quotations where query_id='$id'");
  
     $querydata=$query->where('id','=',$id)->get();
        $termsdata=$termcondition->where('query_id','=',$id)->get();
        $quotationid=$quotation->where('query_id','=',$id)->get();
        $qid=$quotationid[0]->id;
        $qitems=DB::SELECT("SELECT * FROM (((query_items INNER JOIN quotationitems ON quotationitems.qitem_id=query_items.id INNER JOIN price_mappings ON query_items.id=price_mappings.item_id)INNER JOIN colors ON query_items.color=colors.id)INNER JOIN brands ON query_items.brand_id=brands.id) WHERE query_items.query_id=$id");
        $pdf = PDF::loadView('backend.quotationpdf.pdf', compact('qdate','querydata','qitems','qid','freights','insurances','termsdata','colors'))->setPaper('A4', 'landscape');
        return $pdf->inline();
         
    //  return view('backend.quotationpdf.pdf',compact('qdate','querydata','qitems','qid','freights','insurances','termsdata'));
     } 
  
    public function queryitems($id){
        $items=QueryItem::with('brand','color','itemList','uom')->where('id','=',$id)->get()->toJson();  
        return $items;

    }
    
    }


