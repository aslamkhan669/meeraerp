<?php
namespace App\Http\Controllers\Backend\QueryManagement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QueryManagement\Query;
use App\Models\ProductManagement\Product;
use App\Models\ProductManagement\Category;
use App\Models\ProductManagement\Color;
use App\Models\ProductManagement\Uom;
use App\Models\ProductManagement\Brand;
use App\Repositories\Backend\QueryManagement\QueryRepository;
use App\Models\Master\Client;
use App\Models\Setting\CompanyDetail;
use App\Models\QueryManagement\QueryItem;
use App\Models\ProductManagement\ItemList;
use Illuminate\Support\Facades\DB;


class QueryController extends Controller
{
   

    protected $queryRepository;
    
    public function __construct(QueryRepository $queryRepository)
    {
        $this->queryRepository = $queryRepository;
       $this->middleware(['permission:Query Formating']);
    }


    public function index(Query $query)
    {
       
        $queries = $this->queryRepository->getActivePaginated(250000, 'updated_at', 'desc');  
        $querystatus=$query->with(['quotations'])->orderBy('updated_at',' desc')->get();
        return view('backend.query.index',compact('queries','querystatus'));
    }

    
    public function create(Client $client,Category $category,CompanyDetail $companydetail,ItemList $itemlist,Product $product,Color $color,Brand $brand,Uom $uom)
    {
        $i_code = DB::table('item_lists')->orderBy('id', 'desc')->first();
        if(empty($i_code)){
        $item_code=1;
        }else{
        $item_code=$i_code->id+1;
        }
        $itemlists=$itemlist->get();
        $companydetails=$companydetail->get();
        $clients=$client->get();
        $products=$product->get();
        $colors=$color->get();
        $brands=$brand->get();
        $uoms=$uom->get();
        $categories=$category->get();
        return view('backend.query.create',compact('item_code','clients','categories','companydetails','itemlists','products','colors','brands','uoms'));
   
    }

    public function saveQuery(Request $request){
        $data = $request->all();
        $queryItems = [];

        for($i=0;$i<sizeof($data['queryItems']);$i++){

        $arr =array(
            //'item_code' => $data['queryItems'][$i]['item_code'],
            'item_id'=> $data['queryItems'][$i]['item_id'],
            'description'=> $data['queryItems'][$i]['description'],
            'specification'=> $data['queryItems'][$i]['specification'],
            'color'=>$data['queryItems'][$i]['color'],
            'model_no'=>$data['queryItems'][$i]['model_no'],
            'brand_id'=>$data['queryItems'][$i]['brand_id'],
            'unit'=>$data['queryItems'][$i]['unit'],
            'remarks'=>$data['queryItems'][$i]['remarks'],
            'image'=>$data['queryItems'][$i]['image'],

            );
            array_push($queryItems,$arr);
        }
        $x = $queryItems;
        $query = [
            
           'client_id'=>$data['query']['client_id'], 
           'center_id'=>$data['query']['center_id'], 
         //  'query_no'=>$data['query']['query_no'],
           'query_date'=>$data['query']['query_date'], 
           'meera_center_code'=>$data['query']['meera_center_code'] ,
           'query_center'=>$data['query']['query_center'] ,
           'po_no'=>$data['query']['po_no'] ,
           'sales_center'=>$data['query']['sales_center'], 
         //  'quote_no'=>$data['query']['quote_no'], 
           'client_detail'=>$data['query']['client_detail'],
     
        ];
        $record['query'] = $query;
        $record['queryItems'] = $queryItems;
        $this->queryRepository->create($record);
        return true;
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $queryItems = [];
        for($i=0;$i<sizeof($data['item_id']);$i++){
        $arr =array(
            'item_id'=> $data['item_id'][$i],
            'description'=> $data['description'][$i],
            'specification'=> $data['specification'][$i],
            'color'=>$data['color'][$i],
            'model_no'=>$data['model_no'][$i],
            'brand_id'=>$data['brand_id'][$i],
            'hsn_code'=>$data['hsn_code'][$i],
            'unit'=>$data['unit'][$i],
            'query_qty'=>$data['qty'][$i],
            'remarks'=>$data['remarks'][$i],
            'image'=>$data['image'][$i],
            );
            array_push($queryItems,$arr);
        }
        $x = $queryItems;
        $query = [
            
           'client_id'=>$data['client_id'], 
           'center_id'=>$data['center_id'], 
           'user_name'=>$data['user_name'],
            'user_id'=>$data['user_id'], 
            'module'=>$data['module'], 
            'action'=>$data['action'], 
            'activity'=>$data['activity'], 
            'normal_oncall'=>$data['normal_oncall'], 
           'meera_center_code'=>$data['meera_center_code'] ,
           'query_center'=>$data['query_center'] ,
           'po_no'=>$data['po_no'] ,
           'sales_center'=>$data['sales_center'], 
          // 'quote_no'=>$data['quote_no'], 
           'client_detail'=>$data['client_detail'],
           'query_person'=>$data['query_person'],
           'contact_no'=>$data['contact_no'],

        ];
        $record['query'] = $query;
        $record['queryItems'] = $queryItems;
        $this->queryRepository->create($record);

        return redirect()->route('admin.query.index')->withFlashSuccess(__('alerts.backend.queries.created'));
    }

    
    public function edit(Query $query,Client $client,ItemList $itemlist,CompanyDetail $companydetail,Category $categ, Product $product,Color $color,Brand $brand,Uom $uom)
    {
        $pid=$query->id;
    
        $queryletters='QR0'.$pid;
        $str=strtoupper($queryletters);
        $sa=substr($str,-2);
        $code='QR'.$sa;

    
        $brandletters='S0'.$pid;
        $str=strtoupper($brandletters);
        $saa=substr($str,-2);
        $scode='S'.$saa;
        $clientinfo=$query->client()->first();
        $centerinfo=$query->companyDetail()->first();
      
         $companydetails=$companydetail->get();
         $clients=$client->get();
         $products=$product->get();
         $colors=$color->get();
         $brands=$brand->get();
         $uoms=$uom->get();
         $querys=$query->normal_oncall;
         $queryitems=$query->queryItems()->get();
         $qid=$queryitems->first();
        $sid=$qid->item_id;
     

         $info = DB::table('products as p')
         ->join('categories as c', 'c.product_id','=','p.id')
         ->join('categories as s', 's.parent_id','=','c.product_id')
         ->join('item_lists as i','i.cat_id','=','s.parent_id')
         ->join('query_items as q','q.item_id','=','i.cat_id')
         ->select('p.product_name','i.description')
         ->where('cat_id','=',$sid)->get();
         $salesinfo=DB::SELECT("SELECT *,qt.updated_at FROM (quotations qt INNER JOIN queries qu ON qt.query_id=qu.id) WHERE qu.id=$pid");
        if(sizeof($salesinfo)>0){
            $salescenter=$salesinfo[0]->sales_center;
            $salesupdate=$salesinfo[0]->updated_at;

        }else{
            $salescenter = "";
            $salesupdate = "";
        }
         
         return view('backend.query.edit', compact('salesupdate','salescenter','code','scode','querys','query','info','productinfo','queryitems','clients','clientinfo','centerinfo','companydetails','itemlists','products','colors','uoms','brands'));
    }

   
    public function update(Query $query,Request $request)
    {
        $data = $request->all();

        for($i=0;$i<sizeof($data['item_id']);$i++){
            if(isset($data['id'][$i])){
                $check=[
                    'id'=>$data['id'][$i]
                ];
            }else{
                $check = [ 'id'=>0];
            }
        $queryItems = QueryItem::updateOrcreate($check,[
               
            'query_id'=>$query->id, 
            'item_id'=>$data['item_id'][$i], 
            'color'=>$data['color'][$i], 
            'brand_id'=>$data['brand_id'][$i], 
            'unit'=>$data['unit'][$i], 
            'description'=>$data['description'][$i], 
            'specification'=>$data['specification'][$i], 
            'model_no'=>$data['model_no'][$i], 
            'hsn_code'=>$data['hsn_code'][$i],
           'query_qty'=>$data['qty'][$i], 
            'remarks'=>$data['remarks'][$i], 
            'image'=>$data['image'][$i]==null?'default.jpg':$data['image'][$i]

            ]);
        }
        $this->queryRepository->update($query, $request->only(
            'client_id', 
            'center_id', 
            'query_no', 
            'normal_oncall', 
            'meera_center_code', 
            'query_center', 
            'po_no', 
            'sales_center', 
            'client_detail'
    ));
        return redirect()->route('admin.query.index')->withFlashSuccess(__('alerts.backend.queries.updated'));
    }

  
    public function destroy(Query $query)
    {
        $this->queryRepository->deleteById($query->id);
        return redirect()->route('admin.query.index')->withFlashSuccess(__('alerts.backend.queries.deleted'));
    }
    Public function queries($cid,Query $query){
        $subcats_data=$query->where('id','=',$cid)->get()->toJson();
       
        return $subcats_data;
    }
    public function newItem(Request $request){
        $item = new QueryItem;
        $item->name = $request->name;
        $item->save();
        return true;
    }
    

    Public function fileupload(Request $request,QueryItem $queryitem){
        $file = $request->file('image');
        
        $img = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path();
        $file->move($destinationPath, $img);
       
        return $img;
    }
    public function deleteItem($id){
        $status = $this->queryRepository->deleteItem($id);
        $string = ($status?'true':'false');
          return $string;
    }
    public function show()
    {
    }
    
    public function getByClient($clientId,Client $client){
        $data= $client->where('id','=',$clientId)->get()->tojson();
        return $data;
    }

    public function getByCompany($centerId,CompanyDetail $companydetail){
        $data= $companydetail->where('id','=',$centerId)->get()->tojson();
        return $data;
    }

    public function getbyitemdesc($item_id,ItemList $itemlist){
        $data=$itemlist->with('product','category','brand','uom')->where('id','=',$item_id)->get()->tojson();  
        return $data;
    }

    Public function parentcategories($cid,Category $category){
        $parentcats_data=$category->where('product_id','=',$cid)->where('parent_id','=',0)->get()->toJson();   
        return $parentcats_data;
    }

    Public function parentbrands($cid,Brand $brand){
        $parentcats_data=$brand->where('product_id','=',$cid)->get()->toJson();     
        return $parentcats_data;
    }
    Public function subcategories($cid,Category $category){
        $subcats_data=$category->where('parent_id','=',$cid)->get()->toJson();
        return $subcats_data;
    }
    public function getByCategory($id,ItemList $itemlist){
        return $itemlist->where('cat_id','=',$id)->get();
    }

}
