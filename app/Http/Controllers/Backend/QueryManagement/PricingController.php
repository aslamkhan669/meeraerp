<?php
namespace App\Http\Controllers\Backend\QueryManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\QueryManagement\Query;
use App\Models\QueryManagement\PriceMapping;
use App\Models\QuotationManagement\Quotation;
use App\Models\QueryManagement\QueryItem;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\ItemMapping;
use App\Models\ProductManagement\CategorySupplier;                                                    
use App\Models\Master\Supplier;                    
use App\Models\Setting\Freight;
use App\Models\Setting\TermCondition;
use App\Models\Setting\TransportInsurance; 
use App\Models\ProductManagement\Brand;                               
use App\Repositories\Backend\QueryManagement\QueryRepository;
use App\Models\Master\Client;
use App\Models\ProductManagement\Color;
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Product; 
use App\Models\LogManagement\Log;


class PricingController extends Controller
{
   

    protected $queryRepository;
    
    public function __construct(QueryRepository $queryRepository)
    {
        $this->queryRepository = $queryRepository;
       $this->middleware(['permission:Pricing Center']);

    }


    public function index(Query $query)
    {
        $queries = $this->queryRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        $querystatus=$query->with(['quotations'])->orderBy('updated_at',' desc')->get();
     //  $status= $querystatus[0]->quotations[0]->is_confirmed;
        return view('backend.pricing.index',compact('queries','querystatus'));
    }
    public function search(Query $query,Request $request)
    {
        $term =$sa=substr(strtoupper($request->term),-2,10);
      $queries = $this->queryRepository->getSearchedPaginated(250000, 'updated_at', 'desc',$term); 
      $querystatus=$query->with(['quotations'])->orderBy('updated_at',' desc')->get();
      return view('backend.pricing.index',compact('queries','querystatus'));
    }
    
    public function show($id,QueryItem $queryitems,TransportInsurance $insurance,Freight $freight,TermCondition $termcondition)
    {
        $freights=$freight->get();
        $insurances=$insurance->get();
        $itemlist=$queryitems->where('query_id','=',$id)->get();
        $term=$termcondition->where('query_id','=',$id)->get();                                                                       
        return view('backend.pricing.create',compact('itemlist','id','freights','insurances','term'));   
    }

    
    public function store(Request $request)
    {
        $this->queryRepository->create($request->all());
        return redirect()->route('admin.pricing.index')->withFlashSuccess(__('alerts.backend.queries.created'));
    }

    


   
    public function edit(Query $query,Client $client)
    {
        $items=$query->queryItems()->get();
        $clientname=$query->client()->first();
        $clients=$client->get();
        return view('backend.query.edit', compact('query','clients','clientname'));
    }

   
    public function update(Query $query,Request $request)
    {
        $this->queryRepository->update($query, $request->only(
            'client_id',
            'query_no', 
            'query_date', 
            'center_code', 
            'meera_center_code', 
            'query_center', 
            'po_no', 
            'client_code', 
            'sales_center', 
            'quote_no', 
            'client_detail'
            
        ));

        return redirect()->route('admin.query.index')->withFlashSuccess(__('alerts.backend.queries.updated'));
    }

  
    public function destroy(Query $query)
    {
        $this->queryRepository->deleteById($query->id);
        return redirect()->route('admin.query.index')->withFlashSuccess(__('alerts.backend.queries.deleted'));
    }

    public function suppliers($id,CategorySupplier $isupplier){
        $data=CategorySupplier::with('supplier')->where('cat_id','=',$id)->get()->tojson();
    return $data;
    }

    public function changecolor($id){
        $data=ItemMapping::where('item_id','=',$id)->get()->tojson();
    return $data;
    }
    public function iteminfo(Request $request){
        $data=ItemMapping::with('supplier')->where('supplier_id','=',$request->suppid)->where('item_id','=',$request->itemid)->get()->tojson();
    return $data;
    }

    public function pricemapping(Request $request, PriceMapping $price){
        
        $match=['item_id'=>$request->item_id, 'supplier_id'=>$request->supplier_id,'total'=>$request->total];
        $quote = PriceMapping::where($match)->first();
        if ($quote === null) {
        $itemid=$request->item_id;
        $price->item_id=$request->item_id;
        $price->supplier_id=$request->supplier_id;
        $price->listprice=$request->listprice;
        $price->insurance_shipping=$request->insurance;
        $price->custom_clearance=$request->clearance;
        $price->discount=$request->discount;
        $price->discounted_price=$request->discounted_price;
        $price->gst=$request->gst;
        $price->total=$request->total;
        $price->quantity=$request->Quantity;
        $price->grand_total=$request->grandamount;
        $price->delivery_term=$request->DeliveryTerm;
        $price->warranty=$request->Warranty;
        $price->standard_package=$request->Std_package;
        $price->is_selected= $request->is_selected;
        $price->save();
        $data='true';
        return $data;
    }else
        {
            $res='false';
        return $res;
        }
    }

    public function mappingupdate(Request $request)
    {
        if($request->id){
       $price=PriceMapping::find($request->id);
       $price->item_id=$request->item_id;
       $price->listprice=$request->listprice;
       $price->discount=$request->discount;
       $price->discounted_price=$request->discounted_price;
       $price->gst=$request->gst;
       $price->quantity=$request->Quantity;
       $price->total=$request->total;
       $price->is_selected= $request->is_selected;
       $price->save();
       $data='true';
       return $data;
   }else
       {
           $res='false';
       return $res;
       }

    }

    public function mappings($id,PriceMapping $pricemap){
        $data=PriceMapping::with('queryItem.itemList','supplier')->where('item_id','=',$id)->get()->tojson();        
        return $data;
    }

    public function queryid(Request $request,Quotation $quotation){
        
   

        $quote = Quotation::where('query_id', '=', $request->queryid)->first();
        if ($quote === null) {
            $quotation->query_id=$request->queryid;
            $quotation->manual_id=0;
            $quotation->is_confirmed=1;
            $quotation->status=1;
            $quotation->info="quotation";
            $quotation->save();

            $log = new Log;
            $log->user_name=$request->userName;
            $log->user_id=$request->userId;
            $log->module=$request->module;
            $log->action=$request->action;
            $log->activity=$request->activity;
            $log->name=$request->queryid;
            $log->save();

            return 'true';
        }else{
            return 'false';
        }
      
    }
   
    public function queryitems($id,QueryItem $qitems,Brand $brand){
        $items=QueryItem::with('brand','color','itemList','uom')->where('id','=',$id)->get()->toJson();  
        return $items;

    }
    public function historyinfo($id){
        $data=PriceMapping::with('supplier')->where('supplier_id','=',$id)->get()->tojson();
    return $data;
    }
    public function infoitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
        $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
        return $items;

    }



    public function termsdata(Request $request){
        $terms = TermCondition::firstOrNew(['query_id'=>$request->Query_id]);
        $terms->query_id=$request->Query_id;    
        $terms->freight_id=$request->Freight_id;
        $terms->insurance_id=$request->Insurance_id;
        $terms->payment_days=$request->Payment_days;
        $terms->validity_days=$request->Validity_days;
        $terms->comment_one=$request->Comment_one;
        $terms->comment_two=$request->Comment_two;
        $terms->comment_three=$request->Comment_three;
        $terms->comment_four=$request->comment_four;
        $terms->save();
       
        $data='true';
        return $data;
     }
}
