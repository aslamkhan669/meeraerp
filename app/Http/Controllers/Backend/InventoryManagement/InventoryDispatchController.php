<?php

namespace App\Http\Controllers\Backend\InventoryManagement;
use App\Http\Controllers\Controller;
use App\Models\InventoryManagement\InventoryProduct;
use App\Repositories\Backend\InventoryManagement\InventoryDispatchRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\InventoryManagement\InventoryDispatch;
use App\Models\DispatchRegister\DispatchRegister;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand; 
use App\Models\ProductManagement\Color;

class InventoryDispatchController extends Controller
{

    protected $inventoryRepository;

    public function __construct(InventoryDispatchRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
        $this->middleware(['permission:DispatchInventory']);
    }

    public function index()
    {
            $inventory = $this->inventoryRepository->getActivePaginated(25, 'updated_at', 'desc');    
            return view('backend.inventorydispatch.index',compact('inventory'));
    }
    public function search(Request $request)
    {
        $term=$request->term;
        $description=$request->description;
        $inventory = $this->inventoryRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$description); 
        return view('backend.inventorydispatch.index',compact('inventory'));
    }
    public function queryitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
        $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
        return $items;
    
    }
 

    Public function show($id, InventoryDispatch $invp)
    {
        $inventorydata=$invp->where('disregisterid','=',$id)->get();
        return view('backend.inventorydispatch.edit',compact('inventorydata'));
    }



}
?>