<?php

namespace App\Http\Controllers\Backend\InventoryManagement;
use App\Http\Controllers\Controller;
use App\Models\InventoryManagement\InventoryProduct;
use App\Repositories\Backend\InventoryManagement\InventoryRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\InventoryManagement\InventoryDispatch;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand; 
use App\Models\ProductManagement\Color;

class InventoryController extends Controller
{

    protected $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
     $this->middleware(['permission:PurchaseInventory']);
    }

    public function index()
    {
            $inventory = $this->inventoryRepository->getActivePaginated(25, 'updated_at', 'desc');    
            return view('backend.inventorymanagement.index',compact('inventory'));
    }
    public function search(Request $request)
    {
        $term=$request->term;
        $description=$request->description;
        $inventory = $this->inventoryRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$description); 
        return view('backend.inventorymanagement.index',compact('inventory'));
    }


    public function queryitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
        $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
        return $items;
    
    }
    Public function show($id, InventoryProduct $invp)
    {
        $inventorydata=$invp->where('poregisterid','=',$id)->get();
        return view('backend.inventorymanagement.edit',compact('inventorydata'));
    }


    public function disstore(Request $request)
    {
        $ch = InventoryDispatch::firstOrNew(['disregisterid'=>$request->POID]);
        $ch->reason=$request->Reason;
        $ch->extra_qty=$request->QNTY;
        $ch->disregisterid=$request->POID;
        $ch->save();
        DB::table('dispatch_registers')
            ->where('id', $request->POID)
            ->update(['status' => '1']);
       return 'true';
    }
}
?>