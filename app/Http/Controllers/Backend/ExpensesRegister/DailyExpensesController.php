<?php

namespace App\Http\Controllers\Backend\ExpensesRegister;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ExpensesRegister\Tea;
use App\Models\ExpensesRegister\Water;
use App\Models\ExpensesRegister\Stationery;
use App\Models\ExpensesRegister\ItPhone;
use App\Models\ExpensesRegister\SalaryExpense;
use App\Models\ExpensesRegister\OtherExpense;
use App\Models\ExpensesRegister\PurchaseExpense;
use App\Models\ExpensesRegister\DispatchExpense;
use App\Models\ExpensesRegister\MiscExpense;
use App\Models\ExpensesRegister\VehicleExpense;
use App\Models\ExpensesRegister\SupplierVisit;
use App\Models\ExpensesRegister\ClientVisit;
use App\Models\ExpensesRegister\DailyExpense;
use App\Repositories\Backend\ExpensesRegister\DailyExpensesRepository;
use Illuminate\Support\Facades\DB;
use App\Models\LogManagement\Log;
use App\Models\PoPlanner\PoRegister;
use Carbon\Carbon;
use Excel;


class DailyExpensesController extends Controller
{
    /**
     * @var DailyExpensesRepository
     */
    protected $dailyexpensesRepository;

    public function __construct(DailyExpensesRepository $dailyexpensesRepository)
    {
        $this->dailyexpensesRepository = $dailyexpensesRepository;
        $this->middleware(['permission:DailyExpenses']); 

    }


    public function index(PoRegister $poregister)
    {
        $dailys = $this->dailyexpensesRepository->getActivePaginated(250000, 'updated_at', 'desc');  
        $data=$poregister->where('checked','=','1')->get();

        return view('backend.dailyexpenses.index',compact('dailys','data'));

    }  
    public function search(Request $request)
    {
        $term=$request->term;
        $dailys = $this->dailyexpensesRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term);
 
        return view('backend.dailyexpenses.index1',compact('dailys'));
    }  
    public function create()
    {
        return view('backend.dailyexpenses.create');

    }

    public function store(Request $request)
    {
        $de =new DailyExpense;
        $de->status=1;
        $de->save();
        $deId= $de->id;

        $pro = new Log;
        $pro->name=$deId;
        $pro->user_name=$request->tdata['username'];
         $pro->user_id=$request->tdata['userid'];
         $pro->module=$request->tdata['mode'];
         $pro->action=$request->tdata['action'];
         $pro->activity=$request->tdata['activity'];
         $pro->save();

        for($i=0;$i<sizeof($request->data);$i++)
        {
        $teas = new Tea();
        $teas->daily_id=$deId;
        $teas->timing=$request->data[$i]['Timing'];
        $teas->qty=$request->data[$i]['Qty'];
        $teas->special=$request->data[$i]['Special'];
        $teas->normal=$request->data[$i]['Normal'];
        $teas->sp_rate=$request->data[$i]['Sprate'];
        $teas->ml_rate=$request->data[$i]['Mlrate'];
        $teas->total=$request->data[$i]['Total'];
        $teas->save();
        }
        for($i=0;$i<sizeof($request->data1);$i++)
        {
        $waters = new Water();
        $waters->daily_id=$deId;
        $waters->type=$request->data1[$i]['Type'];
        $waters->qty=$request->data1[$i]['Qty'];
        $waters->rate=$request->data1[$i]['Rate'];
        $waters->total=$request->data1[$i]['Total'];
        $waters->save();
        }
        for($i=0;$i<sizeof($request->stadata);$i++)
        {
        $sta = new Stationery();
        $sta->daily_id=$deId;
        $sta->item_name=$request->stadata[$i]['ItemName'];
        $sta->qty=$request->stadata[$i]['Qty'];
        $sta->price=$request->stadata[$i]['Price'];
        $sta->total=$request->stadata[$i]['Total'];
        $sta->save();
        }
        for($i=0;$i<sizeof($request->otherdata);$i++)
        {
        $others = new OtherExpense();
        $others->daily_id=$deId;
        $others->type=$request->otherdata[$i]['Type'];
        $others->dop=$request->otherdata[$i]['Dop'];
        $others->duration=$request->otherdata[$i]['Duration'];
        $others->amount=$request->otherdata[$i]['Amount'];
        $others->paid_amount=$request->otherdata[$i]['Paid'];
        $others->due_amt=$request->otherdata[$i]['Due'];
        $others->remark=$request->otherdata[$i]['Remark'];
        $others->save();
        }
        for($i=0;$i<sizeof($request->phonedata);$i++){
        $phones = new ItPhone();
        $phones->daily_id=$deId;
        $phones->mobile=$request->phonedata[$i]['Mobile'];
        $phones->user_Code=$request->phonedata[$i]['UserCode'];
        $phones->recharge_date=$request->phonedata[$i]['Recharge'];
        $phones->plan_duration=$request->phonedata[$i]['Plan'];
        $phones->due_date=$request->phonedata[$i]['Due'];
        $phones->plan_value=$request->phonedata[$i]['PlanValue'];
        $phones->save();
        }
                     for($i=0;$i<sizeof($request->salarydata);$i++){
                        $salarys = new SalaryExpense();
                        $salarys->daily_id=$deId;
                        $salarys->doi=$request->salarydata[$i]['Doi'];
                         $salarys->tmp_Code=$request->salarydata[$i]['Temp'];
                        $salarys->name=$request->salarydata[$i]['Name'];
                        $salarys->role=$request->salarydata[$i]['Role'];
                        $salarys->due_amt=$request->salarydata[$i]['Due'];
                        $salarys->salary=$request->salarydata[$i]['Salary'];
                        $salarys->mop=$request->salarydata[$i]['Mop'];
                        $salarys->dop=$request->salarydata[$i]['Dop'];
                        $salarys->paid_amt=$request->salarydata[$i]['Paid'];
                        $salarys->save();

                         }
                         for($i=0;$i<sizeof($request->clientdata);$i++){
                            $clients = new ClientVisit();
                            $clients->daily_id=$deId;
                            $clients->emp_code=$request->clientdata[$i]['Temp'];
                             $clients->client=$request->clientdata[$i]['Client'];
                            $clients->amt_paid=$request->clientdata[$i]['Paid'];
                            $clients->amt_spend=$request->clientdata[$i]['Spend'];
                            $clients->amt_return=$request->clientdata[$i]['Retur'];
                            $clients->save();

                             }
                             for($i=0;$i<sizeof($request->supplierdata);$i++)
                             {
                                $suppliers = new SupplierVisit();
                                $suppliers->daily_id=$deId;
                                $suppliers->emp_code=$request->supplierdata[$i]['Temp'];
                                $suppliers->supplier=$request->supplierdata[$i]['Supplier'];
                                $suppliers->amt_paid=$request->supplierdata[$i]['Paid'];
                                $suppliers->amt_spend=$request->supplierdata[$i]['Spend'];
                                $suppliers->amt_return=$request->supplierdata[$i]['Retur'];
                                $suppliers->save();
                             }
                             for($i=0;$i<sizeof($request->purchasedata);$i++)
                             {
                                $purchases = new PurchaseExpense();
                                $purchases->daily_id=$deId;
                                $purchases->purchase_person=$request->purchasedata[$i]['Person'];
                                $purchases->amt_recieved=$request->purchasedata[$i]['Recieved'];
                                $purchases->mode_payment=$request->purchasedata[$i]['Mode'];
                                $purchases->amt_spent=$request->purchasedata[$i]['Spend'];
                                $purchases->amt_returned=$request->purchasedata[$i]['Retur'];
                                $purchases->remarks=$request->purchasedata[$i]['Remark'];
                                $purchases->for_what=$request->purchasedata[$i]['What'];
                                $purchases->save();
                             }
                             for($i=0;$i<sizeof($request->dispatchdata);$i++)
                             {
                                $dispatch = new DispatchExpense();
                                $dispatch->daily_id=$deId;
                                $dispatch->dispatch_person=$request->dispatchdata[$i]['Person'];
                                $dispatch->amt_recieved=$request->dispatchdata[$i]['Recieved'];
                                $dispatch->mode_payment=$request->dispatchdata[$i]['Mode'];
                                $dispatch->amt_spent=$request->dispatchdata[$i]['Spend'];
                                $dispatch->amt_returned=$request->dispatchdata[$i]['Retur'];
                                $dispatch->remarks=$request->dispatchdata[$i]['Remark'];
                                $dispatch->for_what=$request->dispatchdata[$i]['What'];
                                $dispatch->save();
                             }
                             for($i=0;$i<sizeof($request->vehicledata);$i++)
                             {
                                $vehicle = new VehicleExpense();
                                $vehicle->daily_id=$deId;
                                $vehicle->person=$request->vehicledata[$i]['Person'];
                                $vehicle->after_reading=$request->vehicledata[$i]['After'];
                                $vehicle->before_reading=$request->vehicledata[$i]['Before'];
                                $vehicle->difference=$request->vehicledata[$i]['Diff'];
                                $vehicle->unit_rate=$request->vehicledata[$i]['Unit'];
                                $vehicle->amount=$request->vehicledata[$i]['Amount'];
                                $vehicle->save();
                             }
                             for($i=0;$i<sizeof($request->miscdata);$i++)
                             {
                                $misc = new MiscExpense();
                                $misc->daily_id=$deId;
                                $misc->person=$request->miscdata[$i]['Person'];
                                $misc->amt_recieved=$request->miscdata[$i]['Recieved'];
                                $misc->mode_payment=$request->miscdata[$i]['Mode'];
                                $misc->amt_spent=$request->miscdata[$i]['Spend'];
                                $misc->amt_returned=$request->miscdata[$i]['Retur'];
                                $misc->remarks=$request->miscdata[$i]['Remark'];
                                $misc->for_what=$request->miscdata[$i]['What'];
                                $misc->save();
                             }
                                   return 'true';
    }

    public function edit(DailyExpense $dailyexpense)
    {
        $clientexp= $dailyexpense->clientVisits()->get();
        $dispatchexp= $dailyexpense->dispatchExpenses()->get();
        $phoneexp= $dailyexpense->itPhones()->get();
        $miscexp= $dailyexpense->miscExpenses()->get();
        $otherexp= $dailyexpense->otherExpenses()->get();
        $purchaseexp= $dailyexpense->purchaseExpenses()->get();
        $stationexp= $dailyexpense->stationeries()->get();
       $salaryexp= $dailyexpense->salaryExpenses()->get();
       $supplierexp= $dailyexpense->supplierVisits()->get();
       $teaexp= $dailyexpense->teas()->get();
       $vehicleexp= $dailyexpense->vehicleExpenses()->get();
       $waterexp= $dailyexpense->waters()->get();

        return view('backend.dailyexpenses.edit',compact('dailyexpense','clientexp','dispatchexp','phoneexp','miscexp','otherexp','purchaseexp','stationexp','salaryexp','supplierexp','teaexp','vehicleexp','waterexp'));

    }
    public function update(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->id;
        $pro->user_name=$request->tdata['username'];
        $pro->user_id=$request->tdata['userid'];
        $pro->module=$request->tdata['mode'];
        $pro->action=$request->tdata['action'];
        $pro->activity=$request->tdata['activity'];
        $pro->save();

        $flight = DailyExpense::find($request->id);
        $flight->status=1;
        $flight->save();
       
        for($i=0;$i<sizeof($request->data);$i++)
        {
        $teas = Tea::find($request->data[$i]['tId']);
        $teas->daily_id=$request->id;
        $teas->timing=$request->data[$i]['Timing'];
        $teas->qty=$request->data[$i]['Qty'];
        $teas->special=$request->data[$i]['Special'];
        $teas->normal=$request->data[$i]['Normal'];
        $teas->sp_rate=$request->data[$i]['Sprate'];
        $teas->ml_rate=$request->data[$i]['Mlrate'];
        $teas->total=$request->data[$i]['Total'];
        $teas->save();
        }
        for($i=0;$i<sizeof($request->data1);$i++)
        {
        $waters = Water::find($request->data1[$i]['wId']);
        $waters->daily_id=$request->id;
        $waters->type=$request->data1[$i]['Type'];
        $waters->qty=$request->data1[$i]['Qty'];
        $waters->rate=$request->data1[$i]['Rate'];
        $waters->total=$request->data1[$i]['Total'];
        $waters->save();
        }
        for($i=0;$i<sizeof($request->stadata);$i++)
        {
         Stationery::updateOrCreate(
         ['daily_id' => $request->id, 'item_name' => $request->stadata[$i]['ItemName'] , 'qty' =>$request->stadata[$i]['Qty'],'price'=>$request->stadata[$i]['Price'], 'total'=>$request->stadata[$i]['Total']    ]
         );
        }
        for($i=0;$i<sizeof($request->otherdata);$i++)
        {
        $others =OtherExpense::find($request->otherdata[$i]['oId']);
        $others->daily_id=$request->id;
        $others->type=$request->otherdata[$i]['Type'];
        $others->dop=$request->otherdata[$i]['Dop'];
        $others->duration=$request->otherdata[$i]['Duration'];
        $others->amount=$request->otherdata[$i]['Amount'];
        $others->paid_amount=$request->otherdata[$i]['Paid'];
        $others->due_amt=$request->otherdata[$i]['Due'];
        $others->remark=$request->otherdata[$i]['Remark'];
        $others->save();
        }
        for($i=0;$i<sizeof($request->phonedata);$i++){
        ItPhone::updateOrCreate(
         ['daily_id' => $request->id, 'mobile' => $request->phonedata[$i]['Mobile'] , 'user_code' =>$request->phonedata[$i]['UserCode'],'recharge_date'=>$request->phonedata[$i]['Recharge'], 'plan_duration'=>$request->phonedata[$i]['Plan'] ,'due_date'=>$request->phonedata[$i]['Due'] ,'plan_value'=>$request->phonedata[$i]['PlanValue']  ]
         );
        }
                     for($i=0;$i<sizeof($request->salarydata);$i++){
                        SalaryExpense::updateOrCreate(
                           ['daily_id' => $request->id, 'doi' => $request->salarydata[$i]['Doi'] , 'tmp_code' =>$request->salarydata[$i]['Temp'],'name'=>$request->salarydata[$i]['Name'], 'role'=>$request->salarydata[$i]['Role'] ,'due_amt'=>$request->salarydata[$i]['Due'] ,'salary'=>$request->salarydata[$i]['Salary'] ,'mop'=>$request->salarydata[$i]['Mop'] ,'dop'=>$request->salarydata[$i]['Dop'],'paid_amt'=>$request->salarydata[$i]['Paid']]
                           );

                         }
                         for($i=0;$i<sizeof($request->clientdata);$i++){
                            ClientVisit::updateOrCreate(
                              ['daily_id' => $request->id, 'emp_code' => $request->clientdata[$i]['Temp'] , 'client' =>$request->clientdata[$i]['Client'],'amt_paid'=>$request->clientdata[$i]['Paid'], 'amt_spend'=>$request->clientdata[$i]['Spend'] ,'amt_return'=>$request->clientdata[$i]['Retur'] ]
                              );

                             }
                             for($i=0;$i<sizeof($request->supplierdata);$i++)
                             {
                                SupplierVisit::updateOrCreate(
                                 ['daily_id' => $request->id, 'emp_code' => $request->supplierdata[$i]['Temp'] , 'supplier' =>$request->supplierdata[$i]['Supplier'],'amt_paid'=>$request->supplierdata[$i]['Paid'], 'amt_spend'=>$request->supplierdata[$i]['Spend'] ,'amt_return'=>$request->supplierdata[$i]['Retur'] ]
                                 );
                             }
                             for($i=0;$i<sizeof($request->purchasedata);$i++)
                             {

                                PurchaseExpense::updateOrCreate(
                                 ['daily_id' => $request->id, 'purchase_person' => $request->purchasedata[$i]['Person'], 'amt_recieved' =>$request->purchasedata[$i]['Recieved'],'mode_payment'=>$request->purchasedata[$i]['Mode'], 'amt_spent'=>$request->purchasedata[$i]['Spend'],'amt_returned'=>$request->purchasedata[$i]['Retur'] ,'remarks'=>$request->purchasedata[$i]['Remark'],'for_what'=>$request->purchasedata[$i]['What']]
                                 );

                             }
                             for($i=0;$i<sizeof($request->dispatchdata);$i++)
                             {
                         
                                DispatchExpense::updateOrCreate(
                                 ['daily_id' => $request->id, 'dispatch_person' => $request->dispatchdata[$i]['Person'], 'amt_recieved' =>$request->dispatchdata[$i]['Recieved'],'mode_payment'=>$request->dispatchdata[$i]['Mode'], 'amt_spent'=>$request->dispatchdata[$i]['Spend'],'amt_returned'=>$request->dispatchdata[$i]['Retur'] ,'remarks'=>$request->dispatchdata[$i]['Remark'],'for_what'=>$request->dispatchdata[$i]['What']]
                                 );
                             }
                             for($i=0;$i<sizeof($request->vehicledata);$i++)
                             {
                       
                                VehicleExpense::updateOrCreate(
                                 ['daily_id' => $request->id, 'person' => $request->vehicledata[$i]['Person'], 'after_reading' =>$request->vehicledata[$i]['After'],'before_reading'=>$request->vehicledata[$i]['Before'], 'difference'=>$request->vehicledata[$i]['Diff'],'unit_rate'=>$request->vehicledata[$i]['Unit'] ,'amount'=>$request->vehicledata[$i]['Amount']]
                                 );
                             }
                             for($i=0;$i<sizeof($request->miscdata);$i++)
                             {
                                $misc = MiscExpense::find($request->miscdata[$i]['miscId']);
                                $misc->daily_id=$request->id;
                                $misc->person=$request->miscdata[$i]['Person'];
                                $misc->amt_recieved=$request->miscdata[$i]['Recieved'];
                                $misc->mode_payment=$request->miscdata[$i]['Mode'];
                                $misc->amt_spent=$request->miscdata[$i]['Spend'];
                                $misc->amt_returned=$request->miscdata[$i]['Retur'];
                                $misc->remarks=$request->miscdata[$i]['Remark'];
                                $misc->for_what=$request->miscdata[$i]['What'];
                                $misc->save();

                                MiscExpense::updateOrCreate(
                                 ['daily_id' => $request->id, 'person' => $request->miscdata[$i]['Person'], 'amt_recieved' =>$request->miscdata[$i]['Recieved'],'mode_payment'=>$request->miscdata[$i]['Mode'], 'amt_spent'=>$request->miscdata[$i]['Spend'],'amt_returned'=>$request->miscdata[$i]['Retur'] ,'remarks'=>$request->miscdata[$i]['Remark'],'for_what'=>$request->miscdata[$i]['What']]
                                 );
                             }
         return 'true';
        }

        public function show(DailyExpense $dailyexpense)
        {
         $tea_data = $dailyexpense->teas()->get();
         $tea_array[] = array('Timing', 'QTY', 'Special', 'Normal', 'SP Rate','ML rate','Total','Date&time');
         foreach($tea_data as $data)
         {
          $tea_array[] = array(
           'Timing'  => $data->timing,
           'QTY'   => $data->qty,
           'Special'    => $data->special,
           'Normal'  => $data->normal,
           'SP Rate'   => $data->sp_rate,
           'ML Rate'   => $data->ml_rate,
           'Total'   => $data->total,
           'Date&time'   => $data->created_at

          );
         }
  
         $water_data = $dailyexpense->waters()->get();
         $water_array[] = array('Type', 'QTY', 'Rate','Total');
         foreach($water_data as $wdata)
         {
          $water_array[] = array(
           'Type'  => $wdata->type,
           'QTY'   => $wdata->qty,
           'Rate'   => $wdata->rate,
           'Total'   => $wdata->total
          );
         }
         $sta_data = $dailyexpense->stationeries()->get();
         $sta_array[] = array('Item Name', 'QTY', 'Price','Total');
         foreach($sta_data as $sdata)
         {
          $sta_array[] = array(
           'Item Name'  => $sdata->item_name,
           'QTY'   => $sdata->qty,
           'Price'   => $sdata->price,
           'Total'   => $sdata->total
          );
         }
         $it_data = $dailyexpense->itPhones()->get();
         $it_array[] = array('Mobile', 'User Code', 'Recharge Date','Plan Duration','Due Date','Plan Value');
         foreach($it_data as $sdata)
         {
          $it_array[] = array(
           'Mobile'  => $sdata->mobile,
           'User Code'   => $sdata->user_code,
           'Recharge Date'   => $sdata->recharge_date,
           'Plan Duration'   => $sdata->plan_duration,
           'Due Date'   => $sdata->due_date,
           'Plan Value'   => $sdata->plan_value

          );
         }
         $salary_data = $dailyexpense->salaryExpenses()->get();
         $salary_array[] = array('DOI', 'Tmp Code', 'Name','Role','Salary','MOP','DOP','Paid Amt','Due Amt');
         foreach($salary_data as $sadata)
         {
          $salary_array[] = array(
           'DOI'  => $sadata->doi,
           'Tmp Code'   => $sadata->tmp_code,
           'Name'   => $sadata->name,
           'Role'   => $sadata->role,
           'Salary'   => $sadata->salary,
           'MOP'   => $sadata->mop,
           'DOP'   => $sadata->dop,
           'Paid Amt'   => $sadata->paid_amt,
           'Due Amt'   => $sadata->due_amt

          );
         }
         $other_data = $dailyexpense->otherExpenses()->get();
         $other_array[] = array('Other Expenses', 'DOP', 'Duration','Amount','Due Amt','Remark');
         foreach($other_data as $odata)
         {
          $other_array[] = array(
           'Other Expenses'  => $odata->type,
           'DOP'   => $odata->dop,
           'Duration'   => $odata->duration,
           'Amount'   => $odata->amount,
           'Due Amt'   => $odata->due_amt,
           'Remark'   => $odata->remark

          );
         }
         $client_data = $dailyexpense->clientVisits()->get();
         $client_array[] = array('Emp Code', 'Client','Amount Paid','Amt Spend','Amt Return');
         foreach($client_data as $cdata)
         {
          $client_array[] = array(
           'Emp Code'  => $cdata->emp_code,
           'Client'   => $cdata->client,
           'Amount Paid'   => $cdata->amt_paid,
           'Amt Spend'   => $cdata->amt_spend,
           'Amt Return'   => $cdata->amt_return
          );
         }
         $supplier_data = $dailyexpense->supplierVisits()->get();
         $supplier_array[] = array('Emp Code', 'Supplier','Amount Paid','Amt Spend','Amt Return');
         foreach($supplier_data as $sudata)
         {
          $supplier_array[] = array(
           'Emp Code'  => $sudata->emp_code,
           'Supplier'   => $sudata->supplier,
           'Amount Paid'   => $sudata->amt_paid,
           'Amt Spend'   => $sudata->amt_spend,
           'Amt Return'   => $sudata->amt_return
          );
         }
         $purchase_data = $dailyexpense->purchaseExpenses()->get();
         $purchase_array[] = array('Purchase Person', 'Amt Recieved','Amt Spend','Amt Return','Mode Of Payment','Remarks','for what');
         foreach($purchase_data as $pudata)
         {
          $purchase_array[] = array(
           'Purchase Person'  => $pudata->purchase_person,
           'Amt Recieved'   => $pudata->amt_recieved,
           'Amt Spend'   => $pudata->amt_spent,
           'Amt Return'   => $pudata->amt_returned,
           'Mode Of Payment'   => $pudata->mode_payment,
           'Remarks'   => $pudata->remarks,
           'for what'   => $pudata->for_what
          );
         }
         $dispatch_data = $dailyexpense->dispatchExpenses()->get();
         $dispatch_array[] = array('Dispatch Person', 'Amt Recieved','Amt Spend','Amt Return','Mode Of Payment','Remarks','for what');
         foreach($dispatch_data as $didata)
         {
          $dispatch_array[] = array(
           'Dispatch Person'  => $didata->dispatch_person,
           'Amt Recieved'   => $didata->amt_recieved,
           'Amt Spend'   => $didata->amt_spent,
           'Amt Return'   => $didata->amt_returned,
           'Mode Of Payment'   => $didata->mode_payment,
           'Remarks'   => $didata->remarks,
           'for what'   => $didata->for_what
          );
         }
         $vehicle_data = $dailyexpense->vehicleExpenses()->get();
         $vehicle_array[] = array('Person', 'Amt Recieved','Amt Spend','Amt Return','Mode Of Payment','Remarks','for what');
         foreach($vehicle_data as $vdata)
         {
          $vehicle_array[] = array(
           'Person'  => $vdata->person,
           'After Reading'   => $vdata->after_reading,
           'Before Reading'   => $vdata->before_reading,
           'Defference'   => $vdata->difference,
           'Unit Rate'   => $vdata->unit_rate,
           'Amount'   => $vdata->amount
          );
         }
         $misc_data = $dailyexpense->miscExpenses()->get();
         $misc_array[] = array('Person', 'Amt Recieved','Amt Spend','Amt Return','Mode Of Payment','Remarks','for what');
         foreach($misc_data as $midata)
         {
          $misc_array[] = array(
           'Person'  => $midata->person,
           'Amt Recieved'   => $midata->amt_recieved,
           'Amt Spend'   => $midata->amt_spent,
           'Amt Return'   => $midata->amt_returned,
           'Mode Of Payment'   => $midata->mode_payment,
           'Remarks'   => $midata->remarks,
           'for what'   => $midata->for_what
          );
         }
         Excel::create('Daily Data', function($excel) use ($tea_array,$water_array,$sta_array,$it_array,$salary_array,$other_array,$client_array,$supplier_array,$purchase_array,$dispatch_array,$vehicle_array,$misc_array){
          $excel->setTitle('Daily Tea');
          $excel->sheet('Daily Tea', function($sheet) use ($tea_array){
           $sheet->fromArray($tea_array, null, 'A1', false, false);
          });
          $excel->setTitle('Daily Water');
          $excel->sheet('Daily Water', function($sheet) use ($water_array){
           $sheet->fromArray($water_array, null, 'A1', false, false);
          });
          $excel->setTitle('Daily Stationary');
          $excel->sheet('Daily Stationary', function($sheet) use ($sta_array){
           $sheet->fromArray($sta_array, null, 'A1', false, false);
          });
          $excel->setTitle('Daily ItPhone');
          $excel->sheet('Daily ItPhone', function($sheet) use ($it_array){
           $sheet->fromArray($it_array, null, 'A1', false, false);
          });
          $excel->setTitle('Salary ');
          $excel->sheet('Salary', function($sheet) use ($salary_array){
           $sheet->fromArray($salary_array, null, 'A1', false, false);
          });
          $excel->setTitle('Other');
          $excel->sheet('Other', function($sheet) use ($other_array){
           $sheet->fromArray($other_array, null, 'A1', false, false);
          });
          $excel->setTitle('Client Visit');
          $excel->sheet('Client Visit', function($sheet) use ($client_array){
           $sheet->fromArray($client_array, null, 'A1', false, false);
          });
          $excel->setTitle('Supplier Visit');
          $excel->sheet('Supplier Visit', function($sheet) use ($supplier_array){
           $sheet->fromArray($supplier_array, null, 'A1', false, false);
          });
          $excel->setTitle('Purchase');
          $excel->sheet('Purchase', function($sheet) use ($purchase_array){
           $sheet->fromArray($purchase_array, null, 'A1', false, false);
          });
          $excel->setTitle('Dispatch');
          $excel->sheet('Dispatch', function($sheet) use ($dispatch_array){
           $sheet->fromArray($dispatch_array, null, 'A1', false, false);
          });
          $excel->setTitle('Vehicle');
          $excel->sheet('Vehicle', function($sheet) use ($vehicle_array){
           $sheet->fromArray($vehicle_array, null, 'A1', false, false);
          });
          $excel->setTitle('Miscellanceses');
          $excel->sheet('Miscellanceses', function($sheet) use ($misc_array){
           $sheet->fromArray($misc_array, null, 'A1', false, false);
          });
         })->download('xlsx');
        }
    
        public function deleteStationery($id){
         $flight = Stationery::find($id);
         $flight->delete(); 
         $string = ($flight?'true':'false');
           return $string;
     }
     public function deleteItPhone($id){
      $itphone = ItPhone::find($id);
      $itphone->delete(); 
      $string = ($itphone?'true':'false');
        return $string;
  }
  public function deleteSalary($id){
   $salary = SalaryExpense::find($id);
   $salary->delete(); 
   $string = ($salary?'true':'false');
     return $string;
}
public function deleteClient($id){
   $client = ClientVisit::find($id);
   $client->delete(); 
   $string = ($client?'true':'false');
     return $string;
}  public function deleteSupplier($id){
   $supplier = SupplierVisit::find($id);
   $supplier->delete(); 
   $string = ($supplier?'true':'false');
     return $string;

}  public function deletePurchase($id){
   $purchase = PurchaseExpense::find($id);
   $purchase->delete(); 
   $string = ($purchase?'true':'false');
     return $string;

}  public function deleteDispatch($id){
   $dispatch = DispatchExpense::find($id);
   $dispatch->delete(); 
   $string = ($dispatch?'true':'false');
     return $string;
}
public function deleteVehicle($id){
   $vehicle = VehicleExpense::find($id);
   $vehicle->delete(); 
   $string = ($vehicle?'true':'false');
     return $string;

}  public function deleteMisc($id){
   $misc = MiscExpense::find($id);
   $misc->delete(); 
   $string = ($misc?'true':'false');
     return $string;
}
}
