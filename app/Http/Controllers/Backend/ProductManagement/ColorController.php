<?php

namespace App\Http\Controllers\Backend\ProductManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductManagement\Color;
use App\Repositories\Backend\ProductManagement\ColorRepository;




class ColorController extends Controller
{
   

    protected $colorRepository;
    
    public function __construct(ColorRepository $colorRepository)
    {
        $this->colorRepository = $colorRepository;
        $this->middleware(['permission:Color']);
    }


    public function index(Color $color)
    {
       
        $colors = $this->colorRepository->getActivePaginated(250000, 'updated_at', 'desc');    
        return view('backend.color.index',compact('colors'));
    }

    
    public function create()
    {
        
        return view('backend.color.create');
   
    }

    
    public function store(Request $request)
    {
        $this->colorRepository->create($request->all());
        return redirect()->route('admin.color.index')->withFlashSuccess(__('alerts.backend.colors.created'));
    }

    


   
    public function edit(Color $color)
    {
        
        return view('backend.color.edit', compact('color'));
    }

   
    public function update(Color $color,Request $request)
    {
        $this->colorRepository->update($color, $request->only(
                
                'color',
                'is_active',
                'user_name',
                'user_id',
                'module',
                'action',
                'activity'
        ));

        return redirect()->route('admin.color.index')->withFlashSuccess(__('alerts.backend.colors.updated'));
    }

  
    public function destroy(Color $color)
    {
        $this->colorRepository->deleteById($color->id);

        return redirect()->route('admin.color.index')->withFlashSuccess(__('alerts.backend.colors.deleted'));
    }
}
