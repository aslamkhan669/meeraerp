<?php

namespace App\Http\Controllers\Backend\ProductManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductManagement\Brand;
use App\Models\ProductManagement\Product;                            
use App\Repositories\Backend\ProductManagement\BrandRepository;
use App\Repositories\Backend\ProductManagement\ProductRepository;
use App\Http\Requests\Backend\ProductManagement\StoreBrandRequest;
use App\Http\Requests\Backend\ProductManagement\UpdateBrandRequest;
use App\Http\Requests\Backend\ProductManagement\ManageBrandRequest;
use DB;



class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $brandRepository;
    
    public function __construct(BrandRepository $brandRepository)
    {
        $this->brandRepository = $brandRepository;
       $this->middleware(['permission:Brands']);   
    }


    public function index(Brand $brand)
    {

        return view('backend.brand.index')
      ->withbrand($this->brandRepository->getActivePaginated(250000, 'updated_at', 'desc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product,Brand $brand)
    {
        $b_code = DB::table('brands')->orderBy('id', 'desc')->first();
        if(empty($b_code)){
            $brand_code=1;
            }else{
            $brand_code=$b_code->id+1;
        }
        
        $products=$product->get();   
        return view('backend.brand.create',compact('products','brand_code'));
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->brandRepository->create($request->all());
        return redirect()->route('admin.brand.index')->withFlashSuccess(__('alerts.backend.brand.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand,Product $product)
    {
        $uniq_product=$brand->product()->first();
        $products=$product->get();
        
        return view('backend.brand.edit', compact('brand','products','uniq_product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Brand $brand,Request $request)
    {
        $this->brandRepository->update($brand, $request->only(
                'product_id',
                'brand_code',
                'brand_name',
                'is_active',
                'user_name',
                'user_id',
                'module',
                'action',
                'activity'
        ));

        return redirect()->route('admin.brand.index')->withFlashSuccess(__('alerts.backend.brand.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(brand $brand)
    {
        $this->brandRepository->deleteById($brand->id);

        return redirect()->route('admin.brand.index')->withFlashSuccess(__('alerts.backend.brand.deleted'));
    }
    public function combodata($key,Brand $brand){

        $data=$brand->where('brand_name','LIKE','%'.$key.'%')->select('id','brand_name')->get()->toJson();
        return $data;
    }
   
}
