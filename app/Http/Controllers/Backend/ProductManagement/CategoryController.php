<?php
namespace App\Http\Controllers\Backend\ProductManagement;

use App\Models\ProductManagement\Category;
use App\Models\ProductManagement\Product;
use App\Http\Controllers\Controller;
use App\Events\Backend\Category\CategoryDeleted;
use App\Repositories\Backend\ProductManagement\CategoryRepository;
use App\Http\Requests\Backend\Category\StoreCategoryRequest;
use App\Http\Requests\Backend\Category\ManageCategoryRequest;
use App\Http\Requests\Backend\Category\UpdateCategoryRequest;
use App\Http\Requests\Backend\Subcategory\StoreSubcategoryRequest;
use App\Http\Requests\Backend\Subcategory\ManageSubcategoryRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use PDF;


/**
 * Class UserController.
 */
class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;


    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
       $this->middleware(['permission:Category&Subcategory']);   
    }


    public function index()
    {
          $categories = $this->categoryRepository->getActivePaginated(25, 'updated_at', 'desc');    
            return view('backend.category.index',compact('categories'));
    }

    public function searchcategory(Request $request)
    {
        $term=$request->term;
        $categories = $this->categoryRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term); 
        return view('backend.category.index',compact('categories'));
    }
    public function create(Product $product)
    {
        $b_code = DB::table('categories')->orderBy('id', 'desc')->select('id')->first();
        if(empty($b_code)){
            $code=1;
            }else{
            $code=$b_code->id+1;
        }
        $productname=$product->get();
        $parentCategory = $this->categoryRepository->where("parent_id","=",0)->get();     
        return view('backend.category.create',compact("parentCategory","code","productname"));
    }

    public function store(Request $request)
    {
        $this->categoryRepository->create($request->only(
            'product_id',
            'category_name',
            'code',
            'is_active',
             'parent_id' ,
             'user_name',
            'user_id',
            'module',
            'action',
            'activity'         
         )
           );
     
        return redirect()->route('admin.category.index')->withFlashSuccess(__('alerts.backend.categories.created'));
    }
    public function edit(Category $category,Product $product)
    {        $productname=$product->get();
        $productinfo=$category->product()->first();
        $parentCategory = $category->where([["parent_id","=",0],["product_id",'=',$productinfo->id]])->get();
        
        $isParent = $category->parent_id == 0?1:0;
        return view('backend.category.edit',compact('parentCategory','category','isParent','productname','productinfo'));
    }

    public function update(Category $category,Request $request)
    {
        $image = null;
        $file = $request->file('image');
        $parentid = $category->parent_id;
        $request->merge(['parent_id' => $parentid]);
        $update =  $request->only(           
            'product_id',
            'category_name',
            'code',
            'is_active',
            'parent_id',
            'user_name',
            'user_id',
            'module',
            'action',
            'activity'   
        );
        
        if($file!=null){
            $img = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $file->move($destinationPath, $img);
            $image='uploads/'.$img; 
            $update['image'] = $image;
        }
   $this->categoryRepository->update($category,$update,$image);
if($category->parent_id == 0){
        return redirect()->route('admin.category.index')->withFlashSuccess(__('alerts.backend.categories.updated'));
      }
    else{
        return redirect()->route('admin.subcategory.index')->withFlashSuccess(__('alerts.backend.subcategories.updated'));  
    }
    }

    public function destroy(Category $category)
    {
        $this->categoryRepository->deleteById($category->id);
        event(new CategoryDeleted($category));
        return redirect()->route('admin.category.index')->withFlashSuccess(__('alerts.backend.categories.deleted'));
    }

    public function subindex(Product $product,Category $category,Request $request)
    {
          $subcategories = $this->categoryRepository->getSubActivePaginated(25, 'updated_at', 'desc'); 
            return view('backend.subcategory.index',compact('subcategories','product','category'));
    }

    public function searchsub(Product $product,Category $category,Request $request)
    {
      $term=$request->term;
      $cat=$request->parent_id;
      $subcategories = $this->categoryRepository->getSubSearched(25000, 'updated_at', 'desc',$term,$cat); 
      return view('backend.subcategory.index',compact('subcategories','product','category'));
    }

    public function subcreate(Product $product)
    {
        $b_code = DB::table('categories')->orderBy('id', 'desc')->select('id')->first();
        if(empty($b_code)){
            $code=1;
            }else{
            $code=$b_code->id+1;
        }
        $productname=$product->get();
        $categories=$this->categoryRepository->where("parent_id","=",0)->get();
        return view('backend.subcategory.create',compact('categories','productname','code'));
    }

    public function substore(Request $request)
    {
        $image = null;
        $file = $request->file('image');
        $create =  $request->only(
            'product_id',
            'category_name',
            'code',
            'is_active',
            'parent_id',
            'user_name',
            'user_id',
            'module',
            'action',
            'activity'    
        );
        if($file!=null){
            $img = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $file->move($destinationPath, $img);
            $image='uploads/'.$img; 
            $create['image'] = $image;
        }
             $this->categoryRepository->createNew($create,$image);
        return redirect()->route('admin.subcategory.index')->withFlashSuccess(__('alerts.backend.subcategories.created'));
    }

    public function combodata($key,Category $cat){
        $data=$cat->where('category_name','LIKE','%'.$key.'%')->where('parent_id','!=',0)->select('id','category_name')->get()->toJson();
        return $data;
    }

    
    public function comboinfo($key,Category $cat){

        $data=$cat->where('category_name','LIKE','%'.$key.'%')->select('id','category_name')->get()->toJson();
        return $data;
    }
    
}