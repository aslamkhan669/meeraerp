<?php
namespace App\Http\Controllers\Backend\ProductManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand; 
use App\Models\ProductManagement\Category;
use App\Models\ProductManagement\Color;                                                                                                             
use App\Repositories\Backend\ProductManagement\ItemListRepository;
use App\Http\Requests\Backend\ProductManagement\StoreItemRequest;
use App\Http\Requests\Backend\ProductManagement\UpdateItemRequest;
use App\Http\Requests\Backend\ProductManagement\ManageItemRequest;
use DB;
use App\Models\LogManagement\Log;


class ItemListController extends Controller
{

    protected $itemlistRepository;
    
    public function __construct(ItemListRepository $itemlistRepository)
    {
        $this->itemlistRepository = $itemlistRepository;
       $this->middleware(['permission:Item List']);
    }


    public function index()
    {
        $itemlist =$this->itemlistRepository->getActivePaginated(25, 'updated_at', 'desc');

        return view('backend.itemlist.index',compact('itemlist'));
    }
    
    public function searchitem(Request $request)
    {
        $itemno =$request->itemno;
        $modelno=$request->modelno;
        $hsncode=$request->hsncode;
        $specif=$request->specif;
        $term=$request->term;
        $product_id=$request->product_id;     
        $brand_id=$request->brand_id;        
   
    $itemlist = $this->itemlistRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$specif,$hsncode,$modelno,$product_id,$brand_id,$itemno); 
        return view('backend.itemlist.index',compact('itemlist'));
    }

    public function create(Category $category,Product $product,Uom $uom,Color $color)
    {
        $i_code = DB::table('item_lists')->orderBy('id', 'desc')->first();
        if(empty($i_code)){
        $item_code=1;
        }else{
        $item_code=$i_code->id+1;
        }
        $categories=$category->where('parent_id','=',0)->get();
        $products=$product->get();  
        $uoms=$uom->get();   
        $colors=$color->get();   
        return view('backend.itemlist.create',compact('products','uoms','colors','item_code','categories'));
    }

    public function store(Request $request)
    {
        $file = $request->file('image');
        $img = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('/uploads');
        $file->move($destinationPath, $img);
        $image='uploads/'.$img;
        $this->itemlistRepository->newcreate($request->all(),$image);
        return redirect()->route('admin.itemlist.index')->withFlashSuccess(__('alerts.backend.item.created'));
    }

    public function storebyitem(Request $request)
    {
        $this->itemlistRepository->createitem($request->all());
        return 'true';
    }
    public function edit(ItemList $itemlist,Brand $brand,Category $category, Product $product,Uom $uom,Color $color)
    {    
        $pid=$itemlist->id;
        $prodid=$itemlist->product_id;

        $brandletters='I000000'.$pid;
        $str=strtoupper($brandletters);
        $sa=substr($str,-6);
        $prodletters='P00'.$prodid;
        $strin=strtoupper($prodletters);
        $saa=substr($strin,-2);
        $code='P'.$saa.'I'.$sa; 
        $itemParent = [];
        $itemSub = false;    
        $itemcat = $category->where('id','=',$itemlist->cat_id)->first();
        if($itemcat->parent_id != 0){
         $itemSub = true;   
         $itemParent = $category->where('id','=',$itemcat->parent_id)->first();
        }
        else{
            $itemParent =  $itemcat;
            $itemSub = false;
        }
        $categories=$category->where([['is_active','=','1'],['parent_id','=',0],["product_id",'=',$itemParent->product_id]])->get();
        $subcategories=$category->where([['is_active','=','1'],['parent_id','<>',0],["product_id",'=',$itemcat->product_id]])->get();
        $products=$product->get();
        $uoms=$uom->get();   
        $colors=$color->get(); 
        $brands=$brand->get();   
        $brandParent = $itemlist->brand()->first();

        return view('backend.itemlist.edit', compact('brandParent','brands','code','itemlist','products','uoms','colors','categories','subcategories','itemSub','itemParent','itemcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemList $itemlist,Request $request)
    {
        $image = array();

        $pro = new Log;
        $pro->name=$request->description;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->save();

        $file = $request->file('image');
        if($file!=null){
            $img = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads');
            $file->move($destinationPath, $img);
             $image='uploads/'.$img;
           }
        $this->itemlistRepository->update($itemlist, $request->only(
            'product_id',
            'cat_id',
            'brand_id',
            'model_no',
            'hsn_code',
            'unit', 
            'color',
            'description',
            'remarks', 
            'specification',
            'is_active'

    ),$image);


        return redirect()->route('admin.itemlist.index')->withFlashSuccess(__('alerts.backend.item.updated'));
    }


    public function destroy(ItemList $itemlist)
    {
        $this->itemlistRepository->deleteById($itemlist->id);
        return redirect()->route('admin.itemlist.index')->withFlashSuccess(__('alerts.backend.item.deleted'));
    }
 
    Public function fileuploaditem(Request $request,ItemList $itemlist){
        $file = $request->file('image');
        
        $image = time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('/uploads');
        $file->move($destinationPath, $image);
        $img='uploads/'.$image;
        return $img;
    }
  
    Public function productcodes($pid,Product $product){
        $parent_data=$product->where('id','=',$pid)->get()->toJson();
        return $parent_data;
    }

    Public function parentcategories($cid,Category $category){
        $parentcats_data=$category->where('product_id','=',$cid)->where('parent_id','=',0)->get()->toJson();   
        return $parentcats_data;
    }

    Public function parentbrands($cid,Brand $brand){
        $parentcats_data=$brand->where('product_id','=',$cid)->get()->toJson();     
        return $parentcats_data;
    }
    Public function subcategories($cid,Category $category){
        $subcats_data=$category->where('parent_id','=',$cid)->get()->toJson();
        return $subcats_data;
    }
    public function getByCategory($id,ItemList $itemlist){
        return $itemlist->where('cat_id','=',$id)->get();
    }


   
}
