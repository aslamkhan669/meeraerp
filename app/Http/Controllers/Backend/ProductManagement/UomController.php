<?php

namespace App\Http\Controllers\Backend\ProductManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductManagement\Uom;
use App\Events\Backend\Uom\UomDeleted;
use App\Repositories\Backend\ProductManagement\UomRepository;
use App\Http\Requests\Backend\ProductManagement\UpdateUomRequest;
use App\Http\Requests\Backend\ProductManagement\ManageUomRequest;



class UomController extends Controller
{
   

    protected $uomRepository;
    
    public function __construct(UomRepository $uomRepository)
    {
        $this->uomRepository = $uomRepository;
        $this->middleware(['permission:Uom']);   
    }


    public function index(Uom $uom)
    {
       
        return view('backend.uom.index')
      ->withuom($this->uomRepository->getActivePaginated(250000, 'updated_at', 'desc'));
    }

    
    public function create()
    {
        
        return view('backend.uom.create');
   
    }

    
    public function store(Request $request)
    {
        $this->uomRepository->create($request->all());
        return redirect()->route('admin.uom.index')->withFlashSuccess(__('alerts.backend.uom.created'));
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit(Uom $uom)
    {
        
        return view('backend.uom.edit', compact('uom'));
    }

   
    public function update(Uom $uom,UpdateUomRequest $request)
    {
        $this->uomRepository->update($uom, $request->only(
                
                'unit',
                
                'is_active',
                'user_name',
                'user_id',
                'module',
                'action',
                'activity'
            
        ));

        return redirect()->route('admin.uom.index')->withFlashSuccess(__('alerts.backend.uom.updated'));
    }

  
    public function destroy(Uom $uom)
    {
        $id=$uom->id;
        $this->uomRepository->deleteById($uom->id);

        event(new UomDeleted($uom));

        return redirect()->route('admin.uom.index')->withFlashSuccess(__('alerts.backend.uom.deleted'));
    }
}
