<?php

namespace App\Http\Controllers\Backend\ProductManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\CategorySupplier; 
use App\Models\ProductManagement\ItemMapping; 
use App\Models\ProductManagement\Category;
use App\Models\ProductManagement\Color;
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand;                                                                                                              
use App\Repositories\Backend\ProductManagement\ItemMappingRepository;
use DB;
use Illuminate\Support\Str;
use App\Models\LogManagement\Log;


class ItemMappingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $itemmappingRepository;
    
    public function __construct(ItemMappingRepository $itemmappingRepository)
    {
        $this->middleware(['permission:ItemList Pricing']);
        $this->itemmappingRepository = $itemmappingRepository;
    }


    public function index()
    {
      return view('backend.itemmapping.index')
      ->withitemmapping($this->itemmappingRepository->getActivePaginated(25, 'updated_at', 'desc'));
    }

    public function search(Request $request)
    {
        $itemno =$request->itemno;
        $modelno=$request->modelno;
        $hsncode=$request->hsncode;
        $specif=$request->specif;
        $term=$request->term;
        $brand_id=$request->brand_id; 
        $product_id=$request->product_id; 
        $itemmapping = $this->itemmappingRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$specif,$hsncode,$modelno,$product_id,$brand_id,$itemno); 
        return view('backend.itemmapping.index',compact('itemmapping'));
    }
    
    public function show($id,ItemList $itemlist,CategorySupplier $categorysupplier,Category $category)
    {
        $itemlists=$itemlist->where('id','=',$id)->get();
        $itemcat=$itemlists[0]->cat_id;
        $categoryid=$category->where('id','=',$itemcat)->get();
        $parentcat=$categoryid[0]->parent_id;
        $iteminfo=$categorysupplier->where('cat_id','=',$parentcat)->get();
        

        return view('backend.itemmapping.create',compact('itemlists','id','iteminfo'));   
    }
    public function store(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->description;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->name=$request->Pid;
         $pro->save();

        $data = $request->all();
        $pid = $data['Pid'];
        for($i=0;$i<sizeof($data['listprice']);$i++){
            DB::statement("UPDATE item_lists SET status ='1' where id = $pid");

       $qitems =new ItemMapping;
       $qitems->listprice=$data['listprice'][$i];     
       $qitems->insurance_shipping=$data['insurance_shipping'][$i];
       $qitems->custom_clearance=$data['custom_clearance'][$i];
       $qitems->discount=$data['discount'][$i];
       $qitems->discounted_price=$data['discounted_price'][$i];
       $qitems->gst=$data['gst'][$i];
       $qitems->total=$data['total'][$i];
       $qitems->quantity=$data['quantity'][$i];
       $qitems->grand_amount=$data['grand_amount'][$i];
       $qitems->delivery_term=$data['delivery_term'][$i];
       $qitems->warranty=$data['warranty'][$i];
       $qitems->standard_package=$data['standard_package'][$i];
       $qitems->item_id=$data['item_id'][$i];
       $qitems->supplier_id=$data['supplier_id'][$i];
       $qitems->save();

    }
    return redirect()->route('admin.itemmapping.index')->withFlashSuccess(__('alerts.backend.item.updated'));
    }
    public function edit($id,ItemList $itemlist,CategorySupplier $categorysupplier,Category $category)
    { 
        $itemlists=$itemlist->where('id','=',$id)->get();
        $itemcat=$itemlists[0]->cat_id;
        $categoryid=$category->where('id','=',$itemcat)->get();
        $parentcat=$categoryid[0]->parent_id;
        $supp_catinfo=$categorysupplier->where('cat_id','=',$parentcat)->get();
        $iteminfo=ItemMapping::where('item_id','=',$id)->get();
      return view('backend.itemmapping.edit',compact('itemlists','id','supp_catinfo','iteminfo'));   
    }
    public function update(Request $request)
    {
        $pro = new Log;
        $pro->name=$request->description;
        $pro->user_name=$request->user_name;
         $pro->user_id=$request->user_id;
         $pro->module=$request->module;
         $pro->action=$request->action;
         $pro->activity=$request->activity;
         $pro->name=$request->Pid;
         $pro->save();

        $data = $request->all();
        $pid = $data['Pid'];
        for($i=0;$i<sizeof($data['listprice']);$i++){
            DB::statement("UPDATE item_lists SET status ='1' where id = $pid");
          
       $qitems = ItemMapping::find($data['id'][$i]);
       $qitems->listprice=$data['listprice'][$i];     
       $qitems->insurance_shipping=$data['insurance_shipping'][$i];
       $qitems->custom_clearance=$data['custom_clearance'][$i];
       $qitems->discount=$data['discount'][$i];
       $qitems->discounted_price=$data['discounted_price'][$i];
       $qitems->gst=$data['gst'][$i];
       $qitems->total=$data['total'][$i];
       $qitems->quantity=$data['quantity'][$i];
       $qitems->grand_amount=$data['grand_amount'][$i];
       $qitems->delivery_term=$data['delivery_term'][$i];
       $qitems->warranty=$data['warranty'][$i];
       $qitems->standard_package=$data['standard_package'][$i];
       $qitems->item_id=$data['item_id'][$i];
       $qitems->supplier_id=$data['supplier_id'][$i];
       $qitems->save();

    }
    return redirect()->route('admin.itemmapping.index')->withFlashSuccess(__('alerts.backend.item.updated'));
    }
    public function infoitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
        $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
        return $items;

    }
    public function itempriceinfo(Request $request){
        $data=ItemMapping::with('supplier')->where('supplier_id','=',$request->suppid)->where('item_id','=',$request->itemid)->get()->tojson();
    return $data;
    }
}
