<?php

namespace App\Http\Controllers\Backend\ProductManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductManagement\Product;
use App\Models\ProductManagement\Category;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Brand;
use App\Repositories\Backend\ProductManagement\ProductRepository;
use App\Http\Requests\Backend\ProductManagement\StoreProductRequest;
use App\Http\Requests\Backend\ProductManagement\UpdateProductRequest;
use App\Http\Requests\Backend\ProductManagement\ManageProductRequest;
use App\Events\Backend\ProductManagement\ProductCreated;
use App\Events\Backend\ProductManagement\ProductUpdated;
use App\Events\Backend\ProductManagement\ProductDeleted;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $productRepository;
    
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
       $this->middleware(['permission:Products']); 
         
        
    }


    public function index(Product $products)
    {
      $product = $this->productRepository->getActivePaginated(25, 'updated_at', 'desc'); 
      return view('backend.product.index',compact('product'));
    }

    public function search(Product $products,Request $request)
    {
        $term=$request->term;
      $product = $this->productRepository->getSearchedPaginated(2500, 'updated_at', 'desc',$term); 
      return view('backend.product.index',compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $b_code = DB::table('products')->orderBy('id', 'desc')->select('id')->first();
        if(empty($b_code)){
            $code=1;
            }else{
            $code=$b_code->id+1;
        }
    
        return view('backend.product.create',compact('code','user'));
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->productRepository->create($request->all());
        return redirect()->route('admin.product.index')->withFlashSuccess(__('alerts.backend.product.created'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product,Category $category,Brand $brand)
    {   
        $pid=$product->id;
    
        $brandletters='P0'.$pid;
        $str=strtoupper($brandletters);
        $sa=substr($str,-2);
        $code='P'.$sa;
        return view('backend.product.edit',compact('uniq_category','categories','product','uniq_brand','brands','selectedcategory','subcategories','code'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Product $product,Request $request)
    {		
    		$this->productRepository->update($product, $request->only(
            'product_name',
            'product_code',
            'product_owner',
            'is_active',
            'user_name',
            'user_id',
            'module',
            'action',
            'activity'
        ));
                
        return redirect()->route('admin.product.index')->withFlashSuccess(__('alerts.backend.product.updated'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->productRepository->deleteById($product->id);

        return redirect()->route('admin.product.index')->withFlashSuccess(__('alerts.backend.product.deleted'));
    }

 
    public function combodata($key,Product $product){

        $data=$product->where('product_name','LIKE','%'.$key.'%')->select('id','product_name')->get()->toJson();
        return $data;
    }
  
}
