<?php

namespace App\Http\Controllers\Backend\PurchaseTeam;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PoFormatting\PoFormatting;
use App\Models\PoPlanner\PoRegister;
use App\Models\QuotationManagement\Quotation;
use App\Models\QueryManagement\QueryItem;
use App\Models\PurchaseOrder\PoItem;
use App\Models\PoPlanner\PurchasedOrder;
use App\Repositories\Backend\PurchaseTeam\PurchaseTeamRepository;
use Auth;
use Illuminate\Support\Facades\DB;

class PurchaseTeamController extends Controller
{
    protected $purchaseteamRepository;    
    public function __construct(PurchaseTeamRepository $purchaseteamRepository)
    {
        $this->purchaseteamRepository = $purchaseteamRepository;
    }

    public function index()
    {
     $purchasedata = $this->purchaseteamRepository->getActivePaginated(25, 'id', 'desc');    
      // $data =DB::Select(DB::raw("SELECT sum(por.payment_amount) p,cs.contact_number1,cs.client_name,cs.street,cs.sector,cs.city,cs.state,cs.zone,pf.c_p_owner_no,pf.c_purchaser_name,por.pt_user_id,pds.updated_at,qs.client_id,cs.sales_center,cs.created_at,pds.bill_challan_no FROM po_registers por INNER JOIN purchased_orders pds on por.id=pds.poregisterid INNER JOIN po_items pi on por.poitemid=pi.qty INNER JOIN po_formattings pf on pi.po_id=pf.id INNER JOIN quotations qu on pf.quotation_id=qu.id INNER JOIN queries qs on qu.query_id=qs.id INNER JOIN clients cs on qs.client_id=cs.id GROUP BY pds.bill_challan_no")); 

        return view('backend.PurchaseTeam.index',compact('purchasedata'));
    }

    public function edit($id,PurchasedOrder $purchaseOrder,PoRegister $poregister)
    {
        $data=PoRegister::find($id);
        return view('backend.PurchaseTeam.edit',compact('id','data'));
    }
    
    public function create(Quotation $quotation)
    {
        $quotationno=$quotation->where('is_confirmed','=',1)->get();
        return view('backend.poformat.create',compact('quotationno'));
    }

    public function show(){
        $purchasedata = $this->purchaseteamRepository->getActivePaginated(25, 'id', 'asc');    
        return view('backend.PurchaseTeam.index',compact('purchasedata'));
    }

    public function store(Request $request)
    {
       for($i=0;$i<sizeof($request->data);$i++){
            $qitems = PoRegister::firstOrNew(['poitemid'=>$request->data[$i]['PoId']]);
       $qitems->pt_user_id=$request->data[$i]['PurchaseTeam'];     
       $qitems->poitemid=$request->data[$i]['PoId'];
       $qitems->qty=$request->data[$i]['Qty'];
       $qitems->purchase_date=$request->data[$i]['PurchaseDate'];
       $qitems->day_shift=$request->data[$i]['DayShift'];
       $qitems->priority=$request->data[$i]['Priority'];
       $qitems->transportation_mode=$request->data[$i]['TransportationMode'];
       $qitems->payment_amount=$request->data[$i]['Gtotal'];
       $qitems->payment_mode=$request->data[$i]['PaymentMode'];
       $qitems->bank=$request->data[$i]['Bank'];
       $qitems->save();
       $data='true';
       return $data;
   }
    }

    public function purchaseorder(Request $request){
        $terms = PurchasedOrder::firstOrNew(['poregisterid'=>$request->PoRegister]);
        $terms->poregisterid=$request->PoRegister;    
        $terms->ml_purchased=$request->MLpurchased;
        $terms->payment_mode=$request->CashMode;    
        $terms->pending_qty=$request->pendingQty;
        $terms->remarks=$request->Remark;
        $terms->save();
       // $terms->poRegister()->update(['pending'=>$request->pendingQty]);

        // DB::statement("UPDATE quotations SET is_confirmed = '1' where query_id = $request->Query_id");        
        return 'true';
     }

    public function purchaseritems(QueryItem $qitem,$id){
        $items=QueryItem::with('brand','color','itemList','uom')->where('item_id','=',$id)->get()->toJson();  
        return $items;
    }

    public function poitems(Request $request){
        for($i=0;$i<sizeof($request->dataArray);$i++){
        $poformatting = new PoItem();
        $poformatting->qitemid=$request->dataArray[$i]['QueryItemId'];
        $poformatting->po_id =$request->poid;
        $poformatting->qty=$request->dataArray[$i]['Qty'];
        $poformatting->grand_total =$request->dataArray[$i]['Gtotal'];
        if($poformatting->save()) {
            return 'true';
        }
    }
    }

}
