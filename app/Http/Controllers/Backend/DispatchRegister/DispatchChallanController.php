<?php

namespace App\Http\Controllers\Backend\DispatchRegister;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PoPlanner\PoRegister;
use App\Repositories\Backend\DispatchRegister\DispatchChallanRepository;
use App\Models\PoPlanner\PurchasedOrder;
use App\Models\DispatchRegister\DisChallan;
use App\Models\DispatchRegister\DispatchChallan;
use App\Models\DispatchRegister\DispatchedOrder;
use App\Models\DispatchRegister\DispatchChallanItem;
use App\Models\DispatchRegister\DispatchRegister;
use App\Models\Setting\Freight;
use App\Models\Setting\CompanyDetail;
use App\Models\Setting\TransportInsurance;
use Illuminate\Support\Facades\DB;
use App\Models\PoPlanner\PurchaseExpense;
use App\Models\PurchaseOrder\PoFormatting;
use App\Models\Master\Client;
use App\Models\Master\Supplier;
use App\Models\ProductManagement\ItemList;
use App\Models\ProductManagement\Product; 
use App\Models\ProductManagement\Uom; 
use App\Models\ProductManagement\Brand; 
use App\Models\ProductManagement\Color;
use PDF;
use App\Models\LogManagement\Log;
use App\Models\QueryManagement\QueryItem;


class DispatchChallanController extends Controller
{

   //PoAccount controller 
    protected $dispatchChallanRepository;
    
    public function __construct(DispatchChallanRepository $dispatchChallanRepository)
    {
        $this->dispatchChallanRepository = $dispatchChallanRepository;
        $this->middleware(['permission:DispatchChallan']); 

    }
    public function index()
    {   
        $dispatchchallans = $this->dispatchChallanRepository->getActivePaginated(25, 'updated_at', 'desc');    

       return view('backend.dispatchchallan.index',compact('dispatchchallans'));
    }
    public function search(Request $request)
    {
        $term=$request->term;
        $clientpono=$request->clientpono;
        $dispatchchallans = $this->dispatchChallanRepository->getSearchedPaginated(25000, 'updated_at', 'desc',$term,$clientpono); 
        return view('backend.dispatchchallan.index',compact('dispatchchallans'));
    }
    public function create(DispatchRegister $disreg)
    {
        $poinfo=$disreg->get();

        return view('backend.dispatchchallan.create',compact('poinfo'));
    }
    public function edit($id,DispatchedOrder $dispatchedorder,TransportInsurance $insurance,Freight $freight,PoRegister $poregister)
    {
        $data=DisChallan::where('po_id','=',$id)->get();
      $info=DB::SELECT("SELECT * FROM ((((dispatched_orders do INNER JOIN dispatch_registers dr ON do.disregisterid=dr.id)INNER JOIN po_items pi ON dr.poitemid=pi.id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN price_mappings pm ON qi.id=pm.item_id) WHERE dr.dt_user_id=$id AND do.bill_challan='challan'");

      $freightinfo=$freight->get();
      $insuranceinfo=$insurance->get();
        return view('backend.dispatchchallan.edit', compact('data','id','info','centerinfo','termsdata','freightinfo','insuranceinfo'));
    }
    Public function storebychallan(Request $request) 
    {
        $pro = new Log;
        $pro->name=$request->data1['paymentDays'];
        $pro->user_name=$request->data1['username'];
         $pro->user_id=$request->data1['userid'];
         $pro->module=$request->data1['mode'];
         $pro->action=$request->data1['action'];
         $pro->activity=$request->data1['activity'];
         $pro->save();

        $ch =new DispatchChallan;
        $ch->client_id=$request->data1['client_id'];
        $ch->center_id=$request->data1['center_id'];
       // $ch->supplier_challan=$request->data1['supplier_challan'];
        $ch->dt_user_id=$request->data1['dt_user_id'];
        $ch->po_id=$request->data1['po_id'];
        $ch->payment_term=$request->data1['paymentDays'];
        $ch->freight_charges=$request->data1['freightId'];
        $ch->material_insurance=$request->data1['insuranceId'];
        $ch->po_validity=$request->data1['Validity'];
        $ch->comment1=$request->data1['Line1'];
        $ch->comment2=$request->data1['Line2'];
        $ch->comment3=$request->data1['Line3'];
        $ch->comment4=$request->data1['Line4'];

        $ch->save();
        $cid= $ch->id;
        for($i=0;$i<sizeof($request->data);$i++){
            $qitems =new DispatchChallanItem;
               $qitems->dt_challan_id=$cid;     
       $qitems->item_id=$request->data[$i]['item_id'];
       $qitems->qty=$request->data[$i]['quantity'];
       $qitems->gst=$request->data[$i]['gst'];
       $qitems->save();
        }
       $data='true';
       return $data;
    
   }
   public function infoitems($id,ItemList $itemlist,Brand $brand,Color $color,Uom $uom,Product $product){
    $items=$itemlist::with('brand','color','uom')->where('id','=',$id)->get()->toJson();  
    return $items;

}

public function streamPDF($id,CompanyDetail $company,DispatchChallan $challan,DispatchChallanItem $challanitem,Supplier $supplier,Client $client,TransportInsurance $insurance,Freight $freight)
{
    $freights=$freight->get();
    $insurances=$insurance->get();

   $data=$challan->where('po_id','=',$id)->get();

    $info=$company->where('id','=',$data[0]->center_id)->get();
   $clientinfo=$client->where('id','=',$data[0]->client_id)->get();
  $challanitem=$challanitem->where('dt_challan_id','=',$data[0]->id)->get();
  $termsdata=DB::SELECT("SELECT * FROM ((((( dispatch_registers dr INNER JOIN po_items pi ON dr.poitemid=pi.id)INNER JOIN po_formattings pf ON pi.po_id=pf.id)INNER JOIN quotations q ON pf.quotation_id=q.id)INNER JOIN queries qu ON q.query_id=qu.id)INNER JOIN termsconditions t ON qu.id=t.query_id) WHERE dr.dt_user_id=$id");
  $fid= $data[0]->freight_charges;
 $freightinfo=$freight->where('id','=',$fid)->get();
  $tid= $data[0]->material_insurance;
  $insuranceinfo=$insurance->where('id','=',$tid)->get();
 $pdf = PDF::loadview('backend.dispatchchallan.pdf', compact('id','data','challanitem','freightinfo','insuranceinfo','data','clientinfo','info','termsdata'))->setPaper('A4', 'landscape');
 return $pdf->inline();
//return view('backend.dispatchchallan.pdf', compact('id','data','challanitem','freightinfo','insuranceinfo','data','clientinfo','info','termsdata'));

   
 
}

public function poitems($id){
    $qitems=DB::SELECT("SELECT * FROM (( po_items pi INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN price_mappings pm ON qi.id=pm.item_id) WHERE pi.po_id=$id");
    return $qitems;
 }
 public function poitem(Request $request){
    for($i=0;$i<sizeof($request->dataArray);$i++){
    $poformatting = DisChallan::firstOrNew(['poitemid'=>$request->dataArray[$i]['PItemId']]);
    $poformatting->poitemid=$request->dataArray[$i]['PItemId'];
    $poformatting->po_id=$request->dataArray[$i]['Poid'];
    $poformatting->qty=$request->dataArray[$i]['Qty'];
    $poformatting->qitemid=$request->dataArray[$i]['QueryItemId'];
    $poformatting->grand_total =$request->dataArray[$i]['Gtotal'];
    if(!$poformatting->save()) {
        return 'false';
    }
}
    return 'true';
}

public function combodata($key,PoFormatting $poform){

    $data=$poform->where('id','LIKE','%'.$key.'%')->select('id')->get()->toJson();
    return $data;
}

public function queryitems($id){
    $qitems=QueryItem::with('brand','color','uom','itemlist')->where('item_id','=',$id)->get()->toJson();  
    return $qitems;
 }
}
