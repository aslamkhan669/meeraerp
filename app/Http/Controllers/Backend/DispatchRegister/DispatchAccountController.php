<?php

namespace App\Http\Controllers\Backend\DispatchRegister;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PoPlanner\PoRegister;
use App\Repositories\Backend\DispatchRegister\DispatchAccountRepository;
use App\Models\PoPlanner\PurchasedOrder;
use App\Models\Account\Challan;
use App\Models\Account\ChallanItem;
use App\Models\Setting\Freight;
use App\Models\Setting\TransportInsurance;
use Illuminate\Support\Facades\DB;  
use App\Models\DispatchRegister\DispatchedOrder;
use App\Models\DispatchRegister\DispatchRegister;
use App\Models\DispatchRegister\DispatchExpense;
use App\Models\PurchaseOrder\PoFormatting;
use App\Models\LogManagement\Log;
use App\Models\ProductManagement\ItemList;
use App\Models\DispatchRegister\DispatchInfo;
use App\Models\InventoryManagement\InventoryDispatch;



class DispatchAccountController extends Controller
{

    protected $dispatchAccountRepository;
    
    public function __construct(DispatchAccountRepository $dispatchAccountRepository)
    {
        $this->dispatchAccountRepository = $dispatchAccountRepository;
        $this->middleware(['permission:DispatchAccount']);

    }


    public function index()
    {   
        //$data = $dis->where('dt_user_id','=',$id); 
        //$poregisters=DB::SELECT("SELECT * FROM clients cs INNER JOIN queries q on cs.id=q.client_id INNER JOIN quotations qu on q.id=qu.query_id INNER JOIN po_formattings pf on qu.id= pf.quotation_id INNER JOIN po_items pi on pf.id=pi.po_id INNER JOIN po_registers pr on pi.id = pr.poitemid INNER JOIN purchased_orders po on pr.id=po.poregisterid INNER JOIN dispatch_registers dr on po.id=dr.poitemid INNER JOIN users u on dr.dt_user_id=u.id where po.ml_recieved=1");
        $poregisters = $this->dispatchAccountRepository->getActivePaginated(25, 'updated_at', 'desc');    
       $info=DispatchRegister::with('dispatchedOrder')->get();

        return view('backend.dispatchaccount.index',compact('poregisters','info'));
    }

    public function edit($id,$date,DispatchRegister $disRegister)
    {
        $data=$disRegister->where([['dt_user_id','=',$id],['purchase_date','=',$date]])->get();

        return view('backend.dispatchaccount.edit', compact('data','id'));
    }
    
    public function create()
    {
     
    }

    public function poaccounts(Request $request){
        $terms = PurchasedOrder::firstOrNew(['poregisterid'=>$request->PoRegisterId]);
        $terms->poregisterid=$request->PoRegisterId;    
        $terms->ml_recieved=$request->MLrecieved;  
        $terms->bill_challan_no=$request->BillNo;  
        $terms->save();

        
        return 'true';
     }
     public function dispatchaccounts(Request $request){

        $pro = new Log;
        $pro->name=$request->dataArray[0]['DisRegisterId'];
        $pro->user_name=$request->dataArray[0]['user_name'];
         $pro->user_id=$request->dataArray[0]['user_id'];
         $pro->module=$request->dataArray[0]['module'];
         $pro->action=$request->dataArray[0]['action'];
         $pro->activity=$request->dataArray[0]['activity'];
         $pro->save();

        for($i=0;$i<sizeof($request->dataArray);$i++){   
         $terms = DispatchedOrder::firstOrNew(['disregisterid'=>$request->dataArray[$i]['DisRegisterId']]);
         $terms->dispatchRegister()->update(['dt_qty'=>$request->dataArray[$i]['Qty']]);
         $terms->dispatchRegister()->update(['payment_amount'=>$request->dataArray[$i]['Amount']]);
         $terms->disregisterid=$request->dataArray[$i]['DisRegisterId'];
         $terms->ml_recieved=$request->dataArray[$i]['MLrecieved']; 
         $terms->bill_challan=$request->dataArray[$i]['BillChallan'];  
         $terms->bill_challan_no=$request->dataArray[$i]['BillNo'];
         $terms->recieved_doc=$request->dataArray[$i]['recievedDoc'];  
         $terms->poitem_reciever=$request->dataArray[$i]['poitemReciever'];
         $terms->complete="1";
         $terms->save();
         $terms->dispatchRegister()->update(['checked'=>1]);
         }
         if($terms->save()) {
             return 'true';
         }
      }

    public function store(Request $request)
    {
       

    
    }
    Public function storebychallan(Request $request) 
    {
        $ch = Challan::firstOrNew(['supplier_challan'=>$request->data1['supplier_challan']]);
        $ch->client_id=$request->data1['client_id'];
        $ch->supplier_id=$request->data1['supplier_id'];
        $ch->supplier_challan=$request->data1['supplier_challan'];
        $ch->save();
        $cid= $ch->id;
       
        for($i=0;$i<sizeof($request->data[$i]['description']);$i++){
            $qitems = new ChallanItem();

               $qitems->challan_id=$cid;     

       $qitems->description=$request->data[$i]['description'];     
       $qitems->hsn_code=$request->data[$i]['hsn_code'];
       $qitems->qty=$request->data[$i]['quantity'];
       $qitems->total=$request->data[$i]['total'];
       $qitems->gst=$request->data[$i]['gst'];
       $qitems->grand_total=$request->data[$i]['grand_total'];
    
       $qitems->save();
       $data='true';
       return $data;
        }
   }
   public function dispitem($id){
    $items=ItemList::with('brand','color','uom','product')->where('id','=',$id)->get()->toJson();  
    return $items;
}
public function show($id,$info,DispatchedOrder $disOrder,DispatchRegister $disRegister)
{
    $data =DB::SELECT("SELECT sum(dr.payment_amount) p,d.bill_challan_no,d.bill_challan FROM dispatch_registers dr INNER JOIN dispatched_orders d on dr.id=d.disregisterid where dr.dt_user_id=$id AND dr.purchase_date='$info' GROUP BY d.bill_challan_no"); 
    return view('backend.dispatchaccount.show', compact('data'));
}

public function invstore(Request $request)
{
    $ch = InventoryDispatch::firstOrNew(['disregisterid'=>$request->POID]);
    $ch->reason=$request->Reason;
    $ch->extra_qty=$request->QNTY;
    $ch->disregisterid=$request->POID;
    $ch->bill_no=$request->billNo;
    $ch->location=$request->location;
    $ch->save();
    DB::table('dispatch_registers')
        ->where('id', $request->POID)
        ->update(['status' => '1']);
   return 'true';
}

public function indexinfo(PoRegister $poregister)
{   
 
    $info=DispatchedOrder::where('pending_qty','>',0)->get();
    return view('backend.dispatchaccount.indexpending',compact('info'));
}
}
