<?php
namespace App\Http\Controllers\Backend\DispatchRegister;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PurchaseOrder\PoFormatting;
use App\Models\QuotationManagement\Quotation;
use App\Models\QueryManagement\QueryItem;
use App\Models\QueryManagement\PriceMapping;
use App\Models\QueryManagement\Query;
use App\Models\PurchaseOrder\PoItem;
use App\Repositories\Backend\PurchaseOrder\PoFormattingRepository;
use App\Repositories\Backend\DispatchRegister\DispatchRegisterRepository;
use Illuminate\Support\Facades\DB;
use App\Models\DispatchRegister\DispatchedOrder;
use App\Models\DispatchRegister\DispatchRegister;
use App\Models\DispatchRegister\DispatchExpense;
use App\Models\PoPlanner\PurchasedOrder;
use App\Models\LogManagement\Log;
use App\Models\ProductManagement\ItemList;


class DispatchRegisterController extends Controller
{
    protected $poformattingRepository;
    
    public function __construct(PoFormattingRepository $poformattingRepository)
    {
        $this->poformattingRepository = $poformattingRepository;
        $this->middleware(['permission:DispatchPlanner']);
    }
    // protected $dispatchRegisterRepository;
    
    // public function __construct(DispatchRegisterRepository $dispatchRegisterRepository)
    // {
    //     $this->dispatchRegisterRepository = $dispatchRegisterRepository;
    //     $this->middleware(['permission:DispatchPlanner']);
    // }
    public function index()
    {  
        // $data=PurchasedOrder::all();
       // $data=DB::SELECT("SELECT * FROM clients cs INNER JOIN queries q on cs.id=q.client_id INNER JOIN quotations qu on q.id=qu.query_id INNER JOIN po_formattings pf on qu.id= pf.quotation_id INNER JOIN po_items pi on pf.id=pi.po_id INNER JOIN po_registers pr on pi.id = pr.poitemid INNER JOIN purchased_orders po on pr.id=po.poregisterid where po.ml_recieved=1");
        $data = $this->poformattingRepository->getActivePaginated(25, 'updated_at', 'desc');    

        return view('backend.dispatchregister.index',compact('data'));
    }

    public function test(PoFormatting $po){
        $data=$po->poItems->poRegisters->purchasedOrders->where('ml_purchased','=',1)->get();
    }
    
    public function store(Request $request)
    {   
        $pro = new Log;
        $pro->name=$request->data[0]['PoId'];
        $pro->user_name=$request->data[0]['user_name'];
         $pro->user_id=$request->data[0]['user_id'];
         $pro->module=$request->data[0]['module'];
         $pro->action=$request->data[0]['action'];
         $pro->activity=$request->data[0]['activity'];
         $pro->save();

        for($i=0;$i<sizeof($request->data);$i++){
            $qitems = DispatchRegister::firstOrNew(['poitemid'=>$request->data[$i]['PoId']]);
            $qitems->dt_user_id=$request->data[$i]['PurchaseTeam'];     
            $qitems->poitemid=$request->data[$i]['PoId'];
            $qitems->dt_qty=$request->data[$i]['Qty'];
            $qitems->payment_amount=$request->data[$i]['Amount'];
            $qitems->on_bill_challan=$request->data[$i]['onBill'];
            $qitems->purchase_date=$request->data[$i]['PurchaseDate'];
            $qitems->day_shift=$request->data[$i]['DayShift'];
            $qitems->priority=$request->data[$i]['Priority'];
            $qitems->transportation_mode=$request->data[$i]['TransportationMode'];
            $qitems->save();
            $qitems->poItem()->update(['dis_complete'=>1]);

        }
            $data='true';
            return $data;
    }


 



    Public function show($id,PoFormatting $po,Quotation $quotation){
      // $data=DB::SELECT("SELECT pi.qty,pi.grand_total,pi.po_id,cs.sales_center,cs.updated_at,cs.contact_number1,cs.street,cs.landmark,cs.sector,cs.city,cs.state,cs.id,q.client_id,pf.quotation_id,pf.c_p_owner_no,qi.item_id,il.image FROM clients cs INNER JOIN queries q on cs.id=q.client_id INNER JOIN query_items qi on q.id=qi.query_id INNER JOIN item_lists il on il.id=qi.item_id INNER JOIN quotations qu on q.id=qu.query_id INNER JOIN po_formattings pf on qu.id= pf.quotation_id INNER JOIN po_items pi on pf.id=pi.po_id INNER JOIN po_registers pr on pi.id = pr.poitemid INNER JOIN purchased_orders po on pr.id=po.poregisterid where po.ml_recieved=1 AND cs.id=$id");
     
        $users=DB::table('users')
        ->select('users.id','users.first_name')
        ->join('model_has_roles','model_has_roles.model_id','=','users.id')
        ->where(['model_has_roles.role_id'=>42])
       // ->where(['model_has_roles.role_id'=>0])

        ->get();
        $poitems=PoFormatting::with('poItems','quotation')->where('id','=',$id)->get();
        $quotationno=$quotation->where([['is_confirmed','=',1],['id','=',$poitems[0]->quotation_id]])->get();
        $qid=$quotationno[0]->query_id;
        $companyinfo=DB::SELECT("SELECT * FROM queries q INNER JOIN company_details c ON q.center_id=c.id WHERE q.id=$qid");
        return view('backend.dispatchregister.edit',compact('poitems','data','users','companyinfo'));
    }
    public function dispatchitems($id){
        $items=ItemList::with('brand','color','uom','product')->where('id','=',$id)->get()->toJson();  
        return $items;
    }
    public function comboinfo($key){

        $data = DB::SELECT("SELECT * FROM ((((inventory_dispatchs ip INNER JOIN dispatch_registers dr ON dr.id=ip.disregisterid) INNER JOIN po_items pi ON dr.poitemid=pi.id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN item_lists il ON qi.item_id=il.id) WHERE il.description LIKE '%$key%'"); 
        return $data;
    }
    public function comboiteminfo($key){
    
        $data = DB::SELECT("SELECT * FROM ((((inventory_dispatchs ip INNER JOIN dispatch_registers dr ON dr.id=ip.disregisterid) INNER JOIN po_items pi ON dr.poitemid=pi.id)INNER JOIN query_items qi ON pi.qitemid=qi.id)INNER JOIN item_lists il ON qi.item_id=il.id) WHERE il.id LIKE '%$key%'"); 
        return $data;
    }
}
