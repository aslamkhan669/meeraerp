<?php

namespace App\Models\ManualPurchasing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $oncallquotation_id
 * @property int $manualitem_id
 * @property string $profit
 * @property string $profit_amount
 * @property string $gstamount
 * @property string $mtotal
 * @property string $pup
 * @property string $mqty
 * @property string $mgrand_total
 * @property string $created_at
 * @property string $updated_at
 * @property ManualItem $manualItem
 * @property OncallQuotation $oncallQuotation
 */
class OncallQuotationitem extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['oncallquotation_id', 'manualitem_id', 'profit', 'profit_amount', 'gstamount', 'mtotal', 'pup', 'mqty', 'mgrand_total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manualItem()
    {
        return $this->belongsTo('App\\Models\ManualPurchasing\ManualItem', 'manualitem_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function oncallQuotation()
    {
        return $this->belongsTo('App\\Models\ManualPurchasing\OncallQuotation', 'oncallquotation_id');
    }
}
