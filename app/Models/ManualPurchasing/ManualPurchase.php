<?php

namespace App\Models\ManualPurchasing;

use Illuminate\Database\Eloquent\Model;
use App\Models\ManualPurchasing\Traits\Attribute\ManualPurchaseAttribute;

/**
 * @property int $id
 * @property int $client_id
 * @property int $center_id
 * @property string $meera_center_code
 * @property string $query_center
 * @property string $sales_center
 * @property string $client_detail
 * @property string $created_at
 * @property string $updated_at
 * @property CompanyDetail $companyDetail
 * @property Client $client
 */
class ManualPurchase extends Model
{
    use ManualPurchaseAttribute;

    /**
     * @var array
     */
    protected $fillable = ['client_id', 'center_id', 'meera_center_code', 'query_center','po_no', 'sales_center', 'client_detail','query_person','contact_no','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function companyDetail()
    {
        return $this->belongsTo('App\Models\Setting\CompanyDetail', 'center_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Master\Client');
    }
    public function manualItems()
    {
        return $this->hasMany('App\Models\ManualPurchasing\ManualItem','manual_id');
    }
    public function manualQuotations()
    {
        return $this->hasMany('App\Models\ManualPurchasing\OncallQuotation');
    }
}
