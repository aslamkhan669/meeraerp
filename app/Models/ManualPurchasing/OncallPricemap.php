<?php

namespace App\Models\ManualPurchasing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $item_id
 * @property int $supplier_id
 * @property string $listprice
 * @property string $insurance_shipping
 * @property string $custom_clearance
 * @property string $discount
 * @property string $discounted_price
 * @property string $gst
 * @property string $total
 * @property string $quantity
 * @property string $grand_total
 * @property string $delivery_term
 * @property string $warranty
 * @property string $standard_package
 * @property int $is_selected
 * @property string $created_at
 * @property string $updated_at
 * @property QueryItem $queryItem
 * @property Supplier $supplier
 */
class OncallPricemap extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['item_id', 'supplier_id', 'listprice', 'insurance_shipping', 'custom_clearance', 'discount', 'discounted_price', 'gst', 'total', 'quantity', 'grand_total', 'delivery_term', 'warranty', 'standard_package', 'is_selected', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function queryItem()
    {
        return $this->belongsTo('App\Models\QueryManagement\QueryItem', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo('App\Models\Master\Supplier');
    }
}
