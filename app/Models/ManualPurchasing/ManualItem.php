<?php

namespace App\Models\ManualPurchasing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $manual_id
 * @property int $item_id
 * @property int $color
 * @property int $brand_id
 * @property int $unit
 * @property string $description
 * @property string $specification
 * @property string $model_no
 * @property string $remarks
 * @property string $image
 * @property string $hsn_code
 * @property string $created_at
 * @property string $updated_at
 * @property Brand $brand
 * @property Color $color
 * @property ItemList $itemList
 * @property ManualPurchase $manualPurchase
 * @property Uom $uom
 */
class ManualItem extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['manual_id', 'item_id', 'color', 'brand_id', 'unit', 'description', 'specification', 'model_no', 'remarks', 'image', 'hsn_code','query_qty', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\ProductManagement\Brand');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function color()
    {
        return $this->belongsTo('App\Models\ProductManagement\Color', 'color');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itemList()
    {
        return $this->belongsTo('App\Models\ProductManagement\ItemList', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manualPurchase()
    {
        return $this->belongsTo('App\Models\ManualPurchasing\ManualPurchase', 'manual_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uom()
    {
        return $this->belongsTo('App\Models\ProductManagement\Uom', 'unit');
    }
}
