<?php

namespace App\Models\ManualPurchasing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $manual_id
 * @property int $is_confirmed
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property ManualPurchase $manualPurchase
 * @property OncallQuotationitem[] $oncallQuotationitems
 */
class OncallQuotation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['manual_id', 'is_confirmed', 'status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manualPurchase()
    {
        return $this->belongsTo('App\Models\ManualPurchasing\ManualPurchase', 'manual_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function oncallQuotationitems()
    {
        return $this->hasMany('App\\Models\ManualPurchasing\OncallQuotationitem', 'oncallquotation_id');
    }
}
