<?php

namespace App\Models\ExpensesRegister\Traits\Attribute;

/**
 * Trait RoleAttribute.
 */
trait DailyExpensesAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.dailyexpenses.edit', $this).'" class="btn btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'"></i></a>';
    }



    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
    
        return '<div class="btn-group btn-group-sm" customer="group" aria-label="User Actions">
			  '.$this->edit_button.'
			</div>';
    }


    /**
     * @return string
     */
    public function getSubActionButtonsAttribute()
    {
    
        return '<div class="btn-group btn-group-sm" customer="group" aria-label="User Actions">
			  '.$this->edit_button.'
			</div>';
    }
}
