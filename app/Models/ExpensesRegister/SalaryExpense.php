<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $doi
 * @property string $tmp_code
 * @property string $name
 * @property string $role
 * @property string $salary
 * @property string $mop
 * @property string $dop
 * @property string $paid_amt
 * @property string $due_amt
 * @property string $created_at
 * @property string $updated_at
 * @property DailyExpense[] $dailyExpenses
 */
class SalaryExpense extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id', 'doi', 'tmp_code', 'name', 'role', 'salary', 'mop', 'dop', 'paid_amt', 'due_amt', 'created_at', 'updated_at'];

        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
