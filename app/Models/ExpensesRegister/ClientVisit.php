<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $emp_code
 * @property string $client
 * @property int $amt_paid
 * @property int $amt_spend
 * @property int $amt_return
 * @property string $created_at
 * @property string $updated_at
 * @property DailyExpense[] $dailyExpenses
 */
class ClientVisit extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id', 'emp_code', 'client', 'amt_paid', 'amt_spend', 'amt_return', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
