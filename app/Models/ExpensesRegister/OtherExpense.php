<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $type
 * @property string $dop
 * @property string $duration
 * @property string $amount
 * @property string $due_amt
 * @property string $remark
 * @property string $created_at
 * @property string $updated_at
 * @property DailyExpense[] $dailyExpenses
 */
class OtherExpense extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id', 'type', 'dop', 'duration', 'amount','paid_amount' ,'due_amt', 'remark', 'created_at', 'updated_at'];

        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
