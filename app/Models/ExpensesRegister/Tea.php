<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $timing
 * @property int $qty
 * @property string $special
 * @property string $normal
 * @property int $sp_rate
 * @property int $ml_rate
 * @property int $total
 * @property string $created_at
 * @property string $updated_at
 * @property DailyExpense[] $dailyExpenses
 */
class Tea extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id', 'timing', 'qty', 'special', 'normal', 'sp_rate', 'ml_rate', 'total', 'created_at', 'updated_at'];

        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
