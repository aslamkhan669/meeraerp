<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $mobile
 * @property string $user_code
 * @property string $recharge_date
 * @property string $plan_duration
 * @property string $due_date
 * @property string $plan_value
 * @property string $created_at
 * @property string $updated_at
 * @property DailyExpense[] $dailyExpenses
 */
class ItPhone extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id', 'mobile', 'user_code', 'recharge_date', 'plan_duration', 'due_date', 'plan_value', 'created_at', 'updated_at'];

  
        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
