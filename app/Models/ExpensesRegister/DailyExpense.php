<?php

namespace App\Models\ExpensesRegister;
use App\Models\ExpensesRegister\Traits\Attribute\DailyExpensesAttribute;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property ClientVisit[] $clientVisits
 * @property DispatchExpense[] $dispatchExpenses
 * @property ItPhone[] $itPhones
 * @property MiscExpense[] $miscExpenses
 * @property OtherExpense[] $otherExpenses
 * @property PurchaseExpense[] $purchaseExpenses
 * @property SalaryExpense[] $salaryExpenses
 * @property Stationery[] $stationeries
 * @property SupplierVisit[] $supplierVisits
 * @property Tea[] $teas
 * @property VehicleExpense[] $vehicleExpenses
 * @property Water[] $waters
 */
class DailyExpense extends Model
{
    use DailyExpensesAttribute;
    /**
     * @var array
     */
    protected $fillable = ['status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientVisits()
    {
        return $this->hasMany('App\Models\ExpensesRegister\ClientVisit', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dispatchExpenses()
    {
        return $this->hasMany('App\Models\ExpensesRegister\DispatchExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itPhones()
    {
        return $this->hasMany('App\Models\ExpensesRegister\ItPhone', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function miscExpenses()
    {
        return $this->hasMany('App\Models\ExpensesRegister\MiscExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function otherExpenses()
    {
        return $this->hasMany('App\Models\ExpensesRegister\OtherExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchaseExpenses()
    {
        return $this->hasMany('App\Models\ExpensesRegister\PurchaseExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salaryExpenses()
    {
        return $this->hasMany('App\Models\ExpensesRegister\SalaryExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stationeries()
    {
        return $this->hasMany('App\Models\ExpensesRegister\Stationery', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function supplierVisits()
    {
        return $this->hasMany('App\Models\ExpensesRegister\SupplierVisit', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teas()
    {
        return $this->hasMany('App\Models\ExpensesRegister\Tea', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicleExpenses()
    {
        return $this->hasMany('App\Models\ExpensesRegister\VehicleExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function waters()
    {
        return $this->hasMany('App\Models\ExpensesRegister\Water', 'daily_id');
    }
}
