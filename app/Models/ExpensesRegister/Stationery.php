<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $item_name
 * @property int $qty
 * @property int $price
 * @property int $total
 * @property string $created_at
 * @property string $updated_at
 * @property DailyExpense[] $dailyExpenses
 */
class Stationery extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id', 'item_name', 'qty', 'price', 'total', 'created_at', 'updated_at'];

        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
