<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $person
 * @property string $after_reading
 * @property string $before_reading
 * @property string $difference
 * @property string $unit_rate
 * @property string $amount
 * @property string $created_at
 * @property string $updated_at
 */
class VehicleExpense extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id','person', 'after_reading', 'before_reading', 'difference', 'unit_rate', 'amount', 'created_at', 'updated_at'];

    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
