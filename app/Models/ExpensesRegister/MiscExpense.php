<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $person
 * @property string $amt_recieved
 * @property string $amt_spent
 * @property string $amt_returned
 * @property string $mode_payment
 * @property string $for_what
 * @property string $remarks
 * @property string $created_at
 * @property string $updated_at
 */
class MiscExpense extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id','person', 'amt_recieved', 'amt_spent', 'amt_returned', 'mode_payment', 'for_what', 'remarks', 'created_at', 'updated_at'];

    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
