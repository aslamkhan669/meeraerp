<?php

namespace App\Models\ExpensesRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $emp_code
 * @property string $supplier
 * @property int $amt_paid
 * @property int $amt_spend
 * @property int $amt_return
 * @property string $created_at
 * @property string $updated_at
 * @property DailyExpense[] $dailyExpenses
 */
class SupplierVisit extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['daily_id', 'emp_code', 'supplier', 'amt_paid', 'amt_spend', 'amt_return', 'created_at', 'updated_at'];


        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dailyExpense()
    {
        return $this->belongsTo('App\Models\ExpensesRegister\DailyExpense', 'daily_id');
    }
}
