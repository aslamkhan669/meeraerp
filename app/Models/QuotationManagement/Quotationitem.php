<?php

namespace App\Models\QuotationManagement;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $quotation_id
 * @property int $qitem_id
 * @property string $profit
 * @property string $gstamount
 * @property string $total
 * @property string $pup
 * @property string $created_at
 * @property string $updated_at
 * @property QueryItem $queryItem
 * @property Quotation $quotation
 */
class Quotationitem extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['quotation_id', 'qitem_id', 'profit', 'profit_amount','gstamount', 'qtotal', 'pup','qqty','qgrand_total','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function queryItem()
    {
        return $this->belongsTo('App\Models\QueryManagement\QueryItem', 'qitem_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quotation()
    {
        return $this->belongsTo('App\Models\QuotationManagement\Quotation');
    }
}
