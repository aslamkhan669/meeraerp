<?php

namespace App\Models\QuotationManagement;

use Illuminate\Database\Eloquent\Model;
use App\Models\QuotationManagement\Traits\Attribute\QuotationAttribute;


/**
 * @property int $id
 * @property int $query_id
 * @property string $profit
 * @property string $created_at
 * @property string $updated_at
 * @property Query $query
 */
class Quotation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['query_id', 'is_confirmed','status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function queries()
    {
        return $this->belongsTo('App\Models\QueryManagement\Query','query_id');
    }

    public function clients()
    {
        return $this->belongsTo('App\Models\Master\Client','client_id');
    }

        /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function poFormattings()
    {
        return $this->hasMany('App\Models\QueryManagement\PoFormatting');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotationitems()
    {
        return $this->hasMany('App\Models\QuotationManagement\Quotationitem');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function termsconditions()
    {
        return $this->hasMany('App\Models\setting\Termscondition');
    }

    
}
