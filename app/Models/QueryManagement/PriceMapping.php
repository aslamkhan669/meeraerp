<?php

namespace App\Models\QueryManagement;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $qitem_id
 * @property int $supplier_id
 * @property string $list_price
 * @property string $discount
 * @property string $unit_price
 * @property string $gst
 * @property string $total
 * @property string $created_at
 * @property string $updated_at
 * @property QueryItem $queryItem
 * @property Supplier $supplier
 */
class PriceMapping extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['item_id', 'supplier_id', 'listprice', 'insurance_shipping', 'custom_clearance', 'discount', 'discounted_price', 'gst', 'total', 'quantity', 'grand_total', 'delivery_term', 'warranty','standard_package','is_selected', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function queryItem()
    {
        return $this->belongsTo('App\Models\QueryManagement\QueryItem', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo('App\Models\Master\Supplier');
    }
}
