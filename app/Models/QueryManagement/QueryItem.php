<?php

namespace App\Models\QueryManagement;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $item_id
 * @property int $cat_id
 * @property int $color
 * @property int $brand_id
 * @property int $unit
 * @property int $item_code
 * @property string $description
 * @property string $specification
 * @property string $model_no
 * @property string $standard_package
 * @property string $remarks
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * @property Brand $brand
 * @property Category $category
 * @property Color $color
 * @property Query $query
 * @property Uom $uom
 */
class QueryItem extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['query_id', 'item_id', 'color', 'brand_id', 'unit', 'item_code', 'description', 'query_qty','specification', 'model_no', 'standard_package', 'remarks', 'image', 'hsn_code','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\ProductManagement\Brand','brand_id');
    }

    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itemList()
    {
        return $this->belongsTo('App\Models\ProductManagement\ItemList', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function color()
    {
        return $this->belongsTo('App\Models\ProductManagement\Color', 'color');
    }

 


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uom()
    {
        return $this->belongsTo('App\Models\ProductManagement\Uom', 'unit');
    }

    public function poItems()
    {
        return $this->hasMany('App\PoItem', 'qitemid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function priceMappings()
    {
        return $this->hasMany('App\Models\QueryManagement\PriceMapping', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotationitems()
    {
        return $this->hasMany('App\Models\QuotationManagement\Quotationitem', 'qitem_id');
    }

}
