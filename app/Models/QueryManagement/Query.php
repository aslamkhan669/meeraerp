<?php

namespace App\Models\QueryManagement;

use Illuminate\Database\Eloquent\Model;
use App\Models\QueryManagement\QueryItem;
use App\Models\QueryManagement\Traits\Attribute\QueryAttribute;

/**
 * @property int $id
 * @property int $client_id
 * @property string $query_no
 * @property string $query_date
 * @property string $center_code
 * @property string $meera_center_code
 * @property string $query_center
 * @property string $po_no
 * @property string $client_code
 * @property string $sales_center
 * @property string $quote_no
 * @property string $client_detail
 * @property string $created_at
 * @property string $updated_at
 * @property Client $client
 */
class Query extends Model
{  
      use queryAttribute;

    /**
     * @var array
     */
    protected $fillable = ['client_id', 'center_id', 'query_no', 'query_date', 'meera_center_code', 'query_center', 'po_no', 'sales_center', 'quote_no','normal_oncall', 'client_detail','query_person','contact_no', 'created_at', 'updated_at'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function companyDetail()
    {
        return $this->belongsTo('App\Models\Setting\CompanyDetail', 'center_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Master\Client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function queryItems()
    {
        return $this->hasMany('App\Models\QueryManagement\QueryItem','query_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotations()
    {
        return $this->hasMany('App\Models\QuotationManagement\Quotation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function termsconditions()
    {
        return $this->hasMany('App\Models\Setting\Termscondition');
    }

}
