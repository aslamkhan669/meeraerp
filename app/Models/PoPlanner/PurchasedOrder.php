<?php
namespace App\Models\PoPlanner;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $poregisterid
 * @property int $ewaybill
 * @property int $ml_purchased
 * @property int $ml_recieved
 * @property string $payment_mode
 * @property int $bill_challan_no
 * @property string $bill_challan_amt
 * @property string $created_at
 * @property string $updated_at
 * @property PoRegister $poRegister
 */
class PurchasedOrder extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['poregisterid', 'ewaybill','ewaybill_no','remarks','qty','pending_qty','pt_user_id', 'challan_recieved','ml_purchased', 'ml_recieved','po_generate', 'payment_mode', 'bill_challan_no', 'bill_challan_amt','complete', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poRegister()
    {
        return $this->belongsTo('App\Models\PoPlanner\PoRegister','poregisterid');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'pt_user_id');
    }
}
