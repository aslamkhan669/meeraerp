<?php
namespace App\Models\PoPlanner;

use Illuminate\Database\Eloquent\Model;
use App\Models\PoPlanner\Traits\Attribute\PoRegisterAttribute;

/**
 * @property int $id
 * @property int $poitemid
 * @property int $qty
 * @property string $purchase_date
 * @property string $day_shift
 * @property string $priority
 * @property string $transportation_mode
 * @property string $payment_amount
 * @property string $bank
 * @property string $created_at
 * @property string $updated_at
 * @property PoItem $poItem
 */
class PoRegister extends Model
{
    use PoRegisterAttribute;

    protected $fillable = ['poitemid', 'pt_user_id', 'qty', 'purchase_date', 'day_shift','criticality','priority', 'transportation_mode', 'payment_amount', 'bank', 'payment_mode', 'created_at', 'updated_at','status','checked','complete'];

    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'pt_user_id');
    }

    public function poItem()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoItem', 'poitemid');
    }

    public function purchasedOrders()
    {
        return $this->hasMany('App\Models\PoPlanner\PurchasedOrder', 'poregisterid');
    }

    public function inventoryProducts()
    {
        return $this->hasMany('App\Models\InventoryManagement\InventoryProduct', 'poregisterid');
    }
}
