<?php
namespace App\Models\PoPlanner;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $poformatid
 * @property string $amt_recieved
 * @property string $amt_spent
 * @property string $amt_returned
 * @property string $mode_payment
 * @property string $remarks
 * @property string $total
 * @property string $created_at
 * @property string $updated_at
 * @property PoFormatting $poFormatting
 */
class PurchaseExpense extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['poformatid', 'amt_recieved', 'amt_spent', 'amt_returned', 'mode_payment', 'remarks', 'total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poFormatting()
    {
        return $this->belongsTo('App\Models\PoFormatting\PoFormatting', 'poformatid');
    }
}
