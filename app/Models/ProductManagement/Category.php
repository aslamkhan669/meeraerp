<?php

namespace App\Models\ProductManagement;
use App\Models\ProductManagement\Traits\Attribute\CategoryAttribute;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $category_name
 * @property int $parent_id
 * @property string $hsn_code
 * @property string $code
 * @property string $is_active
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * @property Product[] $products
 */
class Category extends Model
{
    use CategoryAttribute;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'category_name', 'parent_id', 'hsn_code', 'code', 'is_active', 'image', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\ProductManagement\Product');
    }
    public function category()
    {
        return $this->belongsTo(self::class, 'parent_id');   
     }
  
}