<?php
namespace App\Models\ProductManagement;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductManagement\Traits\Attribute\ItemAttribute;


/**
 * @property int $id
 * @property int $product_id
 * @property int $cat_id
 * @property int $unit
 * @property int $color
 * @property string $item_code
 * @property string $hsn_code
 * @property string $model_no
 * @property string $package_qty
 * @property string $purchase_source
 * @property string $description
 * @property string $remarks
 * @property string $specification
 * @property string $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property Category $category
 * @property Color $color
 * @property Product $product
 * @property Uom $uom
 */
class ItemList extends Model
{
    use ItemAttribute;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'cat_id', 'brand_id','unit', 'color', 'item_code','hsn_code','model_no', 'package_qty', 'purchase_source', 'description', 'remarks', 'specification','image','is_active','product_cost','status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\ProductManagement\Category', 'cat_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function color()
    {
        return $this->belongsTo('App\Models\ProductManagement\Color', 'color');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\ProductManagement\Product');
    }
    public function brand()
    {
        return $this->belongsTo('App\Models\ProductManagement\Brand');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uom()
    {
        return $this->belongsTo('App\Models\ProductManagement\Uom', 'unit');
    }

    public function queryItems()
    {
        return $this->hasMany('App\Models\QueryManagement\QueryItem', 'item_id');
    }
}
