<?php
namespace App\Models\ProductManagement;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductManagement\Traits\Attribute\BrandAttribute;

/**
 * @property int $id
 * @property int $product_id
 * @property string $brand_name
 * @property string $brand_code
 * @property string $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property Brand $product
 * @property ItemList[] $itemLists
 */
class Brand extends Model
{

    use BrandAttribute;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'brand_name', 'brand_code', 'is_active', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\ProductManagement\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itemList()
    {
        return $this->hasMany('App\Models\ProductManagement\ItemList');
    }
    public function queryItems()
    {
        return $this->hasMany('App\Models\QueryManagement\QueryItem');
    }
}
