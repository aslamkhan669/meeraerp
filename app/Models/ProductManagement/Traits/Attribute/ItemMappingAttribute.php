<?php

namespace App\Models\ProductManagement\Traits\Attribute;

/**
 * Trait RoleAttribute.
 */
trait ItemMappingAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.itemmapping.edit', $this).'" class="btn btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'"></i></a>';
    }



    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
      
        return '<div class="btn-group btn-group-sm" customer="group" aria-label="User Actions">
			  '.$this->edit_button.'
			  '.$this->delete_button.'
			</div>';
    }
}
