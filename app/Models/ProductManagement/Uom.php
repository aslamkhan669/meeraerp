<?php

namespace App\Models\ProductManagement;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductManagement\Traits\Attribute\UomAttribute;


/**
 * @property int $id
 * @property string $unit
 * @property string $created_at
 * @property string $updated_at
 */
class Uom extends Model
{
    use UomAttribute;

    /**
     * @var array
     */
    protected $fillable = ['unit','is_active', 'created_at', 'updated_at'];

    public function itemlist()
    {
        return $this->hasMany('App\Models\ProductManagement\ItemList');
    }
    public function queryItems()
    {
        return $this->hasMany('App\Models\QueryManagement\QueryItem', 'unit');
    }
}
