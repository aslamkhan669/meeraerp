<?php

namespace App\Models\ProductManagement;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductManagement\Traits\Attribute\ColorAttribute;

/**
 * @property int $id
 * @property string $color
 * @property string $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class Color extends Model
{
    use ColorAttribute;

    /**
     * @var array
     */
    protected $fillable = ['color', 'is_active', 'created_at', 'updated_at'];

    public function itemlist()
    {
        return $this->hasMany('App\Models\ProductManagement\ItemList');
    }

    public function queryItems()
    {
        return $this->hasMany('App\Models\QueryManagement\QueryItem', 'color');
    }
}
