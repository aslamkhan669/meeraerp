<?php

namespace App\Models\ProductManagement;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductManagement\Traits\Attribute\ProductAttribute;
/**
 * @property int $id
 * @property int $category_id
 * @property int $brand_id
 * @property string $product_name
 * @property string $product_code
 * @property string $product_owner
 * @property string $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property Brand $brand
 * @property Category $category
 */
class Product extends Model
{
    use ProductAttribute;
    /**
     * @var array
     */
    protected $fillable = ['product_name', 'product_code', 'product_owner', 'is_active', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category()
    {
        return $this->hasMany('App\Models\ProductManagement\Category');
    }
}
