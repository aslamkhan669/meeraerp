<?php

namespace App\Models\ProductManagement;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductManagement\Traits\Attribute\ItemMappingAttribute;


/**
 * @property int $id
 * @property int $item_id
 * @property int $supplier_id
 * @property string $listprice
 * @property string $insurance_shipping
 * @property string $custom_clearance
 * @property string $discount
 * @property string $discounted_price
 * @property string $gst
 * @property string $total
 * @property string $quantity
 * @property string $delivery_term
 * @property string $warranty
 * @property string $standard_package
 * @property string $created_at
 * @property string $updated_at
 * @property ItemList $itemList
 * @property Supplier $supplier
 */
class ItemMapping extends Model
{
    use ItemMappingAttribute;

    /**
     * @var array
     */
    protected $fillable = ['item_id', 'supplier_id', 'listprice', 'insurance_shipping', 'custom_clearance', 'discount', 'discounted_price', 'gst', 'total', 'quantity','grand_amount', 'delivery_term', 'warranty', 'standard_package', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itemList()
    {
        return $this->belongsTo('App\Models\ProductManagement\ItemList', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo('App\Models\Master\Supplier');
    }
}
