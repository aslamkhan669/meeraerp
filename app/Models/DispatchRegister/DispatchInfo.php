<?php

namespace App\Models\DispatchRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $d_order_id
 * @property string $material_recieved
 * @property int $qty
 * @property int $reject_qty
 * @property string $created_at
 * @property string $updated_at
 * @property DispatchedOrder $dispatchedOrder
 */
class DispatchInfo extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['bill_challan_no','material_recieved', 'qty', 'reject_qty', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

}
