<?php

namespace App\Models\DispatchRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $dt_challan_id
 * @property int $item_id
 * @property int $qty
 * @property string $gst
 * @property string $total
 * @property string $created_at
 * @property string $updated_at
 * @property DispatchChallan $dispatchChallan
 * @property ItemList $itemList
 */
class DispatchChallanItem extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'dispatch_challanitems';

    /**
     * @var array
     */
    protected $fillable = ['dt_challan_id', 'item_id', 'qty', 'gst', 'total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dispatchChallan()
    {
        return $this->belongsTo('App\Models\DispatchRegister\DispatchChallan', 'dt_challan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itemList()
    {
        return $this->belongsTo('App\Models\ProductManagement\ItemList', 'item_id');
    }
}
