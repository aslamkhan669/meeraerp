<?php

namespace App\Models\DispatchRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $poitemid
 * @property int $dt_user_id
 * @property int $qty
 * @property string $purchase_date
 * @property string $day_shift
 * @property string $priority
 * @property string $transportation_mode
 * @property string $payment_amount
 * @property string $bank
 * @property string $payment_mode
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property PoItem $poItem
 */
class DispatchRegister extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['poitemid', 'dt_user_id', 'dt_qty', 'purchase_date', 'day_shift', 'priority', 'transportation_mode', 'payment_amount', 'bank', 'payment_mode', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User', 'dt_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poItem()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoItem', 'poitemid');
    }
    public function dispatchedOrder()
    {
        return $this->hasMany('App\Models\DispatchRegister\DispatchedOrder', 'disregisterid');
    }
}
