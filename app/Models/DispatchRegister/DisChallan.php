<?php

namespace App\Models\DispatchRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $poitemid
 * @property string $qty
 * @property string $grand_total
 * @property string $created_at
 * @property string $updated_at
 * @property PoItem $poItem
 */
class DisChallan extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['poitemid', 'qty', 'po_id','qitemid', 'grand_total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poItem()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoItem', 'poitemid');
    }
    public function poFormatting()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoFormatting', 'po_id');
    }
    public function queryItem()
    {
        return $this->belongsTo('App\Models\QueryManagement\QueryItem', 'qitemid');

    }
}
