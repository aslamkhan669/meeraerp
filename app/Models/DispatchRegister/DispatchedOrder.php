<?php

namespace App\Models\DispatchRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $disregisterid
 * @property int $ewaybill
 * @property int $ml_purchased
 * @property int $ml_recieved
 * @property string $payment_mode
 * @property int $bill_challan_no
 * @property string $bill_challan_amt
 * @property string $created_at
 * @property string $updated_at
 * @property DispatchRegister $dispatchRegister
 */
class DispatchedOrder extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['disregisterid', 'ewaybill','poitem_reciever','recieved_doc','remarks','pending_qty','ml_purchased', 'ml_recieved', 'bill_challan','payment_mode', 'bill_challan_no', 'bill_challan_amt','complete', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dispatchRegister()
    {
        return $this->belongsTo('App\Models\DispatchRegister\DispatchRegister', 'disregisterid');
    }
}
