<?php

namespace App\Models\DispatchRegister;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $supplier_id
 * @property int $client_id
 * @property string $supplier_challan
 * @property int $dt_user_id
 * @property string $created_at
 * @property string $updated_at
 * @property DispatchChallanitem[] $dispatchChallanitems
 */
class DispatchChallan extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['center_id', 'client_id', 'supplier_challan', 'dt_user_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dispatchChallanitems()
    {
        return $this->hasMany('App\Models\DispatchRegister\DispatchChallanitem', 'dt_challan_id');
    }
}
