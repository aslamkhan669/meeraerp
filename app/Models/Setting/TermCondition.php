<?php
namespace App\Models\Setting;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $quotation_id
 * @property int $freight_id
 * @property int $insurance_id
 * @property string $payment_days
 * @property string $validity_days
 * @property string $warranty_months
 * @property string $created_at
 * @property string $updated_at
 * @property Freight $freight
 * @property Insurance $insurance
 * @property Quotation $quotation
 */
class TermCondition extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'termsconditions';

    /**
     * @var array
     */
    protected $fillable = ['query_id', 'freight_id', 'insurance_id', 'payment_days', 'validity_days', 'warranty_months', 'comment_one', 'comment_two','comment_three','comment_four','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function freight()
    {
        return $this->belongsTo('App\Models\Setting\Freight');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function queroes()
    {
        return $this->belongsTo('App\Models\QueryManagement\Query');
    }

    public function transportInsurance()
    {
        return $this->belongsTo('App\Models\Setting\TransportInsurance', 'insurance_id');
    }
}
