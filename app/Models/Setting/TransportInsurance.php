<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use App\Models\Setting\Traits\Attribute\TransportInsuranceAttribute;

/**
 * @property int $id
 * @property string $Needed
 * @property string $Not Needed
 * @property string $created_at
 * @property string $updated_at
 * @property Termscondition[] $termsconditions
 */
class TransportInsurance extends Model
{
    use TransportInsuranceAttribute;

    /**
     * @var array
     */
    protected $fillable = ['conditions', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function termsconditions()
    {
        return $this->hasMany('App\Termscondition', 'insurance_id');
    }

   
}
