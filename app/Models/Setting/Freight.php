<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use App\Models\Setting\Traits\Attribute\FreightAttribute;

/**
 * @property int $id
 * @property string $Applicable
 * @property string $Not Applicable
 * @property string $created_at
 * @property string $updated_at
 * @property Termscondition[] $termsconditions
 */
class Freight extends Model
{
    use FreightAttribute;

    /**
     * @var array
     */
    protected $fillable = ['conditions', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function termsconditions()
    {
        return $this->hasMany('App\Termscondition');
    }
}
