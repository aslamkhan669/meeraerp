<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use App\Models\Setting\Traits\Attribute\CompanyDetailAttribute;

/**
 * @property int $id
 * @property string $company_name
 * @property string $company_code
 * @property string $contact_person
 * @property string $phone_no
 * @property string $email
 * @property string $pan_no
 * @property string $mobile
 * @property string $delivery_address
 * @property string $address
 * @property string $aadhar_no
 * @property string $gst_no
 * @property string $state_code
 * @property string $reg_type
 * @property string $created_at
 * @property string $updated_at
 */
class CompanyDetail extends Model
{
    use CompanyDetailAttribute;

    /**
     * @var array
     */
    protected $fillable = ['company_name', 'company_code', 'contact_person', 'phone_no', 'email', 'pan_no', 'mobile', 'delivery_address', 'address', 'aadhar_no', 'gst_no', 'state_code', 'reg_type', 'created_at', 'updated_at'];

    public function queries()
    {
        return $this->hasMany('App\Models\QueryManagement\Query', 'center_id');
    }
    
    public function details()
    {
        return $this->hasManyThrough(
            'App\Models\QueryManagement\Query',
            'App\Models\QuotationManagement\Quotation',
            'query_id', // Foreign key on users table...
            'center_id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'id' // Local key on users table...
        );
    }
}
