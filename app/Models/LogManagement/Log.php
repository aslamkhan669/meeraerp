<?php

namespace App\Models\LogManagement;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $user_name
 * @property string $user_id
 * @property string $module
 * @property string $action
 * @property string $activity
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Log extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_name', 'user_id', 'module', 'action', 'activity', 'name', 'created_at', 'updated_at'];

}
