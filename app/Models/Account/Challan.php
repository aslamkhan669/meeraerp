<?php
namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $supplier_id
 * @property int $client_id
 * @property string $supplier_challan
 * @property string $created_at
 * @property string $updated_at
 * @property ChallanItem[] $challanItems
 */
class Challan extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['supplier_id', 'center_id', 'supplier_challan','pt_user_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function challanItems()
    {
        return $this->hasMany('App\Models\Account\ChallanItem');
    }
    public function poRegister()
    {
        return $this->belongsTo('App\PoRegister', 'po_reg_id');
    }
}
