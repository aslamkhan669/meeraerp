<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $poitemid
 * @property int $qitemid
 * @property string $qty
 * @property string $grand_total
 * @property string $created_at
 * @property string $updated_at
 * @property PoItem $poItem
 * @property QueryItem $queryItem
 */
class PoChallan extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['poitemid', 'qitemid', 'qty', 'grand_total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poItem()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoItem', 'poitemid');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function queryItem()
    {
        return $this->belongsTo('App\Models\QueryManagement\QueryItem', 'qitemid');

    }
    public function poFormatting()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoFormatting', 'po_id');
    }
}
