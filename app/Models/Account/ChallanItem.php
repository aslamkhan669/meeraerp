<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $challan_id
 * @property int $item_id
 * @property string $description
 * @property string $hsn_code
 * @property int $qty
 * @property string $total
 * @property string $gst
 * @property string $grand_total
 * @property string $created_at
 * @property string $updated_at
 * @property Challan $challan
 */
class ChallanItem extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['challan_id', 'item_id', 'qty', 'total', 'gst', 'grand_total', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function challan()
    {
        return $this->belongsTo('App\Models\Account\Challan');
    }
    public function itemList()
    {
        return $this->belongsTo('App\Models\ProductManagement\ItemList', 'item_id');
    }
}
