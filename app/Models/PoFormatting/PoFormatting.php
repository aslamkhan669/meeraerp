<?php

namespace App\Models\PoFormatting;

use Illuminate\Database\Eloquent\Model;
 use App\Models\PoFormatting\PoFormatting;
 use App\Models\PoFormatting\Traits\Attribute\PoAttribute;


/**
 * @property int $id
 * @property int $quotation_id
 * @property string $c_p_owner_no
 * @property string $c_purchaser_name
 * @property string $c_reciever_name
 * @property string $created_at
 * @property string $updated_at
 * @property Quotation $quotation
 */
class PoFormatting extends Model
{
    use PoAttribute;

    /**
     * @var array
     */
    protected $fillable = ['quotation_id', 'c_p_owner_no', 'c_purchaser_name', 'c_reciever_name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quotation()
    {
        return $this->belongsTo('App\Models\QuotationManagement\Quotation');    
    }

    public function poItems()
    {
        return $this->hasMany('App\Models\PurchaseOrder\PoItem', 'po_id');
    }
}
