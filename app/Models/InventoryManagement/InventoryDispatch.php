<?php

namespace App\Models\InventoryManagement;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $disregisterid
 * @property string $reason
 * @property string $extra_qty
 * @property string $created_at
 * @property string $updated_at
 * @property DispatchRegister $dispatchRegister
 */
class InventoryDispatch extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'inventory_dispatchs';

    /**
     * @var array
     */
    protected $fillable = ['disregisterid', 'reason', 'extra_qty', 'bill_no','location','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dispatchRegister()
    {
        return $this->belongsTo('App\Models\DispatchRegister\DispatchRegister', 'disregisterid');
    }
}
