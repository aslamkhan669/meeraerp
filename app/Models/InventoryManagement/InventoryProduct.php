<?php

namespace App\Models\InventoryManagement;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $poregisterid
 * @property string $reason
 * @property string $extra_qty
 * @property string $created_at
 * @property string $updated_at
 * @property PoRegister $poRegister
 */
class InventoryProduct extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['poregisterid', 'reason', 'extra_qty','bill_no','location', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poRegister()
    {
        return $this->belongsTo('App\Models\PoPlanner\PoRegister', 'poregisterid');
    }
}
