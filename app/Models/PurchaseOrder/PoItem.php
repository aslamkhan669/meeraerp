<?php

namespace App\Models\PurchaseOrder;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $qitemid
 * @property int $po_id
 * @property string $qty
 * @property string $grand_total
 * @property string $created_at
 * @property string $updated_at
 * @property PoFormatting $poFormatting
 * @property QueryItem $queryItem
 */
class PoItem extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['qitemid', 'po_id', 'qty', 'grand_total','complete','dis_complete', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poFormatting()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoFormatting', 'po_id');
    }
    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function queryItem()
    {
        return $this->belongsTo('App\Models\QueryManagement\QueryItem', 'qitemid');
    }

    

    public function poRegisters()
    {
        return $this->hasMany('App\Models\PoPlanner\PoRegister', 'poitemid');
    }
    public function dispatchRegisters()
    {
        return $this->hasMany('App\Models\DispatchRegister\DispatchRegister', 'poitemid');
    }
}
