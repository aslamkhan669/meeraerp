<?php

namespace App\Models\PurchaseOrder\Traits\Attribute;

/**
 * Trait RoleAttribute.
 */
trait PoFormattingAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.poformatting.edit', $this).'" class="btn btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'"></i></a>';
    }


    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
    
        return '<div class="btn-group btn-group-sm" customer="group" aria-label="User Actions">
			  '.$this->edit_button.'
			</div>';
    }


    /**
     * @return string
     */
    public function getSubActionButtonsAttribute()
    {
    
        return '<div class="btn-group btn-group-sm" customer="group" aria-label="User Actions">
			  '.$this->edit_button.'
			  '.$this->delete_button.'
			</div>';
    }
}
