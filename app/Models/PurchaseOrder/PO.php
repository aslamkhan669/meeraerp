<?php

namespace App\Models\PurchaseOrder;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $poitemid
 * @property int $po_id
 * @property string $qty
 * @property string $grand_total
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property PoFormatting $poFormatting
 * @property PoItem $poItem
 */
class PO extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'pos';

    /**
     * @var array
     */
    protected $fillable = ['poitemid', 'qitemid', 'qty', 'grand_total', 'status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function queryItem()
    {
        return $this->belongsTo('App\Models\QueryManagement\QueryItem', 'qitemid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poItem()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoItem', 'poitemid');
    }
    public function poFormatting()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoFormatting', 'po_id');
    }
}
