<?php

namespace App\Models\PurchaseOrder;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $poform_id
 * @property string $payment_term
 * @property string $freight_charges
 * @property string $material_insurance
 * @property string $po_validity
 * @property string $comment1
 * @property string $comment2
 * @property string $comment3
 * @property string $comment4
 * @property string $created_at
 * @property string $updated_at
 * @property PoFormatting $poFormatting
 */
class PoTermcondition extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['poform_id', 'payment_term', 'freight_charges', 'material_insurance', 'po_validity', 'comment1', 'comment2', 'comment3', 'comment4', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function poFormatting()
    {
        return $this->belongsTo('App\Models\PurchaseOrder\PoFormatting', 'poform_id');
    }
}
