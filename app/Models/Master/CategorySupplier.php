<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $cat_id
 * @property int $supplier_id
 * @property string $created_at
 * @property string $updated_at
 * @property Category $category
 * @property Supplier $supplier
 */
class CategorySupplier extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'categorysuppliers';

    /**
     * @var array
     */
    protected $fillable = ['cat_id', 'supplier_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\ProductManagement\Category', 'cat_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo('App\Models\Master\Supplier');
    }
}
