<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Traits\Attribute\ClientTypeAttribute;

/**
 * @property int $id
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 */
class ClientType extends Model
{
    use ClientTypeAttribute;

    /**
     * @var array
     */
    protected $fillable = ['type', 'is_active', 'created_at', 'updated_at'];

}
