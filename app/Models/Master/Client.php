<?php

namespace App\Models\Master;
use App\Models\Master\Traits\Attribute\ClientAttribute;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $client_name
 * @property string $client_code
 * @property string $client_type
 * @property string $rating
 * @property string $purchase_category
 * @property string $sales_center
 * @property string $contact_person1
 * @property string $contect_number1
 * @property string $contact_person2
 * @property string $contect_number2
 * @property string $contact_person3
 * @property string $contect_number3
 * @property string $contact_person4
 * @property string $contect_number4
 * @property string $street
 * @property string $landmark
 * @property string $sector
 * @property string $city
 * @property string $pincode
 * @property string $state
 * @property string $zone
 * @property string $pancard
 * @property string $adharcard
 * @property string $gst_no
 * @property string $payment_term
 * @property string $payment_mode
 * @property string $excisable
 * @property string $demand_frequency
 * @property string $created_at
 * @property string $updated_at
 */
class Client extends Model
{
    use ClientAttribute;

    /**
     * @var array
     */
    protected $fillable = ['client_name', 'client_code', 'client_type', 'rating', 'purchase_category', 'sales_center', 'email','email1','email2','email3','email4','email5','contact_person1', 'contact_number1', 'contact_person2', 'contact_number2', 'contact_person3', 'contact_number3', 'contact_person4', 'contact_number4','contact_person5', 'contact_number5','contact_person6', 'contact_number6','contact_person7', 'contact_number7','contact_person8', 'contact_number8','street', 'landmark', 'sector', 'city', 'pincode', 'state', 'zone', 'pancard', 'adharcard', 'gst_no', 'payment_term', 'payment_mode', 'excisable', 'demand_frequency', 'created_at', 'updated_at'];

    public function clienttype()
    {
        return $this->belongsTo('App\Models\Master\ClientType', 'client_type');
    }
    public function paymentterm()
    {
        return $this->belongsTo('App\Models\Master\PaymentTerm', 'payment_term');
    }
    public function paymentmode()
    {
        return $this->belongsTo('App\Models\Master\PaymentMode', 'payment_mode');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function queries()
    {
        return $this->hasMany('App\Models\Master\Query');
    }

    public function quotation()
    {
        return $this->hasMany('App\Models\QuotationManagement\Quotation');
    }
}
