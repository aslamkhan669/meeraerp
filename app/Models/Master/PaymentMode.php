<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Traits\Attribute\PaymentModeAttribute;

/**
 * @property int $id
 * @property string $mode
 * @property string $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class PaymentMode extends Model
{
    use PaymentModeAttribute;

    /**
     * @var array
     */
    protected $fillable = ['mode', 'is_active', 'created_at', 'updated_at'];

}
