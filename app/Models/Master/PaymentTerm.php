<?php

namespace App\Models\Master;
use App\Models\Master\Traits\Attribute\TermAttribute;

use Illuminate\Database\Eloquent\Model;
/**
 * @property int $id
 * @property string $term_name
 * @property string $is_active
 * @property string $created_at
 * @property string $updated_at
 */
class PaymentTerm extends Model
{
    use TermAttribute;
    /**
     * @var array
     */
    protected $fillable = ['term_name', 'is_active', 'created_at', 'updated_at'];

}
