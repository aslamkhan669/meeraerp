<?php

namespace App\Models\Master\Traits\Attribute;

/**
 * Trait RoleAttribute.
 */
trait TermAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.paymentterm.edit', $this).'" class="btn btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'"></i></a>';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a href="'.route('admin.paymentterm.destroy', $this).'"
			 data-method="delete"
			 data-trans-button-cancel="'.__('buttons.general.cancel').'"
			 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
			 data-trans-title="'.__('strings.backend.general.are_you_sure').'"
			 class="btn btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'"></i></a> ';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
    
        return '<div class="btn-group btn-group-sm" customer="group" aria-label="User Actions">
			  '.$this->edit_button.'
			  '.$this->delete_button.'
			</div>';
    }


    /**
     * @return string
     */
    public function getSubActionButtonsAttribute()
    {
    
        return '<div class="btn-group btn-group-sm" customer="group" aria-label="User Actions">
			  '.$this->edit_button.'
			  '.$this->delete_button.'
			</div>';
    }
}
