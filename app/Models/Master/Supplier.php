<?php

namespace App\Models\Master;
use App\Models\Master\Traits\Attribute\SupplierAttribute;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $supplier_name
 * @property string $supplier_code
 * @property string $pricing_center
 * @property string $supplier_type
 * @property string $mobile
 * @property string $landline
 * @property string $email
 * @property string $rating
 * @property string $contact_person1
 * @property string $contact_number1
 * @property string $contact_person2
 * @property string $contact_number2
 * @property string $contact_person3
 * @property string $contact_number3
 * @property string $contact_person4
 * @property string $contact_number4
 * @property string $supplier_catalog
 * @property string $street
 * @property string $landmark
 * @property string $sector
 * @property string $city
 * @property string $pincode
 * @property string $state
 * @property string $website
 * @property string $zone
 * @property string $pancard
 * @property string $adharcard
 * @property string $gst_no
 * @property string $bank_name
 * @property string $ifsc_code
 * @property string $account_no
 * @property string $cancelled_cheque
 * @property string $created_at
 * @property string $updated_at
 * @property Categorysupplier[] $categorysuppliers
 * @property PriceMapping[] $priceMappings
 */

class Supplier extends Model


{
    use SupplierAttribute;

    /**
     * @var array
     */
    protected $fillable = ['supplier_name', 'supplier_code', 'pricing_center', 'supplier_type','supplier_category', 'mobile', 'landline', 'email', 'rating', 'contact_person1', 'contact_number1', 'contact_person2', 'contact_number2', 'contact_person3', 'contact_number3', 'contact_person4', 'contact_number4', 'supplier_catalog', 'street', 'landmark', 'sector', 'city', 'pincode', 'state', 'website', 'zone', 'pancard', 'adharcard', 'gst_no', 'bank_name', 'ifsc_code', 'account_no', 'cancelled_cheque', 'created_at', 'updated_at'];



    public function categorysuppliers()
    {
        return $this->hasMany('App\Models\Master\Categorysupplier');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quotations()
    {
        return $this->hasMany('App\Quotation');
    }
    public function categories()
    {
        return $this->belongsTo('App\Models\ProductManagement\Category','cat_id','id');
    }
    public function priceMappings()
    {
        return $this->hasMany('App\Models\QueryManagement\PriceMapping', 'supplier_id');
    }
}
