<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property ClientVisit[] $clientVisits
 * @property DispatchExpense[] $dispatchExpenses
 * @property ItPhone[] $itPhones
 * @property MiscExpense[] $miscExpenses
 * @property OtherExpense[] $otherExpenses
 * @property PurchaseExpense[] $purchaseExpenses
 * @property SalaryExpense[] $salaryExpenses
 * @property Stationery[] $stationeries
 * @property SupplierVisit[] $supplierVisits
 * @property Tea[] $teas
 * @property VehicleExpense[] $vehicleExpenses
 * @property Water[] $waters
 */
class DailyExpenses extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['status', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientVisits()
    {
        return $this->hasMany('App\ClientVisit', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dispatchExpenses()
    {
        return $this->hasMany('App\DispatchExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function itPhones()
    {
        return $this->hasMany('App\ItPhone', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function miscExpenses()
    {
        return $this->hasMany('App\MiscExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function otherExpenses()
    {
        return $this->hasMany('App\OtherExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function purchaseExpenses()
    {
        return $this->hasMany('App\PurchaseExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salaryExpenses()
    {
        return $this->hasMany('App\SalaryExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stationeries()
    {
        return $this->hasMany('App\Stationery', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function supplierVisits()
    {
        return $this->hasMany('App\SupplierVisit', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teas()
    {
        return $this->hasMany('App\Tea', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehicleExpenses()
    {
        return $this->hasMany('App\VehicleExpense', 'daily_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function waters()
    {
        return $this->hasMany('App\Water', 'daily_id');
    }
}
