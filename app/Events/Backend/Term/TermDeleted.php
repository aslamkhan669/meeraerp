<?php

namespace App\Events\Backend\Term;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class TermDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $term;

    /**
     * @param $user
     */
    public function __construct($term)
    {
        $this->term = $term;
    }
}
