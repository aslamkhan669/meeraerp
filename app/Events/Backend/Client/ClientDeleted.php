<?php

namespace App\Events\Backend\Client;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class ClientDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $client;

    /**
     * @param $user
     */
    public function __construct($client)
    {
        $this->client = $client;
    }
}
