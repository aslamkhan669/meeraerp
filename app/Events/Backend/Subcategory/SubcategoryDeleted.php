<?php

namespace App\Events\Backend\Subcategory;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class SubcategoryDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $subcategory;

    /**
     * @param $user
     */
    public function __construct($subcategory)
    {
        $this->subcategory = $subcategory;
    }
}
