<?php

namespace App\Events\Backend\Supplier;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class SupplierDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $supplier;

    /**
     * @param $user
     */
    public function __construct($supplier)
    {
        $this->supplier = $supplier;
    }
}
