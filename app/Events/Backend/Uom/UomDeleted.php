<?php

namespace App\Events\Backend\Uom;

use Illuminate\Queue\SerializesModels;

/**
 * Class UserDeleted.
 */
class UomDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $uom;

    /**
     * @param $user
     */
    public function __construct($uom)
    {
        $this->uom = $uom;
    }
}
